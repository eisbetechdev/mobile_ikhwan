# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\khadafi\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
#-keepnames class com.amanah.bisatopupcom.MyAppGlideModule
# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

-dontwarn com.google.firebase.**
-dontwarn com.google.android.gms.crash.internal.service.**

-dontwarn com.squareup.picasso.**

-ignorewarnings

-keep class * {
    public private *;
}


# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-keepnames public class * extends io.realm.RealmModel
-keep public class * extends io.realm.RealmModel { *; }
-keepnames public class * extends io.realm.RealmObject
-keep public class * extends io.realm.RealmObject { *; }
-keepattributes *Annotation*

-dontwarn com.android.installreferrer

# Add *one* of the following rules to your Proguard configuration file.
# Alternatively, you can annotate classes and class members with @android.support.annotation.Keep

# keep everything in this package from being removed or renamed
-keep class com.amanah.bisatopupcom.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.amanah.bisatopupcom.** { *; }

# Add *one* of the following rules to your Proguard configuration file.
# Alternatively, you can annotate classes and class members with @android.support.annotation.Keep

# keep everything in this package from being removed or renamed
-keep class com.amanah.bisatopupcom.api.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.amanah.bisatopupcom.api.** { *; }
