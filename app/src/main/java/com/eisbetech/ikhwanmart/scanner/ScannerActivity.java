package com.eisbetech.ikhwanmart.scanner;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.database.User;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.services.network.HttpRequest;

public class ScannerActivity extends AppCompatActivity {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.scanner_view)
    CodeScannerView mScannerView;
    @BindView(R.id.btn_my_qr_code)
    ImageView btnMyQrCode;
    private CodeScanner mCodeScanner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        ButterKnife.bind(this);

        mCodeScanner = new CodeScanner(this, mScannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                Intent payIntent = new Intent(ScannerActivity.this, ActivityPayByQr.class);
                payIntent.putExtra("qr", result.getText());
                startActivity(payIntent);
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });*/
            }
        });

    }

    public void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_qr_code);

        ImageView text = (ImageView) dialog.findViewById(R.id.iv_code);

        User profile_user = User.getUser();
        String noHp = profile_user.phone_number;
        String encode1 = HttpRequest.Base64.encodeBytes(noHp.getBytes());
        String encode2 = HttpRequest.Base64.encodeBytes(encode1.getBytes());

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(encode2, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            text.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        dialog.show();

    }


    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    @OnClick({R.id.back_button, R.id.btn_my_qr_code})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.btn_my_qr_code:
                showDialog();
                break;
        }
    }
}
