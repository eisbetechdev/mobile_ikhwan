package com.eisbetech.ikhwanmart.scanner;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.loopj.android.http.Base64;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import faranjit.currency.edittext.CurrencyEditText;

public class ActivityPayByQr extends AppCompatActivity {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.nama_toko)
    TextView mNamaToko;
    @BindView(R.id.jumlah_dibayar)
    CurrencyEditText mJumlahDibayar;
    @BindView(R.id.button_bayar)
    Button mButtonBayar;
    @BindView(R.id.inisial)
    TextView mInisial;
    @BindView(R.id.catatan_tambahan)
    EditText mCatatanTambahan;
    private String current;
    private Bundle extras;
    private String resultQr;
    private String nomerTujuan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_by_qr);
        ButterKnife.bind(this);

        extras = getIntent().getExtras();
        resultQr = extras.getString("qr");

        nomerTujuan = new String(Base64.decode(new String(Base64.decode(resultQr, Base64.DEFAULT)), Base64.DEFAULT));
        Log.d("unik", nomerTujuan + "");

        cek_transfer(nomerTujuan, "10000");

        Locale localeID = new Locale("in", "ID");
        final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
    }

    public void cek_transfer(String nomer, String nominal) {
        Helper.hideSoftKeyboard(this);
        final ProgressDialog progressDialog = new ProgressDialog(this);


        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("no_hp", nomer);
            params.put("nominal", nominal);


            BisatopupApi.get("/transaksi/cek-transfer", params, this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(ActivityPayByQr.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        Log.d("respon", responseString);
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        int is_agent = object.getJSONObject("user").getInt("is_agent");
                        Log.d("respon", is_agent + "");

                        if (!error) {
                            if (is_agent == 1) {
                                JSONObject user = object.getJSONObject("user");
                                Helper.hideSoftKeyboard(ActivityPayByQr.this);
                                mNamaToko.setText(user.getString("name"));
                                mInisial.setText(user.getString("name").substring(0, 1));
                            } else {
                                Drawable img = new IconicsDrawable(ActivityPayByQr.this, FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                                new LovelyStandardDialog(ActivityPayByQr.this)
                                        .setTopColorRes(R.color.accent)
                                        .setButtonsColorRes(R.color.accent)
                                        .setIcon(img)
                                        .setTitle("Error")
                                        .setMessage("Toko yang dituju bukan Agent")
                                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                finish();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                finish();
                                            }
                                        })
                                        .show();
                            }
                            /*Drawable img = new IconicsDrawable(ActivityPayByQr.this, FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                            new LovelyStandardDialog(ActivityPayByQr.this)
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    .setTitle("Konfirmasi Transfer")
                                    .setMessage("Anda akan transfer sejumlah " + Helper.format_money(txt_nominal.getText().toString()) + " ke nomor " + user.getString("phone_number") + " atas nama " + user.getString("name") + ", apakah anda yakin?")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            User user = User.getUser();
                                            if (user!=null && user.lock_pin_enable!=null && user.lock_pin_enable) {
                                                Intent intent = new Intent(getActivity(), LockPinActivity.class);
                                                intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                                                intent.setAction("Masukkan PIN");
                                                startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                                            } else {
                                                transfer_saldo();
                                            }
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, null)
                                    .show();*/
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(ActivityPayByQr.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Drawable img = new IconicsDrawable(ActivityPayByQr.this, FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                        new LovelyStandardDialog(ActivityPayByQr.this)
                                .setTopColorRes(R.color.accent)
                                .setButtonsColorRes(R.color.accent)
                                .setIcon(img)
                                .setTitle("Error")
                                .setMessage("Transfer saldo tidak dapat dilakukan.")
                                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                })
                                .show();
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void transfer_saldo(String nomer, String nominal, String note) {
        final ProgressDialog progressDialog = new ProgressDialog(this);


        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("no_hp", nomer);
            params.put("nominal", nominal);
            params.put("note", note);


            BisatopupApi.get("/transaksi/transfer-saldo", params, this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(ActivityPayByQr.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");

                            Drawable img = new IconicsDrawable(ActivityPayByQr.this, FontAwesome.Icon.faw_check_circle).sizeDp(32).color(Color.WHITE);

                            new LovelyStandardDialog(ActivityPayByQr.this)
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                    .setTitle("Transaksi Sukses")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                        }
                                    })
                                    .setMessage("Pembayaran ke Toko " + mNamaToko.getText().toString() + " Sebesar Rp. " + mJumlahDibayar.getText().toString() + " berhasil")
                                    .show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(ActivityPayByQr.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick({R.id.back_button, R.id.button_bayar})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                break;
            case R.id.button_bayar:
                String regex = "[-\\\\[\\\\]^/,'*:.!><~@#$%+=?|\\\"\\\\\\\\()]+";
                transfer_saldo(nomerTujuan, mJumlahDibayar.getText().toString().replaceAll(regex, ""),mCatatanTambahan.getText().toString());
                break;
        }
    }
}
