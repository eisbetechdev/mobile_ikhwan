package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.service.GPSTracker;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import id.zelory.compressor.Compressor;
import mehdi.sakout.fancybuttons.FancyButton;

public class UpgradeAgentActivity extends BaseActivity implements IPickResult, DatePickerDialog.OnDateSetListener {
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 110;
    private static final int MY_PERMISSIONS_REQUEST_COURSE_LOCATION = 111;

    GPSTracker gps;
    private DatePicker dpResult;

    private int year;
    private int month;
    private int day;

    private double latitude;
    private double longitude;
    static final int DATE_DIALOG_ID = 999;

    @BindView(R.id.txt_nama)
    protected MaterialEditText txt_nama;

    @BindView(R.id.txt_alamat)
    protected MaterialEditText txt_alamat;

    @BindView(R.id.txt_tgl_lahir)
    protected MaterialEditText txt_tgl_lahir;

    @BindView(R.id.img_ktp)
    protected ImageView img_ktp;

    @BindView(R.id.layout_continue)
    protected LinearLayout layout_continue;

    @BindView(R.id.rd_male)
    protected RadioButton rd_male;

    @BindView(R.id.rd_female)
    protected RadioButton rd_female;

    @BindView(R.id.btn_lanjut)
    protected FancyButton btn_lanjut;

    @BindView(R.id.layout_thanks)
    protected LinearLayout layout_thanks;
    @BindView(R.id.layout_form)
    protected LinearLayout layout_form;

    @BindView(R.id.btn_pick_image)
    protected FancyButton btn_pick_image;


    String upload_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_agent);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Upgrade Akun");
            getSupportActionBar().setElevation(0);
        }
        img_ktp.setVisibility(View.GONE);
        layout_continue.setVisibility(View.GONE);
        layout_thanks.setVisibility(View.GONE);
        layout_form.setVisibility(View.GONE);
        getProfile();

        txt_tgl_lahir.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    show_date();
                }
            }
        });

        Helper.checkPermission(this);

// Should we show an explanation?
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            getLocation();
        } else {

            // The ACCESS_COARSE_LOCATION is denied, then I request it and manage the result in
            // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COURSE_LOCATION);
            }
            // The ACCESS_FINE_LOCATION is denied, then I request it and manage the result in
            // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            }

        }
    }

    private void getLocation(){
        gps = new GPSTracker(UpgradeAgentActivity.this);
        // Show an expanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
        // check if GPS enabled
        if(gps.canGetLocation()){

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    @OnClick(R.id.txt_tgl_lahir)
    public void show_date() {
        Calendar now = Calendar.getInstance();
        String date = txt_tgl_lahir.getText().toString().trim();

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                UpgradeAgentActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (!date.isEmpty()) {
            String[] dateArr = date.split("-");
            try {
                dpd = DatePickerDialog.newInstance(
                        UpgradeAgentActivity.this,
                        Integer.parseInt(dateArr[2]),
                        Integer.parseInt(dateArr[1]) - 1,
                        Integer.parseInt(dateArr[0])
                );
            } catch (Exception Ec) {
            }
        }

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    // display current date
    public void setCurrentDateOnView() {


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

//        // set current date into textview
//        tvDisplayDate.setText(new StringBuilder()
//                // Month is 0 based, just add 1
//                .append(month + 1).append("-").append(day).append("-")
//                .append(year).append(" "));

        // set current date into datepicker
        dpResult.init(year, month, day, null);

    }


    @OnClick(R.id.btn_pick_image)
    public void pick_image() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            } else {
                PickImageDialog.build(
                        new PickSetup()
                                .setButtonOrientation(LinearLayoutCompat.VERTICAL)
                ).show(this);
            }
        } else {
            PickImageDialog.build(
                    new PickSetup()
                            .setButtonOrientation(LinearLayoutCompat.VERTICAL)
            ).show(this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == MY_CAMERA_REQUEST_CODE) {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

                    PickImageDialog.build(
                            new PickSetup()
                                    .setButtonOrientation(LinearLayoutCompat.VERTICAL)
                    ).show(this);

                } else {

                    Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

                }

            }

            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted
                        getLocation();
                    } else {
                        // permission denied
                        finish();
                        Helper.showMessage(getApplicationContext(),"Akses ke gps diperlukan");
                    }
                    break;
                }
                case MY_PERMISSIONS_REQUEST_COURSE_LOCATION: {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted
                    } else {
                        // permission denied
                        finish();
                        Helper.showMessage(getApplicationContext(),"Akses ke gps diperlukan");
                    }
                    break;
                }

            }

        } catch (
                Exception ec)

        {
            ec.printStackTrace();
            Crashlytics.logException(ec);;
        }

    }

    @OnClick(R.id.btn_lanjut)
    public void upload() {
        save(upload_image, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            upload_image = pickResult.getPath();
            File actualImage = new File(upload_image);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(actualImage);
            //If you want the Bitmap.
            img_ktp.setImageBitmap(compressedImageBitmap);

            //Image path
            //r.getPath();
            img_ktp.setVisibility(View.VISIBLE);
            layout_continue.setVisibility(View.VISIBLE);

            try {
                File compressedImage = new Compressor(this)
                        .setMaxWidth(640)
                        .setMaxHeight(480)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.WEBP)
                        .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES).getAbsolutePath())
                        .compressToFile(actualImage);

                upload_image = compressedImage.getPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void save(String image_path, final boolean is_show_loading) {
        final ProgressDialog progressDialog = new ProgressDialog(UpgradeAgentActivity.this);
        ;
        btn_lanjut.setEnabled(false);
        if (is_show_loading && progressDialog != null) {

            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Sedang upload data, mohon menunggu...");
            if (is_show_loading) progressDialog.show();
        }
        try {
            File profile_file_img = new File(image_path);

            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("nama", txt_nama.getText());
            params.put("alamat", txt_alamat.getText());
            params.put("latitude", latitude);
            params.put("logitude", longitude);
            params.put("img", profile_file_img);
            params.put("tgl_lahir", txt_tgl_lahir.getText().toString().trim());

            if (rd_male.isChecked()) {
                params.put("jenis_kelamin", "male");
            } else {
                params.put("jenis_kelamin", "female");
            }

            params.setForceMultipartEntityContentType(true);
            BisatopupApi.post("/profile/upload-ktp", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    btn_lanjut.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(UpgradeAgentActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    btn_lanjut.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                            layout_form.setVisibility(View.GONE);
                            layout_thanks.setVisibility(View.VISIBLE);
                            btn_lanjut.setVisibility(View.GONE);

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(UpgradeAgentActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProfile() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(UpgradeAgentActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/profile/index", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    try {
                        progressDialog.dismiss();

                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            layout_form.setVisibility(View.GONE);
                            layout_thanks.setVisibility(View.VISIBLE);
                            btn_pick_image.setVisibility(View.GONE);
                            String message = object.getString("message");
                            Toast.makeText(UpgradeAgentActivity.this, message, Toast.LENGTH_SHORT).show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(UpgradeAgentActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {


                    try {

                        progressDialog.dismiss();

                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(UpgradeAgentActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONObject data = response.getJSONObject("data");

                        int is_agent = data.getInt("is_agent");
                        int is_upload = data.getInt("is_upload");

                        if (is_upload == 1) {
                            layout_form.setVisibility(View.GONE);
                            layout_thanks.setVisibility(View.VISIBLE);
                        } else {
                            layout_form.setVisibility(View.VISIBLE);
                        }

                        if (is_agent == 1) {
                            UpgradeAgentActivity.this.finish();
                            Toast.makeText(UpgradeAgentActivity.this, "Anda sudah menjadi Agen.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
        txt_tgl_lahir.setText(date);
    }
}
