package com.eisbetech.ikhwanmart.database;


import com.crashlytics.android.Crashlytics;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by khadafi on 3/25/2017.
 */

public class Migration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            RealmObjectSchema personSchema = schema.get("Person");

            // Combine 'firstName' and 'lastName' in a new field called 'fullName'
            oldVersion++;
        }

        if (oldVersion == 1) {
            schema.get("User").addField("is_agent", int.class);
            oldVersion = 4;
        }

        if (oldVersion == 3) {
            schema.get("User").addField("is_agent", int.class);
            oldVersion++;
        }

        if (oldVersion == 4) {
            schema.get("User").addField("profile_url", String.class);
            oldVersion++;
        }

        if (oldVersion == 5) {
            schema.get("User").addField("kode_affiliasi", String.class);
            schema.get("User").addField("sponsored_by", String.class);
            oldVersion++;

        }
        if (oldVersion == 6) {
            schema.get("User").addField("total_affiliasi", String.class);
            schema.get("User").addField("total_pendapatan", String.class);
            oldVersion++;
        }
        if (oldVersion == 7) {
            schema.get("User").addField("nama_agen", String.class);
            oldVersion++;

        }
        if (oldVersion == 8) {
            schema.get("User").addField("jenis_kelamin", String.class);
            schema.get("User").addField("tgl_lahir", String.class);
            oldVersion++;
        }
        if (oldVersion == 9) {
            schema.get("User").addField("user_id", int.class);
            oldVersion++;
        }
        if (oldVersion == 10) {
            schema.get("Message").addField("date", String.class);
            oldVersion++;
        }
        if (oldVersion == 11) {
            schema.get("User").addField("device_id", String.class);
            oldVersion++;
        }
        if (oldVersion == 12) {
            schema.get("Favorite").addField("category", String.class);
            oldVersion++;
        }
        if (oldVersion == 13) {
            schema.get("Favorite").addField("is_sync", int.class);

            schema.get("User").addField("product_version", int.class);

            RealmObjectSchema petSchema = schema.create("ProductDetail")
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("product_id", int.class)
                    .addField("is_gangguan", int.class)
                    .addField("product_name", String.class)
                    .addField("price", String.class)
                    .addField("base_price", String.class)
                    .addField("desc", String.class)
                    ;

            schema.get("Product").addField("order_num", int.class);
            schema.get("ProductDetail").addField("is_active", int.class);
            schema.get("ProductDetail").addField("grup", int.class);

            schema.get("ProductDetail").removeField("base_price");
            schema.get("ProductDetail").removeField("price");

            schema.get("ProductDetail").addField("price",Double.class);
            schema.get("ProductDetail").addField("base_price",Double.class);

            try{
                petSchema.setNullable("price",true);
                petSchema.setNullable("base_price",true);
                petSchema.setNullable("desc",true);
            }catch (Exception ec){
                Crashlytics.logException(ec);
                ec.printStackTrace();
            }

            try{
                petSchema.setRequired("price",false);
                petSchema.setNullable("base_price",false);
                petSchema.setNullable("desc",false);
            }catch (Exception ec){
                Crashlytics.logException(ec);
                ec.printStackTrace();
            }

            oldVersion = 20;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
