package com.eisbetech.ikhwanmart.database;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by khadafi on 9/4/2016.
 */
public class User extends RealmObject {
    @PrimaryKey
    int id;

    public String fullname;
    public int user_id;
    public String username;
    public String first_name;
    public String last_name;
    public String email;
    public String phone_number;
    public String role;
    public String token;
    public String profile_url;
    public String wallet;
    public String kode_affiliasi;
    public String device_id;
    public String sponsored_by;
    public Boolean lock_pin_enable;
    public String pin;
    public String total_affiliasi;
    public String total_pendapatan;
    public String nama_agen;
    public String jenis_kelamin;
    public String tgl_lahir;
    public int product_version;

    public int poin;
    public int is_agent;

    public static User getUser() {
        Realm realm = RealmController.getInstance().getRealm();
        User user = realm.where(User.class).findFirst();
        if (user != null && user.lock_pin_enable == null) {
            realm.beginTransaction();
            user.lock_pin_enable = false;
            user.pin = "";
            if(user.fullname.split("\\w+").length>1){

                user.last_name = user.fullname.substring(user.fullname.lastIndexOf(" ")+1);
                user.first_name = user.fullname.substring(0, user.fullname.lastIndexOf(' '));
            }
            else{
                user.first_name = user.fullname;
            }
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
        }
        return user;
    }

    public static void createUser(final String fullname,final String email, final String token) {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<User> results = realm.where(User.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
                User user = realm.createObject(User.class,1);
                user.fullname = fullname;
                user.email  = email;

                if(user.fullname.split("\\w+").length>1){

                    user.last_name = user.fullname.substring(user.fullname.lastIndexOf(" ")+1);
                    user.first_name = user.fullname.substring(0, user.fullname.lastIndexOf(' '));
                }
                else{
                    user.first_name = user.fullname;
                }

                user.token  = token;
                user.lock_pin_enable = false;
                user.pin = "";
            }
        });
    }

    public static void ClearUser() {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<User> results = realm.where(User.class).findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }


}
