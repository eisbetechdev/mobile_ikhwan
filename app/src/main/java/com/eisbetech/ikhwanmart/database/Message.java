package com.eisbetech.ikhwanmart.database;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by khadafi on 9/4/2016.
 */
public class Message extends RealmObject {
    @PrimaryKey
    public int id;

    public String title;
    public String content;
    public String full_content;
    public String image_url;
    public String url;
    public String date;
    public boolean is_read;

    public int getNextPrimaryKey(Realm realm) {
        Message message = realm.where(Message.class).findFirst();
        if (message == null) {
            return 1;
        } else return realm.where(Message.class).max("id").intValue() + 1;
    }
}
