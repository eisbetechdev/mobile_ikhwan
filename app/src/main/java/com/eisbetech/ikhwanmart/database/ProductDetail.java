package com.eisbetech.ikhwanmart.database;



import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Khadafi on 11/02/2018.
 */

public class ProductDetail extends RealmObject {
    @PrimaryKey
    public int id;

    public int product_id;
    public String product_name;
    public double price;
    public double base_price;
    public String desc;
    public int is_gangguan;
    public int is_active;
    public int grup;

    public static void ClearData() {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<ProductDetail> results = realm.where(ProductDetail.class).findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }

    public static void Delete(ProductDetail data) {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<ProductDetail> results = realm.where(ProductDetail.class)
                .equalTo("id", data.id)
                .findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }

    public static void AddOrUpdate(final ProductDetail data) {
        try {
            Realm realm = RealmController.getInstance().getRealm();


            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    ProductDetail productDetail = bgRealm.where(ProductDetail.class)
                            .equalTo("id", data.id)
                            .findFirst();

                    if (productDetail == null) {
                        productDetail = new ProductDetail();
                        productDetail.id = data.id;
                    }

                    productDetail.product_id = data.product_id;
                    productDetail.product_name = data.product_name;
                    productDetail.price = data.price;
                    productDetail.base_price = data.base_price;
                    productDetail.desc = data.desc;
                    productDetail.is_gangguan = data.is_gangguan;
                    productDetail.is_active = data.is_active;
                    productDetail.grup = data.grup;

                    bgRealm.copyToRealmOrUpdate(productDetail);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                }
            });
        } catch (Exception ec) {
            ec.printStackTrace();
//            Crashlytics.logException(e);;
        }
    }
    public static void AddOrUpdateList(final List<ProductDetail> productDetails) {
        try {
            Realm realm = RealmController.getInstance().getRealm();


            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {

                    for (ProductDetail data :
                            productDetails) {
                        ProductDetail productDetail = bgRealm.where(ProductDetail.class)
                                .equalTo("id", data.id)
                                .findFirst();

                        if (productDetail == null) {
                            productDetail = new ProductDetail();
                            productDetail.id = data.id;
                        }

                        productDetail.product_id = data.product_id;
                        productDetail.product_name = data.product_name;
                        productDetail.price = data.price;
                        productDetail.base_price = data.base_price;
                        productDetail.desc = data.desc;
                        productDetail.is_gangguan = data.is_gangguan;
                        productDetail.is_active = data.is_active;
                        productDetail.grup = data.grup;
                        bgRealm.copyToRealmOrUpdate(productDetail);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                }
            });
        } catch (Exception ec) {
            ec.printStackTrace();
//            Crashlytics.logException(e);;
        }
    }


    public static void Update(ProductDetail data) {
        try {
            Realm realm = RealmController.getInstance().getRealm();
            ProductDetail productDetail = realm.where(ProductDetail.class)
                    .equalTo("id", data.id)
                    .findFirst();

            if (productDetail != null) {


                realm.beginTransaction();
                productDetail.product_id = data.product_id;
                productDetail.product_name = data.product_name;
                productDetail.price = data.price;
                productDetail.base_price = data.base_price;
                productDetail.desc = data.desc;
                productDetail.is_gangguan = data.is_gangguan;
                productDetail.is_active = data.is_active;
                productDetail.grup = data.grup;
                realm.copyToRealmOrUpdate(productDetail);
                realm.commitTransaction();
            }
        } catch (Exception ec) {
            ec.printStackTrace();
        }
    }
}
