package com.eisbetech.ikhwanmart.database;



import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by khadafi on 9/7/2016.
 */
public class Product extends RealmObject {
    @PrimaryKey
    public int id;

    public int parent_id;
    public String product_name;
    public String product;
    public String code;
    public String prefix;
    public String description;
    public String img_url;
    public int is_active;
    public int is_gangguan;

    public int order_num;

    public static void ClearData() {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<Product> results = realm.where(Product.class).findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }

    public static void Delete(Product data) {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<Product> results = realm.where(Product.class)
                .equalTo("id", data.id)
                .findAll();

        // All changes to data must happen in a transaction
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }

    public static void AddOrUpdate(final Product data) {
        try {
            Realm realm = RealmController.getInstance().getRealm();

// Asynchronously update objects on a background thread
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    Product product = bgRealm.where(Product.class)
                            .equalTo("id", data.id)
                            .findFirst();

                    if (product == null) {
                        product = new Product();
                        product.id = data.id;
                    }

                    product.parent_id = data.parent_id;
                    product.code = data.code;
                    product.description = data.description;
                    product.img_url = data.img_url;
                    product.prefix = data.prefix;
                    product.product = data.product;
                    product.product_name = data.product_name;
                    product.order_num = data.order_num;
                    bgRealm.copyToRealmOrUpdate(product);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {

                }
            });
        } catch (Exception ec) {
            ec.printStackTrace();
//            Crashlytics.logException(e);;
        }
    }

    public static void AddOrUpdateList(final List<Product> products) {
        try {
            Realm realm = RealmController.getInstance().getRealm();

// Asynchronously update objects on a background thread
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    for (Product data : products
                            ) {
                        Product product = bgRealm.where(Product.class)
                                .equalTo("id", data.id)
                                .findFirst();

                        if (product == null) {
                            product = new Product();
                            product.id = data.id;
                        }

                        product.parent_id = data.parent_id;
                        product.code = data.code;
                        product.description = data.description;
                        product.img_url = data.img_url;
                        product.prefix = data.prefix;
                        product.product = data.product;
                        product.product_name = data.product_name;
                        product.order_num = data.order_num;
                        bgRealm.copyToRealmOrUpdate(product);
                    }


                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {

                }
            });
        } catch (Exception ec) {
            ec.printStackTrace();
//            Crashlytics.logException(e);;
        }
    }
}
