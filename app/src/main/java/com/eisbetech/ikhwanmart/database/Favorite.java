package com.eisbetech.ikhwanmart.database;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by khadafi on 12/22/2016.
 */

public class Favorite extends RealmObject {
    @PrimaryKey
    public int id;

    public String nama;
    public String phone_number;
    public int product_id;
    public String category;
    public int is_sync;

    public int getNextPrimaryKey(Realm realm) {
        Favorite favorite = realm.where(Favorite.class).findFirst();
        if (favorite == null) {
            return 1;
        } else return realm.where(Favorite.class).max("id").intValue() + 1;
    }
}
