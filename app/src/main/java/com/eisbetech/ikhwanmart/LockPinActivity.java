package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.database.User;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LockPinActivity extends AppCompatActivity {
    public static final String TAG = "PinLockView";

    @BindView(R.id.profile_image)
    protected ImageView profile_image;

    @BindView(R.id.profile_name)
    protected TextView profile_name;

    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;

    public static int RESULT_PIN_INPUT_NEW = 101;
    public static int RESULT_PIN_INPUT_CONFIRM = 102;
    public static int RESULT_PIN_INPUT_VALIDATE = 103;
    public static int INPUT_CANCEL = 104;
    private Intent intent;

    String prev_pin = "";

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            int request_code = intent.getIntExtra("request_code", 101);
            String pin_sent = intent.getStringExtra("pin");
            Log.d(TAG, "Pin complete: " + pin);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("pin", pin);


            User user = User.getUser();

            if (request_code == RESULT_PIN_INPUT_CONFIRM) {
                setResult(request_code, returnIntent);
                if (pin.equals(pin_sent)) {
                    finish();
                } else {
                    Toast.makeText(LockPinActivity.this, "Pin tidak sama, masukkan pin yang sama", Toast.LENGTH_SHORT).show();
                }
            } else if (request_code == RESULT_PIN_INPUT_VALIDATE) {
                setResult(request_code, returnIntent);
                if (user.pin.equals(pin)) {
                    returnIntent.putExtra("validate", true);
                    finish();
                } else {
                    returnIntent.putExtra("validate", false);
                    Toast.makeText(LockPinActivity.this, "Pin yang anda masukkan salah", Toast.LENGTH_SHORT).show();
                }
            } else if (request_code == RESULT_PIN_INPUT_NEW) {

                profile_name.setText("Konfirmasi PIN");

                mPinLockView.resetPinLockView();
                if(!prev_pin.isEmpty()){
                    if(prev_pin.equals(pin)){
                        setResult(request_code, returnIntent);
                        finish();
                    }else{
                        Toast.makeText(LockPinActivity.this, "Pin tidak cocok", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    prev_pin = pin;
                }
            } else {
                setResult(request_code, returnIntent);
                finish();
            }

        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);

        }
    };

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lock_pin);

        ButterKnife.bind(this);

        intent = getIntent();

        String action = intent.getAction();
        if (action != null) {
            profile_name.setText(action);
        }

        Drawable img = new IconicsDrawable(LockPinActivity.this, FontAwesome.Icon.faw_lock).sizeDp(100).color(Color.WHITE);
        profile_image.setImageDrawable(img);

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);
        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();

        mPinLockView.setPinLength(4);
        mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.white));

        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL);
    }
}
