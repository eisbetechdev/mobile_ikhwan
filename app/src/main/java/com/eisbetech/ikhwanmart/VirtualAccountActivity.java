package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.VirtualAccount;
import com.eisbetech.ikhwanmart.item.VirtualAccountItem;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class VirtualAccountActivity extends BaseActivity {
    private FastItemAdapter<VirtualAccountItem> fastItemAdapter;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_account);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Virtual Account");
            getSupportActionBar().setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent());
                finish();
            }
        });

        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        fastItemAdapter.withSelectable(true);
        rotateloading.stop();

        load_va();
    }

    private void load_va() {
        Helper.hideSoftKeyboard(this);
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();

            BisatopupApi.get("/nicepay/list-va-topup", params, this,
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();

                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getApplicationContext(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            progressDialog.dismiss();

                            try {
                                final JSONObject response = new JSONObject(responseString);
                                if (response.getBoolean(K.error)) {
                                    Helper.showMessage(getApplicationContext(), response.getString(K.message));
                                } else {
                                    List<VirtualAccountItem> virtualAccountItems = new ArrayList<>();
                                    JSONArray data = response.getJSONArray("data_va");
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject obj = data.getJSONObject(i);
                                        VirtualAccount virtualAccount = new VirtualAccount();
                                        virtualAccount.payment_id = obj.getInt("payment_id");
                                        virtualAccount.nama_bank = obj.getString("nama_bank");
                                        virtualAccount.kode_bank = obj.getString("kode_bank");
                                        virtualAccount.image_url = obj.getString("image_url");
                                        virtualAccount.admin = obj.getString("admin");
                                        virtualAccount.virtual_account = obj.getString("virtual_account");
                                        virtualAccountItems.add(new VirtualAccountItem(virtualAccount));
                                    }

                                    fastItemAdapter.add(virtualAccountItems);
                                    fastItemAdapter.notifyAdapterDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
