package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.WalletTransaksi;
import com.eisbetech.ikhwanmart.item.WalletTransaksiItem;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.items.ProgressItem;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class WalletActivity extends BaseActivity {

    @BindView(R.id.txt_wallet)
    protected TextView txt_wallet;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    private FooterAdapter<ProgressItem> footerAdapter;

    SessionManager session;

    HashMap<String, String> user;

    @BindView(R.id.menu_green)
    protected FloatingActionMenu menu_green;

    /*@BindView(R.id.btn_kirim_saldo)
    protected FloatingActionButton btn_kirim_saldo;

    @BindView(R.id.btn_deposit)
    protected FloatingActionButton btn_deposit;*/

    @BindView(R.id.btn_redeem_voucher)
    protected FloatingActionButton btn_redeem_voucher;

    private FastItemAdapter<WalletTransaksiItem> fastItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Deposit Wallet");
            getSupportActionBar().setElevation(0);
        }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//
//        Drawable img = new IconicsDrawable(this, FontAwesome.Icon.faw_plus).sizeDp(32).color(Color.WHITE);
//        fab.setImageDrawable(img);
//
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//                Intent intent = new Intent(WalletActivity.this,MainLayout.class);
//                intent.setAction("deposit");
//                startActivity(intent);
//            }
//        });

        /*btn_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_green.toggle(true);
                Intent intent = new Intent(WalletActivity.this, MainLayout.class);
                intent.setAction("deposit");
                startActivity(intent);
            }
        });

        btn_kirim_saldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_green.toggle(true);
                Intent intent = new Intent(WalletActivity.this, MainLayout.class);
                intent.setAction("transfer_saldo");
                startActivity(intent);
            }
        });*/

        btn_redeem_voucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu_green.toggle(true);
                Intent intent = new Intent(WalletActivity.this, MainLayout.class);
                intent.setAction("redeem_voucher");
                startActivity(intent);
            }
        });

        session = new SessionManager(getApplicationContext(), this);
        footerAdapter = new FooterAdapter<>();
        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);


        list.addOnScrollListener(new EndlessRecyclerOnScrollListener(footerAdapter) {

            @Override
            public void onLoadMore(int currentPage) {
               // footerAdapter.clear();
               // footerAdapter.add(new ProgressItem().withEnabled(false));

                getTransaksi(currentPage, true);
                System.out.println(currentPage);
            }
        });

        fastItemAdapter.withSelectable(true);

        android_empty.setVisibility(View.GONE);

//        getProfile();
//
//        getTransaksi(1, true);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<WalletTransaksiItem>() {


            @Override
            public boolean onClick(View v, IAdapter<WalletTransaksiItem> adapter, WalletTransaksiItem item, int position) {
                String tanggal = item.walletTransaksi.tanggal;

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                try {
                    date = formatter.parse(item.walletTransaksi.tanggal);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
//                calendar.add(Calendar.HOUR, 3);
                    SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                    tanggal = (format2.format(calendar.getTime()));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                String saldo = "Sebelum : "+Helper.format_money(item.walletTransaksi.saldo_sebelum)+"<br> Setelah : "+Helper.format_money(item.walletTransaksi.sisa_saldo);
                Drawable img = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_info).sizeDp(32).color(Color.WHITE);
                new LovelyInfoDialog(WalletActivity.this)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(img)
                        //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                        .setNotShowAgainOptionEnabled(0)
                        .setTitle("Info Transaksi")
                        .setMessage(Helper.fromHtml(item.walletTransaksi.title + "<br><br>Jumlah : <b>" + item.walletTransaksi.nominal + "</b> <br>"+saldo+" <br><br> pada tanggal : " + tanggal))
                        .show();

                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (menu_green.isOpened()) {
            menu_green.toggle(true);
        } else {
            super.onBackPressed();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fastItemAdapter.clear();
        getTransaksi(1, true);
        getProfile();
    }

    private void getProfile() {
        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        txt_wallet.setText("Loading...");

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", email);

            BisatopupApi.get("/profile/index", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONObject data = response.getJSONObject("data");
                        User user = realm.where(User.class).equalTo("email", email).findFirst();


                        if (!realm.isInTransaction()) {
                            realm.beginTransaction();
                        }
                        if (user == null) {
                            user = realm.createObject(User.class, 1);
                        }
                        user.email = email;
                        user.fullname = data.getString("name");
                        user.phone_number = data.getString("phone_number");
                        user.role = data.getString("role");
                        user.poin = data.getInt("poin");
                        user.wallet = data.getString("wallet");
                        realm.commitTransaction();

                        txt_wallet.setText(user.wallet);

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getTransaksi(final int page, final boolean showloading) {


        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("page", page);

            BisatopupApi.get("/profile/riwayat-wallet", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();
                   // footerAdapter.clear();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                   // footerAdapter.clear();
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONArray data = response.getJSONArray("data");
//
                        List<WalletTransaksiItem> walletTransaksiItems = new ArrayList<>();
//
//
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            WalletTransaksi walletTransaksi = new WalletTransaksi();
                            walletTransaksi.nominal = obj.getString("jumlah");
                            walletTransaksi.title = obj.getString("note");
                            walletTransaksi.tanggal = obj.getString("created_date");
                            walletTransaksi.sisa_saldo = obj.getString("sisa_saldo");
                            walletTransaksi.saldo_sebelum = obj.getString("saldo_sebelumnya");

                            walletTransaksiItems.add(new WalletTransaksiItem(walletTransaksi));
                        }
//
                        fastItemAdapter.add(walletTransaksiItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (page == 1) android_empty.setVisibility(View.VISIBLE);

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

}
