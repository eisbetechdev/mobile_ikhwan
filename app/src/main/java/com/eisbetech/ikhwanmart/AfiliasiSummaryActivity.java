package com.eisbetech.ikhwanmart;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.fragment.FragmentDaftarDownline;
import com.eisbetech.ikhwanmart.fragment.FragmentTransaksiDownline;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AfiliasiSummaryActivity extends BaseActivity {
    @BindView(R.id.txt_total_affiliasi)
    protected TextView txt_total_affiliasi;

    @BindView(R.id.txt_total_pendapatan)
    protected TextView txt_total_pendapatan;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afiliasi_summary);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Detil Agen Anda");
            getSupportActionBar().setElevation(0);
        }

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Daftar Downline", FragmentDaftarDownline.class, new Bundler().putInt("id", 1).get())
                .add("Transaksi Downline", FragmentTransaksiDownline.class, new Bundler().putInt("id", 84).get())

                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

        user = User.getUser();
        txt_total_affiliasi.setText(user.total_affiliasi);
        txt_total_pendapatan.setText(Helper.fromHtml("Total Pendapatan Anda : <b>"+ Helper.format_money(user.total_pendapatan)+"</b>"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }
}
