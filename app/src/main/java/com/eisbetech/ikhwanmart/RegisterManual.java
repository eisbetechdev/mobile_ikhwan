package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.service.ReferrerReceiver;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class RegisterManual extends BaseActivity {
    String TAG = "RegisterManual";

    @BindView(R.id.txt_name)
    EditText txt_name;
    @BindView(R.id.txt_email)
    EditText txt_email;
    @BindView(R.id.txt_password)
    EditText txt_password;
    @BindView(R.id.txt_password_confirmation)
    EditText txt_password_confirmation;

    @BindView(R.id.txt_sponsor)
    EditText txt_sponsor;

    @BindView(R.id.txt_syarat)
    TextView txt_syarat;

    String token;
    String phone_number;
    String syarat = "By signing up to this application, you are agree to our term and privacy";
    SessionManager session;
    HashMap<String, String> user;
    private FirebaseAuth mAuth;

    String type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_manual);

        ButterKnife.bind(this);

        session = new SessionManager(this, this);
        mAuth = FirebaseAuth.getInstance();

        Drawable img = new IconicsDrawable(RegisterManual.this, FontAwesome.Icon.faw_user_md)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_name.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(RegisterManual.this, FontAwesome.Icon.faw_envelope)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_email.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(RegisterManual.this, FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(RegisterManual.this, FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password_confirmation.setCompoundDrawables(img, null, null, null);

        SharedPreferences preferences = getSharedPreferences(ReferrerReceiver.REFERRER, Context.MODE_PRIVATE);

        String utm_content = preferences.getString(ReferrerReceiver.UTM_CONTENT, "");

        String partToClick = "our term";

        Helper.createLink(txt_syarat, syarat, partToClick,
                        new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                Intent intent = new Intent(RegisterManual.this, ViewWeb.class);
                                intent.putExtra(K.title, S.terms_n_condition);
                                intent.putExtra(K.url, S.url_terms);
                                startActivity(intent);
                            }

                        });

        partToClick = "privacy";

        Helper.createLink(txt_syarat, syarat, partToClick,
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(RegisterManual.this, ViewWeb.class);
                        intent.putExtra(K.title, S.privacy_policy);
                        intent.putExtra(K.url, S.url_privacy);
                        startActivity(intent);
                    }

                });

        if (!utm_content.equals("")) {
            txt_sponsor.setVisibility(View.GONE);
            txt_sponsor.setText(utm_content);
        }

        Intent intent = getIntent();

        try {
            if (intent.getStringExtra("email") != null) {
                txt_email.setText(intent.getStringExtra("email"));
                txt_email.setEnabled(false);
            }

            if (intent.getStringExtra("name") != null) {
                txt_name.setText(intent.getStringExtra("name"));
            }
            if (intent.getStringExtra("token") != null) {
                token = intent.getStringExtra("token");
            }
            if (intent.getStringExtra("phone_number") != null) {
                phone_number = intent.getStringExtra("phone_number");
            }
            if (intent.getStringExtra("type") != null) {
                type = intent.getStringExtra("type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_create)
    void createAccount() {
        String name = txt_name.getText().toString();
        String email = txt_email.getText().toString().trim();
        String password = txt_password.getText().toString();
        String confirm_password = txt_password_confirmation.getText().toString();

        if (name.equals("")) {
            Toast.makeText(RegisterManual.this, "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.equals("")) {
            Toast.makeText(RegisterManual.this, "Email tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.equals("")) {
            Toast.makeText(RegisterManual.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (!password.equals(confirm_password)) {
            Toast.makeText(RegisterManual.this, "Password tidak sama", Toast.LENGTH_SHORT).show();
            return;
        } else if (!Helper.isValidEmail(email)) {
            Toast.makeText(RegisterManual.this, "Email tidak valid", Toast.LENGTH_SHORT).show();
        } else {
            if(type !=null && type.equals("sosmed")){
                registerSpw();
            }else{
                register();
            }
        }
        //  RegisterManual.this.finish();
//        Intent i = new Intent(RegisterManual.this, MainLayout.class);
//        i.setAction("phone_number_input");
//        startActivity(i);

    }

    private void register() {
        User.ClearUser();
        final ProgressDialog progressDialog = new ProgressDialog(RegisterManual.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("name", txt_name.getText());
            params.put("email", txt_email.getText().toString().trim());
            params.put("password", txt_password.getText());
            params.put("sponsor", txt_sponsor.getText());
            params.put("phone_number", phone_number);
            params.put("token", token);

            SharedPreferences preferences = getSharedPreferences(ReferrerReceiver.REFERRER, Context.MODE_PRIVATE);

            String utm_campaign = preferences.getString(ReferrerReceiver.UTM_CAMPAIGN,"");
            String utm_source = preferences.getString(ReferrerReceiver.UTM_SOURCE,"");
            String utm_medium = preferences.getString(ReferrerReceiver.UTM_MEDIUM,"");
            String utm_term = preferences.getString(ReferrerReceiver.UTM_TERM,"");
            String utm_content = preferences.getString(ReferrerReceiver.UTM_CONTENT,"");

            params.put(ReferrerReceiver.UTM_CAMPAIGN, utm_campaign);
            params.put(ReferrerReceiver.UTM_SOURCE, utm_source);
            params.put(ReferrerReceiver.UTM_MEDIUM, utm_medium);
            params.put(ReferrerReceiver.UTM_TERM, utm_term);
            params.put(ReferrerReceiver.UTM_CONTENT, utm_content);

            BisatopupApi.post("/home/register-accwithtoken", params, RegisterManual.this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(RegisterManual.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(RegisterManual.this, "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                           login(token);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(RegisterManual.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerSpw() {
        User.ClearUser();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("name", txt_name.getText());
            params.put("email", txt_email.getText().toString().trim());
            params.put("password", txt_password.getText());
            params.put("sponsor", txt_sponsor.getText());

            SharedPreferences preferences = getSharedPreferences(ReferrerReceiver.REFERRER, Context.MODE_PRIVATE);

            String utm_campaign = preferences.getString(ReferrerReceiver.UTM_CAMPAIGN,"");
            String utm_source = preferences.getString(ReferrerReceiver.UTM_SOURCE,"");
            String utm_medium = preferences.getString(ReferrerReceiver.UTM_MEDIUM,"");
            String utm_term = preferences.getString(ReferrerReceiver.UTM_TERM,"");
            String utm_content = preferences.getString(ReferrerReceiver.UTM_CONTENT,"");

            params.put(ReferrerReceiver.UTM_CAMPAIGN, utm_campaign);
            params.put(ReferrerReceiver.UTM_SOURCE, utm_source);
            params.put(ReferrerReceiver.UTM_MEDIUM, utm_medium);
            params.put(ReferrerReceiver.UTM_TERM, utm_term);
            params.put(ReferrerReceiver.UTM_CONTENT, utm_content);

            BisatopupApi.post("/user/register-acc", params, RegisterManual.this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(RegisterManual.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(RegisterManual.this, "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"),object.getString("key"));
                            Intent i = new Intent(RegisterManual.this, MainLayout.class);
                            i.setAction("phone_number_input");
                            startActivity(i);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(RegisterManual.this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void login(String token) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            String fbtoken = Helper.getFirebaseInstanceId(this);
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("token", token);
            params.put("fbtoken", fbtoken);
            params.put("device_id", Helper.getDeviceId(this));

            BisatopupApi.post("/home/fbaccess-token", params, this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(RegisterManual.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(RegisterManual.this, "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            Toast.makeText(RegisterManual.this, "Login berhasil", Toast.LENGTH_SHORT).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), object.getString("key"));
                            session.createLoginSession(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), "");
                            loginFirebase(object.getString("token"));
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(RegisterManual.this, message, Toast.LENGTH_SHORT).show();

                            boolean need_register = object.getBoolean("needregister");
                            if (need_register) {

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void loginFirebase(String mCustomToken) {
        showProgressDialog();
        mAuth.signInWithCustomToken(mCustomToken)
                .addOnCompleteListener(this, new com.google.android.gms.tasks.OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgressDialog();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                            finish();
                            session.checkLogin();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            Toast.makeText(RegisterManual.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this,LoginNew.class));
    }
}
