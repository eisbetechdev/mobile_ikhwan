package com.eisbetech.ikhwanmart.payment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;

/**
 * Created by zaki on 3/28/16.
 */
public class VirtualAccountAlfa extends AppCompatActivity {

    private static final int REQUEST_PHONE = 1;
    private static String[] PERMISSION_PHONE = {Manifest.permission.READ_PHONE_STATE};
    Button btnSubmit;
    TelephonyManager telephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.va_alfa);

        setupLayout();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ico_arrow_left));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });


        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(VirtualAccountAlfa.this, String.valueOf(PERMISSION_PHONE))
                        != PackageManager.PERMISSION_GRANTED) {
                    getPermissionFirst();
                } else {
                    //new VirtualAccountPayment().execute();
                }


            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void getPermissionFirst() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {

            ActivityCompat.requestPermissions(VirtualAccountAlfa.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PHONE);

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PHONE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //new VirtualAccountPayment().execute();
            } else {
                Toast.makeText(getApplicationContext(), "Permission Failed", Toast.LENGTH_SHORT);
            }
        }
    }


    private void setupLayout() {

        TextView textVa;
        Button btnSubmit;

        textVa = (TextView) findViewById(R.id.textVa);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

//        AppsUtil.applyFont(getApplicationContext(), textVa, "fonts/dokuregular.ttf");
//        AppsUtil.applyFont(getApplicationContext(), btnSubmit, "fonts/dokuregular.ttf");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }



}
