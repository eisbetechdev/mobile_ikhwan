package com.eisbetech.ikhwanmart;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by khadafi on 10/29/2016.
 */

public class SamplePreferenceFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        addPreferencesFromResource(R.xml.preferences);
    }
}
