package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.eisbetech.ikhwanmart.fragment.FragmentLoadWeb;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/23/2017.
 */

public class CaraBayarBankActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cara_bayar_bank);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Cara Pembayaran Bank Transfer");
            getSupportActionBar().setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent());
                finish();
            }
        });

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(CaraBayarBankActivity.this)
                .add("Bank BCA", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-bca").get())
                .add("Bank Mandiri", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-mandiri").get())
                .add("Bank Permata", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-permata").get())
                .add("Bank BRI", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-bri").get())
                .add("Bank BNI", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-bni").get())
                .add("CIMB Niaga", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-niaga").get())
                .add("Bank Danamon", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-danamon").get())
                .add("Bank Panin", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-panin").get())
                .add("Bank BTN", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-btn").get())
                .add("Bank Lainnya", FragmentLoadWeb.class, new Bundler().putString("url", "https://ikhwan.eisbetech.com/mobile/transfer-lainnya").get())
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
