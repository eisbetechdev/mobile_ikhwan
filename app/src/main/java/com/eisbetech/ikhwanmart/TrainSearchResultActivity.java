package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.TrainItem;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Train;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.view.IconicsImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class TrainSearchResultActivity extends AppCompatActivity {
    @BindView(R.id.list)
    protected RecyclerView list;
    private ProgressDialog mProgressDialog;
    FastItemAdapter<TrainItem> fastItemAdapter;
    List<TrainItem> trains;
    Boolean sortUp = false;
    SimpleDateFormat mdformat = new SimpleDateFormat("dd MMMM yyyy");
    Train selectedTrain = new Train();
    Intent inToDetailActivity = null;

    @BindView(R.id.button_harga)
    protected LinearLayout button_harga;

    @BindView(R.id.text_harga)
    protected TextView text_harga;

    @BindView(R.id.icon_sort_harga)
    protected IconicsImageView icon_sort_harga;

    @BindView(R.id.button_berangkat)
    protected LinearLayout button_berangkat;

    @BindView(R.id.text_berangkat)
    protected TextView text_berangkat;

    @BindView(R.id.icon_sort_berangkat)
    protected IconicsImageView icon_sort_berangkat;

    @BindView(R.id.button_tiba)
    protected LinearLayout button_tiba;

    @BindView(R.id.text_tiba)
    protected TextView text_tiba;

    @BindView(R.id.icon_sort_tiba)
    protected IconicsImageView icon_sort_tiba;

    @BindView(R.id.button_kursi)
    protected LinearLayout button_kursi;

    @BindView(R.id.text_kursi)
    protected TextView text_kursi;

    @BindView(R.id.icon_sort_kursi)
    protected IconicsImageView icon_sort_kursi;

    @BindView(R.id.stasiunAsalCode)
    protected TextView stasiunAsalCode;

    @BindView(R.id.stasiunTujuanCode)
    protected TextView stasiunTujuanCode;

    @BindView(R.id.tanggal_kereta)
    protected TextView tanggal_kereta;

    @BindView(R.id.jumlahDewasa)
    protected TextView jumlahDewasa;

    @BindView(R.id.jumlahBayi)
    protected TextView jumlahBayi;

    @BindView(R.id.list_not_found)
    protected LinearLayout list_not_found;

    @BindView(R.id.tipePerjalanan)
    protected TextView tipePerjalanan;

    @OnClick(R.id.button_harga)
    protected void sortHarga(){
       updateSortView(K.sort_price);
    }

    @OnClick(R.id.button_berangkat)
    protected void sortBerangkat(){
        updateSortView(K.sort_depart);
    }

    @OnClick(R.id.button_tiba)
    protected void sortTiba() {
        updateSortView(K.sort_arrive);
    }

    @OnClick(R.id.button_kursi)
    protected void sortKursi(){
       updateSortView(K.sort_seat);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.train_search_result);
        ButterKnife.bind(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        showProgressDialog();

        inToDetailActivity = new Intent(getApplicationContext(),TrainDetailActivity.class);
        list_not_found.setVisibility(View.GONE);
        updateSortView(K.initial);

        selectedTrain = getIntent().getParcelableExtra(K.selected_train);
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(selectedTrain.getStasiunAsalName() + " - "+ selectedTrain.getStasiunTujuanName());
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        stasiunAsalCode.setText(selectedTrain.getStasiunAsalCode());
        stasiunTujuanCode.setText(selectedTrain.getStasiunTujuanCode());

        String tgl_berangkat = getFormatTanggal1(selectedTrain.getTglBerangkat());
        tanggal_kereta.setText(tgl_berangkat);

        jumlahDewasa.setText(String.format("%s %s", selectedTrain.getJumlahDewasa(), S.adult));
        if (selectedTrain.getJumlahBayi() != null) {
            jumlahBayi.setText(String.format("%s %s", selectedTrain.getJumlahBayi(), S.infant));
        }
        fastItemAdapter = new FastItemAdapter<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        trains = new ArrayList<>();
        getJadwalKereta(selectedTrain);
        hideProgressDialog();

        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<TrainItem>() {
            @Override
            public boolean onClick(View v, IAdapter<TrainItem> adapter, TrainItem item, int position) {
                String sisaTiket = item.train.getSisa_kursi();
                String isSelectedPulang = tipePerjalanan.getText().toString();

                if (!sisaTiket.equals(S.run_out)) {
                    if (selectedTrain.getTglPulang() != null  && !isSelectedPulang.contains(S.return_)) {
                        inToDetailActivity.putExtra(K.selected_departure_schedule, item.train);

                        tanggal_kereta.setText(getFormatTanggal1(selectedTrain.getTglPulang()));
                        stasiunAsalCode.setText(selectedTrain.getStasiunTujuanCode());
                        stasiunTujuanCode.setText(selectedTrain.getStasiunAsalCode());

                        showProgressDialog();
                        getJadwalKeretaPulang(selectedTrain);
                        hideProgressDialog();
                    } else if (isSelectedPulang.contains(S.return_)){
                        inToDetailActivity.putExtra(K.selected_return_schedule, item.train);
                        startActivity(inToDetailActivity);
                    } else {
                        inToDetailActivity.putExtra(K.selected_departure_schedule, item.train);
                        startActivity(inToDetailActivity);
                    }
                } else {
                    Helper.showMessage(getApplicationContext(), S.ticket_sold_out);
                }
                return false;
            }
        });
    }

    private String getFormatTanggal1(String stringTgl) {
        String result = "";
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(mdformat.parse(stringTgl));

            String simpleDate = mdformat.format(cal.getTime());
            String day = S.daynames[cal.get(Calendar.DAY_OF_WEEK) - 1];

            result = day + " ," + simpleDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getFormatTanggaAPI(String stringTgl) {
        String result = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            cal.setTime(mdformat.parse(stringTgl));
            result = sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void getJadwalKereta(Train train) {
        try {
            RequestParams params = new RequestParams();
            params.add(K.origin, train.getStasiunAsalCode().trim());
            params.add(K.destination, train.getStasiunTujuanCode().trim());
            params.add(K.date, getFormatTanggaAPI(train.getTglBerangkat()));

            BisatopupApi.post("/kereta_api/cari", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    String a =  responseString;
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        loadData(response.getJSONArray(K.data));
                        tipePerjalanan.setText("Silahkan pilih dari "  + trains.size()+
                                " jadwal Kereta Berangkat");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getJadwalKeretaPulang(Train train) {
        try {
            RequestParams params = new RequestParams();
            params.add(K.origin, train.getStasiunTujuanCode().trim());
            params.add(K.destination, train.getStasiunAsalCode().trim());
            params.add(K.date, getFormatTanggaAPI(train.getTglPulang()));

            BisatopupApi.post("/kereta_api/cari", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    String a =  responseString;
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        trains = new ArrayList<>();
                        loadData(response.getJSONArray(K.data));
                        tipePerjalanan.setText("Silahkan pilih dari "  + trains.size() +
                                " jadwal Kereta Pulang ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData(JSONArray data) {
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj = null;
                obj = (JSONObject) data.get(i);
                Train train = new Train();
                train.setTrain_id(obj.getString(K.transporter_no));
                train.setTrain_type(obj.getString(K.class_.toUpperCase()) +
                        " ("+obj.getString(K.subclass)+")");
                train.setTrain_name(String.format("%s %s", obj.getString(K.transporter_name),
                        obj.getString(K.transporter_no)));
                train.setAdultFare(obj.getString(K.adult_fare));
                train.setChildFare(obj.getString(K.infant_fare));
                train.setStasiunAsalCode(obj.getString(K.org));
                train.setStasiunTujuanCode(obj.getString(K.des));
                train.setStasiunAsalName(selectedTrain.getStasiunAsalName());
                train.setStasiunTujuanName(selectedTrain.getStasiunTujuanName());
                train.setTglBerangkat(selectedTrain.getTglBerangkat());
                train.setTglPulang(selectedTrain.getTglPulang());
                train.setJumlahBayi(selectedTrain.getJumlahBayi());
                train.setJumlahDewasa(selectedTrain.getJumlahDewasa());

                String depart_time = obj.getString(K.dep_datetime);
                String arrive_time = obj.getString(K.arv_datetime);

                String keterangan_waktu = getDurasi(depart_time, arrive_time);
                train.setKeterangan_waktu(keterangan_waktu);
                train.setWaktu(depart_time.split("-")[1] +" - "+ arrive_time.split("-")[1]);

                String sisaKursi = !obj.getString(K.avb).equals("0") ? obj.getString(K.avb) +
                        " Kursi tersisa" : S.run_out;
                train.setSisa_kursi(sisaKursi);
                trains.add(new TrainItem(train));
            }
            fastItemAdapter.clear();
            fastItemAdapter.add(trains);
            fastItemAdapter.notifyDataSetChanged();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getDurasi(String dep_datetime, String arv_datetime) {
        SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm");
        dep_datetime = dep_datetime.replace("-", "");
        arv_datetime = arv_datetime.replace("-", "");
        String durasi = "";
        try {
            Date start = format.parse(dep_datetime);
            Date end = format.parse(arv_datetime);

            long diff = end.getTime() - start.getTime();
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000);

            durasi = diffHours + " jam " + diffMinutes + " mnt";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return durasi;
    }


    private void updateSortView(String sortType) {
        IconicsDrawable icon;
        if (sortUp) {
            icon = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_arrow_up);
            sortUp = false;
        } else {
            icon = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_arrow_down);
            sortUp = true;
        }
        switch (sortType) {
            case K.initial:
                icon_sort_harga.setVisibility(View.GONE);
                icon_sort_berangkat.setVisibility(View.GONE);
                icon_sort_tiba.setVisibility(View.GONE);
                icon_sort_kursi.setVisibility(View.GONE);
                break;
            case K.sort_price:
                Collections.sort(trains, new Comparator<TrainItem>() {
                    @Override
                    public int compare(TrainItem ti1, TrainItem ti2) {
                        Integer price1 = Integer.parseInt(ti1.train.getAdultFare());
                        Integer price2 = Integer.parseInt(ti2.train.getAdultFare());

                        if (!sortUp) {
                            return (price1 > price2) ? -1 : (price1 < price2) ? 1 : 0;
                        } else {
                            return (price1 < price2) ? -1 : (price1 > price2) ? 1 : 0;
                        }
                    }
                });
                fastItemAdapter.clear();
                fastItemAdapter.add(trains);
                fastItemAdapter.notifyDataSetChanged();
                list.setAdapter(fastItemAdapter);

                icon_sort_harga.setVisibility(View.VISIBLE);
                icon_sort_berangkat.setVisibility(View.GONE);
                icon_sort_tiba.setVisibility(View.GONE);
                icon_sort_kursi.setVisibility(View.GONE);

                icon_sort_harga.setIcon(icon);
                icon_sort_harga.setBackgroundColor(getResources().getColor(R.color.primary_light));

                text_harga.setTextColor(getResources().getColor(R.color.primary_light));
                button_harga.setBackgroundColor(getResources().getColor(R.color.primary));

                text_berangkat.setTextColor(getResources().getColor(R.color.primary));
                button_berangkat.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_tiba.setTextColor(getResources().getColor(R.color.primary));
                button_tiba.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_kursi.setTextColor(getResources().getColor(R.color.primary));
                button_kursi.setBackgroundColor(getResources().getColor(R.color.grey_background));
                break;
            case K.sort_depart:
                Collections.sort(trains, new Comparator<TrainItem>() {
                    @Override
                    public int compare(TrainItem ti1, TrainItem ti2) {
                        String waktu1 = ti1.train.getWaktu().split("-")[0].trim();
                        String waktu2 = ti2.train.getWaktu().split("-")[0].trim();

                        Calendar cal1 = Calendar.getInstance();
                        cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(waktu1.split(":")[0]));
                        cal1.set(Calendar.MINUTE, Integer.parseInt(waktu1.split(":")[1]));
                        cal1.set(Calendar.SECOND, 0);
                        cal1.set(Calendar.MILLISECOND, 0);

                        Calendar cal2 = Calendar.getInstance();
                        cal2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(waktu2.split(":")[0]));
                        cal2.set(Calendar.MINUTE, Integer.parseInt(waktu2.split(":")[1]));
                        cal2.set(Calendar.SECOND, 0);
                        cal2.set(Calendar.MILLISECOND, 0);

                        if (!sortUp) {
                            return (cal1.after(cal2)) ? -1 : (cal1.before(cal2)) ? 1 : 0;
                        } else {
                            return (cal1.before(cal2)) ? -1 : (cal1.after(cal2)) ? 1 : 0;
                        }
                    }
                });
                fastItemAdapter.clear();
                fastItemAdapter.add(trains);
                fastItemAdapter.notifyDataSetChanged();
                list.setAdapter(fastItemAdapter);

                icon_sort_harga.setVisibility(View.GONE);
                icon_sort_berangkat.setVisibility(View.VISIBLE);
                icon_sort_tiba.setVisibility(View.GONE);
                icon_sort_kursi.setVisibility(View.GONE);

                icon_sort_berangkat.setIcon(icon);
                icon_sort_berangkat.setBackgroundColor(getResources().getColor(R.color.primary_light));
                text_berangkat.setTextColor(getResources().getColor(R.color.primary_light));
                button_berangkat.setBackgroundColor(getResources().getColor(R.color.primary));

                text_harga.setTextColor(getResources().getColor(R.color.primary));
                button_harga.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_tiba.setTextColor(getResources().getColor(R.color.primary));
                button_tiba.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_kursi.setTextColor(getResources().getColor(R.color.primary));
                button_kursi.setBackgroundColor(getResources().getColor(R.color.grey_background));
                break;
            case K.sort_arrive:
                Collections.sort(trains, new Comparator<TrainItem>() {
                    @Override
                    public int compare(TrainItem ti1, TrainItem ti2) {
                        String waktu1 = ti1.train.getWaktu().split("-")[1].trim();
                        String waktu2 = ti2.train.getWaktu().split("-")[1].trim();

                        Calendar cal1 = Calendar.getInstance();
                        cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(waktu1.split(":")[0]));
                        cal1.set(Calendar.MINUTE, Integer.parseInt(waktu1.split(":")[1]));
                        cal1.set(Calendar.SECOND, 0);
                        cal1.set(Calendar.MILLISECOND, 0);

                        Calendar cal2 = Calendar.getInstance();
                        cal2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(waktu2.split(":")[0]));
                        cal2.set(Calendar.MINUTE, Integer.parseInt(waktu2.split(":")[1]));
                        cal2.set(Calendar.SECOND, 0);
                        cal2.set(Calendar.MILLISECOND, 0);

                        if (!sortUp) {
                            return (cal1.after(cal2)) ? -1 : (cal1.before(cal2)) ? 1 : 0;
                        } else {
                            return (cal1.before(cal2)) ? -1 : (cal1.after(cal2)) ? 1 : 0;
                        }
                    }
                });
                fastItemAdapter.clear();
                fastItemAdapter.add(trains);
                fastItemAdapter.notifyDataSetChanged();
                list.setAdapter(fastItemAdapter);

                icon_sort_harga.setVisibility(View.GONE);
                icon_sort_berangkat.setVisibility(View.GONE);
                icon_sort_tiba.setVisibility(View.VISIBLE);
                icon_sort_kursi.setVisibility(View.GONE);

                icon_sort_tiba.setIcon(icon);
                icon_sort_tiba.setBackgroundColor(getResources().getColor(R.color.primary_light));

                text_tiba.setTextColor(getResources().getColor(R.color.primary_light));
                button_tiba.setBackgroundColor(getResources().getColor(R.color.primary));

                text_harga.setTextColor(getResources().getColor(R.color.primary));
                button_harga.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_berangkat.setTextColor(getResources().getColor(R.color.primary));
                button_berangkat.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_kursi.setTextColor(getResources().getColor(R.color.primary));
                button_kursi.setBackgroundColor(getResources().getColor(R.color.grey_background));
                break;
            case K.sort_seat:
                Collections.sort(trains, new Comparator<TrainItem>() {
                    @Override
                    public int compare(TrainItem ti1, TrainItem ti2) {
                        String kursi1 = ti1.train.getSisa_kursi();
                        String kursi2 = ti2.train.getSisa_kursi();

                        int kursi_1 = kursi1.contains(S.seat) ? Integer.parseInt(
                                kursi1.replace(S.seat_available, "").trim()) : 0;
                        int kursi_2 = kursi2.contains(S.seat) ? Integer.parseInt(
                                kursi2.replace(S.seat_available, "").trim()) : 0;

                        if (!sortUp) {
                            return (kursi_1 > kursi_2) ? -1 : (kursi_1 < kursi_2) ? 1 : 0;
                        } else {
                            return (kursi_1 < kursi_2) ? -1 : (kursi_1 > kursi_2) ? 1 : 0;
                        }
                    }
                });
                fastItemAdapter.clear();
                fastItemAdapter.add(trains);
                fastItemAdapter.notifyDataSetChanged();
                list.setAdapter(fastItemAdapter);

                icon_sort_harga.setVisibility(View.GONE);
                icon_sort_berangkat.setVisibility(View.GONE);
                icon_sort_tiba.setVisibility(View.GONE);
                icon_sort_kursi.setVisibility(View.VISIBLE);

                icon_sort_kursi.setIcon(icon);
                icon_sort_kursi.setBackgroundColor(getResources().getColor(R.color.primary_light));

                text_kursi.setTextColor(getResources().getColor(R.color.primary_light));
                button_kursi.setBackgroundColor(getResources().getColor(R.color.primary));

                text_harga.setTextColor(getResources().getColor(R.color.primary));
                button_harga.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_berangkat.setTextColor(getResources().getColor(R.color.primary));
                button_berangkat.setBackgroundColor(getResources().getColor(R.color.grey_background));
                text_tiba.setTextColor(getResources().getColor(R.color.primary));
                button_tiba.setBackgroundColor(getResources().getColor(R.color.grey_background));

                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mProgressDialog.hide();
                    if (trains.size() == 0)
                        list_not_found.setVisibility(View.VISIBLE);
                }
            }, 3000);
        }
    }
}

