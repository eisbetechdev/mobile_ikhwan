package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.crashlytics.android.Crashlytics;
import com.freshchat.consumer.sdk.Freshchat;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class LoginManual extends BaseActivity {
    @BindView(R.id.txt_username)
    EditText txt_username;

    @BindView(R.id.txt_password)
    EditText txt_password;

    SessionManager session;
    HashMap<String, String> user;
    private FirebaseAuth mAuth;

    String TAG = "LoginManual";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_manual);

        ButterKnife.bind(this);
        Drawable img = new IconicsDrawable(LoginManual.this, FontAwesome.Icon.faw_user_md)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_username.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(LoginManual.this, FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password.setCompoundDrawables(img, null, null, null);
        session = new SessionManager(this, LoginManual.this);
        mAuth = FirebaseAuth.getInstance();
        
        
    }
    @OnClick(R.id.btn_login)
    void login_acc() {
        login();
    }

    @OnClick(R.id.txt_lupa_password)
    public void lupa_pwd() {
        Intent inte = new Intent(LoginManual.this, MainLayout.class);
        inte.setAction("reset_password");
        startActivity(inte);
    }

    private void login() {
        final ProgressDialog progressDialog = new ProgressDialog(LoginManual.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("username", txt_username.getText());
            params.put("password", txt_password.getText());
            params.put("device_id", Helper.getDeviceId(LoginManual.this));

            BisatopupApi.post("/home/login", params,LoginManual.this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(LoginManual.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                            Toast.makeText(LoginManual.this, "Login berhasil", Toast.LENGTH_SHORT).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), object.getString("key"));
                            session.createLoginSession(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), "");
                            loginFirebase(object.getString("token"));
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(LoginManual.this, message, Toast.LENGTH_SHORT).show();

                            if (object.getJSONObject("data").getInt("is_verified") == 0) {
                                Intent i = new Intent(LoginManual.this, MainLayout.class);
                                i.setAction("phone_number_input");
                                startActivity(i);
                            } else {
                                int need_verify = object.getInt("need_verify");
                                if (need_verify == 1) {
                                    LoginManual.this.finish();
                                    Toast.makeText(LoginManual.this, "Verifikasi diperlukan.", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(LoginManual.this, MainLayout.class);
                                    i.setAction("verifikasi_phone");
                                    startActivity(i);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerUser() {
        final ProgressDialog progressDialog = new ProgressDialog(LoginManual.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        final User user = User.getUser();
        String token = Helper.getFirebaseInstanceId(LoginManual.this);
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("name", user.fullname);
            params.put("token", token);
            params.put("device_id", Helper.getDeviceId(LoginManual.this));

            BisatopupApi.post("/user/register-user", params,LoginManual.this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {

                        LoginManual.this.finish();
                        session.checkLogin();

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loginFirebase(String mCustomToken) {
        Toast.makeText(LoginManual.this, "Sedang Login, mohon menunggu", Toast.LENGTH_LONG).show();
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(txt_username.getText().toString(), txt_password.getText().toString())
                .addOnCompleteListener(LoginManual.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgressDialog();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            registerUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            mAuth.createUserWithEmailAndPassword(txt_username.getText().toString(), txt_password.getText().toString())
                                    .addOnCompleteListener(LoginManual.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                Log.d(TAG, "registerFirebase:success");
                                                mAuth.signInWithEmailAndPassword(txt_username.getText().toString(), txt_password.getText().toString())
                                                        .addOnCompleteListener(LoginManual.this, new OnCompleteListener<AuthResult>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                                if (task.isSuccessful()) {
                                                                    FirebaseUser user = mAuth.getCurrentUser();
                                                                    registerUser();
                                                                } else {
                                                                    Toast.makeText(LoginManual.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Log.w(TAG, "registerFirebase:failure", task.getException());
                                                Toast.makeText(LoginManual.this, "Failed.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    }
                });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, LoginNew.class));
    }
}
