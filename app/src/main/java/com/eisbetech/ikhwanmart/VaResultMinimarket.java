package com.eisbetech.ikhwanmart;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.crashlytics.android.Crashlytics;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by khadafi on 8/9/2017.
 *
 */

public class VaResultMinimarket extends BaseActivity {
    private Bundle bundle;
    private Transaksi transaksi;

    @BindView(R.id.tv_invoice)
    protected TextView tvInvoice;

    @BindView(R.id.totalValue)
    protected TextView TxtTotal;

    @BindView(R.id.kodePembayaran)
    protected TextView txtKodePembayaran;

    @BindView(R.id.btn_cara_bayar)
    protected Button btnCaraBayar;

    @BindView(R.id.tanggal_invoice)
    protected TextView tanggal_invoice;

    @BindView(R.id.tv_hour)
    protected TextView tvHour;

    @BindView(R.id.tv_minute)
    protected TextView tvMinute;

    @BindView(R.id.tv_second)
    protected TextView tvSecond;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.va_result_alfa);
        ButterKnife.bind(this);

        bundle = getIntent().getExtras();
        transaksi = bundle.getParcelable(K.transaction);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(S.payment + " #" + transaksi.trans_id);
            getSupportActionBar().setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        refreshView();


    }

    private void refreshView() {


        if (bundle != null) {

            bundle = getIntent().getExtras();
            transaksi = bundle.getParcelable(K.transaction);

            if (bundle != null) {

                Log.d("bundle", bundle.getString(K.data));

                try {
                    JSONObject jsonObject = new JSONObject(bundle.getString(K.data));

                    tvInvoice.setText(("00000000" + transaksi.trans_id).substring(transaksi.trans_id.length()));
                    TxtTotal.setText(Helper.format_money(transaksi.harga));
                    txtKodePembayaran.setText(jsonObject.getString(K.res_pay_code));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = formatter.parse(transaksi.expired_date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm");
                tanggal_invoice.setText(format2.format(calendar.getTime()) + " WIB");
                tanggal_invoice.setVisibility(View.VISIBLE);

                long now = Calendar.getInstance().getTimeInMillis();
                new CountDownTimer(calendar.getTimeInMillis() - now, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                        long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                        long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                        tvHour.setText(String.format("%02d", hours));
                        tvMinute.setText(String.format("%02d", minutes));
                        tvSecond.setText(String.format("%02d", seconds));
                        //tvCountDown.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
                    }
                    public void onFinish() {
                        Helper.showMessage(getApplicationContext(),"Waktu transfer anda sudah habis.");
                        finish();
                    }
                }.start();
            } catch (ParseException e) {
                e.printStackTrace();
                Crashlytics.logException(e);

            }
        }
    }

    @OnClick(R.id.btn_copy_rek)
    public void copyRek() {
        copy(S.account_number, txtKodePembayaran.getText().toString());
    }

    @OnClick(R.id.btn_copy_jumlah)
    public void copyJumlah() {
        copy(S.amount, transaksi.harga);
    }

    @OnClick(R.id.tv_invoice)
    public void copyTransactionNo() {
        copy(S.transaction_no, tvInvoice.getText().toString());
    }

    public void copy(String txt, String val) {
        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.
                CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(txt, val);
        clipboard.setPrimaryClip(clip);
        Helper.showMessage(this, txt + S.successfully_copied);
    }

    @OnClick(R.id.btn_cara_bayar)
    public void cara_bayar(){
        Intent intent = new Intent(this,LoadWebActivity.class);
        intent.putExtra("url","https://ikhwan.eisbetech.com/mobile/minimarket?kode_bayar="+txtKodePembayaran.getText());

        startActivity(intent);
    }
}
