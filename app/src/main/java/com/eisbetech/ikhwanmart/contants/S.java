package com.eisbetech.ikhwanmart.contants;

/**
 * Created by irvantommi on 25/08/2017.
 * put All String constants here
 */

public class S {
    public static final String[] daynames = new String[] {"Minggu", "Senin" , "Selasa", "Rabu",
            "Kamis" , "Jumat", "Sabtu"};

    // action
    public static final String action_add_affiliate = "tambah_afiliasi";
    public static final String action_add_favorite = "add_favorite";
    public static final String action_bill = "tagihan";
    public static final String action_bill_input = "input_tagihan";
    public static final String action_bills_reminder = "tagihan_reminder";
    public static final String action_bills_reminder_list = "list_tagihan_reminder";
    public static final String action_change_password = "change_password";
    public static final String action_checkout = "checkout";
    public static final String action_daftar = "daftar";
    public static final String action_detail = "detail";
    public static final String action_deposit = "deposit";
    public static final String action_edit_no_hp = "edit_no_hp";
    public static final String action_edit_profile = "edit_profile";
    public static final String action_favorite = "favorite";
    public static final String action_flight = "flight";
    public static final String action_media = "media";
    public static final String action_member_affiliation = "afiliasi_member";
    public static final String action_message = "message";
    public static final String action_price_check = "cek_harga";
    public static final String action_print_trx = "print_trx";
    public static final String action_printer_list = "list_printer";
    public static final String action_profile = "profile";
    public static final String action_phone_number_input = "phone_number_input";
    public static final String action_phone_verification = "verifikasi_phone";
    public static final String action_redeem_voucher = "redeem_voucher";
    public static final String action_refresh_menu = "refresh_menu";
    public static final String action_reset_password = "reset_password";
    public static final String action_support = "Support";
    public static final String action_train = "kereta_api";
    public static final String action_transaksi_detail = "transaksi_detail";
    public static final String action_transaksi_notif = "transaksi_notif";
    public static final String action_transfer_saldo = "transfer_saldo";
    public static final String action_update_image_profile = "update_image_profile";
    public static final String action_voucher_game = "voucher_game";
    public static final String action_voucher_tv = "voucher_tv";
    public static final String action_voucher_other = "voucher_lain";

    // airline
    public static final String airline_airasia = "airasia";
    public static final String airline_batik = "batik";
    public static final String airline_citilink = "citilink";
    public static final String airline_sriwijaya = "sriwijaya";

    // error
    public static final String error = "Error : ";
    public static final String error_download_receipt = "Download struk error.";
    public static final String error_io_exception = "Error : IOException ";
    public static final String error_malformed_url = "Error : MalformedURLException ";
    public static final String error_paychan = "PAYCHAN ERROR";
    public static final String error_pls_check_internet_connection = "Error : Please check your internet connection ";

    // firebase topic
    public static final String topic_agent = "agent";
    public static final String topic_member = "member";
    public static final String topic_news = "news";

    // info
    public static final String info_agent = "Masukkan nominal deposit anda, minimal deposit" +
            " sebesar <b>Rp 100.000.</b> <br> Contoh : 100000,150000,200000";
    public static final String info_member = "Masukkan nominal deposit anda, minimal deposit" +
            " sebesar <b>Rp 20.000.</b><br> Contoh : 20000,30000,50000";

    // format
    public static final String format_rp = "Rp %s,-";

    // menu
    public static final String menu_chat = "CHAT";
    public static final String menu_history = "HISTORY";
    public static final String menu_notification = "NOTIFICATION";

    // product
    public static final String product_balance_gopay = "Saldo GOPAY";
    public static final String product_balance_grabpay = "Saldo GRABPAY";
    public static final String product_balance_mutation = "Mutasi Saldo";
    public static final String product_credit_card = "Kartu Credit";
    public static final String product_data_package = "Paket Data";
    public static final String product_electricity_bill = "Tagihan Listrik";
    public static final String product_flight = "Pesawat";
    public static final String product_game_voucher = "Voucher Game";
    public static final String product_help_service = "Layanan Bantuan";
    public static final String product_faq = "F.A.Q";
    public static final String product_insurance_bpjs = "Asuransi (BPJS)";
    public static final String product_internet_bill = "Tagihan Internet";
    public static final String product_multifinance = "Multifinance";
    public static final String product_pdam = "PDAM";
    public static final String product_pln_token = "Token PLN";
    public static final String product_postpaid_phone = "Telepon Pascabayar";
    public static final String product_pulse = "Pulsa";
    public static final String product_soon = " (Soon)";
    public static final String product_state_gas = "Gas Negara (PGN)";
    public static final String product_telkom_bill = "Tagihan Telkom";
    public static final String product_train = "Kereta Api";
    public static final String product_tv = "Televisi";
    public static final String product_tv_voucher = "Voucher TV";
    public static final String product_other_voucher = "Voucher Lainnya";

    // tag
    public static final String tag_home = "home";
    public static final String tag_bill_reminder = "reminder_tagihan";
    public static final String tag_bill_reminder_list = "tagihan_reminder_list";
    public static final String tag_favorite = "favorite";
    public static final String tag_message_history = "message_history";
    public static final String tag_support = "support";
    public static final String tag_transaksi = "transaksi";

    // url
    private static final String url_base = "http://ikhwan.eisbetech.com";
    public static final String url_faq = url_base + "/mobile/faq";
    public static final String url_privacy = url_base + "/mobile/privacy";
    public static final String url_terms = url_base + "/mobile/terms";

    // others
    public static final String account_number = "Nomor rekening";
    public static final String add_affiliate = "Tambah Agen";
    public static final String add_favorite = "Tambah Favorit";
    public static final String add_to_passenger_data = "Tambah ke data Penumpang";
    public static final String adult = "Dewasa";
    public static final String affiliate = "Agen";
    public static final String amount = "Jumlah";
    public static final String are_u_sure = "Apakah anda yakin ?";
    public static final String are_u_sure_wanna_exit = "Apakah anda yakin untuk keluar?";
    public static final String are_u_sure_wanna_delete_all_notif = "Apakah anda yakin akan menghapus semua data notifikasi ini?";
    public static final String bca = "BCA";
    public static final String bri = "BRI";
    public static final String bni = "BNI";
    public static final String bill = "Tagihan";
    public static final String bills_reminder = "Pengingat Tagihan";
    public static final String bills_reminder_list = "Daftar Pengingat Tagihan";
    public static final String buy_voucher = "Beli Voucher";
    public static final String change_password = "Ubah password";
    public static final String checkout = "Checkout";
    public static final String clear_stack_activity = "clearStackActivity";
    public static final String confirm_delete = "Konfirmasi Hapus";
    public static final String confirm_transfer = "Konfirmasi Transfer";
    public static final String congrats_ur_transaction_has_been_successfull = "Selamat transaksi Anda sudah sukses.";
    public static final String credit_card = "kartu_kredit";
    public static final String customer_info = "Informasi Pemesan";
    public static final String datepicker_dialog = "Datepickerdialog";
    public static final String delete_from_passenger_data = "Hapus dari data Penumpang";
    public static final String depart = "berangkat";
    public static final String depart_date = "Tanggal Berangkat";
    public static final String deposit = "deposit";
    public static final String deposit_e_wallet = "Deposit E-Wallet";
    public static final String destination_airport = "Bandara Tujuan";
    public static final String download_receipt = "Download Struk";
    public static final String edit_profile = "Edit Profile";
    public static final String edit_mobile_number = "Edit Nomor Handphone";
    public static final String empty = "";
    public static final String enter_invoice_amount = "Masukkan Jumlah Tagihan";
    public static final String enter_mobile_number = "Masukkan Nomor Handphone";
    public static final String exit = "Exit";
    public static final String favorite_transaction = "Transaksi Favorit";
    public static final String forgot_password = "Lupa Password";
    public static final String from = "from";
    public static final String gas = "gas";
    public static final String get = "GET";
    public static final String how_to_topup = "Cara mengisi saldo bisatopup";
    public static final String infant = "Bayi";
    public static final String infant_cant_exceed_adult = "Jumlah Penumpang bayi tidak bisa melebihi dewasa";
    public static final String input_order_data = "Isi data Pemesanan";
    public static final String insurance = "asuransi";
    public static final String internet = "internet";
    public static final String konten_bisa = "Konten Bisa";
    public static final String loading = "Loading ...";
    public static final String loading_detail = "Loading Detail ...";
    public static final String loading_your_data = "Loading your data ...";
    public static final String logout = "Logout";
    public static final String make_sure_you_ve_transferred_before_continue_are_you_sure = "Pastikan anda sudah melakukan transfer sebelum melanjutkan, apakah anda yakin?";
    public static final String mandiri = "Mandiri";
    public static final String cimb = "CIMB";
    public static final String mandiri_syariah = "Mandiri Syariah";
    public static final String message_history = "Message History";
    public static final String mohon_menunggu = "Mohon menunggu...";
    public static final String multifinance = "multifinance";
    public static final String nominal = "Nominal";
    public static final String order_detail = "Detil Pemesanan";
    public static final String origin_airport = "Bandara Asal";
    public static final String other_info = "Infomasi Lain";
    public static final String payment = "Pembayaran";
    public static final String payment_cant_be_processed = "Pembayaran tidak dapat di proses.";
    public static final String payment_success = "Pembayaran Berhasil";
    public static final String permission_failed = "Permission Failed";
    public static final String pdf = ".pdf";
    public static final String pdam = "pdam";
    public static final String please_wait = "Please Wait...";
    public static final String pln = "pln";
    public static final String post_paid_phone = "telpon_pasca";
    public static final String ppob = "PPOB";
    public static final String print = "Print";
    public static final String print_receipt = "Print Struk";
    public static final String printer_not_connected = "Printer tidak tersambung";
    public static final String price_list = "Daftar Harga";
    public static final String privacy_policy = "Privacy and Policy";
    public static final String profile = "Profile";
    public static final String redeem_voucher = "Redeem Voucher";
    public static final String rekening = "rekening";
    public static final String return_ = "Pulang"; //cant use 'return'
    public static final String return_date = "Tanggal Pulang";
    public static final String run_out = "Habis";
    public static final String seat = "Kursi";
    public static final String seat_available = "Kursi tersisa";
    public static final String select_printer = "Pilih Printer";
    public static final String sorry_under_development = "Mohon maaf, Produk ini sedang dalam pengembangan";
    public static final String station_destination = "Stasiun Tujuan";
    public static final String station_origin = "Stasiun asal";
    public static final String successfully_copied = " berhasil di copy";
    public static final String support = "Support";
    public static final String telkom = "telkom";
    public static final String terms_n_condition = "Terms and Condition";
    public static final String text_plain = "text/plain";
    public static final String ticket_sold_out = "Tiket Habis";
    public static final String to = "to";
    public static final String train_ticket = "Tiket Kereta Api";
    public static final String transaction_detail = "Detil Transaksi";
    public static final String transaction_success = "Transaksi Sukses";
    public static final String transaction_no = "Nomor Transaksi";
    public static final String transaction_number_history = "Riwayat Nomor Transaksi";
    public static final String favorite_number_title = "Nomor Favorite";
    public static final String transfer_saldo = "Transfer Saldo";
    public static final String tv = "tv";
    public static final String unable_to_find_market_app = "unable to find market app";
    public static final String verification = "Verifikasi";
    public static final String verification_needed = "Verifikasi diperlukan.";
    public static final String voucher_game = "Voucher Game";
    public static final String voucher_other = "Voucher Lain";
    public static final String voucher_tv = "Voucher TV";
    public static final String your_transaction_cant_be_processed_pls_contact_our_cs_tq = "Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih";

}
