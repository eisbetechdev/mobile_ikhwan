package com.eisbetech.ikhwanmart.contants;

/**
 * Created by irvantommi on 25/08/2017.
 * put All integer constants here
 */

public class I {
    // payment
    public static final int payment_bca = 1;
    public static final int payment_mandiri = 2;
    public static final int payment_bni = 3;
    public static final int payment_bri = 4;
    public static final int payment_bsm = 6;
    public static final int payment_wallet = 13;
    public static final int payment_doku = 14;
    public static final int payment_minimarket = 15;
    public static final int payment_atm = 16;

    public static final int payment_bca_va = 22;
    public static final int payment_mandiri_va = 23;
    public static final int payment_bni_va = 24;
    public static final int payment_bri_va = 25;
    public static final int payment_cimb_va = 26;
    public static final int payment_danamon_va = 30;
    public static final int payment_permata_va = 29;
    public static final int payment_maybank_va = 28;
    public static final int payment_hanabank_va = 27;

    // request
    public static final int req_confirm_payment = 21;
    public static final int req_search_destination_airport = 22;
    public static final int req_search_destination_station = 23;
    public static final int req_search_origin_airport = 24;
    public static final int req_search_origin_station = 25;

    // other
    public static final int a = 0;

    public static final int[] VA_DATA= {22,23,24,25,26,27,28,29,30};

}
