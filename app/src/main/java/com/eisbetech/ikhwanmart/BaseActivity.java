package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.eisbetech.ikhwanmart.api.Mcrypt;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import io.realm.Realm;

/**
 * Created by khadafi on 7/31/2016.
 */
public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;
    public FirebaseRemoteConfig mFirebaseRemoteConfig;
    public long cacheExpiration;
    public Realm realm;
    //    private RealmConfiguration realmConfig;
    public Mcrypt mcrypt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        // Create the Realm configuration
//        realmConfig = new RealmConfiguration.Builder(this)
//                .deleteRealmIfMigrationNeeded()
//                .build();
//        // Open the Realm for the UI thread.
//        realm = Realm.getInstance(realmConfig);

        realm = RealmController.with(this).getRealm();
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        cacheExpiration = 3600; // 1 hour in seconds.

        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mcrypt = new Mcrypt();
    }

    @Override
    public void onResume() {
        super.onResume();
        realm = RealmController.with(this).getRealm();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
