package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.eisbetech.ikhwanmart.adapter.FlightiItem;
import com.eisbetech.ikhwanmart.api.FlightApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Flight;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class FlightResultActivity extends AppCompatActivity {
    @BindView(R.id.list)
    protected RecyclerView list;

    FastItemAdapter<FlightiItem> fastItemAdapter;
    List<FlightiItem> flights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_result);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle("CGK - KNO");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);

//            actionBar.setDisplayShowCustomEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(false);
//
//            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
//                    ActionBar.LayoutParams.MATCH_PARENT);
//           // layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL;
//            View cView = getLayoutInflater().inflate(R.layout.action_bar_custom, null);
//            actionBar.setCustomView(cView, layoutParams);
        }
        flights = new ArrayList<>();
        fastItemAdapter = new FastItemAdapter<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        searching();
        searchingAirasia();
        searchingCitilink();
        searchingSriwijaya();
    }

    private void searching() {
        final ProgressDialog progressDialog = new ProgressDialog(FlightResultActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading_your_data);
        progressDialog.show();

        try {
            RequestParams params = new RequestParams();
            params.add(K.airport_depart, "KNO");
            params.add(K.airport_arrive, "CGK");
            params.add(K.date_depart, "04/11/2016");
            params.add(K.date_return, "25/11/2016");
            params.add(K.adult, "1");
            params.add(K.child, "0");
            params.add(K.infant, "0");
            params.add(K.trip_type, "one-way");

            FlightApi.post("/lion/cari-tiket-murah", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject response = new JSONObject(responseString);
                        JSONArray data = response.getJSONArray(K.data);
                        LoadData(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchingCitilink() {
        try {
            RequestParams params = new RequestParams();
            params.add(K.airport_depart, "KNO");
            params.add(K.airport_arrive, "CGK");
            params.add(K.date_depart, "04/09/2016");
            params.add(K.date_return, "25/09/2016");
            params.add(K.adult, "1");
            params.add(K.child, "0");
            params.add(K.infant, "0");
            params.add(K.trip_type, "one-way");

            FlightApi.post("/citilink/cari-tiket-murah", params,  new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        JSONArray data = response.getJSONArray(K.data);
                        LoadData(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchingSriwijaya() {
        try {
            RequestParams params = new RequestParams();
            params.add(K.airport_depart, "KNO");
            params.add(K.airport_arrive, "CGK");
            params.add(K.date_depart, "04/09/2016");
            params.add(K.date_return, "25/09/2016");
            params.add(K.adult, "1");
            params.add(K.child, "0");
            params.add(K.infant, "0");
            params.add(K.trip_type, "one-way");

            FlightApi.post("/sriwijaya/cari-tiket-murah", params,  new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        JSONArray data = response.getJSONArray(K.data);
                        LoadData(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchingAirasia() {
        try {
            RequestParams params = new RequestParams();
            params.add(K.airport_depart, "KNO");
            params.add(K.airport_arrive, "CGK");
            params.add(K.date_depart, "04/09/2016");
            params.add(K.date_return, "25/09/2016");
            params.add(K.adult, "1");
            params.add(K.child, "0");
            params.add(K.infant, "0");
            params.add(K.trip_type, "one-way");

            FlightApi.post("/airasia/cari-tiket-murah", params,  new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        JSONArray data = response.getJSONArray(K.data);
                        LoadData(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoadData(JSONArray data) {
        flights = new ArrayList<>();
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.airasia))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.garuda))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.airasia))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.garuda))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.garuda))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.airasia))
//                )
//        );
//        flights.add(new FlightiItem()
//                .withImage((ContextCompat.getDrawable(this, R.drawable.garuda))
//                )
//        );

        try {
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj = null;
                obj = (JSONObject) data.get(i);

                Flight flight = new Flight();
                flight.setKode_penerbangan(obj.getString(K.flight_number));
                flight.setMaskapai_name(obj.getString(K.airline_name));
                String price = NumberFormat.getNumberInstance(Locale.getDefault()).format(
                        Double.parseDouble(obj.getString(K.total_fare)));

                flight.setPrice(String.format(S.format_rp, price));
                flight.setHarga(Double.parseDouble(obj.getString(K.total_fare)));
                flight.setWaktu(obj.getString(K.depart_time) +" - "+obj.getString(K.arrival_time));

                Drawable image = (ContextCompat.getDrawable(this, R.drawable.lion));

                String maskapaiName = flight.getMaskapai_name().toLowerCase();
                if (maskapaiName.contains(S.airline_batik)) {
                    image = (ContextCompat.getDrawable(this, R.drawable.batik_air));
                } else if (maskapaiName.contains(S.airline_sriwijaya)) {
                    image = (ContextCompat.getDrawable(this, R.drawable.sriwijaya));
                } else if (maskapaiName.contains(S.airline_airasia)) {
                    image = (ContextCompat.getDrawable(this, R.drawable.airasia));
                } else if (maskapaiName.contains(S.airline_citilink)) {
                    image = (ContextCompat.getDrawable(this, R.drawable.citilink));
                }
                FlightiItem flightiItem = new FlightiItem(flight).withImage(image);
                flights.add(flightiItem);
            }

            Collections.sort(flights, new Comparator<FlightiItem>() {
                public int compare(FlightiItem obj1, FlightiItem obj2)
                {
                    double price1 = obj1.flight.getHarga();
                    double price2 = obj2.flight.getHarga();
                    return (price1 < price2) ? -1: (price1 > price2) ? 1:0 ;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        fastItemAdapter.add(flights);
        fastItemAdapter.notifyDataSetChanged();
    }
}
