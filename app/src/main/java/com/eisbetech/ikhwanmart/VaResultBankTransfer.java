package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Payment;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.freshchat.consumer.sdk.Freshchat;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 8/9/2017.
 *
 */

public class VaResultBankTransfer extends BaseActivity {
    private Bundle bundle;
    private Transaksi transaksi;
    private String trans_id;

    @BindView(R.id.tv_invoice)
    protected TextView tvInvoice;

    @BindView(R.id.totalValue)
    protected TextView TxtTotal;

    @BindView(R.id.ll_bank_code)
    protected LinearLayout llBankCode;

    @BindView(R.id.kodePembayaran)
    protected TextView txtKodePembayaran;

    @BindView(R.id.nama_rekening)
    protected TextView tvNamaRekening;

    @BindView(R.id.iv_bank)
    protected ImageView ivBank;

    @BindView(R.id.v_btn_cara_bayar)
    protected View vBtnCaraBayar;

    @BindView(R.id.btn_cara_bayar)
    protected Button btnCaraBayar;

    @BindView(R.id.btn_confirm)
    protected FancyButton btnConfirm;

    @BindView(R.id.ll_confirm_payment)
    protected LinearLayout llConfirmPayment;

    @BindView(R.id.tv_payment_success)
    protected TextView tvPaymentSuccess;

    @BindView(R.id.tanggal_invoice)
    protected TextView tanggal_invoice;

    @BindView(R.id.tv_hour)
    protected TextView tvHour;

    @BindView(R.id.tv_minute)
    protected TextView tvMinute;

    @BindView(R.id.tv_second)
    protected TextView tvSecond;

    String no_rekening;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.va_result_bank_transfer);
        ButterKnife.bind(this);

        refreshView();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(S.payment + " #" + transaksi.trans_id);
            getSupportActionBar().setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent());
                finish();
            }
        });
    }

    @OnClick(R.id.btn_bantuan)
    public void bantuan() {
        Freshchat.showConversations(this);
    }

    @OnClick(R.id.btn_cara_bayar)
    public void caraBayar() {
        startActivity(new Intent(VaResultBankTransfer.this, CaraBayarBankActivity.class));
    }

    @OnClick(R.id.btn_confirm)
    public void validateConfirm() {
        Drawable img = new IconicsDrawable(this, FontAwesome.Icon.faw_sign_out)
                .sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle(S.confirm_transfer)
                .setMessage(S.make_sure_you_ve_transferred_before_continue_are_you_sure)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirm();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    @OnClick(R.id.btn_copy_rek)
    public void copyRek() {
        copy(S.account_number, txtKodePembayaran.getText().toString());
    }

    @OnClick(R.id.btn_copy_jumlah)
    public void copyJumlah() {
        copy(S.amount, transaksi.harga);
    }

    @OnClick(R.id.tv_invoice)
    public void copyTransactionNo() {
        copy(S.transaction_no, tvInvoice.getText().toString());
    }

    public void copy(String txt, String val) {
        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.
                CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(txt, val);
        clipboard.setPrimaryClip(clip);
        Helper.showMessage(this, txt + S.successfully_copied);
    }

    private void refreshView() {
        bundle = getIntent().getExtras();
        transaksi = bundle.getParcelable(K.transaction);

        if (bundle != null) {
            Log.d("bundle", bundle.getString(K.data));
            try {
                trans_id = transaksi.trans_id;
                JSONObject jsonObject = new JSONObject(bundle.getString(K.data));

                tvInvoice.setText(("00000000" + transaksi.trans_id).substring(transaksi.trans_id.length()));
                TxtTotal.setText(Helper.format_money(transaksi.harga));
                txtKodePembayaran.setText(jsonObject.getString(K.res_pay_code));
                no_rekening = jsonObject.getString(K.res_pay_code);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            int pmId = transaksi.payment_method.id;
            switch (pmId) {
                case I.payment_doku:
                case I.payment_minimarket:
                case I.payment_atm:
                    llConfirmPayment.setVisibility(View.GONE);
                    tvPaymentSuccess.setText(String.format("%s. %s", 3, S.payment_success));
                    break;
                default:
                    llBankCode.setVisibility(View.GONE);
                    txtKodePembayaran.setText(Helper.fromHtml(transaksi.payment_method.rekening));
                    no_rekening = transaksi.payment_method.rekening;
                    tvNamaRekening.setText(Helper.fromHtml(transaksi.payment_method.name));
                    tvPaymentSuccess.setText(String.format("%s. %s", 4, S.payment_success));
                    vBtnCaraBayar.setVisibility(View.GONE);
                    btnCaraBayar.setVisibility(View.GONE);

                    String pmBank = transaksi.payment_method.bank;
                    if (pmBank.contains(S.bca)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bca));
                    } else if (pmBank.contains(S.bni)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bni));
                    } else if (pmBank.contains(S.bri)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bri));
                    } else if (pmBank.contains(S.mandiri_syariah)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bsm));
                    } else if (pmBank.contains(S.mandiri)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_mandiri));
                    }else if (pmBank.contains(S.cimb)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_cimb));
                    }
                    break;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = formatter.parse(transaksi.expired_date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm");
                tanggal_invoice.setText(format2.format(calendar.getTime()) + " WIB");
                tanggal_invoice.setVisibility(View.VISIBLE);

                long now = Calendar.getInstance().getTimeInMillis();
                new CountDownTimer(calendar.getTimeInMillis() - now, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                        long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                        long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                        tvHour.setText(String.format("%02d", hours));
                        tvMinute.setText(String.format("%02d", minutes));
                        tvSecond.setText(String.format("%02d", seconds));
                        //tvCountDown.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
                    }
                    public void onFinish() {
                        Helper.showMessage(getApplicationContext(),"Waktu transfer anda sudah habis.");
                        finish();
                    }
                }.start();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void confirm() {
        btnConfirm.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.trans_id, trans_id);

            BisatopupApi.post("/transaksi/confirm/" + trans_id, params, this,
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();
                            Helper.showMessage(getApplicationContext(), throwable.getMessage());
                            btnConfirm.setEnabled(true);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers,
                                              final String responseString) {
                            progressDialog.dismiss();
                            btnConfirm.setEnabled(true);
                            try {
                                JSONObject object = new JSONObject(responseString);

                                if (!object.getBoolean(K.error)) {
                                    getDetail(true);
                                } else {
                                    Helper.showMessage(getApplicationContext(),
                                            object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDetail(final boolean isShowLoading) {
        final ProgressDialog progressDialog = new ProgressDialog(this);

        if (isShowLoading) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(S.loading_detail);
            progressDialog.show();
        }

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.trans_id, trans_id);

            BisatopupApi.post("/transaksi/detail-mobile/" + trans_id, params, this,
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            if (isShowLoading) {
                                progressDialog.dismiss();
                            }
                            Helper.showMessage(getApplicationContext(), throwable.getMessage());

                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getApplicationContext(),
                                            object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers,
                                              final String responseString) {
                            if (isShowLoading) progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);

                                boolean error = object.getBoolean(K.error);

                                if (!error) {
                                    JSONObject obj = object.getJSONObject(K.transaction);

                                    if (transaksi == null) {
                                        transaksi = new Transaksi();
                                    }

                                    String status = obj.getString(K.status);
                                    transaksi.trans_id = obj.getString(K.trans_id);
                                    transaksi.product_id = obj.getInt(K.product_id);
                                    transaksi.is_tagihan = obj.getInt(K.is_tagihan);

                                    transaksi.tanggal = obj.getString(K.date);
                                    transaksi.time = obj.getString(K.time);
                                    transaksi.harga = obj.getString(K.harga);
                                    transaksi.base_price = obj.getString(K.base_price);
                                    transaksi.product = obj.getString(K.product);
                                    transaksi.product_name = obj.getString(K.product_name);
                                    transaksi.product_detail = obj.getString(K.product_detail);
                                    transaksi.no_pelanggan = obj.getString(K.customer_no);
                                    transaksi.expired_date = obj.getString(K.expired_date);
                                    transaksi.note = obj.getString(K.note);

                                    transaksi.token = obj.getString(K.token);
                                    transaksi.payment = obj.getString(K.payment);
                                    transaksi.status_id = obj.getInt(K.status_id);
                                    transaksi.kode_unik = obj.getString(K.unique_code);

                                    JSONObject pay_ojb = obj.getJSONObject(K.pembayaran);
                                    Payment payment = new Payment();
                                    payment.rekening = pay_ojb.getString(K.rekening);
                                    payment.name = pay_ojb.getString(K.name);
                                    payment.bank = pay_ojb.getString(K.bank);

                                    transaksi.payment_method = payment;
                                    if (!status.equals(transaksi.status)) {
                                        transaksi.status = obj.getString(K.status);
                                        transaksi.status_id = obj.getInt(K.status_id);

                                        if (transaksi.status_id == 4) {
                                            Drawable img = new IconicsDrawable(getApplicationContext(),
                                                    FontAwesome.Icon.faw_check_circle).sizeDp(32)
                                                    .color(Color.WHITE);

                                            new LovelyInfoDialog(getApplicationContext())
                                                    .setTopColorRes(R.color.primary_dark)
                                                    .setIcon(img)
                                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                                    .setTitle(S.transaction_success)
                                                    .setMessage(S.congrats_ur_transaction_has_been_successfull)
                                                    .show();
                                        }
                                        finish();
                                    }
                                } else {
                                    Helper.showMessage(getApplicationContext(),
                                            object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
