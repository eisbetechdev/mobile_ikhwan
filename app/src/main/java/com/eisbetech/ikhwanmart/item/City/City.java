package com.eisbetech.ikhwanmart.item.City;

import com.google.gson.annotations.SerializedName;

public class City{

	@SerializedName("rajaongkir")
	private Rajaongkir rajaongkir;

	public void setRajaongkir(Rajaongkir rajaongkir){
		this.rajaongkir = rajaongkir;
	}

	public Rajaongkir getRajaongkir(){
		return rajaongkir;
	}

	@Override
 	public String toString(){
		return 
			"City{" + 
			"rajaongkir = '" + rajaongkir + '\'' + 
			"}";
		}
}