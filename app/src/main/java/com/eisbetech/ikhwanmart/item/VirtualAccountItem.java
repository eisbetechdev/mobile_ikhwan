package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.LoadWebActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.VirtualAccount;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

public class VirtualAccountItem extends AbstractItem<VirtualAccountItem, VirtualAccountItem.ViewHolder> {
    public VirtualAccount virtualAccount;

    public VirtualAccountItem(VirtualAccount virtualAccount) {
        this.virtualAccount = virtualAccount;
    }



    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.va_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.virtual_account_item;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();

        holder.txt_header.setText(virtualAccount.nama_bank);

        if(virtualAccount.image_url !=null){
            GlideApp.with(ctx)
                    .load(virtualAccount.image_url)
                    .placeholder(R.drawable.android_launcher)
                    .into(holder.image);
        }else {
            GlideApp.with(ctx).clear(holder.image);
        }

        holder.txt_va.setText(virtualAccount.virtual_account);
        holder.txt_info.setText(String.format("Biaya Layanan : %s", Helper.format_money(virtualAccount.admin)));


        holder.btn_copy_rek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.copy("Va berhasil dicopy, silahkan transfer nominal yang diinginkan minimal Rp 20.000",virtualAccount.virtual_account,ctx);
            }
        });

        holder.btn_cara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx,LoadWebActivity.class);
                intent.putExtra("url","https://ikhwan.eisbetech.com/mobile/va/"+virtualAccount.payment_id+"?kode_bayar="+virtualAccount.virtual_account+"&type=open");

                ctx.startActivity(intent);
            }
        });
    }


    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_info)
        protected TextView txt_info;

        @BindView(R.id.txt_header)
        protected TextView txt_header;

        @BindView(R.id.image)
        protected ImageView image;

        @BindView(R.id.txt_va)
        protected TextView txt_va;

        @BindView(R.id.btn_copy_rek)
        protected FancyButton btn_copy_rek;

        @BindView(R.id.btn_cara)
        protected FancyButton btn_cara;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }


    }
}
