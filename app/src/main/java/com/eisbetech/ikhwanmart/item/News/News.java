package com.eisbetech.ikhwanmart.item.News;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class News implements Parcelable {

	@SerializedName("next_page")
	private String nextPage;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("success")
	private boolean success;

	public void setNextPage(String nextPage){
		this.nextPage = nextPage;
	}

	public String getNextPage(){
		return nextPage;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"News{" +
			"next_page = '" + nextPage + '\'' +
			"data = '" + data + '\'' +
			",success = '" + success + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.nextPage);
		dest.writeList(this.data);
		dest.writeByte(this.success ? (byte) 1 : (byte) 0);
	}

	public News() {
	}

	protected News(Parcel in) {
		this.nextPage = in.readString();
		this.data = new ArrayList<DataItem>();
		in.readList(this.data, DataItem.class.getClassLoader());
		this.success = in.readByte() != 0;
	}

	public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
		@Override
		public News createFromParcel(Parcel source) {
			return new News(source);
		}

		@Override
		public News[] newArray(int size) {
			return new News[size];
		}
	};
}