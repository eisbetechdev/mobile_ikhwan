package com.eisbetech.ikhwanmart.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.TransaksiDownline;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 6/24/2017.
 */

public class TransaksiDownlineItem extends AbstractItem<TransaksiDownlineItem, TransaksiDownlineItem.ViewHolder> {

    public TransaksiDownline transaksiDownline;

    public TransaksiDownlineItem(TransaksiDownline transaksiDownline) {
        this.transaksiDownline = transaksiDownline;
    }

    @Override
    public TransaksiDownlineItem.ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.transaksi_dowline_item;
    }

    @Override
    public int getLayoutRes() {
         return R.layout.downline_transaksi_item;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.txt_title.setText(transaksiDownline.note);
        holder.txt_nominal.setText(Helper.format_money(transaksiDownline.jumlah));
        holder.txt_tanggal.setText(Helper.format_tanggal(transaksiDownline.tanggal));

    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_title)
        protected TextView txt_title;

        @BindView(R.id.txt_nominal)
        protected TextView txt_nominal;

        @BindView(R.id.txt_tanggal)
        protected TextView txt_tanggal;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
