package com.eisbetech.ikhwanmart.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.eisbetech.ikhwanmart.NomorItem;
import com.mikepenz.fastadapter.items.AbstractItem;

/**
 * Created by khadafi on 10/19/2016.
 */

public class ItemSample extends AbstractItem<NomorItem, ItemSample.ViewHolder> {

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return 0;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }
}
