package com.eisbetech.ikhwanmart.item.Withdraw;

import com.google.gson.annotations.SerializedName;

public class WithItem {

	@SerializedName("bank_username")
	private String bankUsername;

	@SerializedName("nominal")
	private int nominal;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("bank_account")
	private String bankAccount;

	@SerializedName("status")
	private String status;

	public void setBankUsername(String bankUsername){
		this.bankUsername = bankUsername;
	}

	public String getBankUsername(){
		return bankUsername;
	}

	public void setNominal(int nominal){
		this.nominal = nominal;
	}

	public int getNominal(){
		return nominal;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setBankName(String bankName){
		this.bankName = bankName;
	}

	public String getBankName(){
		return bankName;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}

	public String getBankAccount(){
		return bankAccount;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"WithItem{" +
			"bank_username = '" + bankUsername + '\'' + 
			",nominal = '" + nominal + '\'' + 
			",user_id = '" + userId + '\'' + 
			",bank_name = '" + bankName + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",bank_account = '" + bankAccount + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}