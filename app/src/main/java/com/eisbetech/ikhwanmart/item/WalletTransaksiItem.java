package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.WalletTransaksi;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 */
public class WalletTransaksiItem extends AbstractItem<WalletTransaksiItem, WalletTransaksiItem.ViewHolder> {
    public WalletTransaksi walletTransaksi;

    public WalletTransaksiItem(WalletTransaksi walletTransaksi) {
        this.walletTransaksi = walletTransaksi;
    }

    @Override
    public int getType() {
        return R.id.wallet_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.wallet_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();

        holder.txt_title.setText(walletTransaksi.title);
        holder.txt_nominal.setText(walletTransaksi.nominal);


        try {
            holder.txt_tanggal.setText(Helper.getTimeAgo(walletTransaksi.tanggal));

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = formatter.parse(walletTransaksi.tanggal);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
//                calendar.add(Calendar.HOUR, 3);
                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm");
                holder.txt_tanggal.setText(format2.format(calendar.getTime()));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        try{
            if (Double.parseDouble(walletTransaksi.nominal)<0) {
                holder.txt_nominal.setTextColor(ctx.getResources().getColor(R.color.md_red_400));
            } else {
                holder.txt_nominal.setTextColor(ctx.getResources().getColor(R.color.primary));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        holder.txt_sisa_saldo.setText(Helper.fromHtml("Sebelum : "+Helper.format_money(walletTransaksi.saldo_sebelum)+", Setelah : "+Helper.format_money(walletTransaksi.sisa_saldo)));
    }
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_title)
        protected TextView txt_title;

        @BindView(R.id.txt_nominal)
        protected TextView txt_nominal;

        @BindView(R.id.txt_tanggal)
        protected TextView txt_tanggal;

        @BindView(R.id.txt_sisa_saldo)
        protected TextView txt_sisa_saldo;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }


}
