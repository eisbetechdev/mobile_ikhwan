package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.database.Message;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 8/25/2016.
 */
public class MessageItem extends AbstractItem<MessageItem, MessageItem.ViewHolder> {
    public Message message;

    public MessageItem(Message message) {
        this.message = message;
    }

    @Override
    public int getType() {
        return R.id.message_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.message_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();

        holder.txt_message_title.setText(Helper.fromHtml(message.title));

        if (!message.is_read) {
           // holder.txt_message_body.setTypeface(null, Typeface.BOLD);
            holder.txt_message_body.setText(Helper.fromHtml(message.content));

        } else {
            holder.txt_message_body.setText(Helper.fromHtml(message.content));
        }

        if (message.date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = formatter.parse(message.date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
//                calendar.add(Calendar.HOUR, 3);
                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm");
                holder.txt_date.setText(format2.format(calendar.getTime()));
                holder.layout_date.setVisibility(View.VISIBLE);

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            holder.layout_date.setVisibility(View.GONE);
        }
        holder.image.setVisibility(View.GONE);
        GlideApp.with(ctx).clear(holder.image);

        if(message.image_url != null && !message.image_url.isEmpty()){
            holder.image.setVisibility(View.VISIBLE);

            GlideApp.with(ctx)
                    .load(message.image_url)
                    .fitCenter()
                    .into(holder.image);
        }


        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Drawable img = new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_warning).sizeDp(32).color(Color.WHITE);

                new LovelyStandardDialog(ctx)
                        .setTopColorRes(R.color.accent)
                        .setButtonsColorRes(R.color.accent)
                        .setIcon(img)
                        .setTitle("Delete Data")
                        .setMessage("Apakah anda yakin akan mendelete data ini?")
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Delete(ctx);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();


            }
        });
    }


    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    public void SetRead() {
       try {
           Realm realm = Realm.getDefaultInstance();

           Message results = realm.where(Message.class)
                   .equalTo("id", message.id)
                   .findFirst();

           realm.beginTransaction();
           if (results != null) {
               results.is_read = true;

               realm.copyToRealmOrUpdate(results);
           }

           realm.commitTransaction();

       }catch (Exception e) {
           e.printStackTrace();
       }

    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_message_title)
        protected TextView txt_message_title;

        @BindView(R.id.txt_message_body)
        protected TextView txt_message_body;

        @BindView(R.id.txt_date)
        protected TextView txt_date;


        @BindView(R.id.layout_date)
        protected LinearLayout layout_date;


        @BindView(R.id.image)
        protected ImageView image;

        @BindView(R.id.btn_delete)
        protected FancyButton btn_delete;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }


    private void Delete(final Context ctx) {
        try {
            Realm realm = Realm.getDefaultInstance();

            RealmResults<Message> results = realm.where(Message.class)
                    .equalTo("id", message.id)
                    .findAll();

            realm.beginTransaction();
            results.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
