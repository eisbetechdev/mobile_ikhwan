package com.eisbetech.ikhwanmart.item;

import android.os.Parcel;
import android.os.Parcelable;

public class CartItem implements Parcelable {
    long cart_item_id;
    Produk cart_item_produk;
    int cart_item_qnt;

    public long getCart_item_id() {
        return cart_item_id;
    }

    public void setCart_item_id(long cart_item_id) {
        this.cart_item_id = cart_item_id;
    }

    public Produk getCart_item_produk() {
        return cart_item_produk;
    }

    public void setCart_item_produk(Produk cart_item_produk) {
        this.cart_item_produk = cart_item_produk;
    }

    public int getCart_item_qnt() {
        return cart_item_qnt;
    }

    public void setCart_item_qnt(int cart_item_qnt) {
        this.cart_item_qnt = cart_item_qnt;
    }

    public CartItem(){}

    public CartItem(long cart_item_id, Produk cart_item_produk, int cart_item_qnt) {
        this.cart_item_id = cart_item_id;
        this.cart_item_produk = cart_item_produk;
        this.cart_item_qnt = cart_item_qnt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.cart_item_id);
        dest.writeParcelable(this.cart_item_produk, flags);
        dest.writeInt(this.cart_item_qnt);
    }

    protected CartItem(Parcel in) {
        this.cart_item_id = in.readLong();
        this.cart_item_produk = in.readParcelable(Produk.class.getClassLoader());
        this.cart_item_qnt = in.readInt();
    }

    public static final Creator<CartItem> CREATOR = new Creator<CartItem>() {
        @Override
        public CartItem createFromParcel(Parcel source) {
            return new CartItem(source);
        }

        @Override
        public CartItem[] newArray(int size) {
            return new CartItem[size];
        }
    };
}
