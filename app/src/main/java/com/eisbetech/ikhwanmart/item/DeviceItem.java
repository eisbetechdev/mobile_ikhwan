package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.Device;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by khadafi on 8/25/2016.
 */
public class DeviceItem extends AbstractItem<DeviceItem, DeviceItem.ViewHolder> {
    public Device device;

    public DeviceItem(Device device) {
        this.device = device;
    }

    @Override
    public int getType() {
        return R.id.device_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.device_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();

        holder.txt_title.setText(device.name);
        holder.txt_subtitle.setText(device.address);

//        holder.image.setImageResource(R.drawable.logo_bisa_2);
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_title)
        protected TextView txt_title;

        @BindView(R.id.txt_subtitle)
        protected TextView txt_subtitle;

        @BindView(R.id.image)
        protected CircleImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }


}
