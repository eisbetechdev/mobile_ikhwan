package com.eisbetech.ikhwanmart.item;

import com.eisbetech.ikhwanmart.database.RealmController;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Rekening extends RealmObject {
    @PrimaryKey
    public long id;

    public String nama;
    public String bank;
    public String nomer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNomer() {
        return nomer;
    }

    public void setNomer(String nomer) {
        this.nomer = nomer;
    }

    public static void createRekening(final String nama, final String bank, final String nomer) {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<Rekening> results = realm.where(Rekening.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
                Rekening rekening = realm.createObject(Rekening.class);
                rekening.nama = nama;
                rekening.bank = bank;
                rekening.nomer = nomer;
            }
        });
    }

    public static void ClearRekening() {
        Realm realm = RealmController.getInstance().getRealm();
        final RealmResults<Rekening> results = realm.where(Rekening.class).findAll();

        // All changes to data must happen in a transaction
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Delete all matches
                results.deleteAllFromRealm();
            }
        });
    }
}
