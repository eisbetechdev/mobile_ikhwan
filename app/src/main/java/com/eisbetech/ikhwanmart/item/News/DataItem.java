package com.eisbetech.ikhwanmart.item.News;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem implements Parcelable {

	@SerializedName("cover")
	private String cover;

	@SerializedName("read")
	private int read;

	@SerializedName("post_date")
	private String postDate;

	@SerializedName("subtitle")
	private String subtitle;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private String id;

	@SerializedName("title")
	private String title;

	@SerializedName("category")
	private String category;

	@SerializedName("seen")
	private int seen;

	public void setCover(String cover){
		this.cover = cover;
	}

	public String getCover(){
		return cover;
	}

	public void setRead(int read){
		this.read = read;
	}

	public int getRead(){
		return read;
	}

	public void setPostDate(String postDate){
		this.postDate = postDate;
	}

	public String getPostDate(){
		return postDate;
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
	}

	public String getSubtitle(){
		return subtitle;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setSeen(int seen){
		this.seen = seen;
	}

	public int getSeen(){
		return seen;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"cover = '" + cover + '\'' + 
			",read = '" + read + '\'' + 
			",post_date = '" + postDate + '\'' + 
			",subtitle = '" + subtitle + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			",category = '" + category + '\'' + 
			",seen = '" + seen + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.cover);
		dest.writeInt(this.read);
		dest.writeString(this.postDate);
		dest.writeString(this.subtitle);
		dest.writeString(this.description);
		dest.writeString(this.id);
		dest.writeString(this.title);
		dest.writeString(this.category);
		dest.writeInt(this.seen);
	}

	public DataItem() {
	}

	protected DataItem(Parcel in) {
		this.cover = in.readString();
		this.read = in.readInt();
		this.postDate = in.readString();
		this.subtitle = in.readString();
		this.description = in.readString();
		this.id = in.readString();
		this.title = in.readString();
		this.category = in.readString();
		this.seen = in.readInt();
	}

	public static final Parcelable.Creator<DataItem> CREATOR = new Parcelable.Creator<DataItem>() {
		@Override
		public DataItem createFromParcel(Parcel source) {
			return new DataItem(source);
		}

		@Override
		public DataItem[] newArray(int size) {
			return new DataItem[size];
		}
	};
}