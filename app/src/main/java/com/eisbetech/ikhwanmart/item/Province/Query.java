package com.eisbetech.ikhwanmart.item.Province;

import com.google.gson.annotations.SerializedName;

public class Query{

	@SerializedName("key")
	private String key;

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}

	@Override
 	public String toString(){
		return 
			"Query{" + 
			"key = '" + key + '\'' + 
			"}";
		}
}