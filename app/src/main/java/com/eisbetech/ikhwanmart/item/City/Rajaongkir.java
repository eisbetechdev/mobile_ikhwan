package com.eisbetech.ikhwanmart.item.City;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Rajaongkir{

	@SerializedName("query")
	private Query query;

	@SerializedName("results")
	private List<CityItem> results;

	@SerializedName("status")
	private Status status;

	public void setQuery(Query query){
		this.query = query;
	}

	public Query getQuery(){
		return query;
	}

	public void setResults(List<CityItem> results){
		this.results = results;
	}

	public List<CityItem> getResults(){
		return results;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Rajaongkir{" + 
			"query = '" + query + '\'' + 
			",results = '" + results + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}