package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.Downline;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by khadafi on 6/24/2017.
 */

public class DownlineItem extends AbstractItem<DownlineItem, DownlineItem.ViewHolder> {


    public Downline downline;

    public DownlineItem(Downline downline) {
        this.downline = downline;
    }

    @Override
    public DownlineItem.ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        final Context ctx = holder.itemView.getContext();
        holder.txt_nama.setText(downline.nama);
        holder.txt_phone_number.setText(downline.no_hp);

        if (!downline.profile_url.isEmpty() && downline.profile_url != null && !downline.equals("null")) {
//            Glide.clear(holder.image);
            GlideApp.with(ctx).clear(holder.image);
//            Glide.with(ctx).load(downline.profile_url)
//                    .placeholder(R.drawable.android_launcher)
//                    .into(holder.image);

            GlideApp.with(ctx)
                    .load(downline.profile_url)
                    .placeholder(R.drawable.android_launcher)
                    .into(holder.image);

        } else {
//            Glide.clear(holder.image);
            GlideApp.with(ctx).clear(holder.image);
        }
    }

    @Override
    public int getType() {
        return R.id.downline_item;
    }

    @Override
    public int getLayoutRes() {
         return R.layout.downline_item;
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected CircleImageView image;


        @BindView(R.id.txt_nama)
        protected TextView txt_nama;

        @BindView(R.id.txt_phone_number)
        protected TextView txt_phone_number;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
