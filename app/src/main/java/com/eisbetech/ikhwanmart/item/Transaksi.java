package com.eisbetech.ikhwanmart.item;

import java.util.List;

public class Transaksi {
    long transaction_id;
    long transaction_date;
    long transaction_total;
    String transaction_status;
    long transaction_range_cost;
    List<CartItem> transaction_cart;

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public long getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(long transaction_date) {
        this.transaction_date = transaction_date;
    }

    public long getTransaction_total() {
        return transaction_total;
    }

    public void setTransaction_total(long transaction_total) {
        this.transaction_total = transaction_total;
    }

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public long getTransaction_range_cost() {
        return transaction_range_cost;
    }

    public void setTransaction_range_cost(long transaction_range_cost) {
        this.transaction_range_cost = transaction_range_cost;
    }

    public List<CartItem> getTransaction_cart() {
        return transaction_cart;
    }

    public void setTransaction_cart(List<CartItem> transaction_cart) {
        this.transaction_cart = transaction_cart;
    }

    public Transaksi(){}

    public Transaksi(long transaction_id, long transaction_date, long transaction_total, String transaction_status, long transaction_range_cost, List<CartItem> transaction_cart) {
        this.transaction_id = transaction_id;
        this.transaction_date = transaction_date;
        this.transaction_total = transaction_total;
        this.transaction_status = transaction_status;
        this.transaction_range_cost = transaction_range_cost;
        this.transaction_cart = transaction_cart;
    }
}
