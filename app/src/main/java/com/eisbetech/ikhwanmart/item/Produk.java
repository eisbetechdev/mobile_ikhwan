package com.eisbetech.ikhwanmart.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Produk implements Parcelable {
    long id;
    Kategori kategori;
    String nama;
    long harga;
    long favorite;
    String berat;
    String deskripsi;
    long stok;
    List<String> gambar;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        kategori = kategori;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

    public long getFavorite() {
        return favorite;
    }

    public void setFavorite(long favorite) {
        this.favorite = favorite;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public long getStok() {
        return stok;
    }

    public void setStok(long stok) {
        this.stok = stok;
    }

    public List<String> getGambar() {
        return gambar;
    }

    public void setGambar(List<String> gambar) {
        this.gambar = gambar;
    }

    public Produk(){}

    public Produk(long id, Kategori kategori, String nama, long harga, long favorite, String berat, String deskripsi, long stok, List<String> gambar) {
        this.id = id;
        this.kategori = kategori;
        this.nama = nama;
        this.harga = harga;
        this.favorite = favorite;
        this.berat = berat;
        this.deskripsi = deskripsi;
        this.stok = stok;
        this.gambar = gambar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeParcelable(this.kategori, flags);
        dest.writeString(this.nama);
        dest.writeLong(this.harga);
        dest.writeLong(this.favorite);
        dest.writeString(this.berat);
        dest.writeString(this.deskripsi);
        dest.writeLong(this.stok);
        dest.writeStringList(this.gambar);
    }

    protected Produk(Parcel in) {
        this.id = in.readLong();
        this.kategori = in.readParcelable(Kategori.class.getClassLoader());
        this.nama = in.readString();
        this.harga = in.readLong();
        this.favorite = in.readLong();
        this.berat = in.readString();
        this.deskripsi = in.readString();
        this.stok = in.readLong();
        this.gambar = in.createStringArrayList();
    }

    public static final Creator<Produk> CREATOR = new Creator<Produk>() {
        @Override
        public Produk createFromParcel(Parcel source) {
            return new Produk(source);
        }

        @Override
        public Produk[] newArray(int size) {
            return new Produk[size];
        }
    };
}
