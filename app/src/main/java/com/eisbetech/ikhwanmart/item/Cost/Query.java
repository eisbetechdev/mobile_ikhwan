package com.eisbetech.ikhwanmart.item.Cost;

import com.google.gson.annotations.SerializedName;

public class Query{

	@SerializedName("courier")
	private String courier;

	@SerializedName("origin")
	private String origin;

	@SerializedName("destination")
	private String destination;

	@SerializedName("weight")
	private int weight;

	@SerializedName("key")
	private String key;

	public void setCourier(String courier){
		this.courier = courier;
	}

	public String getCourier(){
		return courier;
	}

	public void setOrigin(String origin){
		this.origin = origin;
	}

	public String getOrigin(){
		return origin;
	}

	public void setDestination(String destination){
		this.destination = destination;
	}

	public String getDestination(){
		return destination;
	}

	public void setWeight(int weight){
		this.weight = weight;
	}

	public int getWeight(){
		return weight;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}

	@Override
 	public String toString(){
		return 
			"Query{" + 
			"courier = '" + courier + '\'' + 
			",origin = '" + origin + '\'' + 
			",destination = '" + destination + '\'' + 
			",weight = '" + weight + '\'' + 
			",key = '" + key + '\'' + 
			"}";
		}
}