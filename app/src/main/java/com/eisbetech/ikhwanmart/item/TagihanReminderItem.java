package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.TagihanReminder;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 */
public class TagihanReminderItem extends AbstractItem<TagihanReminderItem, TagihanReminderItem.ViewHolder> {
    public TagihanReminder tagihanReminder;

    public TagihanReminderItem(TagihanReminder tagihanReminder) {
        this.tagihanReminder = tagihanReminder;
    }

    @Override
    public int getType() {
        return R.id.tag_reminder_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.pengingat_tagihan_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        Context ctx = holder.itemView.getContext();

        if (tagihanReminder.is_enable == 1 ) {
            holder.txt_status.setText("On");
            holder.txt_status.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txt_status.setBackgroundColor(Color.parseColor("#1BA39C"));
        } else {
            holder.txt_status.setText("Off");
            holder.txt_status.setTextColor(Color.parseColor("#FFFFFF"));
            holder.txt_status.setBackgroundColor(Color.parseColor("#E7505A"));
        }
        holder.txt_title.setText(tagihanReminder.product_parent +"-"+ tagihanReminder.product);
        holder.txt_nomor_rek.setText(tagihanReminder.nomor_rekening);
        holder.txt_tgl.setText(Helper.fromHtml("Tagihan akan di cek pada tanggal <b>"+tagihanReminder.tanggal_reminder + "</b> Setiap bulannya"));
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;
        @BindView(R.id.txt_title)
        protected TextView txt_title;

        @BindView(R.id.txt_nomor_rek)
        protected TextView txt_nomor_rek;

        @BindView(R.id.txt_tgl)
        protected TextView txt_tgl;

        @BindView(R.id.txt_status)
        protected TextView txt_status;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
