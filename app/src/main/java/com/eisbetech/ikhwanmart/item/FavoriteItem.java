package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.Favorite;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 8/25/2016.
 */
public class FavoriteItem extends AbstractItem<FavoriteItem, FavoriteItem.ViewHolder> {
    public Favorite favorite;

    public FavoriteItem(Favorite favorite) {
        this.favorite = favorite;
    }

    @Override
    public int getType() {
        return R.id.favorite_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.favorite_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        final Context ctx = holder.itemView.getContext();

        String product = "";

        holder.txt_nama.setText(favorite.nama );
        holder.txt_phone_number.setText(favorite.phone_number);

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Drawable img = new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_warning).sizeDp(32).color(Color.WHITE);

                new LovelyStandardDialog(ctx)
                        .setTopColorRes(R.color.accent)
                        .setButtonsColorRes(R.color.accent)
                        .setIcon(img)
                        .setTitle("Delete Data Favorit")
                        .setMessage("Apakah anda yakin akan mendelete data  ini?")
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Delete(ctx);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();


            }
        });
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    private void Delete(final Context ctx) {

        delete(favorite,ctx);

        Realm realm = Realm.getDefaultInstance();

        RealmResults<Favorite> results = realm.where(Favorite.class)
                .equalTo("id", favorite.id)
                .findAll();

        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();


    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.txt_nama)
        protected TextView txt_nama;

        @BindView(R.id.txt_phone_number)
        protected TextView txt_phone_number;

        @BindView(R.id.btn_delete)
        protected FancyButton btn_delete;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
    private void delete(final Favorite favorite,final Context ctx) {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("nama", favorite.nama);
            params.put("nomor_hp", favorite.phone_number);
            params.put("kategori", favorite.category);

            BisatopupApi.post("/favorite/delete", params, ctx, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
