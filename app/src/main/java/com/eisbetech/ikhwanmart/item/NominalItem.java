package com.eisbetech.ikhwanmart.item;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.Nominal;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by khadafi on 8/25/2016.
 */
public class NominalItem extends AbstractItem<NominalItem, NominalItem.ViewHolder> {
    public Nominal nominal;

    public NominalItem(Nominal nominal) {
        this.nominal = nominal;
    }

    @Override
    public int getType() {
        return R.id.nominal_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.nominal_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        Context ctx = holder.itemView.getContext();

        holder.txt_price.setText(Helper.format_money(nominal.price));
        holder.txt_product.setText(nominal.product_name);

        if(nominal.desc !=null&&  !nominal.desc.isEmpty() && !nominal.desc.equals("null")){
            holder.txt_desc.setText(Helper.fromHtml(nominal.desc));
            holder.txt_desc.setVisibility(View.VISIBLE);
        }else{
            holder.txt_desc.setVisibility(View.GONE);
        }
        if(nominal.is_gangguan == 1){
            holder.parent_layout.setBackgroundColor(ctx.getResources().getColor(R.color.grey_disable));
            holder.txt_price.setTextColor(ctx.getResources().getColor(R.color.grey));
            holder.txt_price.setBackgroundColor(ctx.getResources().getColor(R.color.grey_disable));
            holder.txt_gangguan.setVisibility(View.VISIBLE);
        }else{
            // Get selectable background
            TypedValue typedValue = new TypedValue();
            ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);

            holder.txt_gangguan.setVisibility(View.GONE);
            holder.parent_layout.setBackgroundResource(typedValue.resourceId);
            holder.parent_layout.setBackgroundColor(ctx.getResources().getColor(R.color.white));
            holder.txt_price.setTextColor(ctx.getResources().getColor(R.color.colorPrimary));
            holder.txt_price.setBackgroundColor(ctx.getResources().getColor(R.color.white));

        }

        if(nominal.is_ubah_harga){
            holder.txt_harga.setText(nominal.price);
            holder.layout_harga.setVisibility(View.VISIBLE);
        }else{
            holder.layout_harga.setVisibility(View.GONE);
        }
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.parent_layout)
        protected CardView parent_layout;

        @BindView(R.id.image)
        protected CircleImageView image;

        @BindView(R.id.txt_product)
        protected TextView txt_product;

        @BindView(R.id.txt_harga)
        protected TextView txt_harga;

        @BindView(R.id.layout_harga)
        protected LinearLayout layout_harga;

        @BindView(R.id.txt_desc)
        protected TextView txt_desc;

        @BindView(R.id.txt_price)
        protected TextView txt_price;

        @BindView(R.id.txt_gangguan)
        protected TextView txt_gangguan;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
