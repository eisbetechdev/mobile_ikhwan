package com.eisbetech.ikhwanmart.item.Withdraw;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Withdraw{

	@SerializedName("data")
	private List<WithItem> data;

	@SerializedName("status")
	private boolean status;

	public void setData(List<WithItem> data){
		this.data = data;
	}

	public List<WithItem> getData(){
		return data;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Withdraw{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}