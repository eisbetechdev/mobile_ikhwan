package com.eisbetech.ikhwanmart.item.Cost;

import com.google.gson.annotations.SerializedName;

public class Cost{

	@SerializedName("rajaongkir")
	private Rajaongkir rajaongkir;

	public void setRajaongkir(Rajaongkir rajaongkir){
		this.rajaongkir = rajaongkir;
	}

	public Rajaongkir getRajaongkir(){
		return rajaongkir;
	}

	@Override
 	public String toString(){
		return 
			"Cost{" + 
			"rajaongkir = '" + rajaongkir + '\'' + 
			"}";
		}
}