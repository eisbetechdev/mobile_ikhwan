package com.eisbetech.ikhwanmart.item.City;

import com.google.gson.annotations.SerializedName;

public class Query{

	@SerializedName("province")
	private String province;

	@SerializedName("key")
	private String key;

	public void setProvince(String province){
		this.province = province;
	}

	public String getProvince(){
		return province;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}

	@Override
 	public String toString(){
		return 
			"Query{" + 
			"province = '" + province + '\'' + 
			",key = '" + key + '\'' + 
			"}";
		}
}