package com.eisbetech.ikhwanmart;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.eisbetech.ikhwanmart.fragment.FragmentMedia;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import butterknife.ButterKnife;

public class BisaContentActivity extends AppCompatActivity {
    String action = "";
    Fragment fr = null;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bisa_content);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        action = intent.getAction();

        fr = new FragmentMedia();
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.app_name);

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT);
            // layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL;
            View cView = getLayoutInflater().inflate(R.layout.action_bar_bisa, null);
            actionBar.setCustomView(cView, layoutParams);
//            actionBar.setTitle(intent.getStringExtra("title"));
        }

        fragmentManager = getFragmentManager();

        Bundle bundle = intent.getExtras();
        fr.setArguments(bundle);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fr);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case 1:
                Intent intent = new Intent(BisaContentActivity.this, ViewWeb.class);
                intent.putExtra("title", "Website Ikhwan Mart");
                intent.putExtra("url", "http://ikhwan.co.id");
                startActivity(intent);
                break;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        Drawable icon = new IconicsDrawable(this, GoogleMaterial.Icon.gmd_info_outline)
                .color(Color.parseColor("#00544a")).sizeDp(20);
        menu.add(0, 1, android.view.Menu.NONE, "info").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }


}
