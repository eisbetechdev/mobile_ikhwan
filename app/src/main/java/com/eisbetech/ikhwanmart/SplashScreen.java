package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

public class SplashScreen extends BaseActivity {
    SessionManager session;

    HashMap<String, String> user;

    @BindView(R.id.layout_loading)
    protected LinearLayout layout_loading;


    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        session = new SessionManager(getApplicationContext(),this);
        user = session.getUserDetails();
        layout_loading.setVisibility(View.INVISIBLE);

        String url2 = mFirebaseRemoteConfig.getString("URL_API");



        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
// will use fetch data from the Remote Config service, rather than cached parameter values,
// if cached parameter values are more than cacheExpiration seconds old.
// See Best Practices in the README for more information.
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
//                            Toast.makeText(SplashScreen.this, "Fetch Succeeded",
//                                    Toast.LENGTH_SHORT).show();

                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();

                        } else {
                            Toast.makeText(SplashScreen.this, "Data Failed",
                                    Toast.LENGTH_LONG).show();
                        }
                        //displayWelcomeMessage();
                    }
                });
//        AccountKit.logOut();
        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
//                AccessToken accessToken = AccountKit.getCurrentAccessToken();
//
//
//                if (accessToken != null) {
//                    //Handle Returning User
//                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
//                        @Override
//                        public void onSuccess(final Account account) {
//                            // Get Account Kit ID
//                            String accountKitId = account.getId();
//
//                            // Get phone number
//                            PhoneNumber phoneNumber = account.getPhoneNumber();
//                            if (phoneNumber != null) {
//                                String phoneNumberString = phoneNumber.toString();
//                            }
//
//                            // Get email
//                            String email = account.getEmail();
//                        }
//
//                        @Override
//                        public void onError(final AccountKitError error) {
//                            // Handle Error
//                        }
//                    });
//                } else {
//                    //Handle new or logged out user
//                    startActivity(new Intent(SplashScreen.this, LoginNew.class));
//                }


                if (!session.isLoggedIn()) {
                    finish();
                    // user is not logged in redirect him to Login Activity
                    Intent i = new Intent(SplashScreen.this, LoginNew.class);
                    // Closing all the Activities
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    // Add new Flag to start new Activity
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    // Staring Login Activity
                    startActivity(i);
                } else {
                    if (Helper.isNowConnected(getApplicationContext())) {
                        int product_count = (int) realm.where(Product.class)
                                .count();
                        if (product_count <= 0) {
                            getProduct();
                        }else{
                            finish();
                            Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                            // Closing all the Activities
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            // Add new Flag to start new Activity
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    }else{
                        reconnect();
                    }
                }
            }
        }, SPLASH_TIME_OUT);
    }


    private void reconnect(){
        Drawable img = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_wifi).sizeDp(32).color(Color.WHITE);

        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.md_deep_orange_500)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Koneksi gagal")
                .setMessage("Koneksi internet anda bermasalah, silahkan periksa koneksi internet anda.")
                .setPositiveButton("Coba Lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Helper.isNowConnected(getApplicationContext())) {
                            int product_count = (int) realm.where(Product.class)
                                    .count();
                            if (product_count <= 0) {
                                getProduct();
                            }else{
                                finish();
                                Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                                // Closing all the Activities
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                // Add new Flag to start new Activity
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        }else{
                            reconnect();
                        }
                    }
                })
                .show();
    }

    private void getProduct() {
        final User user = User.getUser();
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Loading...");
//        progressDialog.show();
        layout_loading.setVisibility(View.VISIBLE);
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/product/all", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.INVISIBLE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
//                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.INVISIBLE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);


                        Product.ClearData();

                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt("product_id");
                                        product.parent_id = obj.getInt("parent_id");
                                        product.product_name = obj.getString("product_name");
                                        product.product = obj.getString("product");
                                        product.code = obj.getString("code");
                                        product.prefix = obj.getString("prefix");
                                        product.description = obj.getString("description");
                                        product.img_url = obj.getString("img_url");
                                        product.order_num = obj.getInt("order_num");

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                finish();
                                Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                                // Closing all the Activities
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                // Add new Flag to start new Activity
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
