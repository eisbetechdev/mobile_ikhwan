package com.eisbetech.ikhwanmart;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.freshchat.consumer.sdk.Freshchat;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

public class VaBankActivity extends BaseActivity {
    private Bundle bundle;
    private Transaksi transaksi;
    private String trans_id;
    String no_rekening;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarTop)
    AppBarLayout toolbarTop;
    @BindView(R.id.tv_invoice)
    TextView tvInvoice;
    @BindView(R.id.tanggal_invoice)
    TextView tanggalInvoice;
    @BindView(R.id.tv_hour)
    TextView tvHour;
    @BindView(R.id.tv_minute)
    TextView tvMinute;
    @BindView(R.id.tv_second)
    TextView tvSecond;
    @BindView(R.id.ll_bank_code)
    LinearLayout llBankCode;
    @BindView(R.id.kodePembayaran)
    TextView kodePembayaran;
    @BindView(R.id.btn_copy_rek)
    FancyButton btnCopyRek;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.nama_rekening)
    TextView namaRekening;
    @BindView(R.id.iv_bank)
    ImageView ivBank;
    @BindView(R.id.totalValue)
    TextView totalValue;
    @BindView(R.id.btn_copy_jumlah)
    FancyButton btnCopyJumlah;

    @BindView(R.id.tv_payment_success)
    TextView tvPaymentSuccess;
    @BindView(R.id.btn_bantuan)
    FancyButton btnBantuan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_va_bank);
        ButterKnife.bind(this);
        refreshView();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(S.payment + " #" + transaksi.trans_id);
            getSupportActionBar().setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED, new Intent());
                finish();
            }
        });

    }


    @OnClick({R.id.btn_copy_rek, R.id.btn_copy_jumlah, R.id.btn_cara_bayar, R.id.btn_bantuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_copy_rek:
                copy(S.account_number, kodePembayaran.getText().toString());
                break;
            case R.id.btn_copy_jumlah:
                copy(S.amount, transaksi.harga);
                break;
            case R.id.btn_cara_bayar:
                Intent intent = new Intent(this,LoadWebActivity.class);
                intent.putExtra("url","https://ikhwan.eisbetech.com/mobile/va/"+transaksi.payment_method.id+"?kode_bayar="+kodePembayaran.getText());

                startActivity(intent);
                break;
            case R.id.btn_bantuan:
                Freshchat.showConversations(this);
                break;
        }
    }

    private void refreshView(){
        bundle = getIntent().getExtras();
        transaksi = bundle.getParcelable(K.transaction);

        if (bundle != null) {
            Log.d("bundle", bundle.getString(K.data));
            try {
                trans_id = transaksi.trans_id;
                JSONObject jsonObject = new JSONObject(bundle.getString(K.data));

                tvInvoice.setText(("00000000" + transaksi.trans_id).substring(transaksi.trans_id.length()));
                totalValue.setText(Helper.format_money(transaksi.harga));
                kodePembayaran.setText(jsonObject.getString(K.res_pay_code));
                no_rekening = jsonObject.getString(K.res_pay_code);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            int pmId = transaksi.payment_method.id;
            switch (pmId) {
                case I.payment_doku:
                case I.payment_minimarket:
                case I.payment_atm:
                    tvPaymentSuccess.setText(String.format("%s. %s", 3, S.payment_success));
                    break;
                default:
                    llBankCode.setVisibility(View.GONE);
                    kodePembayaran.setText(Helper.fromHtml(transaksi.payment_method.rekening));
                    no_rekening = transaksi.payment_method.rekening;
                    namaRekening.setText(Helper.fromHtml(transaksi.payment_method.name));
                    tvPaymentSuccess.setText(String.format("%s. %s", 4, S.payment_success));

                    if(transaksi.payment_method.image_url !=null){
                        GlideApp.with(this)
                                .load(transaksi.payment_method.image_url)
                                .placeholder(R.drawable.android_launcher)
                                .into(ivBank);
                    }


                    String pmBank = transaksi.payment_method.bank;
                    if (pmBank.contains(S.bca)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bca));
                    } else if (pmBank.contains(S.bni)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bni));
                    } else if (pmBank.contains(S.bri)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bri));
                    } else if (pmBank.contains(S.mandiri_syariah)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_bsm));
                    } else if (pmBank.contains(S.mandiri)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_mandiri));
                    }else if (pmBank.contains(S.cimb)) {
                        ivBank.setImageDrawable(ContextCompat.getDrawable(this,
                                R.drawable.logo_cimb));
                    }
                    break;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            try {
                Date date = formatter.parse(transaksi.expired_date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
                tanggalInvoice.setText(format2.format(calendar.getTime()) + " WIB");
                tanggalInvoice.setVisibility(View.VISIBLE);

                long now = Calendar.getInstance().getTimeInMillis();
                new CountDownTimer(calendar.getTimeInMillis() - now, 1000) {
                    public void onTick(long millisUntilFinished) {
                        long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                        long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                        long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                        tvHour.setText(String.format("%02d", hours));
                        tvMinute.setText(String.format("%02d", minutes));
                        tvSecond.setText(String.format("%02d", seconds));
                        //tvCountDown.setText(String.format("%02d:%02d:%02d", hours, minutes, seconds));
                    }
                    public void onFinish() {
                        Helper.showMessage(getApplicationContext(),"Waktu transfer anda sudah habis.");
                        finish();
                    }
                }.start();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void copy(String txt, String val) {
        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.
                CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(txt, val);
        clipboard.setPrimaryClip(clip);
        Helper.showMessage(this, txt + S.successfully_copied);
    }
}
