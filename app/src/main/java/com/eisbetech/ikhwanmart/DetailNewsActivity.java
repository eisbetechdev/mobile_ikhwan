package com.eisbetech.ikhwanmart;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.item.News.DataItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailNewsActivity extends AppCompatActivity {

    @BindView(R.id.image_cover_detail)
    ImageView mImageCoverDetail;
    @BindView(R.id.judul_detail_berita)
    TextView mJudulDetailBerita;
    @BindView(R.id.tanggal_detail_berita)
    TextView mTanggalDetailBerita;
    @BindView(R.id.kategori_detail_berita)
    TextView mKategoriDetailBerita;
    @BindView(R.id.content_news)
    WebView mContentNews;
    private Bundle extras;
    private DataItem dataItem;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mContentNews.getSettings().setMixedContentMode(
                    WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        mContentNews.getSettings().setJavaScriptEnabled(true);
        mContentNews.getSettings().setAllowFileAccessFromFileURLs(true);

        extras = getIntent().getExtras();
        if (extras!=null){
            dataItem = extras.getParcelable(Constant.NEWS_KEY);

            Glide.with(getApplicationContext())
                    .load(dataItem.getCover())
                    .into(mImageCoverDetail);

            mJudulDetailBerita.setText(dataItem.getTitle());
            mKategoriDetailBerita.setText(dataItem.getCategory());
            mTanggalDetailBerita.setText(dataItem.getPostDate());
            mContentNews.loadDataWithBaseURL("", dataItem.getDescription(), "text/html", "UTF-8", null);
        }
    }
}
