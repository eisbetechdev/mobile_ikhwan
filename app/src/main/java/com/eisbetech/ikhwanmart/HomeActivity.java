package com.eisbetech.ikhwanmart;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.Message;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.ProductDetail;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.e_commerce.CartFragment;
import com.eisbetech.ikhwanmart.e_commerce.ShopFragment;
import com.eisbetech.ikhwanmart.e_commerce.TransaksiFragment;
import com.eisbetech.ikhwanmart.fragment.Berita.FragmentNews;
import com.eisbetech.ikhwanmart.fragment.Berita.FragmentNewsBerita;
import com.eisbetech.ikhwanmart.fragment.FavoriteFragment;
import com.eisbetech.ikhwanmart.fragment.FragmentSupport;
import com.eisbetech.ikhwanmart.fragment.FragmentTransaksi;
import com.eisbetech.ikhwanmart.fragment.MessageFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanReminderList;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.eisbetech.ikhwanmart.home.HomeFragment;
import com.facebook.accountkit.AccountKit;
import com.freshchat.consumer.sdk.FaqOptions;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatCallbackStatus;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatNotificationConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.freshchat.consumer.sdk.UnreadCountCallback;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mikepenz.actionitembadge.library.utils.BadgeStyle;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.itemanimators.AlphaCrossFadeAnimator;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.eisbetech.ikhwanmart.contants.K.value;

//import net.gotev.uploadservice.UploadService;
//import net.gotev.uploadservice.okhttp.OkHttpStack;

public class HomeActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener {
    private AccountHeader headerResult = null;
    Drawer drawer;
    SessionManager session;

    private FirebaseAnalytics mFirebaseAnalytics;

    HashMap<String, String> user;
    BadgeStyle style = ActionItemBadge.BadgeStyles.RED.getStyle();

    private static final String KEY_PASSCODE_FRAGMENT = "passcode-fragment";
    private static final String KEY_PREFERENCE_FRAGMENT = "preference-fragment";
    public static final String MESSAGE_NOTIFIER = "com.amanahah.bisatopup.messagenotif";

    private SamplePreferenceFragment mSamplePreferenceFragment;
    FragmentManager fragmentManager;

    public static HomeActivity instance;

    public static HomeActivity getInstance() {
        return instance;
    }

    String action = "";
    User profile_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        instance = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        action = intent.getAction();

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.app_name);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
            layoutParams.gravity = Gravity.START;
            View cView = getLayoutInflater().inflate(R.layout.action_bar_custom, null);
            actionBar.setCustomView(cView, layoutParams);
        }
        fragmentManager = getFragmentManager();

        mSamplePreferenceFragment = (SamplePreferenceFragment) fragmentManager.findFragmentByTag(
                KEY_PREFERENCE_FRAGMENT);

        replaceFragmentWithAnimationBackStag(new HomeFragment(), S.tag_home);

        session = new SessionManager(getApplicationContext(), this);

        FreshchatConfig hConfig = new FreshchatConfig(getString(R.string.hotline_id),
                getString(R.string.hotline_key));

        hConfig.setCameraCaptureEnabled(true);
        hConfig.setGallerySelectionEnabled(true);
        hConfig.setTeamMemberInfoVisible(false);


        Freshchat.getInstance(getApplicationContext()).init(hConfig);

        FreshchatNotificationConfig notificationConfig = new FreshchatNotificationConfig()
                .setNotificationSoundEnabled(true)
                .launchActivityOnFinish(HomeActivity.class.getName())
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        Freshchat.getInstance(getApplicationContext()).setNotificationConfig(notificationConfig);

//        Freshchat.resetUser(getApplicationContext());

        //Get the user object for the current installation
        FreshchatUser hlUser = Freshchat.getInstance(getApplicationContext()).getUser();

        user = session.getUserDetails();
        profile_user = User.getUser();

        Freshchat.getInstance(getApplicationContext()).identifyUser(Helper.getDeviceId(this), String.valueOf(profile_user.user_id));

        if (profile_user != null) {
            final String name = user.get(SessionManager.KEY_NAME);
            final String email = user.get(SessionManager.KEY_EMAIL);

            hlUser.setFirstName(profile_user.first_name);
            hlUser.setLastName(profile_user.last_name);
            hlUser.setEmail(email);

            Crashlytics.setUserEmail(email);
            Crashlytics.setUserIdentifier(Helper.getDeviceId(this));

            System.out.println("External id : " + hlUser.getExternalId());
//            hlUser.setExternalId(Helper.getDeviceId(this));
            if (profile_user.phone_number != null) {
                hlUser.setPhone("+62", profile_user.phone_number);
            }

            //Call update User so that the user information is synced with Hotline's servers
            Freshchat.getInstance(getApplicationContext()).setUser(hlUser);

            if (action != null) {
                switch (action) {
                    case S.action_transaksi_notif:
                        replaceFragmentWithAnimationBackStag(new MessageFragment(),
                                S.tag_message_history);
                        break;
                    case S.action_bills_reminder:
                        replaceFragmentWithAnimationBackStag(new TagihanReminderList(),
                                S.tag_bill_reminder);

                        String product = intent.getStringExtra(K.product);
                        String no_rek = intent.getStringExtra(K.account_no);

                        cek_tagihan(product, no_rek);
                        break;
                }
            }
            Helper.getDeviceId(this);
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

            try {
                FirebaseMessaging.getInstance().subscribeToTopic(S.topic_news);
                if (profile_user.is_agent == 1) {
                    FirebaseMessaging.getInstance().subscribeToTopic(S.topic_agent);
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(S.topic_member);
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(S.topic_agent);
                    FirebaseMessaging.getInstance().subscribeToTopic(S.topic_member);
                }
            } catch (Exception ec) {

            }
        } else {
//            getProfile();
            User.ClearUser();
            session.logoutUser();
        }
        // Set up the Http Stack to use. If you omit this or comment it, HurlStack will be
        // used by default
//        UploadService.HTTP_STACK = new OkHttpStack(BisatopupApi.getOkHttpClient());

//        session.is_login();
        profile_user = User.getUser();
        if (profile_user == null) {
            User.ClearUser();
            session.logoutUser();
            return;
        }

        CreateDrawer(toolbar, savedInstanceState);

        // TODO: 28/08/2017 mirip di onNewIntent, gabungin method baru aja
        if (action != null) {
            switch (action) {
                case S.action_transaksi_notif:
                    MessageFragment messageFragment = new MessageFragment();
                    replaceFragmentWithAnimationBackStag(messageFragment, S.tag_message_history);
                    break;
                case S.action_transaksi_detail:
                    String trans_id = intent.getStringExtra(K.transaction_id);
                    Intent intent1 = new Intent(HomeActivity.this, MainLayout.class);
                    intent1.setAction(S.action_detail);
                    intent1.putExtra(K.transaction_id, trans_id);
                    startActivity(intent1);
                    break;
                case S.action_bills_reminder:
                    TagihanReminderList tagihanReminderList = new TagihanReminderList();
                    replaceFragmentWithAnimationBackStag(tagihanReminderList, S.tag_bill_reminder);

                    String product = intent.getStringExtra(K.product);
                    String no_rek = intent.getStringExtra(K.account_no);

                    cek_tagihan(product, no_rek);
                    break;
            }
        }

        cek_app_update();


        User user = realm.where(User.class).findFirst();
        realm.beginTransaction();
        user.device_id = Helper.getDeviceId(this);
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();

        boolean welcome = intent.getBooleanExtra("welcome", false);

        if (welcome) {
            Drawable img = new IconicsDrawable(this, FontAwesome.Icon.faw_info_circle).sizeDp(32).color(Color.WHITE);
            new LovelyInfoDialog(this)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(img)
                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                    .setNotShowAgainOptionEnabled(0)
                    .setTitle("Selamat Datang")
                    .setMessage("Yth Bpk/Ibu " + user.fullname + " selamat datang di Ikhwan Mart. Silahkan baca menu 'FAQ' untuk cara penggunaan aplikasi Ikhwan Mart. \nTerima Kasih.")
                    .show();
        }

        try {
            AppsFlyerLib.getInstance().setCustomerUserId(String.valueOf(user.user_id));
            AppsFlyerLib.getInstance().setUserEmails(user.email);
            AppsFlyerLib.getInstance().setCurrencyCode("IDR");


            Map<String, String> userMeta = new HashMap<String, String>();
            userMeta.put("poin", String.valueOf(user.poin));
            userMeta.put("wallet", user.wallet == null ? "" : user.wallet);
            userMeta.put("gender", user.jenis_kelamin == null ?"":user.jenis_kelamin);
            userMeta.put("userType", user.is_agent == 1 ? "Agen" : "Member");
            userMeta.put("namaAgen", user.nama_agen == null?"":user.nama_agen);

            userMeta.put("kodeAfiliasi", user.kode_affiliasi== null?"":user.kode_affiliasi);
            if (user.sponsored_by != null) {
                userMeta.put("sponsorBy", user.sponsored_by );
            }
            userMeta.put("totalAfiliasi", user.total_affiliasi == null?"":user.total_affiliasi);
            userMeta.put("totalPendapatan", user.total_pendapatan == null ?"":user.total_pendapatan);
            userMeta.put("lock_pin_enable", user.lock_pin_enable ? "Yes" : "True");

            Crashlytics.setString("email", user.email);
            Crashlytics.setString("device_id", user.device_id);
            Crashlytics.setString("phone_number", user.phone_number);

            //Call updateUserProperties to sync the user properties with Hotline's servers
            Freshchat.getInstance(getApplicationContext()).setUserProperties(userMeta);
        } catch (Exception ec) {
            Crashlytics.logException(ec);
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("mproduct");
        myRef.keepSynced(true);
//        final DatabaseReference prodt = database.getReference("mproduct_detail");
//        prodt.keepSynced(true);
        // Read from the database
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

//                Object map =  dataSnapshot.getValue();
                try {

                    List<Product> products = new ArrayList<>();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        String product_name = ds.child("product_name").getValue(String.class);
                        String product = ds.child("product").getValue(String.class);
                        String code = ds.child("code").getValue(String.class);
                        String prefix = ds.child("prefix").getValue(String.class);
                        String description = ds.child("description").getValue(String.class);
                        String img_url = ds.child("img_url").getValue(String.class);
                        int product_id = ds.child("product_id").getValue(Integer.class);
                        int parent_id = ds.child("parent_id").getValue(Integer.class);
                        int is_active = ds.child("is_active").getValue(Integer.class);
                        int order_num = ds.child("order_num").getValue(Integer.class);


                        if (is_active == 1) {
                            System.out.println("Add product - " + product_name);
                            Product productobj = new Product();
                            productobj.product_name = product_name;
                            productobj.product = product;
                            productobj.code = code;
                            productobj.prefix = prefix;
                            productobj.description = description;
                            productobj.img_url = img_url;
                            productobj.id = product_id;
                            productobj.order_num = order_num;
                            productobj.parent_id = parent_id;

                            products.add(productobj);
                        }
                    }

                    Product.AddOrUpdateList(products);
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
//                Log.d(HomeActivity.class.getCanonicalName() , "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(HomeActivity.class.getCanonicalName(), "Failed to read value.", error.toException());
            }
        });

//        prodt.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                List<ProductDetail> productsDetails = new ArrayList<>();
//
//                for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                    String name = ds.child("name").getValue(String.class);
//                    double price = ds.child("price").getValue(Long.class);
//                    double base_price = ds.child("base_price").getValue(Long.class);
//                    String desc = ds.child("desc").getValue(String.class);
//                    int id = ds.child("product_detail_id").getValue(Integer.class);
//                    int is_gangguan = ds.child("is_gangguan").getValue(Integer.class);
//                    int product_id = ds.child("product_id").getValue(Integer.class);
//                    int is_active = ds.child("is_active").getValue(Integer.class);
//                    int grup = 0;
//
//                    try{
//                        grup = ds.child("grup").getValue(Integer.class);
//                    }catch (Exception Ec){
//
//                    }
//
//                    System.out.println("Add product Detail- "+id + "-" + name);
//
//                    ProductDetail productDetail = new ProductDetail();
//                    productDetail.product_id = product_id;
//                    productDetail.id = id;
//                    productDetail.is_gangguan = is_gangguan;
//                    productDetail.price = price;
//                    productDetail.base_price = base_price;
//                    productDetail.product_name = name;
//                    productDetail.is_active = is_active;
//                    productDetail.desc = desc;
//                    productDetail.grup = grup;
//
//                    productsDetails.add(productDetail);
//
//                }
//
//                ProductDetail.AddOrUpdateList(productsDetails);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                // AddRemoveProduct(map);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                AddRemoveProduct(map);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Object map = dataSnapshot.getValue();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        prodt.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                final Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
//                //AddRemoveProductDetail(map);
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
//                AddRemoveProductDetail(map);
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });


        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
//                            Toast.makeText(SplashScreen.this, "Fetch Succeeded",
//                                    Toast.LENGTH_SHORT).show();

                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();

                        } else {

                        }
                        String info_text = mFirebaseRemoteConfig.getString("info_text");
                        String info_id = mFirebaseRemoteConfig.getString("info_id");
                        String info_title = mFirebaseRemoteConfig.getString("info_title");
                        String info_url_image = mFirebaseRemoteConfig.getString("info_url_image");
                        if (!info_id.isEmpty()) {
                            boolean show_m = isFirstTime(info_id);
                            if (show_m && !info_title.isEmpty() && !info_text.isEmpty()) {
                                showmessage(info_title, info_text, info_url_image);
                            }
                        }

                        String product_version = mFirebaseRemoteConfig.getString("product_version");
                        if (!product_version.isEmpty()) {
                            boolean show_m = product_version(product_version);
                            if (show_m) {
                                getProduct();
                            }
                        }

                        //displayWelcomeMessage();
                    }
                });
    }

    private void AddRemoveProduct(Map<String, String> map) {
        String product_name = map.get("product_name");
        String product = map.get("product");
        String code = map.get("code");
        String prefix = map.get("prefix");
        String description = map.get("description");
        String img_url = map.get("img_url");
        int product_id = Integer.parseInt(String.valueOf(map.get("product_id")));
        int parent_id = Integer.parseInt(String.valueOf(map.get("parent_id")));
        int is_active = Integer.parseInt(String.valueOf(map.get("is_active")));
        int order_num = Integer.parseInt(String.valueOf(map.get("order_num")));

        System.out.println("Add product - " + product_name);
        Product productobj = new Product();
        productobj.product_name = product_name;
        productobj.product = product;
        productobj.code = code;
        productobj.prefix = prefix;
        productobj.description = description;
        productobj.img_url = img_url;
        productobj.id = product_id;
        productobj.order_num = order_num;
        productobj.parent_id = parent_id;

        if (is_active == 1) {
            // Product.AddOrUpdate(productobj);
        } else {
            Product.Delete(productobj);
        }
    }

    private void AddRemoveProductDetail(Map<String, String> map) {
        String name = map.get("name");
        double price = Double.parseDouble(String.valueOf(map.get("price")));
        double base_price = Double.parseDouble(String.valueOf(map.get("base_price")));
        String desc = map.get("desc");
        int id = Integer.parseInt(String.valueOf(map.get("product_detail_id")));
        int is_gangguan = Integer.parseInt(String.valueOf(map.get("is_gangguan")));
        int product_id = Integer.parseInt(String.valueOf(map.get("product_id")));
        int is_active = Integer.parseInt(String.valueOf(map.get("is_active")));
        int grup = 0;
        try {
            grup = Integer.parseInt(String.valueOf(map.get("grup")));
        } catch (Exception ec) {
        }

        System.out.println("Add product Detail- " + id + " - " + name);

        ProductDetail productDetail = new ProductDetail();
        productDetail.product_id = product_id;
        productDetail.id = id;
        productDetail.is_gangguan = is_gangguan;
        productDetail.price = price;
        productDetail.base_price = base_price;
        productDetail.product_name = name;
        productDetail.desc = desc;
        productDetail.is_active = is_active;
        productDetail.grup = grup;

        //ProductDetail.AddOrUpdate(productDetail);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        action = intent.getAction();
        if (action != null) {
            switch (action) {
                case S.action_transaksi_notif:
                    MessageFragment messageFragment = new MessageFragment();
                    replaceFragmentWithAnimationBackStag(messageFragment, S.tag_message_history);
                    break;
                case S.action_transaksi_detail:
                    String trans_id = intent.getStringExtra(K.transaction_id);
                    Intent intent1 = new Intent(HomeActivity.this, MainLayout.class);
                    intent1.setAction(S.action_detail);
                    intent1.putExtra(K.transaction_id, trans_id);
                    startActivity(intent1);
                    break;
                case S.action_bills_reminder:
                    TagihanReminderList tagihanReminderList = new TagihanReminderList();
                    replaceFragmentWithAnimationBackStag(tagihanReminderList, S.tag_bill_reminder);

                    String product = intent.getStringExtra(K.product);
                    String no_rek = intent.getStringExtra(K.account_no);

                    cek_tagihan(product, no_rek);
                    break;
            }
        }
    }

    IProfile profile = null;
    SecondaryDrawerItem drawer_item_message;
    SecondaryDrawerItem drawer_item_cs;

    private void CreateDrawer(Toolbar toolbar, Bundle savedInstanceState) {
        User user_dt = User.getUser();

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/muli.ttf");
        Drawable img = new IconicsDrawable(this, FontAwesome.Icon.faw_user)
                .color(Color.parseColor("#ffffff")).sizeDp(64);

        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);

        profile = new ProfileDrawerItem().withName(name + " (" + user_dt.user_id + ")")
                .withEmail(user_dt.phone_number)
                .withTypeface(face)
                .withIcon(getResources().getDrawable(R.drawable.android_launcher));

        if (user_dt.profile_url != null) {
//            Uri resultUri = Uri.parse(user.profile_url);
//            Drawable image_dr;
//            try {
//                InputStream inputStream = getContentResolver().openInputStream(resultUri);
//                image_dr = Drawable.createFromStream(inputStream, resultUri.toString());
//            } catch (FileNotFoundException e) {
//                image_dr = new IconicsDrawable(this, FontAwesome.Icon.faw_user)
//                        .color(Color.parseColor("#ffffff")).sizeDp(24);
//            }

            profile = new ProfileDrawerItem().withName(name + " (" + user_dt.user_id + ")")
                    .withEmail(user_dt.phone_number)
                    .withTypeface(face)
                    .withIcon(getResources().getDrawable(R.drawable.android_launcher)).withIdentifier(100);
        }

        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                //.withCompactStyle(true)
                .withHeaderBackground(R.drawable.header_bg)
                .withTypeface(face)
                .addProfiles(profile
                        //don't ask but google uses 14dp for the add account icon in gmail but 20dp for the normal icons (like manage account)
//                        new ProfileSettingDrawerItem().withName("Add Account").withDescription("Add new GitHub Account").withIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_plus).actionBar().paddingDp(5).colorRes(R.color.material_drawer_dark_primary_text)).withIdentifier(PROFILE_SETTING),
                        //new ProfileSettingDrawerItem().withName("Manage Account").withIcon(GoogleMaterial.Icon.gmd_settings)
                )
                .withProfileImagesClickable(true)
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        Intent i = new Intent(HomeActivity.this, Mainlayout2.class);
                        i.setAction(S.action_profile);
                        startActivity(i);
                        return false;
                    }
                })
                .withSelectionListEnabledForSingleProfile(false)
                .withSavedInstance(savedInstanceState)
                .build();
        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem home_menu = new PrimaryDrawerItem().withIdentifier(1)
                .withName(R.string.drawer_item_home)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_home);

        PrimaryDrawerItem news_menu = new PrimaryDrawerItem().withIdentifier(50)
                .withName("Berita")
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_rss_feed);

        SecondaryDrawerItem drawer_produk = new SecondaryDrawerItem()
                .withIdentifier(51).withName("Semua Produk")
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_card_travel);

        SecondaryDrawerItem drawer_cart = new SecondaryDrawerItem()
                .withIdentifier(52).withName("Keranjang")
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_shopping_cart);

        SecondaryDrawerItem drawer_transaksi = new SecondaryDrawerItem()
                .withIdentifier(53).withName("Riwayat Transaksi")
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_history);

        SecondaryDrawerItem drawer_item_transaksi = new SecondaryDrawerItem()
                .withIdentifier(2).withName(R.string.drawer_item_transaksi)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_add_shopping_cart);

        SecondaryDrawerItem drawer_item_transaksi_favorit = new SecondaryDrawerItem()
                .withIdentifier(3).withName(R.string.drawer_item_transaksi_favorit)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_bookmark);

        long message_count = realm.where(Message.class)
                .equalTo(K.is_read, false)
                .count();

        badgeCount = (int) message_count;

        drawer_item_message = new SecondaryDrawerItem()
                .withIdentifier(4).withName(R.string.drawer_item_Pesan)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_message);

        if (badgeCount > 0) {
            drawer_item_message.withBadge(String.valueOf(badgeCount)).withBadgeStyle(
                    new com.mikepenz.materialdrawer.holder.BadgeStyle()
                            .withColor(Color.RED)
                            .withTextColor(Color.WHITE)
            );
        }

        SecondaryDrawerItem drawer_item_wallet = new SecondaryDrawerItem()
                .withIdentifier(5).withName(R.string.drawer_item_wallet).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_account_balance_wallet);

        SecondaryDrawerItem drawer_item_support = new SecondaryDrawerItem()
                .withIdentifier(6).withName(R.string.drawer_item_support)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_people);

        SecondaryDrawerItem drawer_item_reminder = new SecondaryDrawerItem()
                .withIdentifier(7).withName(R.string.drawer_item_transaksi_reminder)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_alarm);

        SecondaryDrawerItem drawer_item_pengaturan = new SecondaryDrawerItem()
                .withIdentifier(8).withName(R.string.drawer_item_settings)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_settings);

        SecondaryDrawerItem drawer_item_pengaturan_logout = new SecondaryDrawerItem()
                .withIdentifier(9).withName(R.string.drawer_item_logout).withSelectable(false)
                .withTypeface(face)
                .withIcon(FontAwesome.Icon.faw_sign_out);

        SecondaryDrawerItem drawer_kontak_kami = new SecondaryDrawerItem()
                .withIdentifier(10).withName(R.string.drawer_item_kontak_kami).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_email);

        SecondaryDrawerItem drawer_faq = new SecondaryDrawerItem()
                .withIdentifier(11).withName(R.string.drawer_item_faq).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_question_answer);

        SecondaryDrawerItem drawer_tos = new SecondaryDrawerItem()
                .withIdentifier(12).withName(R.string.drawer_item_tos).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_check_box);

        SecondaryDrawerItem drawer_privacy = new SecondaryDrawerItem()
                .withIdentifier(15).withName(R.string.drawer_item_privacy).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_security);

        SecondaryDrawerItem drawer_about = new SecondaryDrawerItem()
                .withIdentifier(13).withName(R.string.drawer_item_tentang).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_info);

        drawer_item_cs = new SecondaryDrawerItem()
                .withIdentifier(14).withName(R.string.drawer_item_cs).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_chat_bubble);

        SecondaryDrawerItem drawer_item_cek_harga = new SecondaryDrawerItem()
                .withIdentifier(16).withName(R.string.drawer_item_cek_harga).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_attach_money);

        SecondaryDrawerItem drawer_item_view_profile = new SecondaryDrawerItem()
                .withIdentifier(17).withName(R.string.drawer_item_view_profile).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_people);

        SecondaryDrawerItem drawer_item_affiliasi = new SecondaryDrawerItem()
                .withIdentifier(18).withName(R.string.drawer_item_affiliasi).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_group);

        /*SecondaryDrawerItem drawer_item_tutorial = new SecondaryDrawerItem()
                .withIdentifier(19).withName(R.string.drawer_item_tutorial).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_book);*/

        SecondaryDrawerItem drawer_rate_kami = new SecondaryDrawerItem()
                .withIdentifier(20).withName(R.string.drawer_item_rate_kami).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_rate_review);

        SecondaryDrawerItem drawer_laporan = new SecondaryDrawerItem()
                .withIdentifier(21).withName(R.string.drawer_item_laporan).withSelectable(false)
                .withTypeface(face)
                .withIcon(GoogleMaterial.Icon.gmd_chat);

        //create the drawer and remember the `Drawer` result object
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withItemAnimator(new AlphaCrossFadeAnimator())
                .withTranslucentStatusBar(false)
                .withToolbar(toolbar)
                .addDrawerItems(
                        home_menu,
                        news_menu,
                        new SectionDrawerItem().withName("E - Commerce").withSelectable(false).withTypeface(face),
                        drawer_produk,
                        drawer_cart,
                        drawer_transaksi,
                        new SectionDrawerItem().withName(R.string.drawer_item_transaksi).withSelectable(false).withTypeface(face),
                        drawer_item_transaksi,
                        drawer_laporan,
                        drawer_item_cek_harga,
                        drawer_item_transaksi_favorit,
                        drawer_item_reminder,
                        new SectionDrawerItem().withName(R.string.drawer_item_profile).withSelectable(false).withTypeface(face),
                        drawer_item_view_profile,
                        drawer_item_message,
                        drawer_item_wallet,
                        drawer_item_affiliasi,
//                        drawer_item_pengaturan,
                        new SectionDrawerItem().withName(R.string.drawer_item_support).withSelectable(false).withTypeface(face),
                        drawer_item_cs,
                        drawer_item_support,
                        drawer_kontak_kami,
                        drawer_rate_kami,
                        new SectionDrawerItem().withName(R.string.drawer_item_lain).withSelectable(false).withTypeface(face),
                        drawer_faq,
                        //drawer_item_tutorial,
                        drawer_tos,
                        drawer_privacy,
                        drawer_about
                )
                .withActionBarDrawerToggleAnimated(true)
                .withSavedInstance(savedInstanceState)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        return false;
                    }
                })
                .withOnDrawerItemClickListener(this)
                .build();

        drawer.addStickyFooterItem(new PrimaryDrawerItem().withIdentifier(100).withName(S.logout)
                .withIcon(FontAwesome.Icon.faw_sign_out));
    }

    public void reload_profile() {
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/muli.ttf");
        User user = User.getUser();
        if (user.profile_url != null) {
            profile = headerResult.getActiveProfile();
            Uri uri = Uri.parse(user.profile_url);
            profile.withName(user.fullname + " (" + user.user_id + ")");
            profile.withEmail(user.phone_number);
            profile.withIcon(user.profile_url);
            headerResult.updateProfile(profile);
        }
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        long identified = drawerItem.getIdentifier();
        if (identified == 10) {
            Helper.sendMail(this, "support@ikhwan.eisbetech.com", S.empty, S.empty);
//            Crashlytics.getInstance().crash();
        } else if (identified == 1) {
            replaceFragmentWithAnimationBackStag(new HomeFragment(), S.tag_home);
        } else if (identified == 50) {
            replaceFragmentWithAnimation(new FragmentNews(), "News");
        } else if (identified == 51) {
            replaceFragmentWithAnimationBackStag(new ShopFragment(), "Berita");
        } else if (identified == 52) {
            replaceFragmentWithAnimationBackStag(new CartFragment(), "Keranjang");
//            Helper.showToast(getApplicationContext(),"Coming Soon");
        } else if (identified == 53) {
//            replaceFragmentWithAnimationBackStag(new TransaksiFragment(), "Riwayat Transaksi");
            Helper.showToast(getApplicationContext(),"Coming Soon");
        } else if (identified == 2) {
            replaceFragmentWithAnimation(new FragmentTransaksi(), S.tag_transaksi);
        } else if (identified == 3) {
            replaceFragmentWithAnimation(new FavoriteFragment(), S.tag_favorite);
        } else if (identified == 4) {
            replaceFragmentWithAnimation(new MessageFragment(), S.tag_message_history);
        } else if (identified == 5) {
            startActivity(new Intent(HomeActivity.this, WalletActivity.class));
        } else if (identified == 6) {
            replaceFragmentWithAnimation(new FragmentSupport(), S.tag_support);
        } else if (identified == 7) {
            replaceFragmentWithAnimation(new TagihanReminderList(), S.tag_bill_reminder_list);
        } else if (identified == 11) {
            FaqOptions faqOptions = new FaqOptions()
                    .showFaqCategoriesAsGrid(false)
                    .showContactUsOnAppBar(true)
                    .showContactUsOnFaqScreens(true)
                    .showContactUsOnFaqNotHelpful(true);
            Freshchat.showFAQs(HomeActivity.this, faqOptions);
        } else if (identified == 12) {
            Intent intent = new Intent(HomeActivity.this, ViewWeb.class);
            intent.putExtra(K.title, S.terms_n_condition);
            intent.putExtra(K.url, S.url_terms);
            startActivity(intent);
        } else if (identified == 15) {
            Intent intent = new Intent(HomeActivity.this, ViewWeb.class);
            intent.putExtra(K.title, S.privacy_policy);
            intent.putExtra(K.url, S.url_privacy);
            startActivity(intent);
        } else if (identified == 14) {
            Freshchat.showConversations(getApplicationContext());
        } else if (identified == 13) {
            startActivity(new Intent(HomeActivity.this, AboutActivity.class));
        } else if (identified == 16) {
            Intent i = new Intent(HomeActivity.this, MainLayout.class);
            i.setAction(S.action_price_check);
            startActivity(i);
        } else if (identified == 17) {
            Intent i = new Intent(HomeActivity.this, Mainlayout2.class);
            i.setAction(S.action_profile);
            startActivity(i);
        } else if (identified == 20) {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Helper.showMessage(this, S.unable_to_find_market_app);
            }
        } else if (identified == 18) {
            profile_user = User.getUser();
            if (profile_user.is_agent == 1) {
                startActivity(new Intent(HomeActivity.this, AffiliasiActivity.class));
            } else {
                Intent i = new Intent(HomeActivity.this, MainLayout.class);
                i.setAction(S.action_member_affiliation);
                startActivity(i);
            }
        } else if (identified == 19) {
//            FaqOptions faqOptions = new FaqOptions()
//                    .showFaqCategoriesAsGrid(false)
//                    .showContactUsOnAppBar(true)
//                    .showContactUsOnFaqScreens(true)
//                    .showContactUsOnFaqNotHelpful(true);
//
//            Freshchat.showFAQs(HomeActivity.this, faqOptions);
            Intent intent = new Intent(HomeActivity.this, ViewWeb.class);
            intent.putExtra(K.title, S.other_info);
            intent.putExtra(K.url, S.url_faq);
            startActivity(intent);
        } else if (identified == 21) {
            Intent intent = new Intent(HomeActivity.this, ReportActivity.class);
            startActivity(intent);
        } else if (identified == 100) {
            Drawable img = new IconicsDrawable(getApplicationContext(),
                    FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
            new LovelyStandardDialog(this)
                    .setTopColorRes(R.color.accent)
                    .setButtonsColorRes(R.color.accent)
                    .setIcon(img)
                    .setTitle(S.logout)
                    .setMessage(S.are_u_sure)
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            logout_user();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        }
        return false;
    }

    public void open_hotline() {
        Freshchat.showConversations(getApplicationContext());
    }

    public void replaceFragmentWithAnimationBackStag(android.support.v4.app.Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void replaceFragmentWithAnimation(android.support.v4.app.Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    private int badgeCount = 0;
    private int chatCount = 0;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        long message_count = realm.where(Message.class)
                .equalTo(K.is_read, false)
                .count();

        badgeCount = (int) message_count;

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);

        Drawable icon = getResources().getDrawable(R.drawable.notification);
        //menu.add(0, 1, android.view.Menu.NONE, "NOTIFICATION").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        RealmResults<Message> results = realm.where(Message.class).findAllAsync();

        if (badgeCount > 0) {
            ActionItemBadge.update(this, menu.findItem(R.id.item_message), icon,
                    ActionItemBadge.BadgeStyles.RED, badgeCount);
        } else {
//            ActionItemBadge.hide(menu.findItem(R.id.item_message));
            ActionItemBadge.update(this, menu.findItem(R.id.item_message), icon,
                    ActionItemBadge.BadgeStyles.GREY, null);
        }

        icon = getResources().getDrawable(R.drawable.chat);
        menu.add(0, 2, android.view.Menu.NONE, S.menu_chat).setIcon(icon).
                setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        if (chatCount > 0) {
            ActionItemBadge.update(this, menu.getItem(1), icon,
                    ActionItemBadge.BadgeStyles.RED, chatCount);
        } else {
//            ActionItemBadge.hide(menu.getItem(1));
            ActionItemBadge.update(this, menu.getItem(1), icon,
                    ActionItemBadge.BadgeStyles.GREY, null);
        }

        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.item_message:
                MessageFragment messageFragment = new MessageFragment();
                replaceFragmentWithAnimation(messageFragment, S.tag_message_history);
                return true;
            case 2:
//                FragmentTransaksi fragmentTransaksi = new FragmentTransaksi();
//                replaceFragmentWithAnimation(fragmentTransaksi, "transaksi");
                Freshchat.showConversations(getApplicationContext());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void refresh_menu() {
        try {
            invalidateOptionsMenu();

            long message_count = realm.where(Message.class)
                    .equalTo(K.is_read, false)
                    .count();

            badgeCount = (int) message_count;

            if (badgeCount > 0) {
                drawer_item_message.withBadge(String.valueOf(badgeCount)).withBadgeStyle(
                        new com.mikepenz.materialdrawer.holder.BadgeStyle()
                                .withColor(Color.RED)
                                .withTextColor(Color.WHITE)
                );

            } else {
                drawer_item_message.withBadge((String) null);
            }
            drawer.updateItem(drawer_item_message);
        } catch (Exception ec) {
            Crashlytics.logException(ec);
            ;
        }
    }

    //This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra(K.message);
            String title = intent.getStringExtra(K.title);
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case S.action_transaksi_notif:
                        Drawable img = new IconicsDrawable(getApplicationContext(),
                                FontAwesome.Icon.faw_envelope_square).sizeDp(32).color(Color.WHITE);
                        new LovelyInfoDialog(HomeActivity.this)
                                .setTopColorRes(R.color.colorPrimary)
                                .setIcon(img)
//This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                        .setNotShowAgainOptionEnabled(0)
                                .setTitle(title)
                                .setMessage(message)
                                .show();

                        invalidateOptionsMenu();
                        //do other stuff here
                        break;
                    case S.action_bills_reminder:
                        String product = intent.getStringExtra(K.product);
                        String no_rek = intent.getStringExtra(K.account_no);

                        cek_tagihan(product, no_rek);
                        break;
                    case S.action_update_image_profile:
                        User user = User.getUser();
                        if (user.profile_url != null) {
                            Uri resultUri = Uri.parse(user.profile_url);
                            Drawable image_dr;
                            try {
                                InputStream inputStream = getContentResolver().openInputStream(resultUri);
                                image_dr = Drawable.createFromStream(inputStream, resultUri.toString());
                            } catch (FileNotFoundException e) {
                                image_dr = getResources().getDrawable(R.drawable.android_launcher);
                            }
//                        img_profile.setImageDrawable(image_dr);
                        }
                        break;
                    case S.action_refresh_menu:
                        invalidateOptionsMenu();
                        break;
                }
            }
        }
    };

    @Override
    public void onResume() {
        getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter(MESSAGE_NOTIFIER));

        Freshchat.getInstance(getApplicationContext()).getUnreadCountAsync(new UnreadCountCallback() {
            @Override
            public void onResult(FreshchatCallbackStatus hotlineCallbackStatus, int unreadCount) {
                //Assuming "badgeTextView" is a text view to show the count on
                try {
                    if (unreadCount > 0) {
                        drawer_item_cs.withBadge(String.valueOf(unreadCount)).withBadgeStyle(
                                new com.mikepenz.materialdrawer.holder.BadgeStyle()
                                        .withColor(Color.RED)
                                        .withTextColor(Color.WHITE)
                        );
                    } else {
                        drawer_item_cs.withBadge((String) null);
                    }
                    chatCount = unreadCount;
                    invalidateOptionsMenu();
                } catch (Exception ec) {
                    Crashlytics.logException(ec);
                    ;
                }
            }
        });
        super.onResume();
    }

    @Override
    protected void onPause() {
        getApplicationContext().unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment);
            if (f instanceof HomeFragment) {
                Drawable img = new IconicsDrawable(getApplicationContext(),
                        FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                new LovelyStandardDialog(this)
                        .setTopColorRes(R.color.accent)
                        .setButtonsColorRes(R.color.accent)
                        .setIcon(img)
                        .setTitle(S.exit)
                        .setMessage(S.are_u_sure_wanna_exit)
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();
            } else {
                super.onBackPressed();
            }
        }
//        super.onBackPressed();
    }

    private void getProfile() {
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(HomeActivity.this);
        String token = Helper.getFirebaseInstanceId(getApplicationContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, email);
            params.put(K.token, token);

            BisatopupApi.get("/profile/index", params, getApplicationContext(),
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
//                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                                    Helper.show_alert("Info", object.getString(K.message), HomeActivity.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
//                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                                    Helper.show_info("Info", object.getString(K.message), HomeActivity.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject response = new JSONObject(responseString);

                                JSONObject object = response.getJSONObject(K.data);

                                User.createUser(object.getString(K.name), object.getString(K.email),
                                        object.getString(K.key));
                                session.createLoginSession(object.getString(K.name),
                                        object.getString(K.email), S.empty);
                                FreshchatUser hlUser = Freshchat.getInstance(getApplicationContext()).getUser();
                                User profile_user = User.getUser();

                                hlUser.setFirstName(profile_user.first_name);
                                hlUser.setLastName(profile_user.last_name);
                                hlUser.setEmail(object.getString(K.email));
//                                hlUser.setExternalId(Helper.getDeviceId(HomeActivity.this));
                                hlUser.setPhone("+62", object.getString(K.phone_number));

                                //Call updateUser so that the user information is synced with Hotline's servers
                                Freshchat.getInstance(getApplicationContext()).setUser(hlUser);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProduct() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);

            BisatopupApi.get("/product/all", params, this, new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);
                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt(K.product_id);
                                        product.parent_id = obj.getInt(K.parent_id);
                                        product.product_name = obj.getString(K.product_name);
                                        product.product = obj.getString(K.product);
                                        product.code = obj.getString(K.code);
                                        product.prefix = obj.getString(K.prefix);
                                        product.description = obj.getString(K.description);
                                        product.img_url = obj.getString(K.img_url);
                                        product.is_active = obj.getInt("is_active");
                                        product.is_gangguan = obj.getInt("is_gangguan");

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction success.
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logout_user() {
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(HomeActivity.this);
        String token = Helper.getFirebaseInstanceId(getApplicationContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, email);
            params.put(K.token, token);

            BisatopupApi.post("/user/logout", params, getApplicationContext(),
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
//                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                                    Helper.show_alert("Info", object.getString(K.message), HomeActivity.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
//                            Helper.showMessage(getApplicationContext(), object.getString(K.message));
                                    Helper.show_info("Info", object.getString(K.message), HomeActivity.this);
                                } else {
                                    Freshchat.resetUser(getApplicationContext());
                                    session.clearSession();
                                    try {
                                        User.ClearUser();
                                        Product.ClearData();
                                        ProductDetail.ClearData();
                                        AccountKit.logOut();
                                    } catch (Exception ec) {
                                        ec.printStackTrace();
                                    }
                                    finish();
                                    Intent intent = new Intent(HomeActivity.this, LoginNew.class);
                                    intent.setAction("logout");
                                    startActivity(intent);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cek_tagihan(String jenis_tagihan, final String no_rek) {
        final Product parent_product = realm.where(Product.class)
                .equalTo(K.product_name, jenis_tagihan)
                .findFirst();

        User user = User.getUser();
        System.out.println(parent_product.code);
        showProgressDialog();
        try {
            RequestParams params = new RequestParams();
            params.add(K.account_number, no_rek);
            params.add(K.phone_number, user.phone_number);
            params.add(K.product, parent_product.code);

            BisatopupApi.post("/tagihan/cek", params, getApplicationContext(),
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            hideProgressDialog();

                            try {
                                JSONObject data = new JSONObject(responseString);
                                if (data.getBoolean(K.error)) {
                                    Helper.showMessage(getApplicationContext(), data.getString(K.message));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            hideProgressDialog();
                            try {
                                JSONObject data = new JSONObject(responseString);
                                if (data.getBoolean(K.error)) {
                                    Helper.showMessage(getApplicationContext(), data.getString(K.message));
                                } else {
                                    JSONObject data_tagihan = data.getJSONObject(K.data);

                                    double juml_tag = data_tagihan.getDouble(K.bill_amount);
                                    double admin = data_tagihan.getDouble(K.admin);
                                    double jml_bayar = data_tagihan.getDouble(K.paid_amount);

                                    if (jml_bayar < juml_tag) {
                                        juml_tag = juml_tag - admin;
                                    }

                                    double total_tag = juml_tag + admin;

                                    final String jumlah = data_tagihan.getString("jumlah_bayar");
                                    final int tagihan_id = data_tagihan.getInt("tagihan_id");

                                    ViewHolder viewHolder = new ViewHolder(R.layout.dialog_cek_tagihan);


                                    final DialogPlus dialog = DialogPlus.newDialog(HomeActivity.this)
                                            .setContentHolder(new ViewHolder(R.layout.dialog_cek_tagihan))
                                            .setGravity(Gravity.BOTTOM)
//                .setHeader(R.layout.dialog_header)
                                            .setFooter(R.layout.dialog_footer_cek_tagihan)
                                            .setExpanded(true, 600)
                                            .setContentHeight(600)
                                            .setCancelable(true)
                                            .setOnCancelListener(new OnCancelListener() {
                                                @Override
                                                public void onCancel(DialogPlus dialog) {
                                                    dialog.dismiss();
                                                }
                                            })
                                            .setOnClickListener(new OnClickListener() {
                                                @Override
                                                public void onClick(DialogPlus dialog, View view) {
                                                    System.out.println("test");
                                                    if (view.getId() == R.id.footer_close_button) {
                                                        dialog.dismiss();
                                                    } else if (view.getId() == R.id.footer_confirm_button) {
                                                        Intent i = new Intent(HomeActivity.this, MainLayout.class);
                                                        i.setAction(S.action_checkout);
                                                        i.putExtra(K.product, S.ppob);
                                                        i.putExtra(K.product_id, parent_product.id);
                                                        i.putExtra(K.nominal, parent_product.product_name);
                                                        i.putExtra(K.price, jumlah);
                                                        i.putExtra(K.is_tagihan, true);
                                                        i.putExtra(K.no_tujuan, no_rek);
                                                        i.putExtra(K.img, S.empty);
                                                        i.putExtra(K.bill_id, tagihan_id);
                                                        startActivity(i);
                                                    }
                                                }
                                            })
                                            .create();

                                    // View view = viewHolder.getInflatedView();
                                    // LayoutInflater inflater = LayoutInflater.from(getActivity());
//                            View view = inflater.inflate(com.orhanobut.dialogplus.R.layout.dialog_view, parent, false);

                                    TextView txt_tagihan_nama = (TextView) dialog.findViewById(R.id.txt_tagihan_nama);
                                    TextView txt_tagihan_periode = (TextView) dialog.findViewById(R.id.txt_tagihan_periode);
                                    TextView txt_tagihan_total = (TextView) dialog.findViewById(R.id.txt_tagihan_total);
                                    TextView txt_tagihan_adm = (TextView) dialog.findViewById(R.id.txt_tagihan_adm);
                                    TextView txt_potongan = (TextView) dialog.findViewById(R.id.txt_potongan);
                                    TextView txt_tagihan_jumlah = (TextView) dialog.findViewById(R.id.txt_tagihan_jumlah);
                                    TextView txt_header_info = (TextView) dialog.findViewById(R.id.txt_header_info);
                                    TextView txt_nomor_rek = (TextView) dialog.findViewById(R.id.txt_nomor_rek);
                                    TextView txt_total_bayar = (TextView) dialog.findViewById(R.id.txt_total_bayar);

                                    txt_nomor_rek.setText(data_tagihan.getString(K.customer_no));
                                    txt_tagihan_nama.setText(data_tagihan.getString(K.nama));
                                    txt_header_info.setText("Informasi tagihan " + data_tagihan.
                                            getString(K.product_name) + " anda");
                                    txt_tagihan_periode.setText(data_tagihan.getString(K.periode));

                                    txt_tagihan_total.setText(Helper.format_money(String.valueOf(total_tag)));

                                    txt_potongan.setText(Helper.format_money(String.valueOf((total_tag - jml_bayar) * -1)));

                                    txt_tagihan_adm.setText(Helper.format_money(data_tagihan.getString(K.admin)));
                                    txt_tagihan_jumlah.setText(Helper.format_money(String.valueOf(juml_tag)));
                                    txt_total_bayar.setText(Helper.format_money(data_tagihan.getString(K.paid_amount)));

                                    dialog.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cek_app_update() {
        User user = User.getUser();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);
            params.put(K.phone_number, user.phone_number);

            BisatopupApi.get("/user/app-version", params, HomeActivity.this,
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            try {
                                JSONObject object = new JSONObject(responseString);

                                String version = object.getString(value);

                                String current_version = Helper.getStringResourceByName(K.version_int,
                                        getApplicationContext());

                                if (Integer.parseInt(current_version) < Integer.parseInt(version)) {
                                    startActivity(new Intent(HomeActivity.this, UpgradeAppActivity.class));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void showmessage(final String title_t, final String content, final String info_url_image) {
        final Drawable img = new IconicsDrawable(this, FontAwesome.Icon.
                faw_envelope_square).sizeDp(32).color(Color.WHITE);
        final LovelyCustomDialog dialog = new LovelyCustomDialog(this)
                .setView(R.layout.dialog_news)
                .setTopColorRes(R.color.colorPrimary)
                //  .setTitle(item.message.title)
                //.setMessage(Html.fromHtml(item.message.content + date_str))
                .setIcon(img)
                .configureView(new LovelyCustomDialog.ViewConfigurator() {

                    public TextView description;
                    public TextView title;
                    public TextView tanggal;
                    public ImageView imageView;

                    @Override
                    public void configureView(View v) {

                        String date_str = "";

                        description = (TextView) v.findViewById(R.id.description);
                        title = (TextView) v.findViewById(R.id.title);
                        tanggal = (TextView) v.findViewById(R.id.tanggal);
                        imageView = (ImageView) v.findViewById(R.id.imageView);
                        tanggal.setVisibility(View.GONE);
                        title.setText(Html.fromHtml(title_t));
                        tanggal.setText(date_str);
                        description.setText(Html.fromHtml(content));
                        imageView.setVisibility(View.GONE);

                        if (!info_url_image.isEmpty()) {
                            imageView.setVisibility(View.VISIBLE);
                            GlideApp.with(HomeActivity.this)
                                    .load(info_url_image)
//                                                .placeholder(R.drawable.android_launcher)
                                    .fitCenter()
                                    .into(imageView);
                        }

                    }
                });

        dialog.setListener(R.id.btn_close, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean isFirstTime(String id) {
        if (id != null) {
            SharedPreferences mPreferences = this.getSharedPreferences("first_time", Context.MODE_PRIVATE);
            String save_id = mPreferences.getString(id, null);
            if (save_id == null) {
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString(id, id);
                editor.commit();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private boolean product_version(String id) {
        if (id != null) {
            SharedPreferences mPreferences = this.getSharedPreferences("product_version", Context.MODE_PRIVATE);
            String save_id = mPreferences.getString(id, null);
            if (save_id == null) {
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putString(id, id);
                editor.commit();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
