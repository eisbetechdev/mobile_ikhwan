package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.adapter.InfoPenumpangItem;
import com.eisbetech.ikhwanmart.adapter.LayoutKeretaItem1;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.DetailList;
import com.eisbetech.ikhwanmart.entity.InfoPenumpang;
import com.eisbetech.ikhwanmart.entity.Train;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.view.IconicsImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

public class TrainDetailActivity extends AppCompatActivity {
    Train selectedJadwalBerangkat =  new Train();
    Train selectedJadwalPulang =  new Train();

    FastItemAdapter<InfoPenumpangItem> infoPenumpangAdapter;
    User user = User.getUser();
    Boolean isAddPemesanData = false;
    int countPenumpang = 0;
    List<InfoPenumpangItem> infoPenumpangList = new ArrayList<>();
    int DATA_PEMESAN = 2;
    public Realm realm;

    @BindView(R.id.stasiunAsalName)
    protected TextView stasiunAsalName;

    @BindView(R.id.stasiunTujuanName)
    protected TextView stasiunTujuanName;

    @BindView(R.id.tanggal_berangkat)
    protected TextView tanggal_berangkat;

    @BindView(R.id.stasiunAsalCode)
    protected TextView stasiunAsalCode;

    @BindView(R.id.stasiunTujuanCode)
    protected TextView stasiunTujuanCode;

    @BindView(R.id.txt_waktu_berangkat)
    protected TextView txt_waktu_berangkat;

    @BindView(R.id.txt_waktu_sampai)
    protected TextView txt_waktu_sampai;

    @BindView(R.id.txt_keterangan_waktu)
    protected TextView txt_keterangan_waktu;

    @BindView(R.id.listInfoPemesan)
    protected RecyclerView listInfoPemesan;

    @BindView(R.id.listInfoPenumpang)
    protected RecyclerView listInfoPenumpang;

    @BindView(R.id.iconDataPemesan)
    protected IconicsImageView iconDataPemesan;

    @BindView(R.id.labelDataPemesan)
    protected TextView labelDataPemesan;

    @BindView(R.id.jumlahPenumpang)
    protected TextView jumlahPenumpang;

    @BindView(R.id.detail_pulang)
    protected LinearLayout detail_pulang;

    @BindView(R.id.tanggal_pulang)
    protected TextView tanggal_pulang;

    @BindView(R.id.txt_waktu_berangkat_plg)
    protected TextView txt_waktu_berangkat_plg;

    @BindView(R.id.txt_waktu_sampai_plg)
    protected TextView txt_waktu_sampai_plg;

    @BindView(R.id.txt_keterangan_waktu_plg)
    protected TextView txt_keterangan_waktu_plg;

    @BindView(R.id.stasiunAsalCode_plg)
    protected TextView stasiunAsalCode_plg;

    @BindView(R.id.stasiunTujuanCode_plg)
    protected TextView stasiunTujuanCode_plg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.train_selected_detail);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        realm =  RealmController.with(this).getRealm();
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(S.input_order_data);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        Intent intent = getIntent();
        selectedJadwalBerangkat = intent.getParcelableExtra(K.selected_departure_schedule);
        selectedJadwalPulang = intent.getParcelableExtra(K.selected_return_schedule);

        infoPenumpangAdapter =  new FastItemAdapter<>();
        infoPenumpangAdapter.withSelectable(true);
        infoPenumpangAdapter.withOnClickListener(new FastAdapter.OnClickListener<InfoPenumpangItem>() {
            @Override
            public boolean onClick(View v, IAdapter<InfoPenumpangItem> adapter, InfoPenumpangItem
                    item, int position) {
                Intent in = new Intent(getApplicationContext(),
                        TrainEditDataInformationActivity.class);

                item.infoPenumpang.setPosisiPenumpang(position);
                in.putExtra(K.selected_passenger, item.infoPenumpang);
                startActivityForResult(in, 1);

                return false;
            }
        });
        loadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            InfoPenumpang infoPenumpang = data.getParcelableExtra(K.selected_passenger);
            if (requestCode == 1) {
                InfoPenumpangItem infoPenumpangItem = new InfoPenumpangItem(infoPenumpang);

                infoPenumpangList.set(infoPenumpang.getPosisiPenumpang(), infoPenumpangItem);
                infoPenumpangAdapter.clear();
                infoPenumpangAdapter.add(infoPenumpangList);
                infoPenumpangAdapter.notifyDataSetChanged();
            } else {
                if (!realm.isInTransaction()) {
                    realm.beginTransaction();
                }
                user.username = infoPenumpang.getName();
                user.pin = infoPenumpang.getUserId();

                realm.commitTransaction();
                getInfoPemesan(user);

                if (isAddPemesanData){
                    InfoPenumpangItem infoPenumpangItem = new InfoPenumpangItem(infoPenumpang);

                    infoPenumpangList.set(0, infoPenumpangItem);
                    infoPenumpangAdapter.clear();
                    infoPenumpangAdapter.add(infoPenumpangList);
                    infoPenumpangAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void loadData() {
        stasiunAsalCode.setText(selectedJadwalBerangkat.getStasiunAsalCode());
        stasiunAsalName.setText(selectedJadwalBerangkat.getStasiunAsalName());
        stasiunTujuanCode.setText(selectedJadwalBerangkat.getStasiunTujuanCode());
        stasiunTujuanName.setText(selectedJadwalBerangkat.getStasiunTujuanName()) ;
        txt_keterangan_waktu.setText(selectedJadwalBerangkat.getKeterangan_waktu());
        tanggal_berangkat.setText(getFormatTanggal1(selectedJadwalBerangkat.getTglBerangkat()));

        String[] waktuBerangkat = selectedJadwalBerangkat.getWaktu().split("-");
        txt_waktu_berangkat.setText(waktuBerangkat[0].trim());
        txt_waktu_sampai.setText(waktuBerangkat[1].trim());

        if (selectedJadwalPulang == null) {
            detail_pulang.setVisibility(View.GONE);
        } else {
            detail_pulang.setVisibility(View.VISIBLE);
            stasiunAsalCode_plg.setText(selectedJadwalPulang.getStasiunAsalCode());
            stasiunTujuanCode_plg.setText(selectedJadwalPulang.getStasiunTujuanCode());

            String[] waktuPulang = selectedJadwalPulang.getWaktu().split("-");
            txt_waktu_berangkat_plg.setText(waktuPulang[0].trim());
            txt_waktu_sampai_plg.setText(waktuPulang[1].trim());

            txt_keterangan_waktu_plg.setText(selectedJadwalPulang.getKeterangan_waktu());
            tanggal_pulang.setText(getFormatTanggal1(selectedJadwalPulang.getTglPulang()));
        }
        getInfoPemesan(user);
        getInfoPenumpang();
    }

    private String getFormatTanggal1(String stringTgl) {
        SimpleDateFormat mdformat = new SimpleDateFormat("dd  MMMM  yyyy");
        String[] daynames = new String[] {"Minggu", "Senin" , "Selasa", "Rabu", "Kamis" , "Jumat",
                "Sabtu"};
        String result = "";
        Calendar cal   = Calendar.getInstance();
        try {
            cal.setTime(mdformat.parse(stringTgl));
            String simpleDate = mdformat.format(cal.getTime());
            String day = daynames[cal.get(Calendar.DAY_OF_WEEK) - 1];
            result = day + " ," + simpleDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void getInfoPemesan(User user) {
        FastItemAdapter<LayoutKeretaItem1> fastItemAdapter = new FastItemAdapter<>();
        List<LayoutKeretaItem1> infoPemesanList = new ArrayList<>();

        DetailList item = new DetailList();
        item.setKey(K.nama);
        item.setValue(user.fullname);
        LayoutKeretaItem1 layout = new LayoutKeretaItem1(item);
        infoPemesanList.add(layout);

        item = new DetailList();
        item.setKey(K.email);
        item.setValue(user.email);
        layout = new LayoutKeretaItem1(item);
        infoPemesanList.add(layout);

        item = new DetailList();
        item.setKey(K.telp);
        item.setValue(user.phone_number);
        layout = new LayoutKeretaItem1(item);
        infoPemesanList.add(layout);

        item = new DetailList();
        item.setKey(K.id_no);
        if (user.pin == null) {
            item.setValue("-");
        } else {
            item.setValue(user.pin);
        }
        layout = new LayoutKeretaItem1(item);
        infoPemesanList.add(layout);

        fastItemAdapter.clear();
        fastItemAdapter.add(infoPemesanList);
        fastItemAdapter.notifyDataSetChanged();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        listInfoPemesan.setLayoutManager(linearLayoutManager);
        listInfoPemesan.setAdapter(fastItemAdapter);
        listInfoPemesan.setNestedScrollingEnabled(false);
    }

    private void getInfoPenumpang() {
        String jumlahDewasa = selectedJadwalBerangkat.getJumlahDewasa();
        String jumlahBayi = selectedJadwalBerangkat.getJumlahBayi();

        String labelInfoPenumpang;
        if (Integer.parseInt(selectedJadwalBerangkat.getJumlahBayi()) > 0) {
            labelInfoPenumpang = jumlahDewasa + " Dewasa "+ jumlahBayi + " Bayi";
        } else {
            labelInfoPenumpang = jumlahDewasa + " Dewasa";
        }
        jumlahPenumpang.setText(String.format("%s%s", "Info Penumpang : ", labelInfoPenumpang));

        countPenumpang =  + Integer.parseInt(jumlahDewasa) +
                Integer.parseInt(selectedJadwalBerangkat.getJumlahBayi());

        int countDewasa = 0;
        int countBayi = 0;
        for (int i =0; i < countPenumpang ; i++){
            InfoPenumpang infoPenumpang  = new InfoPenumpang();
            if (countDewasa < Integer.parseInt(jumlahDewasa) ) {
                countDewasa++;
                infoPenumpang.setTipePenumpang("Dewasa " + countDewasa);
            } else if(countBayi < Integer.parseInt(selectedJadwalBerangkat.getJumlahBayi())){
                countBayi++;
                infoPenumpang.setTipePenumpang("Bayi " + countBayi);
            }
            InfoPenumpangItem infoPenumpangItem = new InfoPenumpangItem(infoPenumpang);
            infoPenumpangList.add(infoPenumpangItem);
        }

        infoPenumpangAdapter.clear();
        infoPenumpangAdapter.add(infoPenumpangList);
        infoPenumpangAdapter.notifyDataSetChanged();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        listInfoPenumpang.setLayoutManager(linearLayoutManager);
        listInfoPenumpang.setAdapter(infoPenumpangAdapter);
        listInfoPenumpang.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.detail_info)
    protected void loadDetailPesanan() {
        Intent in = new Intent(getApplicationContext(), TrainDetailInfoActivity.class);
        in.putExtra(K.selected_departure_schedule, selectedJadwalBerangkat);
        in.putExtra(K.selected_return_schedule, selectedJadwalPulang);
        startActivity(in);
    }

    @OnClick(R.id.updateDataPemesan)
    protected void updateDataPemesan() {
        InfoPenumpang infoPenumpang  = new InfoPenumpang();
        infoPenumpang.setName(user.fullname);
        infoPenumpang.setIdType("KTP");
        infoPenumpang.setUserId(user.pin);
        infoPenumpang.setTipePenumpang("Dewasa " + "1");

        Intent in = new Intent(getApplicationContext(), TrainEditDataInformationActivity.class);
        in.putExtra(K.selected_passenger, infoPenumpang);
        in.putExtra(K.flag,  "2");
        startActivityForResult(in, 2);
    }

    @OnClick(R.id.tambahPemesanKePenumpang)
    protected void tambahPemesanKePenumpang() {
        IconicsDrawable icon;
        if (!isAddPemesanData) {
            InfoPenumpang infoPenumpang = new InfoPenumpang();
            infoPenumpang.setName(user.fullname);
            infoPenumpang.setIdType("KTP");
            infoPenumpang.setUserId(user.pin);
            infoPenumpang.setTipePenumpang("Dewasa " + "1");

            infoPenumpangList.set(0, new InfoPenumpangItem(infoPenumpang));
            infoPenumpangAdapter.clear();
            infoPenumpangAdapter.add(infoPenumpangList);
            infoPenumpangAdapter.notifyDataSetChanged();

            icon = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_minus_circle);
            labelDataPemesan.setText(S.delete_from_passenger_data);
            isAddPemesanData = true;
        } else {
            InfoPenumpangItem infoPenumpangItem  = new InfoPenumpangItem();
            InfoPenumpang infoPenumpang  = new InfoPenumpang();
            infoPenumpang.setTipePenumpang("Dewasa " + "1");
            infoPenumpangItem = new InfoPenumpangItem(infoPenumpang);

            infoPenumpangList.set(0, infoPenumpangItem);
            infoPenumpangAdapter.clear();
            infoPenumpangAdapter.add(infoPenumpangList);
            infoPenumpangAdapter.notifyDataSetChanged();
            icon = new IconicsDrawable(getApplicationContext(), FontAwesome.Icon.faw_plus_circle);
            labelDataPemesan.setText(S.add_to_passenger_data);
            isAddPemesanData = false;
        }
        iconDataPemesan.setIcon(icon);
    }

    @OnClick(R.id.loadSeats)
    protected void loadSeats(){
        try {
            RequestParams dataParam = new RequestParams();
            dataParam.put("pax[dewasa][0][nama]", "Selena Gomez");
            dataParam.put("pax[dewasa][0][nomor_hp]", "089636190198");
            dataParam.put("pax[dewasa][0][nomor_pengenal]", "23423487234");
            dataParam.put("pax[dewasa][0][tanggal_lahir]", "2015-08-09");
            dataParam.put("pax[dewasa][0][tipe]", "ADULT");

            dataParam.put(K.origin , selectedJadwalBerangkat.getStasiunAsalCode());
            dataParam.put(K.destination , selectedJadwalBerangkat.getStasiunTujuanCode());
            dataParam.put(K.adult_amount , selectedJadwalBerangkat.getJumlahDewasa() );
            dataParam.put(K.infant_amount , selectedJadwalBerangkat.getJumlahBayi());
            dataParam.put(K.date , getFormatTanggaAPI(selectedJadwalBerangkat.getTglBerangkat()));
            dataParam.put(K.class_ , "Ekonomi");

            dataParam.put(K.sub_class , selectedJadwalBerangkat.getTrain_type());
            dataParam.put(K.name , selectedJadwalBerangkat.getTrain_name());
            dataParam.put(K.number , selectedJadwalBerangkat.getTrain_id());
            dataParam.put(K.contact_name , user.fullname);
            dataParam.put(K.contact_phone , user.phone_number);
            dataParam.put(K.contact_email , user.email);
            dataParam.put(K.contact_title , "Mr.");
            dataParam.put(K.pax , "1");

            BisatopupApi.post("/kereta/book", dataParam, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    String a =  responseString;
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject response = new JSONObject(responseString);
                        JSONArray data = response.getJSONArray(K.data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String getFormatTanggaAPI(String stringTgl) {
        SimpleDateFormat mdformat = new SimpleDateFormat("dd  MMMM  yyyy");
        String result = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            cal.setTime(mdformat.parse(stringTgl));
            result = sdf.format(cal.getTime());
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
