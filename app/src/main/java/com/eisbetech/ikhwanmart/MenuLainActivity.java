package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.adapter.HomeMenuAdapter;
import com.eisbetech.ikhwanmart.components.ExpandableHeightGridView;
import com.eisbetech.ikhwanmart.entity.Menu;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuLainActivity extends AppCompatActivity {
    @BindView(R.id.grid_view)
    protected ExpandableHeightGridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Menu Lain");
            getSupportActionBar().setElevation(0);
        }

        List<Menu> allItems = getAllMenu();

        gridView.setExpanded(true);
        HomeMenuAdapter homeMenuAdapter = new HomeMenuAdapter(allItems, MenuLainActivity.this);
        gridView.setAdapter(homeMenuAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;

                switch (i) {
                    case 0:
                        intent = new Intent(MenuLainActivity.this, MainLayout.class);
                        intent.putExtra("id", 205);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(MenuLainActivity.this, MainLayout.class);
                        intent.putExtra("id", 20);
                        intent.putExtra("parent_id", 30);
                        intent.putExtra("product", "gas");
                        intent.setAction("tagihan");
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(MenuLainActivity.this, MainLayout.class);
                        intent.putExtra("id", 237);
                        intent.setAction("voucher_tv");
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(MenuLainActivity.this, MainLayout.class);
                        intent.putExtra("id", 238);
                        intent.setAction("voucher_lain");
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(MenuLainActivity.this, MainLayout.class);
                        intent.putExtra("id", 20);
                        intent.putExtra("parent_id", 156);
                        intent.putExtra("product", "kartu_kredit");
                        intent.setAction("tagihan");
                        startActivity(intent);
                        break;
                    case 5 :
                        Toast.makeText(MenuLainActivity.this, "Mohon maaf, Produk ini sedang dalam pengembangan", Toast.LENGTH_SHORT).show();
                        break;
                    case 6 :
                        Toast.makeText(MenuLainActivity.this, "Mohon maaf, Produk ini sedang dalam pengembangan", Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        });
    }

    private List<Menu> getAllMenu() {
        Drawable drawable = getResources().getDrawable(R.drawable.logo_bisa_2);
        Menu menu = null;
        List<Menu> items = new ArrayList<>();
        Drawable icon = new IconicsDrawable(this, GoogleMaterial.Icon.gmd_menu)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_grab), "Saldo GRABPAY",true));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_gas), "Gas Negara (PGN)",true));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.television), "Voucher TV",true));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_voucher), "Voucher Lainnya",true));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_kartu_kredit), "Kartu Credit",true));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_pesawat), "Pesawat (soon)"));
        items.add(new Menu(null, getResources().getDrawable(R.drawable.train_front), "Kereta Api (Soon)"));
        items.add(new Menu(null, icon, ""));

//        items.add(new Menu(null, drawable, "Konten Bisa"));
        return items;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }
}
