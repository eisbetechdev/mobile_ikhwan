package com.eisbetech.ikhwanmart.components;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionMenu;

/**
 * Created by khadafi on 6/21/2017.
 */

public class FloatingActionMenuInFrameLayoutBehavior extends CoordinatorLayout.Behavior<RelativeLayout> {
    public FloatingActionMenuInFrameLayoutBehavior(Context context, AttributeSet attrs) {
        super();
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, RelativeLayout child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, RelativeLayout fabContainer, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, fabContainer, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);

        FloatingActionMenu fabMenu = (FloatingActionMenu) fabContainer.getChildAt(0);
        if (dyConsumed > 0 && !fabMenu.isMenuButtonHidden()) {
            fabMenu.hideMenuButton(true);
        } else if (dyConsumed < 0 && fabMenu.isMenuButtonHidden()) {
            fabMenu.showMenuButton(true);
        }
    }
}
