package com.eisbetech.ikhwanmart.components;

/**
 * Created by khadafi on 8/25/2016.
 */
public interface SpanLookup {
    /**
     * @return number of spans in a row
     */
    int getSpanCount();

    /**
     * @param itemPosition
     * @return start span for the item at the given adapter position
     */
    int getSpanIndex(int itemPosition);

    /**
     * @param itemPosition
     * @return number of spans the item at the given adapter position occupies
     */
    int getSpanSize(int itemPosition);
}
