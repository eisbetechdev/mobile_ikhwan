package com.eisbetech.ikhwanmart.data.model.detailJuzArabic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailJuzArabicResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DetailJuzArabic data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailJuzArabic getDetailJuzArabic() {
        return data;
    }

    public void setDetailJuzArabic(DetailJuzArabic data) {
        this.data = data;
    }
}
