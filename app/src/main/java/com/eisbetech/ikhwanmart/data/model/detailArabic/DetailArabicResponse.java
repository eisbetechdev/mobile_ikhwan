package com.eisbetech.ikhwanmart.data.model.detailArabic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailArabicResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DetailArabic data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailArabic getDetailArabic() {
        return data;
    }

    public void setDetailArabic(DetailArabic data) {
        this.data = data;
    }
}
