package com.eisbetech.ikhwanmart.data.model.detailJuzArabic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class DetailJuzArabic {
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("ayahs")
    @Expose
    private List<AyahJuz> ayahs = null;
    @SerializedName("surahs")
    @Expose
    private Map<String, SurahJuz> surahs;
    @SerializedName("edition")
    @Expose
    private Edition edition;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public List<AyahJuz> getAyahJuzs() {
        return ayahs;
    }

    public void setAyahJuzs(List<AyahJuz> ayahs) {
        this.ayahs = ayahs;
    }

    public Map<String, SurahJuz> getSurahs() {
        return surahs;
    }

    public void setSurahs(Map<String, SurahJuz> surahs) {
        this.surahs = surahs;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }
}
