package com.eisbetech.ikhwanmart.data.service.modul;

import com.eisbetech.ikhwanmart.Utils.UrlAlQuran;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.DetailJuzArabicResponse;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.DetailJuzTranslateResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JuzService {

    @GET(UrlAlQuran.DETAIL_JUZ_ARABIC)
    Call<DetailJuzArabicResponse> getDetailJuzArabic(
            @Path("id") String id
    );

    @GET(UrlAlQuran.DETAIL_JUZ_TRANSLATE)
    Call<DetailJuzTranslateResponse> getDetailJuzTranslate(
            @Path("id") String id
    );
}
