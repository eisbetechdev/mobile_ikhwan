package com.eisbetech.ikhwanmart.data.model.detailJuzTranslate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailJuzTranslateResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DetailJuzTranslate data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailJuzTranslate getData() {
        return data;
    }

    public void setData(DetailJuzTranslate data) {
        this.data = data;
    }
}
