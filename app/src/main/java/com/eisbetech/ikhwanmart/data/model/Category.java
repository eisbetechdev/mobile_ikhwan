package com.eisbetech.ikhwanmart.data.model;

public class Category {
    private String title;
    private String image;

    public Category(String title, String image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

}
