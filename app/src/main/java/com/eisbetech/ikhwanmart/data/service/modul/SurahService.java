package com.eisbetech.ikhwanmart.data.service.modul;

import com.eisbetech.ikhwanmart.Utils.UrlAlQuran;
import com.eisbetech.ikhwanmart.data.model.detailArabic.DetailArabicResponse;
import com.eisbetech.ikhwanmart.data.model.detailTranslate.DetailTranslateResponse;
import com.eisbetech.ikhwanmart.data.model.surah.SurahResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SurahService {
    @GET(UrlAlQuran.LIST_SURAH)
    Call<SurahResponse> getSurah();

    @GET(UrlAlQuran.DETAIL_SURAH_ARABIC)
    Call<DetailArabicResponse> getDetailArabic(
            @Path("id") String id
    );

    @GET(UrlAlQuran.DETAIL_SURAH_TRANSLATE)
    Call<DetailTranslateResponse> getDetailTranslate(
            @Path("id") String id
    );

}
