package com.eisbetech.ikhwanmart.data.model.detailJuzTranslate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class DetailJuzTranslate {
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("ayahs")
    @Expose
    private List<AyahTranslateJuz> ayahs = null;
    @SerializedName("surahs")
    @Expose
    private Map<String, SurahTranslateJuz> surahs;
    @SerializedName("edition")
    @Expose
    private Edition edition;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public List<AyahTranslateJuz> getAyahs() {
        return ayahs;
    }

    public void setAyahs(List<AyahTranslateJuz> ayahs) {
        this.ayahs = ayahs;
    }

    public Map<String, SurahTranslateJuz> getSurahs() {
        return surahs;
    }

    public void setSurahs(Map<String, SurahTranslateJuz> surahs) {
        this.surahs = surahs;
    }


    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }
}
