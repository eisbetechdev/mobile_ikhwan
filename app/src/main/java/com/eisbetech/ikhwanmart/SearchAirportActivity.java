package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.DataAirportUtils;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchAirportActivity extends AppCompatActivity {
    @BindView(R.id.sv_airport)
    protected SearchView svAirport ;

    @BindView(R.id.lv_airport)
    protected ListView lvAirport ;

    protected List<String> listAirport = new ArrayList<>();
    protected ArrayAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airport);
        ButterKnife.bind(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        svAirport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svAirport.setIconified(false);
            }
        });

        EditText searchEditText = svAirport.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.black));
        searchEditText.setHint(S.station_origin);

        ImageView ivLuv = svAirport.findViewById(R.id.search_button);
        ImageView ivCancel = svAirport.findViewById(R.id.search_close_btn);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ivLuv.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
            ivCancel.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        }
        listAirport = DataAirportUtils.getAirportList();

        svAirport.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newKey) {
                 List<String> newSearchList = new ArrayList<>();
                 for (int i = 0; i < listAirport.size(); i++) {
                     String param = listAirport.get(i);
                     if (param.toUpperCase().contains(newKey.toUpperCase())) {
                         newSearchList.add(param);
                     }
                 }
                 ArrayAdapter<String> listAdapter =
                     new ArrayAdapter<String>(getApplicationContext(), android.R.layout
                             .simple_list_item_1, newSearchList){
                         @NonNull
                         public View getView(int position, View convertView, @NonNull
                                 ViewGroup parent){
                             View view = super.getView(position, convertView, parent);
                             TextView tv = view.findViewById(android.R.id.text1);
                             tv.setTextColor(getResources().getColor(R.color.black));
                             return view;
                         }
                     };
                 lvAirport.setAdapter(listAdapter);
                 return false;
            }
        });
        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                listAirport) {
            @NonNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(getResources().getColor(R.color.black));

                return view;
            }
        };
        lvAirport.setAdapter(listAdapter);
        lvAirport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(K.result, view.toString());

                    String selectedAirport = (String) parent.getItemAtPosition(position);
                    returnIntent.putExtra(K.selected_airport, selectedAirport);
                    setResult(1, returnIntent);
                    finish();
                }
            });
    }

    @OnClick(R.id.tv_close)
    protected void closeButton(){
        onBackPressed();
    }
}
