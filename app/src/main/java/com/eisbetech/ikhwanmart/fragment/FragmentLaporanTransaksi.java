package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.MyMarkerView;
import com.eisbetech.ikhwanmart.database.User;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 8/15/2017.
 */

public class FragmentLaporanTransaksi extends BaseFragmentNew implements DatePickerDialog.OnDateSetListener {

    private String date_from;
    private String date_to;
    private String date_step;

    @BindView(R.id.txt_date_from)
    protected TextView txt_date_from;

    @BindView(R.id.txt_date_to)
    protected TextView txt_date_to;

    @BindView(R.id.txt_transaksi_total)
    protected TextView txt_transaksi_total;

    @BindView(R.id.txt_deposit_total)
    protected TextView txt_deposit_total;

    @BindView(R.id.txt_deposit_hari_ini)
    protected TextView txt_deposit_hari_ini;

    @BindView(R.id.txt_trx_hari_ini)
    protected TextView txt_trx_hari_ini;

    @BindView(R.id.lineChart)
    protected LineChart mChart;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_laporan_transaksi;
    }

    @Override
    protected void LoadView() {

        mChart.setPinchZoom(false);

        XAxis xAxis = mChart.getXAxis();
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawLabels(true);
        xAxis.setGranularity(1f);

        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/muli.ttf");

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines

//        leftAxis.setAxisMaximum(200f);
//        leftAxis.setAxisMinimum(-50f);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setGridColor(Color.parseColor("#FFC8C8C8"));
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);
        mChart.getDescription().setText("Tanggal");

        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        SimpleDateFormat format1 = new SimpleDateFormat("01 MMM yyyy", Locale.US);
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        txt_date_from.setText(format1.format(calendar.getTime()));
        txt_date_to.setText(format2.format(calendar.getTime()));
//        setData(45, 100);
        getData();
    }

    @OnClick(R.id.layout_date_from)
    public void open_date_from() {
        show_date("from", txt_date_from.getText().toString().trim());

    }

    @OnClick(R.id.layout_date_to)
    public void open_date_to() {
        show_date("to", txt_date_to.getText().toString().trim());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setData(ArrayList<Entry> values) {

//        ArrayList<Entry> values = new ArrayList<Entry>();
//
//        for (int i = 0; i < count; i++) {
//
//            float val = (float) (Math.random() * range) + 3;
//            values.add(new Entry(i, val));
//        }

        LineDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Total Penjualan (Rp)");
            set1.setDrawValues(false);
            set1.setDrawIcons(false);

//            set1.setMode(set1.getMode() == LineDataSet.Mode.CUBIC_BEZIER
//                    ? LineDataSet.Mode.LINEAR
//                    :  LineDataSet.Mode.CUBIC_BEZIER);
            // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.parseColor("#00544a"));
//            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1.5f);
//            set1.setCircleRadius(1f);
//            set1.setDrawCircleHole(false);
//
//            set1.setLineWidth(1.75f);
            set1.setCircleRadius(3f);
//            set1.setCircleHoleRadius(0.5f);
            set1.setColor(Color.parseColor("#00544a"));
            set1.setCircleColor(Color.parseColor("#00544a"));
//            set1.setHighLightColor(Color.parseColor("#00544a"));

            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_red);
                set1.setFillDrawable(drawable);
            }
            else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mChart.setData(data);
        }
    }


    public void show_date(String date_step, String date) {
        this.date_step = date_step;
        Calendar now = Calendar.getInstance();


        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
        date = format3.format(calendar.getTime());

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (!date.isEmpty()) {
            String[] dateArr = date.split("-");
            try {
                dpd = DatePickerDialog.newInstance(
                        this,
                        Integer.parseInt(dateArr[2]),
                        Integer.parseInt(dateArr[1]) - 1,
                        Integer.parseInt(dateArr[0])
                );
            } catch (Exception Ec) {
            }
        }

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }



    private void getData() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("date_from", getDateSql(txt_date_from.getText().toString().trim()));
            params.put("date_to", getDateSql(txt_date_to.getText().toString().trim()));

            BisatopupApi.get("/transaksi/grafik",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        txt_deposit_hari_ini.setText(Helper.format_money(object.getString("deposit_hari_ini")));
                        txt_deposit_total.setText(Helper.format_money(object.getString("total_deposit")));
                        txt_trx_hari_ini.setText(Helper.format_money(object.getString("transaksi_hari_ini")));
                        txt_transaksi_total.setText(Helper.format_money(object.getString("total_transaksi")));

                        final JSONArray response = object.getJSONArray("transaksi");
                        ArrayList<Entry> values = new ArrayList<Entry>();

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                values.add(new Entry(obj.getInt("day"), obj.getInt("total")));
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        setData(values);
                        mChart.invalidate();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Data tidak dapat diproses", Toast.LENGTH_SHORT).show();
                    }



                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        String date = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;

        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        date = format3.format(calendar.getTime());

        if (date_step.equals("from")) {
            txt_date_from.setText(date);
        } else {
            txt_date_to.setText(date);
        }

        getData();
    }

    private String  getDateSql(String date) {
        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        date = format3.format(calendar.getTime());


        return date;
    }
}
