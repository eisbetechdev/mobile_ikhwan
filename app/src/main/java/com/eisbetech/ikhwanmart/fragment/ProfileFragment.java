package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.LockPinActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.UpgradeAgentActivity;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.MyGlideEngine;
import com.eisbetech.ikhwanmart.components.MyTextView;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.app.Activity.RESULT_OK;

/**
 * Created by khadafi on 3/22/2017.
 */

public class ProfileFragment extends BaseFragment {

    int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    SessionManager session;

    HashMap<String, String> user;

    @BindView(R.id.txt_status)
    protected MyTextView txt_status;

    @BindView(R.id.txt_user_id)
    protected MyTextView txt_user_id;

    @BindView(R.id.txt_name)
    protected MyTextView txt_name;

    @BindView(R.id.txt_mail_1)
    protected MyTextView txt_mail_1;

    @BindView(R.id.txt_mail_2)
    protected MyTextView txt_mail_2;

    @BindView(R.id.txt_nomor_hp_1)
    protected MyTextView txt_nomor_hp_1;

    @BindView(R.id.txt_nomor_hp_2)
    protected MyTextView txt_nomor_hp_2;

    @BindView(R.id.txt_poin)
    protected MyTextView txt_poin;

    @BindView(R.id.img_profile)
    protected CircleImageView img_profile;

    @BindView(R.id.switch_kunci)
    protected Switch switch_kunci;

    @BindView(R.id.btn_upgrade)
    protected FancyButton btn_upgrade;


    boolean checked_trigger = true;
    int REQUEST_CODE_CHOOSE = 10;

    int state = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, root);
        Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user)
                .color(Color.parseColor("#ffffff")).sizeDp(100);

        img_profile.setImageDrawable(getResources().getDrawable(R.drawable.android_launcher));
        getProfile();
        final User user = User.getUser();

        if (user.is_agent == 1) {
            btn_upgrade.setVisibility(View.GONE);
        } else {
            btn_upgrade.setVisibility(View.VISIBLE);
        }

        if (user.profile_url != null) {
            Uri resultUri = Uri.parse(user.profile_url);
            Drawable image_dr;
            try {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(resultUri);
                image_dr = Drawable.createFromStream(inputStream, resultUri.toString());
            } catch (FileNotFoundException e) {
                image_dr = getResources().getDrawable(R.drawable.android_launcher);
            }

            img_profile.setImageDrawable(image_dr);
        }
        switch_kunci.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (checked_trigger) {
                    if (b) {
                        Intent intent = new Intent(getActivity(), LockPinActivity.class);
                        intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_NEW);
                        startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_NEW);
                    } else {
                        Intent intent = new Intent(getActivity(), LockPinActivity.class);
                        intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                        intent.setAction("Masukkan PIN");
                        startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                    }
                } else {
                    //checked_trigger = true;
                }
            }
        });
        return root;
    }

    @OnClick(R.id.btn_upgrade)
    public void upgrade() {
        startActivity(new Intent(getActivity(), UpgradeAgentActivity.class));
    }

    @OnClick(R.id.img_profile)
    public void choice_image() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            Matisse.from(this)
                    .choose(MimeType.of(MimeType.JPEG, MimeType.PNG))
                    .countable(false)
//                .showSingleMediaType(true)
                    .maxSelectable(1)
                    .theme(R.style.Matisse_Dracula)
//                .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(new MyGlideEngine())
                    .forResult(REQUEST_CODE_CHOOSE);
        }

    }


    private void getProfile() {
        final User user = User.getUser();
        if (user.lock_pin_enable != null) {
            switch_kunci.setChecked(user.lock_pin_enable);
        }
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/profile/index",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONObject data = response.getJSONObject("data");

                        txt_name.setText(data.getString("name"));
                        txt_user_id.setText(data.getString("user_id"));
                        txt_mail_1.setText(data.getString("email"));
                        txt_mail_2.setText(data.getString("email"));
                        txt_nomor_hp_1.setText(data.getString("phone_number"));
                        txt_nomor_hp_2.setText(data.getString("phone_number"));
                        txt_poin.setText(data.getString("poin") + " Poin");


                        int is_agent = data.getInt("is_agent");
                        if (is_agent == 0) {
                            txt_status.setText("(Member)");
                            btn_upgrade.setVisibility(View.VISIBLE);
                        } else {
                            txt_status.setText("(Agen)");
                            btn_upgrade.setVisibility(View.GONE);
                        }

                        String email = data.getString("email");

                        User user = realm.where(User.class).equalTo("email", email).findFirst();


                        if (!realm.isInTransaction()) {
                            realm.beginTransaction();
                        }
                        if (user == null) {
                            user = realm.createObject(User.class, 1);
                        }
                        user.email = email;
                        user.fullname = data.getString("name");
                        user.phone_number = data.getString("phone_number");
                        user.role = data.getString("role");
                        user.poin = data.getInt("poin");
                        user.wallet = data.getString("wallet");
                        //  user.sponsored_by = data.getString("sponsored_by");
                        user.kode_affiliasi = data.getString("kode_affiliasi");
                        user.total_affiliasi = data.getString("total_affiliasi");
                        user.total_pendapatan = data.getString("total_pendapatan");
                        user.profile_url = data.getString("profile_url");
                        user.is_agent = data.getInt("is_agent");
                        user.nama_agen = data.getString("nama_agen");
                        user.jenis_kelamin = data.getString("jenis_kelamin");
                        user.tgl_lahir = data.getString("tgl_lahir");
                        realm.commitTransaction();


                        Drawable image_dr = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user)
                                .color(Color.parseColor("#ffffff")).sizeDp(64);
                        //Glide.clear(img_profile);
//                        Glide.with(getActivity().getApplicationContext()).load(data.getString("profile_url"))
//                                .placeholder(image_dr).dontAnimate()
//                                .into(img_profile);

                        GlideApp.with(getActivity().getApplicationContext())
                                .load(data.getString("profile_url"))
                                .placeholder(R.drawable.android_launcher)
                                .into(img_profile);
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getProfile();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        checked_trigger = false;

        User user = User.getUser();
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == LockPinActivity.RESULT_PIN_INPUT_NEW) {
            String pin_sent = data.getStringExtra("pin");
            Toast.makeText(getActivity(), "Pin anda sudah disimpan", Toast.LENGTH_SHORT).show();

            realm.beginTransaction();
            user.lock_pin_enable = true;
            user.pin = pin_sent;
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
            checked_trigger = true;
            state = 1;
           // switch_kunci.setEnabled(false);
        } else if (resultCode == LockPinActivity.RESULT_PIN_INPUT_CONFIRM) {
            String pin_sent = data.getStringExtra("pin");
            Toast.makeText(getActivity(), "Pin anda sudah disimpan", Toast.LENGTH_SHORT).show();

            realm.beginTransaction();
            user.lock_pin_enable = true;
            user.pin = pin_sent;
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
            checked_trigger = true;
            state = 1;
         //   switch_kunci.setEnabled(false);
        } else if (resultCode == LockPinActivity.RESULT_PIN_INPUT_VALIDATE) {
            boolean validate = data.getBooleanExtra("validate", false);
            checked_trigger = false;
            if (validate) {
                switch_kunci.setChecked(false);

                realm.beginTransaction();
                user.lock_pin_enable = false;
                user.pin = "";
                realm.copyToRealmOrUpdate(user);
                realm.commitTransaction();

            } else {
                switch_kunci.setChecked(user.lock_pin_enable);
            }
            checked_trigger = true;
        } else if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            Log.d("Matisse", "Uris: " + Matisse.obtainResult(data));
            CropImage.activity(Matisse.obtainResult(data).get(0))
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setAspectRatio(1, 1)
                    .start(getContext(), this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                if (!realm.isInTransaction()) {
                    realm.beginTransaction();
                }
                user.profile_url = resultUri.toString();
                realm.commitTransaction();
                Drawable image_dr;
//                try {
//                    InputStream inputStream = getActivity().getContentResolver().openInputStream(resultUri);
//                    image_dr = Drawable.createFromStream(inputStream, resultUri.toString() );
//                } catch (FileNotFoundException e) {
//                    image_dr = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user)
//                            .color(Color.parseColor("#ffffff")).sizeDp(24);
//                }
//
//
//                img_profile.setImageDrawable(image_dr);
//                image_dr = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user)
//                            .color(Color.parseColor("#ffffff")).sizeDp(24);
//                Glide.clear(img_profile);
//                Glide.with(getActivity().getApplicationContext()).load(user.profile_url)
//                        .placeholder(image_dr)
//                        .into(img_profile);

                this.upload_profile(resultUri.getPath(), true);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {

            switch_kunci.setChecked(user.lock_pin_enable);
            checked_trigger = true;
        }
    }

    @OnClick(R.id.layout_change_pwd)
    public void change_password() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("change_password");
        startActivity(i);
    }

    @OnClick(R.id.btn_edit_profile)
    public void edit_profile() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("edit_profile");
        startActivity(i);
    }

    @OnClick(R.id.layout_no_hp)
    public void edit_no_hp() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("edit_no_hp");
        startActivity(i);
    }

    public void upload_profile(String image_path, final boolean is_show_loading) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        ;
        if (is_show_loading && progressDialog != null) {

            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Sedang upload data, mohon menunggu...");
            if (is_show_loading) progressDialog.show();
        }
        try {
            File profile_file_img = new File(image_path);

            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();

            params.put("img", profile_file_img);

            params.setForceMultipartEntityContentType(true);

            BisatopupApi.post("/profile/upload-profile",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (is_show_loading && progressDialog != null) progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        String message = object.getString("message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (is_show_loading && progressDialog != null) progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);
                        boolean error = object.getBoolean("error");
                        if (!error) {

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                           getProfile();

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
