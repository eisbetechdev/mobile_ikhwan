package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 3/24/2017.
 */

public class ChangePasswordFragment extends BaseFragment {
    @BindView(R.id.txt_pwd_lama)
    protected TextView txt_pwd_lama;

    @BindView(R.id.txt_pwd_baru)
    protected TextView txt_pwd_baru;

    @BindView(R.id.txt_pwd_baru_conf)
    protected TextView txt_pwd_baru_conf;

    User user;
    SessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        user = User.getUser();
        session = new SessionManager(getContext(), getActivity());
        return root;
    }

    @OnClick(R.id.btn_submit)
    public void submit() {
        if (txt_pwd_lama.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Password lama tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (txt_pwd_baru.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Password baru tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (txt_pwd_baru_conf.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Password baru tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else if (!txt_pwd_baru_conf.getText().toString().equals(txt_pwd_baru.getText().toString())) {
            Toast.makeText(getActivity(), "Password harus sama", Toast.LENGTH_SHORT).show();
        } else {
            changePassword();
        }
    }

    private void changePassword() {
        final User user = User.getUser();

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("pwd_lama",txt_pwd_lama.getText());
            params.put("pwd_baru", txt_pwd_baru.getText());

            BisatopupApi.post("/user/change-password",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        String message = object.getString("message");
                        if (!error) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {

                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
