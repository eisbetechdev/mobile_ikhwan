package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.Message;
import com.eisbetech.ikhwanmart.item.MessageItem;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by khadafi on 10/28/2016.
 *
 */

public class MessageFragment extends BaseFragment {
    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    private FastItemAdapter<MessageItem> fastItemAdapter;

    @BindView(R.id.list)
    protected RecyclerView list;

    private RealmChangeListener<RealmResults<Message>> changeListener;
    RealmResults<Message> results;

    boolean update_list = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_message, container, false);
        ButterKnife.bind(this, root);
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
//        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);

        final List<MessageItem> messageItems = new ArrayList<>();

        realm.beginTransaction();
        RealmResults<Message> messages = realm.where(Message.class).equalTo("is_read", false).findAll();

        for (int i = 0; i < messages.size(); i++) {

            Message message = messages.get(i);
            message.is_read = true;
            realm.copyToRealmOrUpdate(message);
        }
        realm.commitTransaction();

        results = realm.where(Message.class).findAllAsync();
//        results = results.sort("id", Sort.DESCENDING);
//        for (Message message : results) {
//           messageItems.add(new MessageItem(message));
//        }
//
//        fastItemAdapter.add(messageItems);
//        fastItemAdapter.notifyAdapterDataSetChanged();

        results.addChangeListener(new RealmChangeListener<RealmResults<Message>>() {
            @Override
            public void onChange(RealmResults<Message> element) {
                if (update_list) {
                    messageItems.clear();
                    fastItemAdapter.clear();
                    element = element.sort(K.id, Sort.DESCENDING);

                    for (Message message : element) {
                        messageItems.add(new MessageItem(message));
                    }

                    android_empty.setVisibility(element.size() <= 0 ? View.VISIBLE : View.GONE);
                    fastItemAdapter.add(messageItems);
                    fastItemAdapter.notifyAdapterDataSetChanged();

                    try {
                        ((HomeActivity) getActivity()).refresh_menu();
                    } catch (Exception ec) {
                        ec.printStackTrace();
                    }
                }
            }
        });


        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<MessageItem>() {
            @Override
            public boolean onClick(View v, IAdapter<MessageItem> adapter, final MessageItem item,
                                   int position) {
                TextView txt_message_body = v.findViewById(R.id.txt_message_body);
                txt_message_body.setTypeface(null, Typeface.NORMAL);

                update_list = false;
                item.SetRead();


                try {
                    Intent intent_notif = new Intent(HomeActivity.MESSAGE_NOTIFIER);
                    intent_notif.setAction(S.action_refresh_menu);
                    //send broadcast
                    getActivity().sendBroadcast(intent_notif);
                    ((HomeActivity) getActivity()).refresh_menu();

                    final Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.
                            faw_envelope_square).sizeDp(32).color(Color.WHITE);

//                    new LovelyInfoDialog(getActivity())
//                            .setTopColorRes(R.color.colorPrimary)
//                            .setIcon(img)
//                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
////                        .setNotShowAgainOptionEnabled(0)
//                            .setTitle(item.message.title)
//                            .setMessage(Html.fromHtml(item.message.content + date_str))
//                            //.setMessage(Html.fromHtml("<a href='http://www.google.com'> Googles </a>"))
//                            .show();

                    final LovelyCustomDialog dialog = new LovelyCustomDialog(getActivity())
                            .setView(R.layout.dialog_news)
                            .setTopColorRes(R.color.colorPrimary)
                          //  .setTitle(item.message.title)
                            //.setMessage(Html.fromHtml(item.message.content + date_str))
                            .setIcon(img)
                            .configureView(new LovelyCustomDialog.ViewConfigurator() {

                                public TextView description;
                                public TextView title;
                                public TextView tanggal;
                                public ImageView imageView;
                                @Override
                                public void configureView(View v) {

                                    String date_str = "";
                                    if (item.message.date != null) {
                                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                                        try {
                                            Date date = formatter.parse(item.message.date);
                                            Calendar calendar = Calendar.getInstance();
                                            calendar.setTime(date);
//                        calendar.add(Calendar.HOUR, 3);
                                            SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);

                                            date_str = ""+format2.format(calendar.getTime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    description= (TextView) v.findViewById(R.id.description);
                                    title= (TextView) v.findViewById(R.id.title);
                                    tanggal= (TextView) v.findViewById(R.id.tanggal);
                                    imageView= (ImageView) v.findViewById(R.id.imageView);
                                    title.setText(Html.fromHtml(item.message.title));
                                    tanggal.setText(date_str);
                                    description.setText(Html.fromHtml(item.message.content));
                                    imageView.setVisibility(View.GONE);
                                    if(item.message.image_url != null && !item.message.image_url.isEmpty()){
                                        imageView.setVisibility(View.VISIBLE);

                                        GlideApp.with(getActivity().getApplicationContext())
                                                .load(item.message.image_url)
//                                                .placeholder(R.drawable.android_launcher)
                                                .fitCenter()
                                                .into(imageView);
                                    }

                                    if(item.message.full_content != null && !item.message.full_content.isEmpty()){
                                        description.setText(Html.fromHtml(item.message.full_content));

                                    }


                                }
                            });

                    dialog.setListener(R.id.btn_close, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    update_list = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
        return root;
    }

    private void reload_data() {
        results = realm.where(Message.class).findAllAsync();
    }

    @OnClick(R.id.btn_hapus_message)
    public void hapus() {
        update_list = true;
        Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_question).sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle(S.confirm_delete)
                .setMessage(S.are_u_sure_wanna_delete_all_notif)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final RealmResults<Message> results = realm.where(Message.class).findAll();
                        // All changes to data must happen in a transaction
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                // Delete all matches
                                results.deleteAllFromRealm();
                                reload_data();
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        results.removeChangeListeners();
    }
}
