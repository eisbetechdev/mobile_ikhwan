package com.eisbetech.ikhwanmart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import butterknife.ButterKnife;

/**
 * Created by khadafi on 6/20/2017.
 */

public class HargaFragment extends Fragment {

    public HargaFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_harga, container, false);
        ButterKnife.bind(this, root);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getActivity().getSupportFragmentManager(), FragmentPagerItems.with(getActivity())
                .add("Harga Pulsa", FragmentHargaPulsa.class, new Bundler().putInt("id", 1).get())
                .add("Harga Paket Data", FragmentHargaPulsa.class, new Bundler().putInt("id", 84).get())
                .add("Harga Voucher PLN", FragmentHargaPulsa.class, new Bundler().putInt("id",2).get())
                .add("Harga Voucher Game", FragmentHargaPulsa.class, new Bundler().putInt("id", 12).get())
                .add("Harga Saldo Gojek", FragmentHargaPulsa.class, new Bundler().putInt("id", 167).get())
                .add("Harga Saldo Grab", FragmentHargaPulsa.class, new Bundler().putInt("id", 205).get())
                .add("Harga Voucher TV", FragmentHargaPulsa.class, new Bundler().putInt("id", 237).get())
                .add("Harga Voucher Lain", FragmentHargaPulsa.class, new Bundler().putInt("id", 238).get())
                .add("Harga E-Money", FragmentHargaPulsa.class, new Bundler().putInt("id", 244).get())
                .create());

        ViewPager viewPager = (ViewPager) root.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) root.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
