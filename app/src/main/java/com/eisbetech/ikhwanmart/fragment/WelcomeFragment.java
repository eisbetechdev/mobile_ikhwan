package com.eisbetech.ikhwanmart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.CreateAccountFragment;
import com.eisbetech.ikhwanmart.Login;
import com.eisbetech.ikhwanmart.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * A placeholder fragment containing a simple view.
 */
public class WelcomeFragment extends Fragment {


    @BindView(R.id.btn_Google)
    FancyButton btn_Google;

    public WelcomeFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_welcome, container, false);
        ButterKnife.bind(this, root);


        return root;
    }

    @OnClick(R.id.btn_Google)
    public void LoginGoogle() {
        Login activity = (Login) getActivity();
        activity.signIn();
    }

    @OnClick(R.id.btn_facebook)
    public void LoginFaceook() {
        Login activity = (Login) getActivity();
        activity.FacebookLogin();
    }

    @OnClick(R.id.btn_bantuan)
    public void bantuan() {
        Login activity = (Login) getActivity();
        activity.openHotline();
    }


    @OnClick(R.id.txt_login)
    void createAccount() {
        Login activity = (Login) getActivity();
        CreateAccountFragment createAccountFragment = new CreateAccountFragment();
        activity.replaceFragmentWithAnimation(createAccountFragment, "create_account");
    }

    @OnClick(R.id.btn_create)
    void login() {
        Login activity = (Login) getActivity();
        LoginFragment loginFragment = new LoginFragment();
        activity.replaceFragmentWithAnimation(loginFragment, "login");
    }
}
