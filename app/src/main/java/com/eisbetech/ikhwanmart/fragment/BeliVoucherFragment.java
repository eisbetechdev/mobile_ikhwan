package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.ScanBarcodeActivity;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.AddNumberAdapter;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.ProductDetail;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Nominal;
import com.eisbetech.ikhwanmart.item.NominalItem;
import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 9/7/2016.
 */
public class BeliVoucherFragment extends BaseFragment {

    @BindView(R.id.card_product)
    protected CardView card_product;

    @BindView(R.id.layout_loading)
    protected LinearLayout layout_loading;

    @BindView(R.id.txt_nominal)
    protected TextView txt_nominal;

    @BindView(R.id.txt_info_header)
    protected TextView txt_info_header;

    @BindView(R.id.txt_info)
    protected TextView txt_info;

    @BindView(R.id.txt_phone_number)
    protected MaterialEditText txt_phone_number;

//    @BindView(R.id.txt_product)
//    protected TextView txt_product;

    @BindView(R.id.spinner_jenis_voucher)
    protected MaterialSpinner spinner_jenis_voucher;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.img_product)
    protected ImageView img_product;

    @BindView(R.id.layout_info)
    protected LinearLayout layout_info;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.btn_add_number)
    protected ImageView btn_add_number;

    private FastItemAdapter<NominalItem> fastItemAdapter;

    private int parent_id;
    private String action;
    private String phone_number;
    private Product result;
    protected List<Product> products;
    private String product_name;

    public static int PICK_CONTACT = 10;
    public static final int PERMISSION_REQUEST_CONTACT = 11;
    public static final int BARCODE_ACTIVITY = 123;
    String category;
    AppEventsLogger logger;
    List<String> voucher = new ArrayList<>();

    User user;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    public void add_number(String type) {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", false);
        i.putExtra("parent_id", parent_id);
        i.putExtra("type", type);
        startActivityForResult(i, 1);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_voucher, container, false);
        ButterKnife.bind(this, root);

        layout_info.setVisibility(View.GONE);
        card_product.setVisibility(View.GONE);

        Bundle bundle = getArguments();

        parent_id = bundle.getInt("id", 1);
        action = bundle.getString("action");
        phone_number = bundle.getString("phone_number");


        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);

        user = User.getUser();

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<NominalItem>() {
            @Override
            public boolean onClick(View v, IAdapter<NominalItem> adapter, NominalItem item, int position) {
                String regexStr = "^[+]?[0-9]{8,18}$";
                Helper.hideSoftKeyboard(getActivity());
                if (item.nominal.is_gangguan == 1) {
                    Helper.showMessage(getActivity(), "Produk sedang gangguan, silahkan pilih nominal yang lain.");
                    return false;
                } else {
                    if (txt_phone_number.getText().toString().trim().matches(regexStr)) {
                        Intent i = new Intent(getActivity(), MainLayout.class);
                        i.setAction("checkout");
                        i.putExtra("product", product_name);
                        i.putExtra("parent_id", parent_id);
                        i.putExtra("product_id", item.nominal.id);
                        i.putExtra("nominal", item.nominal.product_name.trim());
                        i.putExtra("price", item.nominal.price);
                        i.putExtra("is_tagihan", false);
                        i.putExtra("no_tujuan", txt_phone_number.getText().toString());
                        i.putExtra("img", result.img_url);
                        i.putExtra("category", category);
                        startActivity(i);
                    } else {
                        Helper.showMessage(getActivity(), "Nomor/rekening tujuan tidak valid");
                    }
                }


                return false;
            }
        });


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        list.setLayoutManager(linearLayoutManager);
//        list.setLayoutManager(gridLayoutManager);
        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);
        txt_nominal.setVisibility(View.GONE);
        // getProduct();

        int product_count = (int) realm.where(Product.class)
                .count();

        if (product_count <= 0) {
            getProduct();
        }

        if (parent_id != 2 && parent_id != 167 && parent_id != 205 && parent_id != 244 && parent_id != 256) {
            RegisterTextChange();
        }

        if (parent_id == 84) {
            category = "paket_data";
        } else {
            category = "pulsa";
        }
        if (parent_id == 2) {
            txt_info_header.setText("Masukkan Nomor Pelanggan listrik prabayar di bawah ini. Contoh : 34012330445.");
            category = "token";
        } else if (parent_id == 244) {
            txt_info_header.setText("Masukkan 16 digit nomor kartu E-Money atau E-Toll Anda di kolom Nomor Pengisian di bawah ini.");
        } else if (parent_id == 167) {
            txt_info_header.setText("Masukkan nomor handphone yang terdaftar di aplikasi Gojek, Contoh : 085212342377.");
        } else if (parent_id == 205) {
            txt_info_header.setText("Masukkan nomor handphone yang terdaftar di aplikasi Grab, Contoh : 085212342377.");
        } else if (parent_id == 256) {
            txt_info_header.setText("Masukkan 16 digit nomor kartu BNI Tapcash Anda di kolom Nomor Pengisian di bawah ini.");
        }


        if (phone_number != null && phone_number != "") {
            txt_phone_number.setText(phone_number);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            RegisterTextChange();
            RegisterOnFocusChange();
        }

        logger = AppEventsLogger.newLogger(getActivity());
        Bundle parameters = new Bundle();
        parameters.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "IDR");
        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "BELI_VOUCHER_PULSA");

        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, 0, parameters);

        spinner_jenis_voucher.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                result = realm.where(Product.class)
                        .equalTo("product_name", (String) item)
                        .findFirst();

                showProduct();

            }
        });

        // Transaction was a success.
        if (parent_id == 2 || parent_id == 167 || parent_id == 205 || parent_id == 244 || parent_id == 256) {
            result = realm.where(Product.class)
                    .equalTo("parent_id", parent_id)
                    .findFirst();


            card_product.setVisibility(View.GONE);
            showProduct();
        }

        return root;
    }


    public void camera_permission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            } else {
                Intent intent = new Intent(getActivity(), ScanBarcodeActivity.class);
                startActivityForResult(intent, BARCODE_ACTIVITY);
            }
        } else {
            Intent intent = new Intent(getActivity(), ScanBarcodeActivity.class);
            startActivityForResult(intent, BARCODE_ACTIVITY);
        }

    }

    @OnClick(R.id.btn_add_number)
    public void show_add_dialog() {
        Helper.hideSoftKeyboard(getActivity());
        Holder holder = new ListHolder();
        List<String> menu = new ArrayList<>();
        menu.add("Nomor Pribadi");
        menu.add("Dari Kontak");
        menu.add("Riwayat Transaksi");
        menu.add("Nomor Favorite");
//        menu.add("Scan Barcode");

        AddNumberAdapter addNumberAdapter = new AddNumberAdapter(getActivity(), menu);

        DialogPlus dialogPlus = DialogPlus.newDialog(getContext())
                .setAdapter(addNumberAdapter)
                .setContentHolder(holder)
                .setHeader(R.layout.dialog_header_plus)
                .setOverlayBackgroundResource(R.color.grey_transparent)
                .setCancelable(true)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        dialog.dismiss();
                        switch (position) {
                            case 0:
                                txt_phone_number.setText(user.phone_number);
                                break;
                            case 1:
                                askForContactPermission();
                                break;
                            case 2:
                                add_number("history");
                                break;
                            case 3:
                                add_number("favorite");
                                break;
                            case 4:
                                camera_permission();
                                break;
                            default:
                                break;
                        }
                    }
                })

                .create();

        dialogPlus.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon;
        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_help)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        menu.add(0, 1, Menu.NONE, "Help").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                if (parent_id == 2) {
                    Helper.ShowFaq(getActivity(), "token_listrik", "");
                } else if (parent_id == 167) {
                    Helper.ShowFaq(getActivity(), "isi_gojek", "");
                } else if (parent_id == 205) {
                    Helper.ShowFaq(getActivity(), "isi_grab", "");
                } else if (parent_id == 244) {
                    Helper.ShowFaq(getActivity(), "isi_emoney", "");
                } else if (parent_id == 256) {
                    Helper.ShowFaq(getActivity(), "isi_bni_tapcash", "");
                } else {
                    Helper.ShowFaq(getActivity(), "beli_pulsa_paket_data", "");
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void RegisterOnFocusChange() {

        try {
            txt_phone_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                boolean isTyping = false;
                private Timer timer = new Timer();
                private final long DELAY = 1000; // milliseconds

                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        final String text = txt_phone_number.getText().toString();
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        try {
                                            isTyping = false;

                                            if (getActivity() != null) {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (!text.trim().equals("")) {
                                                            FindNominal();
                                                        } else {
                                                            card_product.setVisibility(View.GONE);
                                                            list.setVisibility(View.GONE);
                                                            txt_nominal.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }
                                        } catch (Exception Rc) {
                                        }
                                    }
                                },
                                DELAY
                        );
                    }
                }
            });
        } catch (Exception ec) {
            ec.printStackTrace();
        }
    }

    private void RegisterTextChange() {

        txt_phone_number.addTextChangedListener(new TextWatcher() {
            boolean isTyping = false;
            private Timer timer = new Timer();
            private final long DELAY = 1000; // milliseconds

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    final String text = editable.toString();
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    isTyping = false;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (!text.trim().equals("")) {
                                                FindNominal();
                                            } else {
                                                card_product.setVisibility(View.GONE);
                                                list.setVisibility(View.GONE);
                                                txt_nominal.setVisibility(View.GONE);
                                            }
                                        }
                                    });
                                }
                            },
                            DELAY
                    );
                } catch (Exception ec) {
                    Toast.makeText(getActivity(), ec.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void FindNominal() {

        try {
            if (parent_id == 84) {
                if (txt_phone_number.getText().toString().substring(0, 3).equals("999")) {
                    String prefix = txt_phone_number.getText().toString().substring(0, 3);
                    result = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 3))
                            .equalTo("parent_id", parent_id)
                            .findFirst();
                    products = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 3))
                            .equalTo("parent_id", parent_id)
                            .findAllSorted("order_num");

                    voucher = new ArrayList<>();


                    for (Product product : products) {
                        voucher.add(product.product_name);

                    }

                    if (voucher.size() > 0) {
                        spinner_jenis_voucher.setItems(voucher);
                        spinner_jenis_voucher.setSelectedIndex(0);
                        showProduct();
                    } else {
                        fastItemAdapter.clear();
                        card_product.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                        txt_nominal.setVisibility(View.GONE);
                        Helper.show_alert("Error", "Produk tidak ditemukan", getActivity());
                        getProductByPrefix(prefix);
                    }
                } else if (txt_phone_number.getText().length() >= 4) {
                    String prefix = txt_phone_number.getText().toString().substring(0, 4);
                    result = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 4))
                            .equalTo("parent_id", parent_id)
                            .findFirst();
                    products = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 4))
                            .equalTo("parent_id", parent_id)
                            .findAllSorted("order_num");


                    voucher = new ArrayList<>();

                    for (Product product : products) {
                        voucher.add(product.product_name);

                    }


                    if (voucher.size() > 0) {
                        spinner_jenis_voucher.setItems(voucher);
                        spinner_jenis_voucher.setSelectedIndex(0);
                        showProduct();
                    } else {
                        fastItemAdapter.clear();
                        card_product.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                        txt_nominal.setVisibility(View.GONE);
                        Helper.show_alert("Error", "Produk tidak ditemukan", getActivity());
                        getProductByPrefix(prefix);
                    }
                } else {
                    card_product.setVisibility(View.GONE);
                    list.setVisibility(View.GONE);
                    txt_nominal.setVisibility(View.GONE);
                }
            } else {
                if (txt_phone_number.getText().toString().substring(0, 3).equals("999")) {
                    String prefix = txt_phone_number.getText().toString().substring(0, 3);

                    result = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 3))
                            .equalTo("parent_id", parent_id)
                            .findFirst();
                    products = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 3))
                            .equalTo("parent_id", parent_id)
                            .findAll();

                    voucher = new ArrayList<>();


                    for (Product product : products) {
                        voucher.add(product.product_name);

                    }

                    if (voucher.size() > 0) {
                        spinner_jenis_voucher.setItems(voucher);
                        spinner_jenis_voucher.setSelectedIndex(0);
                        showProduct();
                    } else {
                        fastItemAdapter.clear();
                        card_product.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                        txt_nominal.setVisibility(View.GONE);
                        Helper.show_alert("Error", "Produk tidak ditemukan", getActivity());
                        getProductByPrefix(prefix);
                    }
                } else if (txt_phone_number.getText().length() >= 4) {
                    String prefix = txt_phone_number.getText().toString().substring(0, 4);

                    result = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 4))
                            .equalTo("parent_id", parent_id)
                            .findFirst();
                    products = realm.where(Product.class)
                            .contains("prefix", txt_phone_number.getText().toString().substring(0, 4))
                            .equalTo("parent_id", parent_id)
                            .findAll();

                    voucher = new ArrayList<>();

                    for (Product product : products) {
                        voucher.add(product.product_name);

                    }

                    if (voucher.size() > 0) {
                        spinner_jenis_voucher.setItems(voucher);
                        spinner_jenis_voucher.setSelectedIndex(0);
                        showProduct();
                    } else {
                        fastItemAdapter.clear();
                        card_product.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                        txt_nominal.setVisibility(View.GONE);
                        Helper.show_alert("Error", "Produk tidak ditemukan", getActivity());
                        getProductByPrefix(prefix);
                    }
                } else {
                    card_product.setVisibility(View.GONE);
                    list.setVisibility(View.GONE);
                    txt_nominal.setVisibility(View.GONE);
                }
            }
        } catch (Exception ec) {
            Crashlytics.logException(ec);
            ;
        }

    }

    private void showProduct() {
        // Helper.hideSoftKeyboard(getActivity());
        if (voucher.size() > 0) {
            card_product.setVisibility(View.VISIBLE);
        } else {
            card_product.setVisibility(View.GONE);
        }
        txt_nominal.setVisibility(View.VISIBLE);
        list.setVisibility(View.VISIBLE);
        fastItemAdapter.clear();


        if (result != null) {
            Log.i("Product", "Ketemu : " + result.product_name);
//            txt_product.setText(result.product_name);
            product_name = result.product_name;
            if (result.img_url != null && !result.img_url.isEmpty()) {
//                Glide.with(img_product).clear(img_product);
//                Glide.clear(img_product);
                GlideApp.with(getActivity().getApplicationContext()).clear(img_product);
//                Glide.with(getActivity().getApplicationContext()).load(result.img_url)
//                        .placeholder(R.drawable.android_launcher)
//                        .fitCenter()
//                        .into(img_product);

                GlideApp.with(getActivity().getApplicationContext())
                        .load(result.img_url)
                        .placeholder(R.drawable.android_launcher)
                        .fitCenter()
                        .into(img_product);


            } else {
                GlideApp.with(getActivity().getApplicationContext()).clear(img_product);
//                Glide.clear(img_product);
            }

            if (result.description != null && !result.description.isEmpty() && !result.description.trim().equals("") && !result.description.equals("null")) {
                layout_info.setVisibility(View.VISIBLE);
                txt_info.setText(Helper.fromHtml("Info : <br> " + result.description));
            } else {
                layout_info.setVisibility(View.GONE);
            }

            RealmResults<ProductDetail> productDetails = realm.where(ProductDetail.class)
                    .equalTo("product_id", result.id)
                    .findAll();

            if (productDetails.size() <= 0) {
                // getNominal(result.id, true);
            } else {
                //getNominalOffline(result.id);
            }
            if (Helper.isNowConnected(getContext())) {
                getNominal(result.id, true);
            } else {
                getNominalOffline(result.id);
                Helper.show_alert("Koneksi internet bermasalah", "Koneksi internet anda sedang bermasalah, mohon periksa settingan internet anda.", getActivity());
            }

        }
    }

    private void getProduct() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        layout_loading.setVisibility(View.VISIBLE);
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/product/all", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.GONE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.GONE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);


                        Product.ClearData();

                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt("product_id");
                                        product.parent_id = obj.getInt("parent_id");
                                        product.product_name = obj.getString("product_name");
                                        product.product = obj.getString("product");
                                        product.code = obj.getString("code");
                                        product.prefix = obj.getString("prefix");
                                        product.description = obj.getString("description");
                                        product.img_url = obj.getString("img_url");

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction was a success.
                                if (parent_id == 2 || parent_id == 167 || parent_id == 205 || parent_id == 244 || parent_id == 256) {
                                    result = realm.where(Product.class)
                                            .equalTo("parent_id", parent_id)
                                            .findFirst();


                                    card_product.setVisibility(View.GONE);
                                    showProduct();
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getProductByPrefix(String prefix) {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        layout_loading.setVisibility(View.VISIBLE);
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/product/prefix2/" + prefix, params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.GONE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    layout_loading.setVisibility(View.GONE);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        JSONObject object = new JSONObject(responseString);

                        final JSONArray response = object.getJSONArray("data");

                        voucher = new ArrayList<>();
                        //Product.ClearData();

                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt("product_id");
                                        product.parent_id = obj.getInt("parent_id");
                                        product.product_name = obj.getString("product_name");
                                        product.product = obj.getString("product");
                                        product.code = obj.getString("code");
                                        product.prefix = obj.getString("prefix");
                                        product.description = obj.getString("description");
                                        product.img_url = obj.getString("img_url");

                                        bgrealm.copyToRealmOrUpdate(product);

                                        voucher.add(product.product_name);

                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction was a success.
                                if (parent_id == 1 || parent_id == 2 || parent_id == 167 || parent_id == 205 || parent_id == 244 || parent_id == 256) {
                                    result = realm.where(Product.class)
                                            .equalTo("parent_id", parent_id)
                                            .findFirst();


                                    card_product.setVisibility(View.GONE);
                                    if (voucher.size() > 0) {
                                        spinner_jenis_voucher.setItems(voucher);
                                        spinner_jenis_voucher.setSelectedIndex(0);
                                        showProduct();

                                    } else {
                                        Helper.show_alert("Error", "Produk tidak ditemukan", getActivity());

                                    }
                                }

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getNominal(final int id, final boolean showloading) {
        final User user = User.getUser();
        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();


            BisatopupApi.get("/product/detail/" + id, params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    android_empty.setVisibility(View.GONE);

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {


                        JSONArray data = new JSONArray(responseString);

                        List<NominalItem> nominalItems = new ArrayList<>();


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Nominal nominal = new Nominal();
                            nominal.product_name = obj.getString("name");
                            nominal.price = obj.getString("price");
                            if (user.is_agent == 1) {
                                nominal.price = obj.getString("base_price");
                            }
                            nominal.id = obj.getString("product_detail_id");
                            nominal.desc = obj.getString("desc");
                            nominal.is_gangguan = obj.getInt("is_gangguan");

                            nominalItems.add(new NominalItem(nominal));

                            ProductDetail productDetail = new ProductDetail();
                            productDetail.product_id = obj.getInt("product_id");
                            productDetail.id = Integer.parseInt(nominal.id);
                            productDetail.is_gangguan = nominal.is_gangguan;
                            productDetail.price = obj.getDouble("price");
                            productDetail.base_price = obj.getDouble("base_price");
                            productDetail.product_name = obj.getString("name");
                            productDetail.is_active = obj.getInt("is_active");
                            productDetail.desc = nominal.desc;
                            ProductDetail.AddOrUpdate(productDetail);
                        }

                        fastItemAdapter.add(nominalItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        android_empty.setVisibility(View.VISIBLE);
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getNominalOffline(int id) {
        String fieldNames[] = {"is_gangguan", "grup", "price"};
        Sort sort[] = {Sort.ASCENDING, Sort.ASCENDING, Sort.ASCENDING};
        RealmResults<ProductDetail> productDetails = realm.where(ProductDetail.class)
                .equalTo("product_id", result.id)
                .equalTo("is_active", 1)
                .sort(fieldNames, sort).findAllAsync();


        productDetails.addChangeListener(new RealmChangeListener<RealmResults<ProductDetail>>() {
            @Override
            public void onChange(RealmResults<ProductDetail> productDetails) {
                List<NominalItem> nominalItems = new ArrayList<>();
                fastItemAdapter.clear();
                for (ProductDetail productDetail :
                        productDetails) {

                    Nominal nominal = new Nominal();
                    nominal.product_name = productDetail.product_name;
                    nominal.price = String.valueOf(productDetail.price);
                    if (user.is_agent == 1) {
                        nominal.price = String.valueOf(productDetail.base_price);
                    }
                    nominal.id = String.valueOf(productDetail.id);
                    nominal.desc = productDetail.desc;
                    nominal.is_gangguan = productDetail.is_gangguan;

                    nominalItems.add(new NominalItem(nominal));
                }

                fastItemAdapter.add(nominalItems);
                fastItemAdapter.notifyAdapterDataSetChanged();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == getActivity().RESULT_OK || resultCode == -1) {

                Uri contactData = data.getData();

                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                Log.i("phoneNUmber", "The phone number is " + number);


                number = number.replace("+", "");
                number = number.replace(" ", "");
                number = number.replace("-", "");

                if (number.substring(0, 2).equals("62")) {
                    number = "0" + number.substring(2);
                }

                if (number.substring(0, 1).equals("8")) {
                    number = "0" + number;
                }
                txt_phone_number.setText(number);
                txt_phone_number.setText("");
                if (!TextUtils.isEmpty(number)) {
                    txt_phone_number.append(number);
                }
                list.requestFocus();

                // Do something with the phone number...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (data != null) {
            String result = data.getStringExtra("result");

            txt_phone_number.setText(result);
            txt_phone_number.setText("");
            if (!TextUtils.isEmpty(result)) {
                txt_phone_number.append(result);
            }

            list.requestFocus();
        }
    }

    private void getContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getContact();
            }
        } else {
            getContact();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContact();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                    ;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_CAMERA_REQUEST_CODE: {
                Intent intent = new Intent(getActivity(), ScanBarcodeActivity.class);
                startActivityForResult(intent, BARCODE_ACTIVITY);
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
