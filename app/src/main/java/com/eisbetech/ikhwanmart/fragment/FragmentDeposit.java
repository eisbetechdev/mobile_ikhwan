package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.VirtualAccountActivity;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Payment;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 12/23/2016.
 */

public class FragmentDeposit extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.radio_bca)
    protected RadioButton radio_bca;

    /*@BindView(R.id.radio_bni)
    protected RadioButton radio_bni;

    @BindView(R.id.radio_bri)
    protected RadioButton radio_bri;

    @BindView(R.id.radio_bsm)
    protected RadioButton radio_bsm;


    @BindView(R.id.radio_cimb)
    protected RadioButton radio_cimb;

    @BindView(R.id.radio_mandiri)
    protected RadioButton radio_mandiri;*/

    @BindView(R.id.radio_bca_va)
    protected RadioButton radio_bca_va;

    /*@BindView(R.id.radio_bni_va)
    protected RadioButton radio_bni_va;

    @BindView(R.id.radio_bri_va)
    protected RadioButton radio_bri_va;

    @BindView(R.id.radio_mandiri_va)
    protected RadioButton radio_mandiri_va;

    @BindView(R.id.radio_cimb_va)
    protected RadioButton radio_cimb_va;

    @BindView(R.id.radio_danamon_va)
    protected RadioButton radio_danamon_va;

    @BindView(R.id.radio_permata_va)
    protected RadioButton radio_permata_va;

    @BindView(R.id.radio_maybank_va)
    protected RadioButton radio_maybank_va;

    @BindView(R.id.radio_doku)
    protected RadioButton radio_doku;

    @BindView(R.id.radio_minimarket)
    protected RadioButton radio_minimarket;

    @BindView(R.id.radio_atm)
    protected RadioButton radio_atm;*/

    @BindView(R.id.txt_nominal)
    protected MaterialEditText txt_nominal;

    @BindView(R.id.txt_info_va)
    protected TextView txt_info;

    @BindView(R.id.btn_buy)
    protected FancyButton btn_buy;

    protected int payment_id;
    protected String product_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_topup, container, false);
        ButterKnife.bind(this, root);

        radio_bca.setChecked(true);
        radio_bca.setOnClickListener(this);
        /*radio_bni.setOnClickListener(this);
        radio_bri.setOnClickListener(this);
        radio_bsm.setOnClickListener(this);
        radio_cimb.setOnClickListener(this);*/

        radio_bca_va.setOnClickListener(this);
        /*radio_bni_va.setOnClickListener(this);
        radio_bri_va.setOnClickListener(this);
        radio_mandiri_va.setOnClickListener(this);
        radio_cimb_va.setOnClickListener(this);
        radio_permata_va.setOnClickListener(this);
        radio_danamon_va.setOnClickListener(this);
        radio_maybank_va.setOnClickListener(this);

        radio_mandiri.setOnClickListener(this);
        radio_doku.setOnClickListener(this);
        radio_minimarket.setOnClickListener(this);
        radio_atm.setOnClickListener(this);*/

        payment_id = 1;
        txt_info.setText(Helper.fromHtml(User.getUser().is_agent == 1 ? S.info_agent :
                S.info_member));

//        txt_nominal.addTextChangedListener(new NumberTextWatcher(txt_nominal, "#.###"));
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_help)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        menu.add(0, 1, Menu.NONE, "Help").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Helper.ShowFaq(getActivity(), S.deposit, S.how_to_topup);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        radio_bca.setChecked(false);
        /*radio_bni.setChecked(false);
        radio_bri.setChecked(false);
        radio_mandiri.setChecked(false);
        radio_cimb.setChecked(false);*/

        radio_bca_va.setChecked(false);
        /*radio_bni_va.setChecked(false);
        radio_bri_va.setChecked(false);
        radio_mandiri_va.setChecked(false);

        radio_cimb_va.setChecked(false);
        radio_permata_va.setChecked(false);
        radio_danamon_va.setChecked(false);
        radio_maybank_va.setChecked(false);

        radio_bsm.setChecked(false);

        radio_minimarket.setChecked(false);
        radio_doku.setChecked(false);
        radio_atm.setChecked(false);*/

        RadioButton thisradio = (RadioButton)view;
        thisradio.setChecked(true);
        switch (view.getId()) {
            case R.id.radio_bca:
                payment_id = I.payment_bca;
                break;
            /*case R.id.radio_mandiri:
                payment_id = I.payment_mandiri;
                break;
            case R.id.radio_bni:
                payment_id = I.payment_bni;
                break;
            case R.id.radio_bri:
                payment_id = I.payment_bri;
                break;
            case R.id.radio_bsm:
                payment_id = I.payment_bsm;
                break;
            case R.id.radio_cimb:
                payment_id = 21;
                break;
            case R.id.radio_wallet:
                payment_id = I.payment_wallet;
                break;
            case R.id.radio_doku:
                payment_id = I.payment_doku;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_minimarket:
                payment_id = I.payment_minimarket;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_atm:
                payment_id = I.payment_atm;
                // txt_info.setVisibility(View.GONE);
                break;*/
            case R.id.radio_bca_va:
                payment_id = I.payment_bca_va;
                // txt_info.setVisibility(View.GONE);
                break;
            /*case R.id.radio_bni_va:
                payment_id = I.payment_bni_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_bri_va:
                payment_id = I.payment_bri_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_mandiri_va:
                payment_id = I.payment_mandiri_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_cimb_va:
                payment_id = I.payment_cimb_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_danamon_va:
                payment_id = I.payment_danamon_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_permata_va:
                payment_id = I.payment_permata_va;
                // txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_maybank_va:
                payment_id = I.payment_maybank_va;
                // txt_info.setVisibility(View.GONE);
                break;*/
        }
    }

    @OnClick(R.id.btn_buy)
    public void Buy() {
        btn_buy.setEnabled(false);
        deposit();
    }

    private void deposit() {
        Helper.hideSoftKeyboard(getActivity());
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            String nominal = txt_nominal.getText().toString();
            nominal = nominal.replace(".","");
            nominal = nominal.replace(",","");

            params.put(K.email, user.email);
            params.put(K.nominal, nominal);
            params.put(K.payment_method, payment_id);

            BisatopupApi.post("/transaksi/deposit",  params, getContext(),
                    new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);

                    try {
                        final JSONObject response = new JSONObject(responseString);
                        if (response.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), response.getString(K.message));
                        } else {
                            final JSONObject obj = response.getJSONObject(K.transaction);
                            Transaksi transaksi = new Transaksi();
                            transaksi.trans_id = obj.getString(K.trans_id);
                            transaksi.tanggal = obj.getString(K.date);
                            transaksi.time = obj.getString(K.time);
                            transaksi.harga = obj.getString(K.harga);
                            transaksi.product = obj.getString(K.product);
                            transaksi.product_name = obj.getString(K.product_name);
                            transaksi.product_detail = obj.getString(K.product_detail);
                            transaksi.product_id = obj.getInt(K.product_id);
                            transaksi.no_pelanggan = obj.getString(K.no_pelanggan);
                            transaksi.status = obj.getString(K.status);
                            transaksi.token = obj.getString(K.token);
                            transaksi.payment = obj.getString(K.payment);
                            transaksi.status_id = obj.getInt(K.status_id);
                            transaksi.base_price = obj.getString(K.base_price);
                            transaksi.kode_unik = obj.getString(K.unique_code);
                            transaksi.expired_date = obj.getString(K.expired_date);
                            transaksi.note = obj.getString(K.note);
                            transaksi.admin_fee = obj.getString(K.admin_fee);
                            transaksi.status_color = obj.getString("status_color");


                            JSONObject pay_ojb = obj.getJSONObject(K.pembayaran);

                            Payment payment = new Payment();
                            payment.rekening = pay_ojb.getString(K.rekening);
                            payment.name = pay_ojb.getString(K.name);
                            payment.bank = pay_ojb.getString(K.bank);
                            payment.image_url = pay_ojb.getString("image_url");
                            payment.id = pay_ojb.getInt(K.payment_id);

                            transaksi.payment_method = payment;

                            getActivity().finish();

                            Intent in = new Intent(S.clear_stack_activity);
                            in.setType(S.text_plain);
                            getActivity().sendBroadcast(in);

                            Intent intent = new Intent(getActivity(), MainLayout.class);
                            intent.setAction(S.action_detail);
                            intent.putExtra(K.trans_id, transaksi);
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txt_info_va)
    public void gotoVa(){
        Intent intent = new Intent(getActivity(), VirtualAccountActivity.class);
        startActivity(intent);
    }
}
