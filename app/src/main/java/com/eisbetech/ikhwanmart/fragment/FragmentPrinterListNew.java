package com.eisbetech.ikhwanmart.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Device;
import com.eisbetech.ikhwanmart.item.DeviceItem;
import com.eisbetech.ikhwanmart.printer_command.BTPrinting;
import com.crashlytics.android.Crashlytics;
import com.lvrenyang.io.IOCallBack;
import com.lvrenyang.io.Pos;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 7/8/2017.
 */

public class FragmentPrinterListNew extends BaseFragment implements IOCallBack {

    public static int nPrintWidth = 384;
    public static boolean bCutter = false;
    public static boolean bDrawer = false;
    public static boolean bBeeper = true;
    public static int nPrintCount = 1;
    public static int nCompressMethod = 0;

    static public final int REQUEST_CONNECT_BT = 0x2300;
    ProgressDialog progressDialog;
    static private final int REQUEST_ENABLE_BT = 0x1000;
    public static String uid = "00001101-0000-1000-8000-00805F9B34FB";

    List<DeviceItem> deviceItems;
    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.btn_refresh)
    protected FancyButton btn_refresh;

    @BindView(R.id.layout_loading)
    LinearLayout layout_loading;

    private FastItemAdapter<DeviceItem> fastItemAdapter;
    static private ArrayAdapter<BluetoothDevice> btDevices = null;

    private BroadcastReceiver broadcastReceiver = null;
    private IntentFilter intentFilter = null;
    static private BluetoothAdapter mBluetoothAdapter = null;
    static private BluetoothSocket mbtSocket = null;


    User user;

    private Activity mActivity;
    ExecutorService es = Executors.newScheduledThreadPool(30);
    Pos mPos = new Pos();
    BTPrinting mBt = new BTPrinting();

    private static String TAG = "SearchBTActivity";

    String text_to_print;

    String nama_agen = "";
    String sn = "";
    boolean need_print_sn = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mActivity = getActivity();
        progressDialog = new ProgressDialog(getActivity());

        Bundle bundle = getArguments();

        if (bundle.getString("data") != null) {
            text_to_print = bundle.getString("data");
        }
        if (bundle.getString("sn") != null) {
            sn = bundle.getString("sn");
        }

        need_print_sn = bundle.getBoolean("need_print_sn", false);


        user = User.getUser();
        if (user.nama_agen != null) {
            nama_agen = user.nama_agen;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_device, container, false);
        ButterKnife.bind(this, root);
        layout_loading.setVisibility(View.GONE);
        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);


        mPos.Set(mBt);
        mBt.SetCallBack(this);
        initBroadcast();

        try {
            if (initDevicesList(false) != 0) {

            }

        } catch (Exception ex) {
            ex.printStackTrace();

        }



        return root;
    }

    private void initBroadcast() {
        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                // TODO Auto-generated method stub
                String action = intent.getAction();
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    try {
                        if (btDevices == null) {
                            btDevices = new ArrayAdapter<BluetoothDevice>(
                                    getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1);
                        }

                        if (btDevices.getPosition(device) < 0) {
                            btDevices.add(device);
//                        mArrayAdapter.add(device.getName() + "\n"
//                                + device.getAddress() + "\n" );
//                        mArrayAdapter.notifyDataSetInvalidated();

                            List<DeviceItem> deviceItems = new ArrayList<>();

                            Device device1 = new Device();
                            device1.name = device.getName();
                            device1.address = device.getAddress();
                            deviceItems.add(new DeviceItem(device1));

                            fastItemAdapter.add(deviceItems);
                            fastItemAdapter.notifyAdapterDataSetChanged();
                        }
                    } catch (Exception ex) {
                        // ex.fillInStackTrace();
                    }

                } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED
                        .equals(action)) {
                    layout_loading.setVisibility(View.VISIBLE);
                } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                        .equals(action)) {
                    layout_loading.setVisibility(View.GONE);
                }

            }

        };
        intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    private void uninitBroadcast() {
        if (broadcastReceiver != null)
            getActivity().unregisterReceiver(broadcastReceiver);
    }

    private int initDevicesList(boolean find) {
        deviceItems = new ArrayList<>();
        flushData();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getContext(),
                    "Bluetooth not supported!!", Toast.LENGTH_LONG).show();
            return -1;
        }

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        Setup();
//

        Intent enableBtIntent = new Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE);
        try {
            if (btDevices == null) {
                btDevices = new ArrayAdapter<BluetoothDevice>(
                        getContext(), android.R.layout.simple_list_item_1, android.R.id.text1);
            }
            if (mBluetoothAdapter.isEnabled()) {
                if(find){
                    mBluetoothAdapter.startDiscovery();
                    Toast.makeText(getContext(),
                            "Getting all available Bluetooth Devices", Toast.LENGTH_SHORT)
                            .show();

                }
                // Do whatever you want to do with your bluetoothAdapter
                Set<BluetoothDevice> all_devices = mBluetoothAdapter.getBondedDevices();
                if (all_devices.size() > 0) {
                    for (BluetoothDevice currentDevice : all_devices) {

                        try {
                            String uuidd = uid;

                            if (currentDevice.getUuids() == null) {
                                uuidd = currentDevice.getUuids()[0]
                                        .getUuid().toString();
                            }
                            Device device1 = new Device();
                            device1.name = currentDevice.getName();
                            device1.address = currentDevice.getAddress();
                            deviceItems.add(new DeviceItem(device1));
                            btDevices.add(currentDevice);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                    fastItemAdapter.add(deviceItems);
                    fastItemAdapter.notifyAdapterDataSetChanged();
                }

            } else {
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return -2;
        }


        return 0;

    }

    private void Setup() {
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<DeviceItem>() {
            @Override
            public boolean onClick(View v, IAdapter<DeviceItem> adapter, DeviceItem item, final int position) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
//                Toast.makeText(mActivity, "Connecting...", Toast.LENGTH_SHORT).show();

                if (mPos.GetIO().IsOpened()) {
                    showProgress("Printing...");
                    es.submit(new TaskPrint(mPos));
                } else {
                    showProgress("Connecting...");
                    es.submit(new TaskOpen(mBt, item.device.address, mActivity));
                }

                return false;
            }

        });
    }

    private void showProgress(String title) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(title);
        progressDialog.show();
    }

    private void hideProgress() {
        if (progressDialog != null) {
            if(progressDialog.isShowing()){
                progressDialog.hide();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uninitBroadcast();
        es.submit(new TaskClose(mBt));

        hideProgress();
    }

    private void flushData() {
        try {
            if (mbtSocket != null) {
                mbtSocket.close();
                mbtSocket = null;
            }

            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.cancelDiscovery();
            }

            if (btDevices != null) {
                btDevices.clear();
                btDevices = null;
            }

            if (fastItemAdapter != null) {

                fastItemAdapter.clear();
                fastItemAdapter.notifyAdapterDataSetChanged();
                fastItemAdapter.notifyDataSetChanged();
                fastItemAdapter = null;
            }

            finalize();

        } catch (Exception ex) {
        } catch (Throwable e) {
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, Menu.FIRST, Menu.NONE, "Refresh Scanning");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT:

                if (resultCode == MainLayout.RESULT_OK) {
                    Set<BluetoothDevice> btDeviceList = mBluetoothAdapter
                            .getBondedDevices();
                    try {
                        if (btDeviceList.size() > 0) {

                            for (BluetoothDevice device : btDeviceList) {


//                                    mArrayAdapter.add(device.getName() + "\n"
//                                            + device.getAddress());
//                                    mArrayAdapter.notifyDataSetInvalidated();
//                                List<DeviceItem> deviceItems = new ArrayList<>();

                                Device device1 = new Device();
                                device1.name = device.getName();
                                device1.address = device.getAddress();
                                deviceItems.add(new DeviceItem(device1));
                                btDevices.add(device);

                            }
                            fastItemAdapter.add(deviceItems);
                            fastItemAdapter.notifyAdapterDataSetChanged();
                        }
                    } catch (Exception ex) {
                    }
                }

                break;
        }

        mBluetoothAdapter.startDiscovery();
    }

    @Override
    public void OnOpen() {

        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                hideProgress();

                Toast.makeText(mActivity, "Connected", Toast.LENGTH_SHORT).show();
                showProgress("Printing...");
                es.submit(new TaskPrint(mPos));
            }
        });
    }

    @Override
    public void OnOpenFailed() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                hideProgress();
                Toast.makeText(mActivity, "Connect Failed, make sure your printer is connected and try again.", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void OnClose() {
        hideProgress();
    }

    public class TaskOpen implements Runnable {
        BTPrinting bt = null;
        String address = null;
        Context context = null;

        public TaskOpen(BTPrinting bt, String address, Context context) {
            this.bt = bt;
            this.address = address;
            this.context = context;
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
            bt.Open(address, context);
        }
    }

    public class TaskClose implements Runnable {
        BTPrinting bt = null;

        public TaskClose(BTPrinting bt) {
            this.bt = bt;
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
            bt.Close();
        }
    }

    static int dwWriteIndex = 1;

    public class TaskPrint implements Runnable {
        Pos pos = null;

        public TaskPrint(Pos pos) {
            this.pos = pos;
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub

            final boolean bPrintResult = PrintTicket(FragmentPrinterListNew.nPrintWidth, FragmentPrinterListNew.bCutter, FragmentPrinterListNew.bDrawer, FragmentPrinterListNew.bBeeper,
                    FragmentPrinterListNew.nPrintCount, FragmentPrinterListNew.nCompressMethod);
            final boolean bIsOpened = pos.GetIO().IsOpened();

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgress();
                    // TODO Auto-generated method stub
                    Toast.makeText(mActivity.getApplicationContext(), bPrintResult ? getResources().getString(R.string.printsuccess) : getResources().getString(R.string.printfailed), Toast.LENGTH_SHORT).show();
//                    mActivity.btnPrint.setEnabled(bIsOpened);
                    getActivity().finish();
                }
            });

        }


        public boolean PrintTicket(int nPrintWidth, boolean bCutter, boolean bDrawer, boolean bBeeper, int nCount, int nCompressMethod) {
            boolean bPrintResult = false;

            //byte[] status = new byte[1];
            //if(pos.POS_QueryStatus(status, 3000, 2))
            {
                // 设置一下打印速度
//                int nPrintSpeed = AppStart.nBSPBaudrate / 10 / (nPrintWidth / 8 + 8) / 8;
//                pos.POS_SetPrintSpeed(nPrintSpeed);

                Bitmap bm1 = getTestImage1(nPrintWidth, nPrintWidth);
//                Bitmap bm2 = mActivity.getTestImage2(nPrintWidth, nPrintWidth);
//                Bitmap bmBlackWhite = getImageFromAssetsFile("blackwhite.png");
//                Bitmap bmIu = getImageFromAssetsFile("logo.png");
//                Bitmap bmYellowmen = getImageFromAssetsFile("yellowmen.png");
//                bmIu = resizeImage(bmIu,nPrintWidth,bmIu.getHeight() * nPrintWidth / bmIu.getWidth());
                for (int i = 0; i < nCount; ++i) {
                    if (!pos.GetIO().IsOpened())
                        break;

                    pos.POS_FeedLine();
                    pos.POS_S_Align(1);
//                    pos.POS_S_TextOut("BISATOPUP\n", 0, 1, 1, 3, 0x11);
//                    pos.POS_S_TextOut("SATU APLIKASI UNTUK SEMUA TRANSAKSI\n\n", 0, 0, 0, 1, 0x11);
                    if (nama_agen != null && !nama_agen.isEmpty() && !nama_agen.equals("null")) {
                        pos.POS_S_TextOut("AGEN \n" + nama_agen+"\n\n", 0, 1, 0, 2, 0x11);
                    }else {
                        pos.POS_S_TextOut("IKWAN MART\n\n", 0, 1, 1, 3, 0x11);
                    }
//                    pos.POS_S_TextOut("REC" + String.format("%03d", i) + "\r\nCaysn Printer\r\n测试页\r\n\r\n", 0, 1, 1, 0, 0x100);
//                    pos.POS_S_TextOut("扫二维码下载苹果APP\r\n", 0, 0, 0, 0, 0x100);
//                    pos.POS_S_SetQRcode("https://appsto.re/cn/2KF_bb.i", 8, 0, 3);
                    pos.POS_S_Align(0);

                    pos.POS_S_TextOut(text_to_print, 0, 0, 0, 1, 0x11);

//                    pos.POS_S_SetBarcode("20160618", 0, 72, 3, 60, 0, 2);

                    //Token
                    pos.POS_S_Align(1);
                    if (need_print_sn) {
                        pos.POS_FeedLine();
                        try{
                           String sns[] = sn.split("/");
                           if(sns.length>0){
                               sn = sns[0];
                           }
                        }catch (Exception ec){
                            ec.printStackTrace();
                            Crashlytics.logException(ec);
                        }
                        pos.POS_S_TextOut("TOKEN\n" + sn + "\n", 0, 1, 0, 2, 0x11);
                    }


                    pos.POS_S_TextOut("IKHWAN.EISBETECH.COM\n", 0, 0, 0, 1, 0x11);
                    pos.POS_S_TextOut("Solusi pembayaran dalam satu aplikasi\n 021-2278-5123\n\n", 0, 0, 0, 1, 0x11);
                    pos.POS_FeedLine();


//                    if(bm1 != null)
//                    {
//                        pos.POS_PrintPicture(bm1, nPrintWidth, 1, nCompressMethod);
//                    }
//                    if(bm2 != null)
//                    {
//                        pos.POS_PrintPicture(bm2, nPrintWidth, 1, nCompressMethod);
//                    }
//                    if(bmBlackWhite != null)
//                    {
//                        pos.POS_PrintPicture(bmBlackWhite, nPrintWidth, 1, nCompressMethod);
//                    }
//                    if(bmIu != null)
//                    {
//                        pos.POS_PrintPicture(bmIu, nPrintWidth, 0, nCompressMethod);
//                    }
//                    if(bmYellowmen != null)
//                    {
//                        pos.POS_PrintPicture(bmYellowmen, nPrintWidth, 0, nCompressMethod);
//                    }
                }

                if (bBeeper)
                    pos.POS_Beep(1, 5);
                if (bCutter)
                    pos.POS_CutPaper();
                if (bDrawer)
                    pos.POS_KickDrawer(0, 100);

                //int dwTicketIndex = dwWriteIndex++;
                //bPrintResult = pos.POS_TicketSucceed(dwTicketIndex, 30000);
                bPrintResult = pos.GetIO().IsOpened();
            }

            return bPrintResult;
        }
    }

    @OnClick(R.id.btn_refresh)
    public void refresh() {
        Toast.makeText(getActivity(), "Refresh device...", Toast.LENGTH_SHORT).show();
        initDevicesList(true);
    }

    public Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;

    }

    public Bitmap getTestImage1(int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();

        paint.setColor(Color.WHITE);
        canvas.drawRect(0, 0, width, height, paint);

        paint.setColor(Color.BLACK);
        for (int i = 0; i < 8; ++i) {
            for (int x = i; x < width; x += 8) {
                for (int y = i; y < height; y += 8) {
                    canvas.drawPoint(x, y, paint);
                }
            }
        }
        return bitmap;
    }
}
