package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.Favorite;
import com.eisbetech.ikhwanmart.database.User;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 12/22/2016.
 */

public class AddFavoriteFragment extends BaseFragment {

    @BindView(R.id.spinner_category)
    protected MaterialSpinner spinner_category;

    @BindView(R.id.txt_name)
    protected EditText txt_name;

    @BindView(R.id.txt_phone_number)
    protected EditText txt_phone_number;

    public static int PICK_CONTACT = 10;
    public static final int PERMISSION_REQUEST_CONTACT = 11;

    @OnClick(R.id.btn_add_favorite)
    public void add_favorite() {
        try
        {
            Favorite favorite = new Favorite();
            favorite.id = favorite.getNextPrimaryKey(realm);
            favorite.nama = txt_name.getText().toString();
            favorite.nama = favorite.nama +" ("+spinner_category.getText()+")";
            favorite.phone_number = txt_phone_number.getText().toString();
            switch (spinner_category.getSelectedIndex()) {
                case 1:
                    favorite.product_id = 1;
                    favorite.category = "pulsa";
                    break;
                case 2:
                    favorite.product_id = 84;
                    favorite.category = "paket_data";
                    break;
                case 3:
                    favorite.product_id = 2;
                    favorite.category = "token";
                    break;
                case 4:
                    favorite.product_id = 30;
                    favorite.category = S.pln;
                    break;
                case 5:
                    favorite.product_id = 92;
                    favorite.category = S.insurance;
                    break;
                case 6:
                    favorite.product_id = 30;
                    favorite.category = S.tv;
                    break;
                case 7:
                    favorite.product_id = 30;
                    favorite.category = S.pdam;
                    break;
                case 8:
                    favorite.product_id = 30;
                    favorite.category = S.post_paid_phone;
                    break;
                case 9:
                    favorite.product_id = 30;
                    favorite.category = S.pln;
                    break;
                case 10:
                    favorite.product_id = 30;
                    favorite.category = S.telkom;
                    break;
                case 11:
                    favorite.product_id = 30;
                    favorite.category = S.multifinance;
                    break;
                case 12:
                    favorite.product_id = 30;
                    favorite.category = S.credit_card;
                    break;
                case 13:
                    favorite.product_id = 156;
                    favorite.category = S.gas;
                    break;
            }
            realm.beginTransaction();
            realm.copyToRealm(favorite);
            realm.commitTransaction();
            Toast.makeText(getActivity(), "Nomor favorite berhasil ditambahkan", Toast.LENGTH_SHORT).show();

            simpan(favorite);
            getActivity().finish();
        }catch (Exception ec) {
            ec.printStackTrace();
        }
    }

    private void simpan(final Favorite favorite) {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("nama", favorite.nama);
            params.put("nomor_hp", favorite.phone_number);
            params.put("kategori", favorite.category);
            params.put("product_id", favorite.product_id);

            BisatopupApi.post("/favorite/add", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                            realm.beginTransaction();
                            favorite.is_sync = 1;
                            realm.copyToRealmOrUpdate(favorite);
                            realm.commitTransaction();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_favorite, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        List<String> category = new ArrayList<>();
        category.add("Pilih category");
        category.add("Pulsa");
        category.add("Paket Data");
        category.add("Voucher PLN PRABAYAR");
        category.add("Tagihan PLN");
        category.add("Tagihan ASURANSI");
        category.add("Tagihan TV");
        category.add("Tagihan PDAM");
        category.add("Tagihan Telepon Pascabayar");
        category.add("Tagihan Telkom");
        category.add("Tagihan Multifinance");
        category.add("Tagihan Kartu Kredit");
        category.add("Tagihan Gas");

        spinner_category.setItems(category);

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == getActivity().RESULT_OK || resultCode == -1) {

                Uri contactData = data.getData();

                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                Log.i("phoneNUmber", "The phone number is " + number);

                number = number.replace("+", "");
                number = number.replace(" ", "");
                number = number.replace("-", "");

                if (number.substring(0, 2).equals("62")) {
                    number = "0" + number.substring(2);
                }

                if (number.substring(0, 1).equals("8")) {
                    number = "0" + number;
                }
                txt_phone_number.setText(number);
                spinner_category.requestFocus();

                // Do something with the phone number...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (data != null) {
            String result = data.getStringExtra("result");
            txt_phone_number.setText(result);
            spinner_category.requestFocus();
        }
    }

    @OnClick(R.id.btn_add_number)
    public void add_number() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", false);
        i.putExtra("parent_id", 1);
        startActivityForResult(i, 1);
    }

    @OnClick(R.id.btn_add_contact)
    public void addContact() {
        askForContactPermission();
    }

    private void getContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getContact();
            }
        } else {
            getContact();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContact();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                    ;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}

