package com.eisbetech.ikhwanmart.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagihanInputFragment extends BaseFragment {

    @BindView(R.id.img_product)
    protected ImageView img_product;

    @BindView(R.id.txt_product)
    protected TextView txt_product;

    @BindView(R.id.txt_product_detail)
    protected TextView txt_product_detail;

    @BindView(R.id.txt_no_tujuan)
    protected TextView txt_no_tujuan;
    @BindView(R.id.txt_nominal)
    protected TextView txt_nominal;

    public TagihanInputFragment() {
        // Required empty public constructor
    }

    int tagihan_id;
    String product_name;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_tagihan_input, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        String product = bundle.getString("product");
        txt_product.setText(product);
        txt_product_detail.setText(bundle.getString("nominal"));
        product_name = bundle.getString("nominal");
        txt_no_tujuan.setText(bundle.getString("no_tujuan"));

        if (bundle.getInt("tagihan_id", 0) != 0) {
            tagihan_id = bundle.getInt("tagihan_id", 0);
        }

        return root;
    }

    @OnClick(R.id.btn_buy)
    public void konfirmasi() {
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Konfirmasi Pembayaran Tagihan")
                .setMessage("Pastikan jumlah bayar yang anda masukkan sudah benar, Apakah anda yakin ingin melanjutkan?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       simpan();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void simpan() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("tagihan_id", tagihan_id);
            params.put("jumlah_bayar", txt_nominal.getText().toString().trim());
            params.put("nomor_rekening", txt_no_tujuan.getText().toString().trim());

            BisatopupApi.post("/tagihan/simpan",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih",Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            Product parent_product = realm.where(Product.class)
                                    .equalTo("product_name", product_name)
                                    .findFirst();

                            Intent i = new Intent(getActivity(), MainLayout.class);
                            i.setAction("checkout");
                            i.putExtra("product", "PPOB");
                            i.putExtra("product_id", parent_product.id);
                            i.putExtra("nominal", parent_product.product_name);
                            i.putExtra("price",  txt_nominal.getText().toString().trim());
                            i.putExtra("is_tagihan", true);
                            i.putExtra("no_tujuan", txt_no_tujuan.getText().toString());
                            i.putExtra("img", "");
                            i.putExtra("tagihan_id", tagihan_id);
                            startActivity(i);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }



                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
