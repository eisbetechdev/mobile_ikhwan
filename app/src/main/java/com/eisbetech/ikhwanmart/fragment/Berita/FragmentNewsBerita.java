package com.eisbetech.ikhwanmart.fragment.Berita;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.adapter.NewsAdapter;
import com.eisbetech.ikhwanmart.item.News.DataItem;
import com.eisbetech.ikhwanmart.item.News.News;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FragmentNewsBerita extends Fragment {

    @BindView(R.id.recyc_news)
    RecyclerView mRecycNews;
    private Unbinder unbinder;
    private View view;

    private List<DataItem> dataItemList;
    private NewsAdapter newsAdapter;
    private LinearLayoutManager linearLayoutManager;
    private String pages;
    private boolean isLoading = true;

    public FragmentNewsBerita() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_berita, container, false);
        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);
        dataItemList = new ArrayList<>();

        newsAdapter = new NewsAdapter(getActivity(), dataItemList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecycNews.setLayoutManager(linearLayoutManager);
        mRecycNews.setAdapter(newsAdapter);

        final OkHttpClient client = new OkHttpClient();
        String url = "http://api.mic.eisbetech.com/news?page=1";
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String textResponse = response.body().string();
                News news = new Gson().fromJson(textResponse, News.class);
                pages = news.getNextPage();
                dataItemList.addAll(news.getData());
                getActivity().runOnUiThread(new Runnable() {
                    @Override public void run() {
                        newsAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }
                });
            }
        });

        mRecycNews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                int visibleThreshold = 5;
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && pages!=null) {
                    final Request req = new Request.Builder()
                            .url(pages)
                            .get()
                            .build();

                    client.newCall(req).enqueue(new Callback() {
                        @Override public void onFailure(Call call, IOException e) {
                        }

                        @Override public void onResponse(Call call, Response response) throws IOException {
                            final String textResponse = response.body().string();
                            News news = new Gson().fromJson(textResponse, News.class);
                            Log.d("unik",news+"");
                            pages = news.getNextPage();
                            dataItemList.addAll(news.getData());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override public void run() {
                                    newsAdapter.notifyDataSetChanged();
                                    isLoading = false;
                                }
                            });
                        }
                    });
                    isLoading = true;
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
