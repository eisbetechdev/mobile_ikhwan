package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 10/28/2016.
 */

public class FragmentSupport extends BaseFragment {

    @BindView(R.id.txt_subject)
    protected EditText txt_subject;

    @BindView(R.id.txt_body)
    protected EditText txt_body;

    @BindView(R.id.btn_submit)
    protected FancyButton btn_submit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_support, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        try {
            String subject = bundle.getString("subject", "");
            txt_subject.setText(subject);
            if (!txt_subject.getText().equals("")) {
                txt_body.requestFocus();
            }
        } catch (Exception ec) {
            ec.printStackTrace();
        }
        return root;
    }


    @OnClick(R.id.btn_submit)
    public void konfirmasi() {
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_info).sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda yakin?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       kirim();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void kirim() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();

            RequestParams params = new RequestParams();
            params.put("subject", txt_subject.getText());
            params.put("body", txt_body.getText());


            BisatopupApi.post("/profile/support",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");

                            Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_check_circle).sizeDp(32).color(Color.WHITE);

                            new LovelyStandardDialog(getActivity())
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                    .setTitle("Sukses")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
//                                            getActivity().finish();
                                        }
                                    })
                                    .setMessage(message)
                                    .show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
