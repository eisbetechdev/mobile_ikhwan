package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.LockPinActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 7/9/2017.
 */

public class FragmentEditHandphone extends BaseFragment {

    @BindView(R.id.txt_no_hp)
    protected MaterialEditText txt_no_hp;

    @BindView(R.id.txt_code)
    protected MaterialEditText txt_code;


    @BindView(R.id.btn_ulangi)
    protected FancyButton btn_ulangi;

    @BindView(R.id.btn_submit)
    protected FancyButton btn_submit;

    @BindView(R.id.layout_verifikasi)
    protected CardView layout_verifikasi;

    @BindView(R.id.layout_btn_verifikasi)
    protected LinearLayout layout_btn_verifikasi;

    @BindView(R.id.layout_input_nomor)
    protected CardView layout_input_nomor;

    String kode_verifikasi;

    User user;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_handphone, container, false);
        ButterKnife.bind(this, root);

        user = User.getUser();

        txt_no_hp.setText(user.phone_number);
        layout_verifikasi.setVisibility(View.GONE);
        btn_ulangi.setVisibility(View.GONE);
        layout_btn_verifikasi.setVisibility(View.GONE);

        return root;
    }

    @OnClick(R.id.btn_ulangi)
    public void ulangi() {
        layout_verifikasi.setVisibility(View.GONE);
        layout_input_nomor.setVisibility(View.VISIBLE);
        btn_submit.setVisibility(View.VISIBLE);
        btn_ulangi.setVisibility(View.GONE);
        layout_btn_verifikasi.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_submit)
    public void validasi() {
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Konfirmasi")
                .setMessage("Pastikan nomor yang anda masukkan sudah benar, kami akan mengirimkan kode verifikasi ke nomor handphone anda")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        User user = User.getUser();
                        if (user!=null && user.lock_pin_enable!=null && user.lock_pin_enable) {
                            Intent intent = new Intent(getActivity(), LockPinActivity.class);
                            intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                            intent.setAction("Masukkan PIN");
                            startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                        } else {
                            verifikasi();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }
    public void verifikasi() {
        if (txt_no_hp.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Nomor Handphone tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("phone_number", txt_no_hp.getText().toString().trim());

            BisatopupApi.post("/user/kirim-verifikasi",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            String kode = object.getString("kode");
//
//                            kode_verifikasi = kode;

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            layout_verifikasi.setVisibility(View.VISIBLE);
                            btn_ulangi.setVisibility(View.VISIBLE);
                            btn_submit.setVisibility(View.GONE);
                            layout_input_nomor.setVisibility(View.GONE);
                            layout_btn_verifikasi.setVisibility(View.VISIBLE);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_verifikasi)
    public void verifikasi_kodes() {
//        if (txt_code.getText().toString().trim().equals(kode_verifikasi)) {
//            update_no_hp();
//        } else {
//            Toast.makeText(getActivity(), "Kode verifikasi salah", Toast.LENGTH_SHORT).show();
//        }
        verifikasi_kode();
    }

    public void verifikasi_kode() {
        Helper.hideSoftKeyboard(getActivity());
        if (txt_code.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Kode tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("no_hp", txt_no_hp.getText());
            params.put("kode", txt_code.getText());


            BisatopupApi.post("/user/verify-code", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            update_no_hp();
                        } else {
                            String message = object.getString("message");
                            Helper.show_alert("Error", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void update_no_hp() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("phone_number", txt_no_hp.getText().toString().trim());
            params.put("kode", txt_code.getText().toString().trim());

            BisatopupApi.post("/user/update-nohp",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == LockPinActivity.RESULT_PIN_INPUT_VALIDATE) {
            boolean validate = data.getBooleanExtra("validate", false);
            if (validate) {
                verifikasi();
            }
        }
    }
}
