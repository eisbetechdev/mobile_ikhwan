package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.database.RealmController;

import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by khadafi on 7/31/2016.
 */
public abstract  class BaseFragmentNew extends Fragment {
    private ProgressDialog mProgressDialog;
    public Realm realm;

    public View view;
    protected abstract int getLayoutResourceId();
    protected abstract void LoadView();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getLayoutResourceId(), container, false);
        ButterKnife.bind(this, root);

        this.view = root;

        LoadView();
        return root;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        realm =  RealmController.with(this).getRealm();
    }

    @Override
    public void onResume() {
        super.onResume();
        realm =  RealmController.with(this).getRealm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
