package com.eisbetech.ikhwanmart.fragment.Berita;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FragmentNews extends Fragment {


    @BindView(R.id.tab_news)
    TabLayout mTabNews;
    @BindView(R.id.content_container_forum)
    ViewPager mContentContainerForum;
    private View view;
    private Unbinder unbinder;

    private NewsTabAdapter newsTabAdapter;

    public FragmentNews() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        newsTabAdapter = new NewsTabAdapter(getChildFragmentManager());
        mContentContainerForum.setAdapter(newsTabAdapter);
        mContentContainerForum.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabNews));
        mTabNews.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mContentContainerForum));

        mContentContainerForum.setOffscreenPageLimit(10);

        return view;
    }

    public class NewsTabAdapter extends FragmentPagerAdapter {

        private Fragment choosed;

        public NewsTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            choosed = new FragmentNewsBerita();

            if (position == 0) {
                choosed = new FragmentNewsBerita();
            } else if (position == 1) {
                choosed = new FragmentNewsBerita();
            } else if (position == 2) {
                choosed = new FragmentNewsBerita();
            } else if (position == 3) {
                choosed = new FragmentNewsBerita();
            } else {
                choosed = new FragmentNewsBerita();
            }

            return choosed;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
