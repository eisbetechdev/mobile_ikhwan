package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.StasiunAsalActivity;
import com.eisbetech.ikhwanmart.StasiunTujuanActivity;
import com.eisbetech.ikhwanmart.TrainSearchResultActivity;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.Train;
import com.mikepenz.iconics.view.IconicsImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DateRangeLimiter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrainFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    DateRangeLimiter dateRangeLimiter;
    Calendar now = Calendar.getInstance();
    SimpleDateFormat mdformat = new SimpleDateFormat("dd MMMM yyyy");
    String activeCalendar = "";
    String tglBerangkat, tglPulang;

    @BindView(R.id.stasiunAsalCode)
    protected TextView stasiunAsalCode ;

    @BindView(R.id.stasiunAsalName)
    protected TextView stasiunAsalName ;

    @BindView(R.id.stasiunTujuanCode)
    protected TextView stasiunTujuanCode ;

    @BindView(R.id.stasiunTujuanName)
    protected TextView stasiunTujuanName;

    @BindView(R.id.tanggal_berangkat)
    protected TextView tanggal_berangkat ;

    @BindView(R.id.tanggal_pulang)
    protected TextView tanggal_pulang ;

    @BindView(R.id.opsiSekaliJalan)
    protected TextView opsiSekaliJalan ;

    @BindView(R.id.opsiPulangPergi)
    protected TextView opsiPulangPergi ;

    @BindView(R.id.linear_tanggal_pulang)
    protected LinearLayout linear_tanggal_pulang ;

    @BindView(R.id.addDewasa)
    protected IconicsImageView addDewasa ;

    @BindView(R.id.subtractDewasa)
    protected IconicsImageView subtractDewasa ;

    @BindView(R.id.jumlahDewasa)
    protected TextView jumlahDewasa ;

    @BindView(R.id.addBayi)
    protected IconicsImageView addBayi ;

    @BindView(R.id.subtractBayi)
    protected IconicsImageView subtractBayi ;

    @BindView(R.id.jumlahBayi)
    protected TextView jumlahBayi ;
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_train, container, false);
        ButterKnife.bind(this, root);

        linear_tanggal_pulang.setVisibility(View.GONE);

        String simpleDate = mdformat.format(now.getTime());
        String day = S.daynames[now.get(Calendar.DAY_OF_WEEK) - 1];

        tanggal_berangkat.setText(day + " ," + simpleDate);
        tanggal_pulang.setText(day + " ," + simpleDate);
        tglBerangkat = simpleDate;
        tglPulang = simpleDate;

        subtractDewasa.setClickable(false);
        subtractBayi.setClickable(false);
        return root;
    }

    @OnClick(R.id.opsiSekaliJalan)
    protected void setOpsiSekaliJalan() {
        opsiSekaliJalan.setTextColor(ContextCompat.getColor(getContext(), R.color.primary));
        opsiPulangPergi.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));

        linear_tanggal_pulang.setVisibility(View.GONE);
    }
    
    @OnClick(R.id.opsiPulangPergi)
    protected void setOpsiPulangPergi() {
        opsiSekaliJalan.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
        opsiPulangPergi.setTextColor(ContextCompat.getColor(getContext(), R.color.primary));
        linear_tanggal_pulang.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.searchStasiunAsal)
    protected void searchStasiunAsal() {
        Intent in = new Intent(getActivity(), StasiunAsalActivity.class);
        startActivityForResult(in, 1);
    }
    
    @OnClick(R.id.searchStasiunTujuan)
    protected void searchStasiunTujuan() {
        Intent in = new Intent(getActivity(), StasiunTujuanActivity.class);
        startActivityForResult(in, 2);
    }

    @OnClick(R.id.subtractDewasa)
    protected void subtractDewasa() {
        int count = Integer.parseInt(jumlahDewasa.getText().toString());
        count = count - 1;
        if (count == 1) {
            subtractDewasa.setClickable(false);
            subtractDewasa.setBackgroundColor(getResources().getColor(R.color.grey_background));
        } else {
            addDewasa.setClickable(true);
            addDewasa.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }
        jumlahDewasa.setText(String.valueOf(count));
    }

    @OnClick(R.id.addDewasa)
    protected void addDewasa() {
        int count = Integer.parseInt(jumlahDewasa.getText().toString());
        count = count + 1;
        if (count == 4) {
            addDewasa.setClickable(false);
            addDewasa.setBackgroundColor(getResources().getColor(R.color.grey_background));
        } else {
            subtractDewasa.setClickable(true);
            subtractDewasa.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }
        jumlahDewasa.setText(String.valueOf(count));
    }

    @OnClick(R.id.subtractBayi)
    protected void subtractBayi() {
        int count = Integer.parseInt(jumlahBayi.getText().toString());
        count = count - 1;
        if (count == 0) {
            subtractBayi.setClickable(false);
            subtractBayi.setBackgroundColor(getResources().getColor(R.color.grey_background));
        } else {
            addBayi.setClickable(true);
            addBayi.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }
        jumlahBayi.setText(String.valueOf(count));
    }

    @OnClick(R.id.addBayi)
    protected void addBayi() {
        int count = Integer.parseInt(jumlahBayi.getText().toString());
        count = count + 1;
        if (count == 4) {
            addBayi.setClickable(false);
            addBayi.setBackgroundColor(getResources().getColor(R.color.grey_background));
        } else {
            subtractBayi.setClickable(true);
            subtractBayi.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }
        jumlahBayi.setText(String.valueOf(count));
    }

    @OnClick(R.id.btn_search)
    protected void search() {
        int dewasa = Integer.parseInt(jumlahDewasa.getText().toString());
        int bayi = Integer.parseInt(jumlahBayi.getText().toString());
        if (bayi > dewasa) {
            Helper.showMessage(getActivity(), S.infant_cant_exceed_adult);
        } else {
            Intent in = new Intent(getActivity(), TrainSearchResultActivity.class);

            Train train = new Train();
            train.setStasiunAsalCode(stasiunAsalCode.getText().toString().replace("(" , "")
                    .replace(")", ""));
            train.setStasiunAsalName(stasiunAsalName.getText().toString());
            train.setStasiunTujuanCode(stasiunTujuanCode.getText().toString().replace("(" , "")
                    .replace(")", ""));
            train.setStasiunTujuanName(stasiunTujuanName.getText().toString());
            train.setTglBerangkat(tglBerangkat);
            if (linear_tanggal_pulang.getVisibility() == View.VISIBLE) {
                train.setTglPulang(tglPulang);
            }
            train.setJumlahDewasa(jumlahDewasa.getText().toString());

            String infant = jumlahBayi.getText().toString();
            if (Helper.isExist(infant)) {
                train.setJumlahBayi(infant);
            }
            in.putExtra(K.selected_train , train);
            startActivity(in);
        }
    }

    @OnClick(R.id.tanggal_berangkat)
    protected void getCalendarBerangkat() {
        activeCalendar = S.depart;
        DatePickerDialog dpd = DatePickerDialog.newInstance(this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setTitle(S.depart_date);
        dpd.setMinDate(Calendar.getInstance());
        dpd.show(getActivity().getFragmentManager(), S.datepicker_dialog);
    }

    @OnClick(R.id.tanggal_pulang)
    protected void getCalendarPulang() {
        activeCalendar = S.return_;
        DatePickerDialog dpd = DatePickerDialog.newInstance(this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setTitle(S.return_date);
        dpd.setMinDate(Calendar.getInstance());
        dpd.show(getActivity().getFragmentManager(), S.datepicker_dialog);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.YEAR, year);
        selectedDate.set(Calendar.MONTH, monthOfYear);
        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String simpleDate = mdformat.format(selectedDate.getTime());
        String day = S.daynames[selectedDate.get(Calendar.DAY_OF_WEEK) - 1];

        if (activeCalendar.equals(S.depart)) {
            tglBerangkat = simpleDate;
            tanggal_berangkat.setText(day + ", " + simpleDate);
        } else {
            tglPulang = simpleDate;
            tanggal_pulang.setText(day + ", " + simpleDate);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager()
                .findFragmentByTag(S.datepicker_dialog);
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 1) {
                String selectedStasiun = data.getStringExtra(K.selected_station);
                String[] split = selectedStasiun.split("\\(");

                stasiunAsalName.setText(split[0]);
                stasiunAsalCode.setText(String.format("%s%s", "(", split[1]));
            } else {
                String selectedStasiun = data.getStringExtra(K.selected_station);
                String[] split = selectedStasiun.split("\\(");

                stasiunTujuanName.setText(split[0]);
                stasiunTujuanCode.setText(String.format("%s%s", "(", split[1]));
            }
        }
    }
}
