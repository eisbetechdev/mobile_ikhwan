package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.NomorItem;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Favorite;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItemAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by khadafi on 10/19/2016.
 */

public class FragmentDaftarNo extends BaseFragment {
    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.txt_cari)
    protected TextView txt_cari;

    private FastItemAdapter<NomorItem> fastItemAdapter;

    @BindView(R.id.list)
    protected RecyclerView list;

    private boolean is_tagihan;
    private int parent_id;

    String type;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_daftar_no, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();
        is_tagihan = bundle.getBoolean("is_tagihan",false);
        parent_id = bundle.getInt("parent_id",1);
        type = bundle.getString("type");

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);

        txt_cari.setFocusableInTouchMode(false);
        txt_cari.setFocusable(false);
        txt_cari.setFocusableInTouchMode(true);
        txt_cari.setFocusable(true);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<NomorItem>() {
            @Override
            public boolean onClick(View v, IAdapter<NomorItem> adapter, NomorItem item, int position) {
                Intent returnIntent = new Intent();
                item.Nomor = item.Nomor.replace(" ","");
                item.Nomor = item.Nomor.replace("-","");
                returnIntent.putExtra("result", item.Nomor);
                getActivity().setResult(MainLayout.NOMOR_ACTIVIT_OK, returnIntent);
                getActivity().finish();
                return true;
            }
        });

        txt_cari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Call this in onQueryTextSubmit() & onQueryTextChange() when using SearchView
                fastItemAdapter.filter(charSequence);

                fastItemAdapter.withFilterPredicate(new IItemAdapter.Predicate<NomorItem>() {

                    @Override
                    public boolean filter(NomorItem item, CharSequence constraint) {
                        return !item.Nomor.startsWith(String.valueOf(constraint));
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if(type != null && type.equals("favorite")){
            getNomorFavorite();
        }else{
            getNomor(true);
        }
        return root;
    }

    private void getNomorFavorite(){
        RealmResults<Favorite> results;

        results = realm.where(Favorite.class)
                .findAllAsync();

        results.addChangeListener(new RealmChangeListener<RealmResults<Favorite>>() {
            @Override
            public void onChange(RealmResults<Favorite> element) {
                List<NomorItem> nomorItems = new ArrayList<>();

                fastItemAdapter.clear();
                element = element.sort("id", Sort.DESCENDING);

                for (Favorite favorite : element) {
                    nomorItems.add(new NomorItem(favorite.phone_number,favorite.nama));
                }

                fastItemAdapter.clear();
                fastItemAdapter.add(nomorItems);
                fastItemAdapter.notifyAdapterDataSetChanged();

                if (element.size() <= 0) {
                    android_empty.setVisibility(View.VISIBLE);
                } else {
                    android_empty.setVisibility(View.GONE);
                }
            }
        });


        get();
    }

    private void get() {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);


            BisatopupApi.get("/favorite/data", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                            JSONArray data = object.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject dt = data.getJSONObject(i);
                                String nama = dt.getString("nama");
                                String kategori = dt.getString("kategori");
                                String nomor_hp = dt.getString("nomor_hp");
                                int product_id = dt.getInt("product_id");

                                Favorite favorite = realm.where(Favorite.class).equalTo("nama",nama)
                                        .equalTo("category",kategori)
                                        .equalTo("phone_number",nomor_hp)
                                        .findFirst();

                                if(favorite == null){
                                    favorite = new Favorite();
                                    favorite.id = favorite.getNextPrimaryKey(realm);
                                    favorite.nama = nama;
                                    favorite.category = kategori;
                                    favorite.phone_number = nomor_hp;
                                    favorite.is_sync = 1;
                                    favorite.product_id = product_id;
                                    realm.beginTransaction();
                                    realm.copyToRealm(favorite);
                                    realm.commitTransaction();
                                }
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getNomor(final boolean showloading) {
        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("is_tagihan",is_tagihan);
            params.put("parent_id",parent_id);


            BisatopupApi.get("/profile/nomor",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    android_empty.setVisibility(View.GONE);

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {

                        JSONObject response = new JSONObject(responseString);

                        JSONArray data = response.getJSONArray("data");

                        List<NomorItem> nomorItems = new ArrayList<>();


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Favorite favorite = realm.where(Favorite.class)
                                    .equalTo("phone_number", obj.getString("nomor").trim())
                                    .findFirst();

                            if (favorite != null) {
                                nomorItems.add(new NomorItem(obj.getString("nomor").trim(),favorite.nama,obj.getString("product").trim()));

                            } else {
                                nomorItems.add(new NomorItem(obj.getString("nomor").trim(),"",obj.getString("product").trim()));

                            }
                        }
                        fastItemAdapter.clear();
                        fastItemAdapter.add(nomorItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                        if(nomorItems.size() == 0){
                            android_empty.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        android_empty.setVisibility(View.VISIBLE);
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
