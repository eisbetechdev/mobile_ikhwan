package com.eisbetech.ikhwanmart.fragment;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.printer_command.Command;
import com.eisbetech.ikhwanmart.printer_command.PrinterCommand;
import com.eisbetech.ikhwanmart.service.BluetoothService;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 7/8/2017.
 */

public class FragmentPrint extends BaseFragment {

    private static BluetoothSocket btsocket;
    private static OutputStream btoutputstream;

    private BluetoothService mService = null;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.layout_loading)
    protected RelativeLayout layout_loading;

    @BindView(R.id.layout_print)
    protected LinearLayout layout_print;

    @BindView(R.id.txt_text_print)
    protected EditText txt_text_print;

    private String trans_id;
    private String sn;
    private boolean need_print_sn;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_print, container, false);
        ButterKnife.bind(this, root);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        trans_id = bundle.getString("trans_id");

        mService = new BluetoothService(getActivity(), new Handler());

        txt_text_print.setTypeface(Typeface.MONOSPACE);
//
//        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/cour.ttf");
//        txt_text_print.setTypeface(tf);

        getStruk(1, true);

        return root;
    }

    @OnClick(R.id.btn_print)
    protected void connect() {
        if (btsocket == null) {
            Intent BTIntent = new Intent(getActivity(), MainLayout.class);
            BTIntent.setAction("list_printer");
            BTIntent.putExtra("data",txt_text_print.getText().toString());
            BTIntent.putExtra("sn",sn);
            BTIntent.putExtra("need_print_sn",need_print_sn);
            this.startActivityForResult(BTIntent, FragmentPrinterList.REQUEST_CONNECT_BT);
        } else {

            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            btoutputstream = opstream;
            print_bt();

        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon;
        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_share)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        menu.add(0, 2, Menu.NONE, "Share").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (btsocket != null) {
                btoutputstream.close();
                btsocket.close();
                btsocket = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mService != null)
            mService.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mService != null) {

            if (mService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                if (btsocket != null) {
                    mService.start();
                    mService.connect(btsocket);
                }
            }
        }
    }

    /****************************************************************************************************/
    /*
     * SendDataString
     */
    private void SendDataString(String data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(getActivity(), "Printer tidak konek", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        if (data.length() > 0) {
            try {
                mService.write(data.getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     *SendDataByte
     */
    private void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
//            Toast.makeText(getActivity(), "Printer tidak konek", Toast.LENGTH_SHORT)
//                    .show();
            btsocket = null;
            return;
        }
        mService.write(data);
    }

    private void print_bt() {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            btsocket = null;
            Toast.makeText(getActivity(), "Printer gagal konek, silahkan coba lagi", Toast.LENGTH_SHORT)
                    .show();
            return;
        } else {
            try {
                SendDataByte(PrinterCommand.byteCommands[3]);

                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x11;
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n".getBytes("GBK"));

                Command.GS_ExclamationMark[2] = 0x00;
                SendDataByte(Command.GS_ExclamationMark);

                SendDataByte("SATU APLIKASI UNTUK SEMUA TRANSAKSI\n\n".getBytes("GBK"));

                Command.ESC_Align[2] = 0x00;
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;
                SendDataByte(Command.GS_ExclamationMark);

                ////                String msg = "Congratulations!\n\n";
//            String dataf = "INFO LISTRIK PRABAYAR\n" +
//                    "\n" +
//                    "\n" +
//                    "MEMBER          : 1221252/6285277677761\n" +
//                    "NO METER        : 56600291167\n" +
//                    "ID PEL          : 523040863017\n" +
//                    "NAMA            : KRISDIYANTO\n" +
//                    "TARIF/DAYA      : R1/000001300 VA\n" +
//                    "NOMINAL         : Rp     20.000,00 \n" +
//                    "ADMIN BANK      : Rp      2.500,00 \n" +
//                    "\n" +
//                    "PERHATIAN: \n" +
//                    "CETAK INI BUKAN BUKTI KWITANSI PEMBAYARAN";
                String dataf = txt_text_print.getText().toString();
//                SendDataByte(PrinterCommand.POS_Print_Text(msg, "GBK", 0, 1, 1, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(dataf, "GBK", 0, 0, 0, 0));
                SendDataByte(dataf.getBytes("GBK"));
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataByte("\n\nIKHWAN.EISBETECH.COM".getBytes("GBK"));

                SendDataString("\n\n\n");
               try {
//                   SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
//                   SendDataByte(PrinterCommand.POS_Set_Cut(1));
//                   SendDataByte(PrinterCommand.POS_Set_PrtInit());
               }catch (Exception ec) {
                   ec.printStackTrace();
               }
                Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_check_circle).sizeDp(32).color(Color.WHITE);

//            SendDataByte(Command.GS_V_m_n);
                if (btsocket != null) {
                    new LovelyInfoDialog(getActivity())
                            .setTopColorRes(R.color.primary_dark)
                            .setIcon(img)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                            .setTitle("Print Sukses")
                            .setMessage("Struk berhasil di print")
                            .show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket = FragmentPrinterList.getSocket();
            if (btsocket != null) {
                mService.connect(btsocket);


                print_bt();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getStruk(final int page, final boolean showloading) {

        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("page", page);

            BisatopupApi.get("/transaksi/struk/" + trans_id,  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();
                    // footerAdapter.clear();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    //   footerAdapter.clear();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            txt_text_print.setText(object.getString("struk"));
                            sn = object.getString("sn");
                            need_print_sn = object.getBoolean("need_print_sn");
                            layout_loading.setVisibility(View.GONE);
                            layout_print.setVisibility(View.VISIBLE);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, txt_text_print.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;

        }
        return true;
    }
}
