package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.freshchat.consumer.sdk.Freshchat;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mukesh.OtpView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 3/19/2017.
 */

public class VerifyPhoneNumber extends BaseFragment {

    @BindView(R.id.txt_code)
    protected OtpView txt_code;

    @BindView(R.id.txt_verifikasi)
    protected TextView txt_verifikasi;

    @BindView(R.id.btn_reset)
    protected FancyButton btn_reset;
    private int countTimer = 1;

    int count = 1;

    User user;
    SessionManager session;

    AppEventsLogger logger;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_verify_number, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        if (txt_code.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        user = User.getUser();
        session = new SessionManager(getContext(), getActivity());

        logger = AppEventsLogger.newLogger(getActivity());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            txt_code.setLetterSpacing(1);
        }

        txt_verifikasi.setText(Helper.fromHtml("Kami sudah mengirim kode verifikasi ke nomor handphone anda."));

        setCoundown();
        txt_code.enableKeypad();

        User user = realm.where(User.class).findFirst();
        if (user != null) {
            realm.beginTransaction();
            user.device_id = Helper.getDeviceId(getActivity());
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
        }

        return root;
    }

    @OnClick(R.id.btn_bantuan)
    public void bantuan() {
        Freshchat.showConversations(getContext());
    }

    public void setCoundown() {
        btn_reset.setEnabled(false);
        new CountDownTimer(countTimer * 60000, 1000) {

            public void onTick(long millisUntilFinished) {
                btn_reset.setText("Kirim Ulang ( " + millisUntilFinished / 1000 + " )");

            }

            public void onFinish() {
                btn_reset.setEnabled(true);
                btn_reset.setText("Kirim Ulang");
            }
        }.start();

    }

    @OnClick(R.id.btn_reset)
    public void reset() {
        resend_code();
    }

    @OnClick(R.id.btn_continue)
    public void btn_continue() {
//        String code = txt_code.getText().toString();
        String code = txt_code.getOTP();
        if (code.trim().equals("")) {
            Toast.makeText(getActivity(), "Kode verifikasi tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            verify_code();
        }
    }

    public void verify_code() {
        Helper.hideSoftKeyboard(getActivity());
//        String code = txt_code.getText().toString();
        String code = txt_code.getOTP();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("phone", user.phone_number);
            params.put("code", code);

            BisatopupApi.post("/user/verify-phone", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                            Bundle parameters = new Bundle();
                            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Nama : " + object.getJSONObject("data").getString("name") + ", Email : " + object.getJSONObject("data").getString("email"));

                            logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, 0, parameters);

                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), object.getString("key"));
                            session.createLoginSession(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), "");
                            registerUser();


                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void resend_code() {
        Helper.hideSoftKeyboard(getActivity());
//        String code = txt_code.getText().toString();
        String code = txt_code.getOTP();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("phone", user.phone_number);
            params.put("code", code);

            BisatopupApi.post("/user/resend-code", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            countTimer++;
                            setCoundown();
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerUser() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        final User user = User.getUser();
        String token = Helper.getFirebaseInstanceId(getContext());
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("name", user.fullname);
            params.put("token", token);
            params.put("device_id", Helper.getDeviceId(getActivity()));

            BisatopupApi.post("/user/register-user", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {

                        // Intent intent_notif = new Intent( Login.MESSAGE_NOTIFIER);
                        // intent_notif.setAction("exit");

                        getActivity().finish();
                        session.checkLogin();

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
