package com.eisbetech.ikhwanmart.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Nominal;
import com.eisbetech.ikhwanmart.item.NominalItem;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 6/20/2017.
 */

public class FragmentHargaPulsa extends BaseFragment {

    @BindView(R.id.layout_info)
    protected LinearLayout layout_info;

    @BindView(R.id.txt_info)
    protected TextView txt_info;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.spinner_jenis_voucher)
    protected MaterialSpinner spinner_jenis_voucher;

    @BindView(R.id.list)
    protected RecyclerView list;

    protected List<Product> parent_produt;
    private int parent_id;
    private Product result;
    private Product parent_product;
    private FastItemAdapter<NominalItem> fastItemAdapter;

    User user ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_harga_pulsa, container, false);
        ButterKnife.bind(this, root);
        Bundle bundle = getArguments();
        parent_id = bundle.getInt("id", 1);
        parent_produt = realm.where(Product.class)
                .equalTo("parent_id", parent_id)
                .findAll();

        List<String> voucher = new ArrayList<>();
        user = User.getUser();
        for (Product product : parent_produt) {
            voucher.add(product.product_name);

        }
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);
        spinner_jenis_voucher.setItems(voucher);
        layout_info.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        list.setLayoutManager(linearLayoutManager);
        //list.setLayoutManager(gridLayoutManager);
        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);
//        txt_nominal.setVisibility(View.GONE);

        parent_product = realm.where(Product.class)
                .equalTo("parent_id", parent_id)
                .findFirst();
        getNominal(parent_product.id,true);

        spinner_jenis_voucher.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                parent_product = realm.where(Product.class)
                        .equalTo("product_name", (String) item)
                        .findFirst();
                getNominal(parent_product.id,true);

            }
        });

        return root;
    }

    private void getNominal(final int id, final boolean showloading) {
        layout_info.setVisibility(View.GONE);

        if (!parent_product.description.isEmpty() && !parent_product.description.trim().equals("") && parent_product.description != null && !parent_product.description.equals("null")) {
            layout_info.setVisibility(View.VISIBLE);
            txt_info.setText(Helper.fromHtml("Info : <br> " + parent_product.description));
        } else {
            layout_info.setVisibility(View.GONE);
        }

        fastItemAdapter.clear();
        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();


            BisatopupApi.get("/product/detail/" + id,  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    android_empty.setVisibility(View.GONE);

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {


                        JSONArray data = new JSONArray(responseString);

                        List<NominalItem> nominalItems = new ArrayList<>();


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Nominal nominal = new Nominal();
                            nominal.product_name = obj.getString("name");
                            nominal.price = obj.getString("price");
                            if (user.is_agent == 1) {
                                nominal.price = obj.getString("base_price");
                            }
                            nominal.id = obj.getString("product_detail_id");
                            nominal.desc = obj.getString("desc");
                            nominal.is_gangguan = obj.getInt("is_gangguan");
                            nominalItems.add(new NominalItem(nominal));
//                            nominal.is_ubah_harga = true;
                        }

                        fastItemAdapter.add(nominalItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        android_empty.setVisibility(View.VISIBLE);
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
