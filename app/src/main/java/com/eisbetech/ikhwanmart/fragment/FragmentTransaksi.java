package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.adapter.TransaksiItem;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Payment;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.eisbetech.ikhwanmart.item.Withdraw.WithItem;
import com.eisbetech.ikhwanmart.item.Withdraw.Withdraw;
import com.eisbetech.ikhwanmart.withdraw.WithdrawItem;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.items.ProgressItem;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by khadafi on 8/25/2016.
 *
 */
public class FragmentTransaksi extends Fragment implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.riwayat_spinner)
    Spinner mRiwayatSpinner;
    private String date_step;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.txt_date_from)
    protected TextView txt_date_from;

    @BindView(R.id.txt_date_to)
    protected TextView txt_date_to;

    @BindView(R.id.txt_no_hp)
    protected TextView txt_no_hp;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.layout_tanggal)
    protected LinearLayout layout_tanggal;

    @BindView(R.id.layout_cari)
    protected LinearLayout layout_cari;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.swiperefresh)
    protected SwipeRefreshLayout swiperefresh;

    private FooterAdapter<ProgressItem> footerAdapter;

    private FastItemAdapter<TransaksiItem> fastItemAdapter;
    private FastItemAdapter<WithdrawItem> withdrawItemFastItemAdapter;
    int mode = 0;
    private Realm realm;
    private SessionManager session;
    private HashMap<String, String> user;
    private User userData;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_transaksi, container, false);
        ButterKnife.bind(this, root);
        android_empty.setVisibility(View.GONE);

        session = new SessionManager(getActivity().getApplicationContext(), getActivity());
        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        realm =  RealmController.with(this).getRealm();
        userData = realm.where(User.class).equalTo("email", email).findFirst();

        fastItemAdapter = new FastItemAdapter<>();
        withdrawItemFastItemAdapter = new FastItemAdapter<>();
        footerAdapter = new FooterAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setNestedScrollingEnabled(false);

        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<TransaksiItem>() {
            @Override
            public boolean onClick(View v, IAdapter<TransaksiItem> adapter, TransaksiItem item,
                                   int position) {
                Intent intent = new Intent(getActivity(), MainLayout.class);
                intent.setAction(S.action_detail);
                intent.putExtra(K.trans_id, item.transaksi);
                startActivity(intent);
                return false;
            }
        });

        onLoadMore();

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fastItemAdapter.clear();
                        withdrawItemFastItemAdapter.clear();
                        if (mode == 0) {
                            getTransaksi(1, true);
                        } else {
                            getWithdrawTransaksi(userData.user_id+"", 1, true);
                        }
                        swiperefresh.setRefreshing(false);
                    }
                }, 500);
            }
        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getTransaksi(1,true);
//            }
//        }, 500);

        SimpleDateFormat format1 = new SimpleDateFormat("01 MMM yyyy", Locale.US);
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        txt_date_from.setText(format1.format(calendar.getTime()));
        txt_date_to.setText(format2.format(calendar.getTime()));

        mRiwayatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mode = position;
                if (position == 0) {
                    fastItemAdapter.clear();
                    getTransaksi(0, true);
                } else {
                    withdrawItemFastItemAdapter.clear();
                    getWithdrawTransaksi(userData.user_id+"", 1, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }


    private void onLoadMore() {
        list.addOnScrollListener(new EndlessRecyclerOnScrollListener(footerAdapter) {
            @Override
            public void onLoadMore(int currentPage) {
//                footerAdapter.clear();
//                footerAdapter.add(new ProgressItem().withEnabled(false));
                if (!rotateloading.isStart()) {
                    if (mode == 0) {
                        getTransaksi(currentPage, true);
                    }
                }
                System.out.println(currentPage);
            }
        });
    }

    @OnClick(R.id.layout_date_from)
    public void open_date_from() {
        show_date(S.from, txt_date_from.getText().toString().trim());
    }

    @OnClick(R.id.layout_date_to)
    public void open_date_to() {
        show_date(S.to, txt_date_to.getText().toString().trim());
    }

    @OnClick(R.id.btn_cari)
    public void cari() {
        fastItemAdapter.clear();
        Helper.hideSoftKeyboard(getActivity());
        if (mode == 0) {
            getTransaksi(1, true);
        } else {
            getWithdrawTransaksi(userData.user_id+"", 1, true);
        }
    }

    public void show_date(String date_step, String date) {
        this.date_step = date_step;
        Calendar now = Calendar.getInstance();

        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        date = format3.format(calendar.getTime());

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (!date.isEmpty()) {
            String[] dateArr = date.split("-");
            try {
                dpd = DatePickerDialog.newInstance(
                        this,
                        Integer.parseInt(dateArr[2]),
                        Integer.parseInt(dateArr[1]) - 1,
                        Integer.parseInt(dateArr[0])
                );
            } catch (Exception Ec) {
            }
        }
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), S.datepicker_dialog);
    }


    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getTransaksi(1, true);
            }
        }, 500);
    }

    public void getWithdrawTransaksi(String id, final int page, final boolean showloading) {
        rotateloading.start();

        final OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("id", id)
                .build();

        final Request request = new Request.Builder()
                .url("http://api.trans.ikhwan.eisbetech.com/api/v1/list/withdraw")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("unik", e.getLocalizedMessage() + "");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rotateloading.stop();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String textResponse = response.body().string();

                final List<WithdrawItem> withItems = new ArrayList<>();
                Withdraw withdraw = new Gson().fromJson(textResponse, Withdraw.class);

                for (WithItem withItem : withdraw.getData()) {
                    withItems.add(new WithdrawItem(withItem));
                }

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Collections.reverse(withItems);
                        withdrawItemFastItemAdapter.add(withItems);
                        list.setAdapter(withdrawItemFastItemAdapter);

                        Log.d("jumlah",withItems.size()+"");

                        if (withItems.size() == 0)
                            android_empty.setVisibility(View.VISIBLE);

                        rotateloading.stop();
                    }
                });
            }
        });
    }

    private void getTransaksi(final int page, final boolean showloading) {
        android_empty.setVisibility(View.GONE);

        if (showloading) {
            rotateloading.start();
        }

        try {
            RequestParams params = new RequestParams();
            params.put(K.page, page);
            params.put(K.date_from, getDateSql(txt_date_from.getText().toString().trim()));
            params.put(K.date_to, getDateSql(txt_date_to.getText().toString().trim()));
            params.put(K.find, txt_no_hp.getText().toString().trim());

            BisatopupApi.get("/transaksi/history", params, getContext(),
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            if (showloading) rotateloading.stop();
                            // footerAdapter.clear();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            if (showloading) rotateloading.stop();
                            //   footerAdapter.clear();
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject response = new JSONObject(responseString);
                                JSONArray data = response.getJSONArray(K.data);
                                List<TransaksiItem> transaksiItems = new ArrayList<>();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject obj = data.getJSONObject(i);
                                    Transaksi transaksi = new Transaksi();
                                    transaksi.trans_id = obj.getString(K.trans_id);
                                    transaksi.product_id = obj.getInt(K.product_id);
                                    transaksi.is_tagihan = obj.getInt(K.is_tagihan);
                                    transaksi.tanggal = obj.getString(K.date);
                                    transaksi.time = obj.getString(K.time);
                                    transaksi.harga = obj.getString(K.harga);
                                    transaksi.product = obj.getString(K.product);
                                    transaksi.product_name = obj.getString(K.product_name);
                                    transaksi.product_detail = obj.getString(K.product_detail);
                                    transaksi.product_detail_id = obj.getString(K.product_detail_id);
                                    transaksi.no_pelanggan = obj.getString(K.no_pelanggan);
                                    transaksi.status = obj.getString(K.status);
                                    transaksi.token = obj.getString(K.token);
                                    transaksi.note = obj.getString(K.note);
                                    transaksi.payment = obj.getString(K.payment);
                                    transaksi.base_price = obj.getString(K.base_price);
                                    transaksi.status_id = obj.getInt(K.status_id);
                                    transaksi.image_url = obj.getString(K.image_url);
                                    transaksi.product_detail_id = obj.getString(K.product_detail);
                                    transaksi.admin_fee = obj.getString(K.admin_fee);
                                    transaksi.kode_unik = obj.getString(K.unique_code);
                                    transaksi.no_hp_pelanggan = obj.getString(K.customer_phone_no);
                                    transaksi.expired_date = obj.getString(K.expired_date);
                                    transaksi.status_color = obj.getString("status_color");
                                    JSONObject pay_ojb = obj.getJSONObject(K.pembayaran);
                                    Payment payment = new Payment();
                                    payment.rekening = pay_ojb.getString(K.rekening);
                                    payment.name = pay_ojb.getString(K.name);
                                    payment.bank = pay_ojb.getString(K.bank);
                                    payment.id = pay_ojb.getInt(K.payment_id);
                                    payment.image_url = pay_ojb.getString("image_url");

                                    transaksi.payment_method = payment;
                                    transaksiItems.add(new TransaksiItem(transaksi));
                                }

                                if (page == 1) {
                                    fastItemAdapter.clear();
                                    onLoadMore();
                                }

                                if (page == 1 && transaksiItems.size() == 0)
                                    android_empty.setVisibility(View.VISIBLE);


                                fastItemAdapter.add(transaksiItems);
                                list.setAdapter(fastItemAdapter);
                                handler = new Handler();

                                refresh = new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            getTransaksi(1, true);
                                        } catch (Exception ec) {
                                            ec.printStackTrace();
                                        }
                                    }
                                };
//                        handler.postDelayed(refresh, 10 * 1000);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (page == 1) android_empty.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Handler handler;
    Runnable refresh;

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format("%02d-%02d-%d", dayOfMonth, (monthOfYear + 1), year);

        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        date = format3.format(calendar.getTime());

        if (date_step.equals(S.from)) {
            txt_date_from.setText(date);
        } else {
            txt_date_to.setText(date);
        }
        getTransaksi(1, true);
    }

    private String getDateSql(String date) {
        Date date_ = null;
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try {
            date_ = format2.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_);

        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        date = format3.format(calendar.getTime());

        return date;
    }
}
