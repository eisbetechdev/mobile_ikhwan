package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.LockPinActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.AppsUtil;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.Favorite;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Payment;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.crashlytics.android.Crashlytics;
import com.doku.sdkocov2.DirectSDK;
import com.doku.sdkocov2.interfaces.iPaymentCallback;
import com.doku.sdkocov2.model.PaymentItems;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 9/8/2016.
 */
public class CheckoutFragment extends BaseFragment implements View.OnClickListener {

    int REQUEST_CODE_VIRTUALACCOUNT = 4;


    @BindView(R.id.btn_buy)
    protected FancyButton btn_buy;

    @BindView(R.id.card_view_bca)
    protected CardView card_view_bca;

    @BindView(R.id.card_view_mandiri)
    protected CardView card_view_mandiri;

    @BindView(R.id.card_view_bni)
    protected CardView card_view_bni;

    @BindView(R.id.card_view_bri)
    protected CardView card_view_bri;

    @BindView(R.id.card_view_bsm)
    protected CardView card_view_bsm;

    @BindView(R.id.card_view_cimb)
    protected CardView card_view_cimb;

    @BindView(R.id.card_view_bca_va)
    protected CardView card_view_bca_va;

    @BindView(R.id.card_view_mandiri_va)
    protected CardView card_view_mandiri_va;

    @BindView(R.id.card_view_bni_va)
    protected CardView card_view_bni_va;

    @BindView(R.id.card_view_bri_va)
    protected CardView card_view_bri_va;

    @BindView(R.id.card_view_danamon_va)
    protected CardView card_view_danamon_va;

    @BindView(R.id.card_view_permata_va)
    protected CardView card_view_permata_va;

    @BindView(R.id.card_view_cimb_va)
    protected CardView card_view_cimb_va;

    @BindView(R.id.card_view_maybank_va)
    protected CardView card_view_maybank_va;

    @BindView(R.id.card_view_doku)
    protected CardView card_view_doku;

    @BindView(R.id.card_view_minimarket)
    protected CardView card_view_minimarket;

    @BindView(R.id.card_view_atm)
    protected CardView card_view_atm;

    @BindView(R.id.txt_info_bank_transfer)
    protected TextView txt_info_bank_transfer;

    @BindView(R.id.txt_info_va)
    protected TextView txt_info_va;

    @BindView(R.id.txt_lain_lain)
    protected TextView txt_lain_lain;

    @BindView(R.id.txt_info)
    protected TextView txt_info;

    @BindView(R.id.txt_product)
    protected TextView txt_product;

    @BindView(R.id.txt_product_detail)
    protected TextView txt_product_detail;

    @BindView(R.id.txt_no_tujuan)
    protected TextView txt_no_tujuan;

    @BindView(R.id.txt_total)
    protected TextView txt_total;

    @BindView(R.id.txt_saldo)
    protected TextView txt_saldo;

    @BindView(R.id.txt_detail_name)
    protected TextView txt_detail_name;

    @BindView(R.id.img_product)
    protected ImageView img_product;

    @BindView(R.id.radio_bca)
    protected RadioButton radio_bca;

    @BindView(R.id.radio_bni)
    protected RadioButton radio_bni;

    @BindView(R.id.radio_bri)
    protected RadioButton radio_bri;

    @BindView(R.id.radio_bsm)
    protected RadioButton radio_bsm;

    @BindView(R.id.radio_mandiri)
    protected RadioButton radio_mandiri;

    @BindView(R.id.radio_cimb)
    protected RadioButton radio_cimb;

    @BindView(R.id.radio_wallet)
    protected RadioButton radio_wallet;

    @BindView(R.id.radio_bca_va)
    protected RadioButton radio_bca_va;

    @BindView(R.id.radio_bni_va)
    protected RadioButton radio_bni_va;

    @BindView(R.id.radio_bri_va)
    protected RadioButton radio_bri_va;

    @BindView(R.id.radio_mandiri_va)
    protected RadioButton radio_mandiri_va;

    @BindView(R.id.radio_cimb_va)
    protected RadioButton radio_cimb_va;

    @BindView(R.id.radio_danamon_va)
    protected RadioButton radio_danamon_va;

    @BindView(R.id.radio_permata_va)
    protected RadioButton radio_permata_va;

    @BindView(R.id.radio_maybank_va)
    protected RadioButton radio_maybank_va;

    @BindView(R.id.radio_doku)
    protected RadioButton radio_doku;

    @BindView(R.id.radio_minimarket)
    protected RadioButton radio_minimarket;

    @BindView(R.id.radio_atm)
    protected RadioButton radio_atm;

    @BindView(R.id.btn_cek)
    protected FancyButton btn_cek;

    @BindView(R.id.layout_detail_name)
    protected LinearLayout layout_detail_name;

    @BindView(R.id.layout_no_hp_pelanggan)
    protected LinearLayout layout_no_hp_pelanggan;

    @BindView(R.id.txt_no_hp_pelanggan)
    protected TextView txt_no_hp_pelanggan;

    @BindView(R.id.txt_nama)
    protected TextView txt_nama;

    @BindView(R.id.sw_favorite)
    protected Switch sw_favorite;

    @BindView(R.id.txt_nama_favorite)
    protected MaterialEditText txt_nama_favorite;

    protected int payment_id;
    protected String product_id;

    boolean is_tagihan = false;
    String category;
    int tagihan_id;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_checkout, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();
        category  = bundle.getString("category");
        String product = bundle.getString("product");
        int parent_id = bundle.getInt("parent_id");
        product_id = bundle.getString("product_id");
        if(product_id == null){
            int id_pro= bundle.getInt("product_id",0);
            if(id_pro != 0){
                product_id = String.valueOf(id_pro);
            }
        }
        is_tagihan = bundle.getBoolean("is_tagihan");
        User user = User.getUser();
        int price = Integer.parseInt(bundle.getString("price"));

        txt_nama_favorite.setVisibility(View.GONE);
        txt_nama.setVisibility(View.GONE);
        if (user.is_agent == 1) {
            card_view_bca.setVisibility(View.GONE);
            card_view_bni.setVisibility(View.GONE);
            card_view_bri.setVisibility(View.GONE);
            card_view_bsm.setVisibility(View.GONE);
            card_view_mandiri.setVisibility(View.GONE);
            card_view_cimb.setVisibility(View.GONE);

            card_view_bca_va.setVisibility(View.GONE);
            card_view_bni_va.setVisibility(View.GONE);
            card_view_bri_va.setVisibility(View.GONE);
            card_view_mandiri_va.setVisibility(View.GONE);

            card_view_danamon_va.setVisibility(View.GONE);
            card_view_cimb_va.setVisibility(View.GONE);
            card_view_permata_va.setVisibility(View.GONE);
            card_view_maybank_va.setVisibility(View.GONE);

            card_view_doku.setVisibility(View.GONE);
            card_view_minimarket.setVisibility(View.GONE);
            card_view_atm.setVisibility(View.GONE);
            txt_info_bank_transfer.setVisibility(View.GONE);
            txt_info_va.setVisibility(View.GONE);
            txt_lain_lain.setVisibility(View.GONE);

        } else {
            card_view_bca.setVisibility(View.VISIBLE);
            card_view_bni.setVisibility(View.GONE);
            card_view_bri.setVisibility(View.GONE);
            card_view_bsm.setVisibility(View.GONE);
            card_view_mandiri.setVisibility(View.GONE);
            card_view_cimb.setVisibility(View.GONE);

            if(price >= 10000){
                txt_info_va.setVisibility(View.VISIBLE);
                card_view_bca_va.setVisibility(View.VISIBLE);
                card_view_bni_va.setVisibility(View.GONE);
                card_view_bri_va.setVisibility(View.GONE);
                card_view_mandiri_va.setVisibility(View.GONE);

                card_view_danamon_va.setVisibility(View.GONE);
                card_view_cimb_va.setVisibility(View.GONE);
                card_view_permata_va.setVisibility(View.GONE);
                card_view_maybank_va.setVisibility(View.GONE);
            }else{
                txt_info_va.setVisibility(View.GONE);
                card_view_bca_va.setVisibility(View.GONE);
                card_view_bni_va.setVisibility(View.GONE);
                card_view_bri_va.setVisibility(View.GONE);
                card_view_mandiri_va.setVisibility(View.GONE);

                card_view_danamon_va.setVisibility(View.GONE);
                card_view_cimb_va.setVisibility(View.GONE);
                card_view_permata_va.setVisibility(View.GONE);
                card_view_maybank_va.setVisibility(View.GONE);
            }

            txt_info_bank_transfer.setVisibility(View.VISIBLE);

            txt_lain_lain.setVisibility(View.VISIBLE);


//            card_view_doku.setVisibility(View.VISIBLE);
//            card_view_minimarket.setVisibility(View.VISIBLE);
//            card_view_atm.setVisibility(View.VISIBLE);
        }

        txt_product.setText(product);
        txt_product_detail.setText(bundle.getString("nominal"));
        txt_no_tujuan.setText(bundle.getString("no_tujuan"));
        txt_total.setText("0");
        String price_txt = bundle.getString("price");
        price_txt = price_txt.replace(".0","");
        try{
            txt_total.setText(Helper.format_money(price_txt));
        }catch (Exception ec){
            Crashlytics.logException(ec);
        }
        txt_saldo.setText("Sisa saldo anda adalah " + user.wallet);

        String img_url = bundle.getString("img");

        if (bundle.getInt("tagihan_id", 0) != 0) {
            tagihan_id = bundle.getInt("tagihan_id", 0);
        }


        if (img_url != null) {

//            Glide.with(getActivity().getApplicationContext()).load(img_url)
//                    .placeholder(R.drawable.android_launcher)
//
//                    .into(img_product);

            GlideApp.with(getActivity().getApplicationContext())
                    .load(img_url)
                    .placeholder(R.drawable.logo)
                    .fitCenter()
                    .into(img_product);

        } else {
//            Glide.clear(img_product);
            GlideApp.with(getActivity().getApplicationContext()).clear(img_product);
//            GlideApp.with(img_product).clear(img_product);

        }
        txt_info.setVisibility(View.GONE);
        radio_wallet.setChecked(true);
        radio_bca.setOnClickListener(this);
        radio_bni.setOnClickListener(this);
        radio_bri.setOnClickListener(this);
        radio_bsm.setOnClickListener(this);
        radio_cimb.setOnClickListener(this);

        radio_bca_va.setOnClickListener(this);
        radio_bni_va.setOnClickListener(this);
        radio_bri_va.setOnClickListener(this);
        radio_mandiri_va.setOnClickListener(this);
        radio_cimb_va.setOnClickListener(this);
        radio_permata_va.setOnClickListener(this);
        radio_danamon_va.setOnClickListener(this);
        radio_maybank_va.setOnClickListener(this);

        radio_mandiri.setOnClickListener(this);
        radio_wallet.setOnClickListener(this);
        radio_doku.setOnClickListener(this);
        radio_minimarket.setOnClickListener(this);
        radio_atm.setOnClickListener(this);

        payment_id = 13;
        layout_detail_name.setVisibility(View.GONE);

        txt_no_hp_pelanggan.setText("");
        if (!TextUtils.isEmpty(user.phone_number)) {
            txt_no_hp_pelanggan.append(user.phone_number);
        }
        if (parent_id == 2) {
            btn_cek.setVisibility(View.VISIBLE);
            layout_no_hp_pelanggan.setVisibility(View.VISIBLE);
            layout_no_hp_pelanggan.requestFocus();
        } else {
            btn_cek.setVisibility(View.GONE);
            layout_no_hp_pelanggan.setVisibility(View.GONE);

        }

        sw_favorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    txt_nama_favorite.setVisibility(View.VISIBLE);
                } else {
                    txt_nama_favorite.setVisibility(View.GONE);
                }
            }
        });

        Favorite favorite = realm.where(Favorite.class)
                .equalTo("phone_number", txt_no_tujuan.getText().toString())
                .findFirst();

        if (favorite != null) {
            sw_favorite.setVisibility(View.GONE);
            txt_nama.setText(favorite.nama);
            txt_nama.setVisibility(View.VISIBLE);
        }

//        TransactionRequest transactionRequest = new TransactionRequest("123", Double.parseDouble(bundle.getString("price")));
//        ItemDetails itemDetails1 = new ItemDetails("123", Integer.parseInt(bundle.getString("price")), 1, bundle.getString("nominal"));
//        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
//        itemDetailsList.add(itemDetails1);
//        transactionRequest.setItemDetails(itemDetailsList);
//
//        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        btn_buy.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        radio_bca.setChecked(false);
        radio_bni.setChecked(false);
        radio_bri.setChecked(false);
        radio_bsm.setChecked(false);
        radio_mandiri.setChecked(false);
        radio_cimb.setChecked(false);

        radio_bca_va.setChecked(false);
        radio_bni_va.setChecked(false);
        radio_bri_va.setChecked(false);
        radio_mandiri_va.setChecked(false);

        radio_cimb_va.setChecked(false);
        radio_permata_va.setChecked(false);
        radio_danamon_va.setChecked(false);
        radio_maybank_va.setChecked(false);

        radio_wallet.setChecked(false);
        radio_minimarket.setChecked(false);
        radio_doku.setChecked(false);
        radio_atm.setChecked(false);

        RadioButton thisradio = (RadioButton) view;
        thisradio.setChecked(true);
        switch (view.getId()) {
            case R.id.radio_wallet:
                payment_id = 13;
                txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_bca:
                payment_id = 1;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_bni:
                payment_id = 3;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_bri:
                payment_id = 4;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_bsm:
                payment_id = 6;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_cimb:
                payment_id = 21;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_mandiri:
                payment_id = 2;
                txt_info.setVisibility(View.VISIBLE);
                break;
            case R.id.radio_doku:
                payment_id = 14;
                txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_minimarket:
                payment_id = 15;
                txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_atm:
                payment_id = 16;
                txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_bca_va:
                payment_id = I.payment_bca_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_bni_va:
                payment_id = I.payment_bni_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_bri_va:
                payment_id = I.payment_bri_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_mandiri_va:
                payment_id = I.payment_mandiri_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_cimb_va:
                payment_id = I.payment_cimb_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_danamon_va:
                payment_id = I.payment_danamon_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_permata_va:
                payment_id = I.payment_permata_va;
                 txt_info.setVisibility(View.GONE);
                break;
            case R.id.radio_maybank_va:
                payment_id = I.payment_maybank_va;
                 txt_info.setVisibility(View.GONE);
                break;

        }
    }

    @OnClick(R.id.btn_buy)
    public void Buy() {
        if(category != null &&  sw_favorite.isChecked()){
            if(txt_nama_favorite.getText().toString().isEmpty()){
                Helper.show_alert("Error","Nama favorite tidak boleh kosong",getActivity());
                return;
            }
        }
        btn_buy.setEnabled(false);
        User user = User.getUser();
        if (user != null && user.lock_pin_enable != null && user.lock_pin_enable) {
            Intent intent = new Intent(getActivity(), LockPinActivity.class);
            intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
            intent.setAction("Masukkan PIN");
            startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
        } else {
            if(category != null &&  sw_favorite.isChecked()){

                Favorite favorite = new Favorite();
                favorite.id = favorite.getNextPrimaryKey(realm);
                favorite.nama = txt_nama_favorite.getText().toString() +" ("+category+")";
                favorite.phone_number = txt_no_tujuan.getText().toString() ;
                favorite.category = category;
                if(category.equals("pulsa")){
                    favorite.product_id = 1;
                }else if(category.equals("token")){
                    favorite.product_id = 2;
                }else if(category.equals("paket_data")){
                    favorite.product_id = 84;
                }else if(category.equals(S.insurance)){
                    favorite.product_id = 92;
                }else if(category.equals(S.gas)){
                    favorite.product_id = 156;
                }else favorite.product_id = 30;

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(favorite);
                realm.commitTransaction();

                simpanFavorite(favorite);
            }
            if (!is_tagihan) {
//                if (payment_id == 14) {
//                    add_payment(2);
//                } else if (payment_id == 15) {
//                    Intent i = new Intent(getActivity(), VirtualAccountAlfa.class);
//                    startActivityForResult(i, REQUEST_CODE_VIRTUALACCOUNT);
//                } else {
//                    beli_pulsa();
//                }
                beli_pulsa();
//                MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.CREDIT_CARD);
//                if (ActivityCompat.checkSelfPermission(getActivity(), String.valueOf(PERMISSION_PHONE))
//                        != PackageManager.PERMISSION_GRANTED) {
//                    getPermissionFirst(2);
//                } else {
//                    add_payment(2);
//                }
            } else {
                bayar_tagihan();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void getPermissionFirst(int paymentChanel) {
        PayChanChoosed = paymentChanel;
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_PHONE_STATE)) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PHONE);

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PHONE);
        }

    }

    String responseToken, Challenge1, Challenge2, Challenge3, debitCard;
    int PayChanChoosed = 0;
    private static final int REQUEST_PHONE = 1;
    private static String[] PERMISSION_PHONE = {Manifest.permission.READ_PHONE_STATE};
    String tokenPayment = null;
    String customerID = null;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (PayChanChoosed != 0) {
                    if (PayChanChoosed == 1 || PayChanChoosed == 2) {

                        if (tokenPayment != null) {
                            // connectSecondPay(PayChanChoosed);
                        } else if (customerID != null) {
                            // connectFirstPay(PayChanChoosed);
                        } else {
                            add_payment(PayChanChoosed);
                        }
                    } else if (PayChanChoosed == 3) {
                        //  new MandiriPayment().execute();
                    }
                } else {
                    Toast.makeText(getContext(), "PAYCHAN ERROR", Toast.LENGTH_SHORT);
                }
            } else {
                Toast.makeText(getContext(), "Permission Failed", Toast.LENGTH_SHORT);
            }
        }
    }

    private PaymentItems InputCard() {
        User user = User.getUser();

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

        String merchan_code = "4837";
        String shared_key = "xxx";
        String invoiceNumber = "123123";

        PaymentItems paymentItems = new PaymentItems();
        paymentItems.setDataAmount(AppsUtil.generateMoneyFormat("15000"));
        paymentItems.setDataBasket("[{\"name\":\"sayur\",\"amount\":\"10000.00\",\"quantity\":\"1\",\"subtotal\":\"10000.00\"},{\"name\":\"buah\",\"amount\":\"10000.00\",\"quantity\":\"1\",\"subtotal\":\"10000.00\"}]");
        paymentItems.setDataCurrency("360");
        paymentItems.setDataWords(AppsUtil.SHA1(AppsUtil.generateMoneyFormat("15000") + merchan_code + "2E7mOz6dw5DE" + invoiceNumber + 360 + telephonyManager.getDeviceId()));
        paymentItems.setDataMerchantChain("NA");
        paymentItems.setDataSessionID(String.valueOf(AppsUtil.nDigitRandomNo(9)));
        paymentItems.setDataTransactionID(invoiceNumber);
        paymentItems.setDataMerchantCode(merchan_code);
        paymentItems.setDataImei(telephonyManager.getDeviceId());
        paymentItems.setMobilePhone(user.phone_number);
        paymentItems.setCustomerID(String.valueOf(user.user_id));
        paymentItems.setPublicKey("xxx");
        paymentItems.isProduction(false);
        return paymentItems;
    }

    private void add_payment(int menuPaymentChannel) {

        String invoiceNumber = String.valueOf(AppsUtil.nDigitRandomNo(10));

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        DirectSDK directSDK = new DirectSDK();

        PaymentItems cardDetails = null;
        cardDetails = InputCard();
//        paymentItems.setTokenPayment(tokenPayment);
        directSDK.setCart_details(cardDetails);

//        directSDK.setPaymentChannel(4);
        directSDK.setPaymentChannel(menuPaymentChannel);

        directSDK.getResponse(new iPaymentCallback() {
            @Override
            public void onSuccess(final String text) {
                try {
                    JSONObject respongetTokenSDK = new JSONObject(text);
                    if (respongetTokenSDK.getString("res_response_code").equalsIgnoreCase("0000")) {
//                      do your background AsyncTask service to merchant server handler here
                        System.out.println(respongetTokenSDK.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(final String text) {
                Log.d("onError", text);
                Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
//error handling here
            }

            @Override
            public void onException(Exception eSDK) {
                eSDK.printStackTrace();
            }
        }, getContext());
    }

    @OnClick(R.id.btn_cek)
    public void cek_detail() {
        cek_token();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == LockPinActivity.RESULT_PIN_INPUT_VALIDATE) {
            boolean validate = data.getBooleanExtra("validate", false);
            if (validate) {
                if (!is_tagihan) {
                    beli_pulsa();
                } else {
                    bayar_tagihan();
                }
            }
        }

    }

    private void cek_token() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("product_detail", product_id);
            params.put("nomor_rekening", txt_no_tujuan.getText().toString().trim());
            params.put("payment_method", payment_id);

            BisatopupApi.post("/tagihan/cek-prabayar", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        final JSONObject response = new JSONObject(responseString);
                        final JSONObject data = response.getJSONObject("data");

                        txt_detail_name.setText(data.getString("nama"));
                        layout_detail_name.setVisibility(View.VISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void beli_pulsa() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);

        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("product_detail", product_id);
            params.put("phone_number", txt_no_tujuan.getText().toString().trim());
            params.put("no_hp_pelanggan", txt_no_hp_pelanggan.getText().toString().trim());
            params.put("payment_method", payment_id);

            BisatopupApi.post("/transaksi/beli", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        Toast.makeText(getActivity(), "Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih", Toast.LENGTH_SHORT).show();

//                        Helper.show_message(getContext(),"Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih",getActivity());
                        Helper.show_alert("Info", "Transaksi anda tidak dapat diproses, mohon kontak cs kami. Terima kasih", getActivity());
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                            Helper.show_message(getContext(),message,getActivity());
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        final JSONObject response = new JSONObject(responseString);
                        final JSONObject obj = response.getJSONObject("transaksi");
                        Transaksi transaksi = new Transaksi();
                        transaksi.trans_id = obj.getString("trans_id");
                        transaksi.tanggal = obj.getString("tanggal");
                        transaksi.time = obj.getString("time");
                        transaksi.harga = obj.getString("harga");
                        transaksi.product_id = obj.getInt("product_id");
                        transaksi.admin_fee = obj.getString("admin_fee");
                        transaksi.product = obj.getString("product");
                        transaksi.product_name = obj.getString("product_name");
                        transaksi.product_detail = obj.getString("product_detail");
                        transaksi.no_pelanggan = obj.getString("no_pelanggan");
                        transaksi.status = obj.getString("status");
                        transaksi.token = obj.getString("token");
                        transaksi.payment = obj.getString("payment");
                        transaksi.status_id = obj.getInt("status_id");
                        transaksi.base_price = obj.getString("base_price");
                        transaksi.kode_unik = obj.getString("kode_unik");
                        transaksi.note = obj.getString("note");
                        transaksi.no_hp_pelanggan = obj.getString("no_hp_pelanggan");
                        transaksi.expired_date = obj.getString("expired_date");
                        transaksi.status_color = obj.getString("status_color");

                        JSONObject pay_ojb = obj.getJSONObject("pembayaran");
                        Payment payment = new Payment();
                        payment.rekening = pay_ojb.getString("rekening");
                        payment.name = pay_ojb.getString("name");
                        payment.bank = pay_ojb.getString("bank");
                        payment.id = pay_ojb.getInt("payment_id");
                        payment.image_url = pay_ojb.getString("image_url");

                        transaksi.payment_method = payment;

                        getActivity().finish();

                        Intent in = new Intent("clearStackActivity");
                        in.setType("text/plain");
                        getActivity().sendBroadcast(in);

                        Intent intent = new Intent(getActivity(), MainLayout.class);
                        intent.setAction("detail");
                        intent.putExtra("trans_id", transaksi);
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bayar_tagihan() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("product_detail", product_id);
            params.put("phone_number", txt_no_tujuan.getText().toString().trim());
            params.put("payment_method", payment_id);
            params.put("transaction_id", tagihan_id);
            params.put("no_hp_pelanggan", txt_no_hp_pelanggan.getText().toString().trim());

            BisatopupApi.post("/transaksi/bayar", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    btn_buy.setEnabled(true);
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            Helper.show_alert("Info", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        final JSONObject response = new JSONObject(responseString);
                        final JSONObject obj = response.getJSONObject("transaksi");
                        Transaksi transaksi = new Transaksi();
                        transaksi.trans_id = obj.getString("trans_id");
                        transaksi.tanggal = obj.getString("tanggal");
                        transaksi.time = obj.getString("time");
                        transaksi.harga = obj.getString("harga");
                        transaksi.product_id = obj.getInt("product_id");

                        transaksi.product = obj.getString("product");
                        transaksi.product_name = obj.getString("product_name");
                        transaksi.product_detail = obj.getString("product_detail");
                        transaksi.no_pelanggan = obj.getString("no_pelanggan");
                        transaksi.status = obj.getString("status");
                        transaksi.token = obj.getString("token");
                        transaksi.payment = obj.getString("payment");
                        transaksi.status_id = obj.getInt("status_id");
                        transaksi.base_price = obj.getString("base_price");
                        transaksi.kode_unik = obj.getString("kode_unik");
                        transaksi.note = obj.getString("note");

                        transaksi.expired_date = obj.getString("expired_date");

                        JSONObject pay_ojb = obj.getJSONObject("pembayaran");
                        Payment payment = new Payment();
                        payment.rekening = pay_ojb.getString("rekening");
                        payment.name = pay_ojb.getString("name");
                        payment.bank = pay_ojb.getString("bank");

                        transaksi.payment_method = payment;

                        getActivity().finish();

                        Intent in = new Intent("clearStackActivity");
                        in.setType("text/plain");
                        getActivity().sendBroadcast(in);

                        Intent intent = new Intent(getActivity(), MainLayout.class);
                        intent.setAction("detail");
                        intent.putExtra("trans_id", transaksi);
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void simpanFavorite(final Favorite favorite) {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("nama", favorite.nama);
            params.put("nomor_hp", favorite.phone_number);
            params.put("kategori", favorite.category);

            BisatopupApi.post("/favorite/add", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                            realm.beginTransaction();
                            favorite.is_sync = 1;
                            realm.copyToRealmOrUpdate(favorite);
                            realm.commitTransaction();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
