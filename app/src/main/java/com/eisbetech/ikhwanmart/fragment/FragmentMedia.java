package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.BisaContentActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.ImageOverlayView;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.entity.Media;
import com.eisbetech.ikhwanmart.item.MediaItem;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 3/26/2017.
 */

public class FragmentMedia extends BaseFragment {

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.txt_title)
    protected TextView txt_title;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    private FastItemAdapter<MediaItem> fastItemAdapter;
    List<MediaItem> mediasList;
    private ImageOverlayView overlayView;
    String[] media_images;
    int id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_media, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();
        id = bundle.getInt("id", 0);
        android_empty.setVisibility(View.GONE);

        txt_title.setText(bundle.getString("title"));
        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        fastItemAdapter.withSelectable(true);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<MediaItem>() {
            @Override
            public boolean onClick(View v, IAdapter<MediaItem> adapter, MediaItem item, int position) {
                if (item.media.has_child != 0) {
                    Intent intent = new Intent(getActivity(), BisaContentActivity.class);
                    intent.putExtra("id", item.media.id);
                    intent.putExtra("title", item.media.title);
                    intent.setAction("media");
                    startActivity(intent);

                } else {
                    if (item.media.type.equals("Image")) {
                        overlayView = new ImageOverlayView(getActivity());
                        new ImageViewer.Builder<>(getActivity(), mediasList)
                                .setFormatter(getCustomFormatter())
                                .setImageChangeListener(getImageChangeListener())
                                .setOverlayView(overlayView)
                                .setStartPosition(position)
                                .show();
                    } else {

//                        String[] uris = new String[1];
//                        String[] extensions = new String[1];
//                        String extension = null;
//                        uris[0]=item.media.url;
//                        Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
////                    intent.putExtra(MediaPlayerActivity.URI_LIST_EXTRA, uris);
//                        intent.setData(Uri.parse(item.media.url));
//                        intent.putExtra(MediaPlayerActivity.EXTENSION_EXTRA, extension);
////                    intent.putExtra(MediaPlayerActivity.EXTENSION_LIST_EXTRA, extensions);
////                    intent.setAction(MediaPlayerActivity.ACTION_VIEW_LIST);
//                        intent.setAction(MediaPlayerActivity.ACTION_VIEW);
//
//                        startActivity(intent);
                    }

                }
                return true;
            }
        });

        getData(0, true);
        return root;
    }

    private ImageViewer.Formatter<MediaItem> getCustomFormatter() {
        return new ImageViewer.Formatter<MediaItem>() {
            @Override
            public String format(MediaItem customImage) {
                return customImage.media.url;
            }
        };
    }
    private ImageViewer.OnImageChangeListener getImageChangeListener() {
        return new ImageViewer.OnImageChangeListener() {
            @Override
            public void onImageChange(int position) {
                MediaItem image = mediasList.get(position);
                overlayView.setShareText(image.media.url);
                overlayView.setDescription(image.media.title);
            }
        };
    }

    private void getData(final int page, final boolean showloading) {
        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("id",id);


            BisatopupApi.get("/media",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {

                        JSONArray data = new JSONArray(responseString);

                        mediasList = new ArrayList<>();
                        media_images = new String[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Media media = new Media();
                            media.id = obj.getInt("id");
                            ;
                            media.parent_id = obj.getInt("parent_id");
                            media.has_child = obj.getInt("has_child");
                            media.title = obj.getString("title");
                            media.sub_title = obj.getString("sub_title");
                            media.image = obj.getString("image");
                            media.type = obj.getString("type");
                            media.url = obj.getString("url");


                            mediasList.add(new MediaItem(media));
                            if (media.type.equals("Image")) {
                                media_images[i] = media.url;
                            }
                        }

                        fastItemAdapter.add(mediasList);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (page == 1) android_empty.setVisibility(View.VISIBLE);

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
