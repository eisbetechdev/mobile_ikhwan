package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.BisaContentActivity;
import com.eisbetech.ikhwanmart.BuildConfig;
import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.WalletActivity;
import com.eisbetech.ikhwanmart.adapter.HomeMenuAdapter;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.ExpandableHeightGridView;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Menu;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import com.freshchat.consumer.sdk.FaqOptions;
import com.freshchat.consumer.sdk.Freshchat;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

public class HomeFragment extends BaseFragment {
    //    @Nullable
//    @BindView(R.id.grid_view)
//    protected ExpandableHeightGridView gridView;

    @BindView(R.id.txt_wallet)
    protected TextView txt_wallet;
//
//    @BindView(R.id.scroll_view)
//    protected ScrollView scroll_view;
//
//    @BindView(R.id.slider)
//    protected SliderLayout sliderShow;

    SessionManager session;
    HashMap<String, String> user;
    boolean slider_load = false;
    protected List<Product> parent_produt;

    boolean first = true;

    @OnClick(R.id.btn_topup)

    public void topup() {
//        Intent intent = new Intent(getActivity(), WalletActivity.class);
        Intent intent = new Intent(getActivity(), MainLayout.class);
        intent.setAction(S.action_deposit);
        startActivity(intent);
    }

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

        session = new SessionManager(getContext(), getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
//        slider_load = false;
//        sliderShow.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onResume() {
//        sliderShow.startAutoCycle();
        super.onResume();
        if (Helper.isNowConnected(getContext())) {
            if (first) {
                refresh();
                first = false;
            } else {
                getProfile();
            }
        } else {
            reconnect();
        }
    }

    private void reconnect() {
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_wifi).sizeDp(32).color(Color.WHITE);

        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.md_deep_orange_500)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Koneksi gagal")
                .setMessage("Koneksi internet anda bermasalah, silahkan periksa koneksi internet anda.")
                .setPositiveButton("Coba Lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Helper.isNowConnected(getContext())) {
                            refresh();
                        } else {
                            reconnect();
                        }
                    }
                })
                .show();
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        Drawable icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_add_alert)
                .color(Color.WHITE).sizeDp(20);
        menu.add(0, 1, android.view.Menu.NONE, S.menu_notification).setIcon(icon)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_history)
                .color(Color.WHITE).sizeDp(20);
        menu.add(0, 2, android.view.Menu.NONE, S.menu_history).setIcon(icon)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);
        List<Menu> allItems = getAllMenu();

//        gridView.setExpanded(true);
//        HomeMenuAdapter homeMenuAdapter = new HomeMenuAdapter(allItems, getActivity());
//        gridView.setAdapter(homeMenuAdapter);
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
//                if (position == 100) {
//                    Intent intent = new Intent(getActivity(), BisaContentActivity.class);
//                    intent.putExtra(K.id, 0);
//                    intent.putExtra(K.title, S.konten_bisa);
//                    intent.setAction(S.action_media);
//                    startActivity(intent);
//                } else if (position == 0) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 1);
//                    intent.putExtra(K.title, "Beli Pulsa");
//                    startActivity(intent);
//                } else if (position == 1) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 84);
//                    intent.putExtra(K.title, "Beli Paket Data");
//                    startActivity(intent);
//                } else if (position == 2) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 2);
//                    intent.putExtra(K.title, "Beli Token PLN");
//                    startActivity(intent);
//                } else if (position == 3) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.pln);
//                    intent.putExtra(K.title, "Tagihan PLN");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 4) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 12);
//                    intent.setAction(S.action_voucher_game);
//                    intent.putExtra(K.title, "Beli Voucher Game");
//                    startActivity(intent);
//                } else if (position == 5) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 92);
//                    intent.putExtra(K.product, S.insurance);
//                    intent.putExtra(K.title, "Asuransi");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 6) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.tv);
//                    intent.putExtra(K.title, "Tagihan TV");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 7) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.pdam);
//                    intent.putExtra(K.title, "Tagihan PDAM");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 8) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.post_paid_phone);
//                    intent.putExtra(K.title, "Tagihan Telepon");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 9) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.telkom);
//                    intent.putExtra(K.title, "Tagihan Telkom");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 10) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.internet);
//                    intent.putExtra(K.title, "Tagihan Internet");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 11) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.multifinance);
//                    intent.putExtra(K.title, "Tagihan Multifinance");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 12) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 167);
//                    intent.putExtra("title", "Topup Gopay");
//                    startActivity(intent);
//                } else if (position == 13) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 205);
//                    intent.putExtra("title", "Topup Grabpay");
//                    startActivity(intent);
//                } else if (position == 14) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 30);
//                    intent.putExtra(K.product, S.gas);
//                    intent.putExtra(K.title, "Tagihan Gas Negara");
//                    intent.setAction(S.action_bill);
//                    startActivity(intent);
//                } else if (position == 15) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 237);
//                    intent.setAction(S.action_voucher_tv);
//                    intent.putExtra(K.title, "Beli Voucher TV");
//                    startActivity(intent);
//                } else if (position == 16) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra(K.id, 238);
//                    intent.setAction(S.action_voucher_other);
//                    intent.putExtra(K.title, "Produk lain");
//                    startActivity(intent);
//                } else if (position == 17) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
////                    intent.putExtra(K.id, 20);
////                    intent.putExtra(K.parent_id, 156);
////                    intent.putExtra(K.product, S.credit_card);
////                    intent.setAction(S.action_bill);
////                    startActivity(intent);
//
//                    intent.putExtra("id", 256);
//                    intent.putExtra("title", "Topup BNI Tapcash");
//                    startActivity(intent);
//                } else if (position == 18) {
//                    Intent intent = new Intent(getActivity(), MainLayout.class);
//                    intent.putExtra("id", 244);
//                    intent.putExtra("title", "Topup Mandiri E-Money");
//                    startActivity(intent);
//                } else if (position == 19) {
////                    Intent intent = new Intent(getActivity(), MainLayout.class);
////                    intent.putExtra(K.product, S.product_flight);
////                    intent.setAction(S.action_flight);
////                    startActivity(intent);
//                    Helper.show_info("Info", "Produk ini masih dalam pengembangan, kami akan menginfokan jika sudah siap digunakan.", getActivity());
//                } else if (position == 20) {
////                    Intent intent = new Intent(getActivity(), MainLayout.class);
////                    intent.putExtra(K.product, S.product_train);
////                    intent.setAction(S.action_train);
//                    Helper.show_info("Info", "Produk ini masih dalam pengembangan, kami akan menginfokan jika sudah siap digunakan.", getActivity());
//                } else if (position == 21) {
//                    Intent intent = new Intent(getActivity(), WalletActivity.class);
//                    startActivity(intent);
//                }
////                else if (position == 15) {
////                    Intent intent = new Intent(getActivity(), MenuLainActivity.class);
////                    startActivity(intent);
////                }
//                else if (position == 22) {
//                    HomeActivity activity = (HomeActivity) getActivity();
//                    activity.open_hotline();
//
////                    Toast.makeText(getActivity(), "Produk ini sedang dalam pengembangan", Toast.LENGTH_SHORT).show();
//                } else if (position == 23) {
//                    FaqOptions faqOptions = new FaqOptions()
//                            .showFaqCategoriesAsGrid(false)
//                            .showContactUsOnAppBar(true)
//                            .showContactUsOnFaqScreens(true)
//                            .showContactUsOnFaqNotHelpful(true);
//                    Freshchat.showFAQs(getActivity(), faqOptions);
//                }
//            }
//        });

//        int product_count = (int) realm.where(Product.class)
//                .count();
//
//        if (product_count <= 0) {
//            getProduct();
//        }

        slider_load = false;
//        scroll_view.smoothScrollTo(0, 0);


//        getProfile();
        return root;
    }

    private void getProduct() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);

            BisatopupApi.get("/product/all", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);
                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt(K.product_id);
                                        product.parent_id = obj.getInt(K.parent_id);
                                        product.product_name = obj.getString(K.product_name);
                                        product.product = obj.getString(K.product);
                                        product.code = obj.getString(K.code);
                                        product.prefix = obj.getString(K.prefix);
                                        product.description = obj.getString(K.description);
                                        product.img_url = obj.getString(K.img_url);

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction success.
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Menu> getAllMenu() {
        Drawable drawable = getResources().getDrawable(R.drawable.logo_bisa_2);
        Drawable icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_menu)
                .color(Color.parseColor("#ffffff")).sizeDp(20);
        Menu menu = null;
        List<Menu> items = new ArrayList<>();
        items.add(new Menu(null, getResources().getDrawable(R.drawable.phone), S.product_pulse));//0
        items.add(new Menu(null, getResources().getDrawable(R.drawable.smartphone), S.product_data_package));//1
        items.add(new Menu(null, getResources().getDrawable(R.drawable.flash), S.product_pln_token));//2
        items.add(new Menu(null, getResources().getDrawable(R.drawable.light_bulb), S.product_electricity_bill));//3
        items.add(new Menu(null, getResources().getDrawable(R.drawable.gamepad), S.product_game_voucher));//4
        items.add(new Menu(null, getResources().getDrawable(R.drawable.security), S.product_insurance_bpjs));//5
        items.add(new Menu(null, getResources().getDrawable(R.drawable.television), S.product_tv));//6
        items.add(new Menu(null, getResources().getDrawable(R.drawable.drop), S.product_pdam));//7
        items.add(new Menu(null, getResources().getDrawable(R.drawable.phone_call), S.product_postpaid_phone));//8
        items.add(new Menu(null, getResources().getDrawable(R.drawable.logo_telkom), S.product_telkom_bill));//9
        items.add(new Menu(null, getResources().getDrawable(R.drawable.worldwide), S.product_internet_bill));//10
        items.add(new Menu(null, getResources().getDrawable(R.drawable.hand_money), S.product_multifinance));//11
        items.add(new Menu(null, getResources().getDrawable(R.drawable.wallet_gopay), S.product_balance_gopay));//12
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_grab), S.product_balance_grabpay));//13
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_gas), S.product_state_gas));//14
        items.add(new Menu(null, getResources().getDrawable(R.drawable.television), S.product_tv_voucher));//15
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_voucher), S.product_other_voucher));//16
//        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_kartu_kredit), S.product_credit_card));//17
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_credit_card), "BNI Tapcash"));//17
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_credit_card), "Mandiri \nE-Money"));//18
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_pesawat), S.product_flight + S.product_soon));//19
        items.add(new Menu(null, getResources().getDrawable(R.drawable.train_front), S.product_train + S.product_soon));//20
        items.add(new Menu(null, getResources().getDrawable(R.drawable.wallet_bisatopup), S.product_balance_mutation));//21
        items.add(new Menu(null, getResources().getDrawable(R.drawable.telemarketer), S.product_help_service));//22
        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_icon_help), "FAQ / Tutorial"));//23
//        items.add(new Menu(null, getResources().getDrawable(R.drawable.ic_menu_lain), "Menu Lain",true));
//        items.add(new Menu(null, icon, ""));
//        items.add(new Menu(null, icon, ""));
//        items.add(new Menu(null, drawable, "Konten Bisa"));
        return items;
    }

    public void refresh() {
        final User user = User.getUser();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);
            params.put(K.phone_number, user.phone_number);

            params.put("model", Helper.getDeviceName());
            params.put("os_version", Helper.getAndroidVersion());

            String device_id =  Helper.getDeviceId(getActivity());
            //params.put("secret", AesCrypto.encrypt(String.format("%s:%s:%s",user.phone_number,user.email,device_id)));

            BisatopupApi.post("/user/refresh", params, getActivity(),
                    new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            try{
                                JSONObject object = new JSONObject(responseString);
                                JSONObject data = object.getJSONObject("error");
                                if(data.getString("message").contains("Unauthorized")){
                                    Toast.makeText(getActivity(), "Sesi habis, silahkan login ulang", Toast.LENGTH_SHORT).show();
                                    HomeActivity homeActivity = (HomeActivity) getActivity();
                                    homeActivity.logout_user();
                                }
                            }catch (Exception e) {
                                //e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            try {
                                JSONObject object = new JSONObject(responseString);

                                String api_key = object.getString("api_key");

                                if (!api_key.isEmpty()) {
                                    realm.beginTransaction();
                                    user.token = api_key;
                                    realm.copyToRealmOrUpdate(user);
                                    realm.commitTransaction();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            getProfile();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProfile() {
        final User userdb = User.getUser();
        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        txt_wallet.setText(S.loading);
        try {
            String token = Helper.getFirebaseInstanceId(getContext());

            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, email);
            params.put(K.token, token);
            params.put("model", Helper.getDeviceName());
            params.put("os_version", Helper.getAndroidVersion());

            String device_id =  Helper.getDeviceId(getActivity());
          //  params.put("secret", AesCrypto.encrypt(String.format("%s:%s:%s",userdb.phone_number,userdb.email,device_id)));


            BisatopupApi.post("/profile/index", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                    try{
                        JSONObject object = new JSONObject(responseString);
                        JSONObject data = object.getJSONObject("error");
                        if(data.getString("message").contains("Unauthorized")){
                            Toast.makeText(getActivity(), "Sesi habis, silahkan login ulang", Toast.LENGTH_SHORT).show();
                            HomeActivity homeActivity = (HomeActivity) getActivity();
                            homeActivity.logout_user();
                        }
                    }catch (Exception e) {
                        //e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONObject data = response.getJSONObject(K.data);
                        User user = realm.where(User.class).equalTo(K.email, email).findFirst();

                        if (!realm.isInTransaction()) {
                            realm.beginTransaction();
                        }
                        if (user == null) {
                            user = realm.createObject(User.class, 1);
                        }
                        user.email = email;
                        user.user_id = data.getInt(K.user_id);
                        user.fullname = data.getString(K.name);
                        user.phone_number = data.getString(K.phone_number);
                        user.role = data.getString(K.role);
                        user.poin = data.getInt(K.poin);
                        user.wallet = data.getString(K.wallet);
                        //  user.sponsored_by = data.getString("sponsored_by");
                        user.kode_affiliasi = data.getString(K.affiliation_code);
                        user.total_affiliasi = data.getString(K.total_affiliate);
                        user.total_pendapatan = data.getString(K.total_income);
                        user.profile_url = data.getString(K.profile_url);
                        user.is_agent = data.getInt(K.is_agent);
                        user.nama_agen = data.getString(K.agent_name);
                        user.jenis_kelamin = data.getString(K.gender);
                        user.tgl_lahir = data.getString(K.birth_date);

                        if (data.has("key")) {
                            user.token = data.getString("key");
                        }

                        realm.commitTransaction();

                        txt_wallet.setText(user.wallet);

                        int need_verify = data.getInt(K.need_verify);
                        if (need_verify == 1) {
                            session.clearSession();
//                            try {
//                                User.ClearUser();
//                            } catch (Exception ec) {
//                                ec.printStackTrace();
//                            }
                            getActivity().finish();
                            Helper.showMessage(getActivity(), S.verification_needed);
                            Intent i = new Intent(getActivity(), MainLayout.class);
                            i.setAction(S.action_phone_verification);
                            startActivity(i);
                        }
                        int force_logout = data.getInt(K.force_logout);

                        if (force_logout == 1) {
                            HomeActivity homeActivity = (HomeActivity) getActivity();
                            homeActivity.logout_user();
                        }

                        if (!slider_load) {
                            JSONArray slider_info = data.getJSONArray(K.mobile_info);
//                            sliderShow.removeAllSliders();
                            for (int i = 0; i < slider_info.length(); i++) {
                                JSONObject slider_obj = slider_info.getJSONObject(i);
                                DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
                                textSliderView.image(slider_obj.getString(K.image_url));
//                                sliderShow.addSlider(textSliderView);
                                slider_load = true;
                            }
                        }

                        HomeActivity activity = (HomeActivity) getActivity();
                        activity.reload_profile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkFirstRun() {

        final String PREFS_NAME = this.getClass().getName();
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;

        // Get current version code
        int currentVersionCode = BuildConfig.VERSION_CODE;

        // Get saved version code
        SharedPreferences prefs = getActivity().getSharedPreferences(PREFS_NAME, getActivity().MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            // This is just a normal run
            return;

        } else if (savedVersionCode == DOESNT_EXIST) {


        } else if (currentVersionCode > savedVersionCode) {

            // TODO This is an upgrade
        }

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
    }

}
