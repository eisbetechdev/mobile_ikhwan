package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.UpgradeAgentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 8/1/2017.
 */

public class FragmentAfiliasiMember extends BaseFragment {

    @BindView(R.id.btn_upgrade)
    protected FancyButton btn_upgrade;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_afiliasi_member, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @OnClick(R.id.btn_upgrade)
    public void upgrade() {
        startActivity(new Intent(getActivity(), UpgradeAgentActivity.class));
    }
}
