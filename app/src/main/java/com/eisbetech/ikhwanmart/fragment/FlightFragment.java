package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.FlightResultActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.SearchAirportActivity;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Created by khadafi on 8/27/2016.
 *
 */
public class FlightFragment extends Fragment {
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    Calendar calendar = Calendar.getInstance();
    String activeCalendar = S.depart;
    DatePickerDialog.OnDateSetListener onDateSetListener;

    @BindView(R.id.tv_origin_name)
    protected TextView tvOriginName;

    @BindView(R.id.tv_origin_code)
    protected TextView tvOriginCode;

    @BindView(R.id.tv_destination_name)
    protected TextView tvDestinationName;

    @BindView(R.id.tv_destination_code)
    protected TextView tvDestinationCode;

    @BindView(R.id.tv_depart_date)
    protected TextView tvDepartDate;

    @BindView(R.id.sw_round_trip)
    protected SwitchCompat swRoundTrip;

    @BindView(R.id.ll_return_date)
    protected LinearLayout llReturnDate;

    @BindView(R.id.tv_return_date)
    protected TextView tvReturnDate;

    @BindView(R.id.np_adult)
    protected NumberPicker npAdult;

    @BindView(R.id.np_child)
    protected NumberPicker npChild;

    @BindView(R.id.np_infant)
    protected NumberPicker npInfant;

    @BindView(R.id.sp_seat)
    protected AppCompatSpinner spSeat;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_flight, container, false);
        ButterKnife.bind(this, root);

        initUI();

        return root;
    }

    private void initUI() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.
                SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setDate(tvDepartDate);
        setDate(tvReturnDate);

        onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                setDate(activeCalendar.equals(S.return_) ? tvReturnDate : tvDepartDate);
            }
        };

        swRoundTrip.setChecked(true);

        npAdult.setMinValue(1);
        npAdult.setMaxValue(7);
        npAdult.setWrapSelectorWheel(false);
        
        npChild.setMinValue(0);
        npChild.setMaxValue(6);
        npChild.setWrapSelectorWheel(false);
        
        npInfant.setMinValue(0);
        npInfant.setMaxValue(4);
        npInfant.setWrapSelectorWheel(false);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.seat_class, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSeat.setAdapter(adapter);
        spSeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) spSeat.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ((TextView) spSeat.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }
        });
    }

    private void setDate(TextView tvDate) {
        String strDate = sdf.format(calendar.getTime());
        String day = S.daynames[calendar.get(Calendar.DAY_OF_WEEK) - 1];
        tvDate.setText(day + ", " + strDate);
    }

    private void showDatePicker() {
//        new DatePickerDialog(getContext(), onDateSetListener, calendar.get(Calendar.YEAR),
//                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        DatePickerDialog dpd = DatePickerDialog.newInstance(onDateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setTitle(activeCalendar.equals(S.return_) ? S.return_date : S.depart_date);
        dpd.setMinDate(Calendar.getInstance());
        dpd.show(getActivity().getFragmentManager(), S.datepicker_dialog);
    }

    @OnClick(R.id.ll_origin)
    protected void selectOrigin() {
        Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
        intent.putExtra(K.search, S.origin_airport);
        startActivityForResult(intent, I.req_search_origin_airport);
    }

    @OnClick(R.id.ll_destination)
    protected void selectDestination() {
        Intent intent = new Intent(getActivity(), SearchAirportActivity.class);
        intent.putExtra(K.search, S.destination_airport);
        startActivityForResult(intent, I.req_search_destination_airport);
    }

    @OnClick(R.id.ll_depart_date)
    protected void selectDepartDate() {
        activeCalendar = S.depart;
        showDatePicker();
    }

    @OnClick(R.id.sw_round_trip)
    protected void isRoundTrip() {
        llReturnDate.setVisibility(swRoundTrip.isChecked() ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.ll_return_date)
    protected void selectReturnDate() {
        activeCalendar = S.return_;
        showDatePicker();
    }

    @OnClick(R.id.btn_search)
    protected void search() {
        startActivity(new Intent(getActivity(), FlightResultActivity.class));
    }
}
