package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.AddNumberAdapter;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.ProductDetail;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Nominal;
import com.eisbetech.ikhwanmart.item.NominalItem;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.itemanimators.SlideDownAlphaAnimator;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by khadafi on 3/25/2017.
 */

public class BeliVoucherGameFragment extends BaseFragment {

    @BindView(R.id.spinner_jenis_voucher)
    protected MaterialSpinner spinner_jenis_voucher;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.txt_no_hp)
    protected MaterialEditText txt_no_hp;

    @BindView(R.id.txt_title)
    protected TextView txt_title;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.txt_info)
    protected TextView txt_info;

    @BindView(R.id.layout_info)
    protected LinearLayout layout_info;

    protected List<Product> parent_produt;
    private int parent_id;
    private Product result;
    private Product parent_product;
    private FastItemAdapter<NominalItem> fastItemAdapter;
    String product;

    User user;
    public static int PICK_CONTACT = 10;
    public static final int PERMISSION_REQUEST_CONTACT = 11;

    public void add_number(String type) {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", false);
        i.putExtra("parent_id", parent_id);
        i.putExtra("type", type);
        startActivityForResult(i, 1);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_voucher_game, container, false);
        ButterKnife.bind(this, root);
        Bundle bundle = getArguments();

        layout_info.setVisibility(View.GONE);

        parent_id = bundle.getInt("id", 1);

        product = bundle.getString("product",null);
        user = User.getUser();

        if(parent_id == 237){

            txt_title.setText("Masukkan ID Pelanggan :");
            txt_no_hp.setHint("ID Pelanggan");
            txt_no_hp.setFloatingLabelText("ID Pelanggan");
            txt_no_hp.setText("");
        }else{
           // txt_no_hp.setText(user.phone_number);

            list.requestFocus();

            txt_no_hp.clearFocus();
        }

        parent_produt = realm.where(Product.class)
                .equalTo("parent_id", parent_id)
//                .equalTo("is_active",1)
                .findAllSorted("order_num");;

        List<String> voucher = new ArrayList<>();

        for (Product product : parent_produt) {
            voucher.add(product.product_name);

        }

        //txt_no_hp.setCursorVisible(false);

        spinner_jenis_voucher.setItems(voucher);

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<NominalItem>() {
            @Override
            public boolean onClick(View v, IAdapter<NominalItem> adapter, NominalItem item, int position) {
                String regexStr = "^[+]?[0-9]{8,18}$";
                if (item.nominal.is_gangguan == 1) {
                    Helper.showMessage(getActivity(), "Produk sedang gangguan, silahkan pilih nominal yang lain.");
                    return false;
                } else {
                    if (txt_no_hp.getText().toString().trim().matches(regexStr)) {
                        Intent i = new Intent(getActivity(), MainLayout.class);
                        i.setAction("checkout");
                        i.putExtra("product", parent_product.product_name);
                        i.putExtra("product_id", item.nominal.id);
                        i.putExtra("nominal", item.nominal.product_name.trim());
                        i.putExtra("price", item.nominal.price);
                        i.putExtra("is_tagihan", false);
                        i.putExtra("no_tujuan", txt_no_hp.getText().toString());

                        startActivity(i);
                    } else {
                        Helper.showMessage(getActivity(), "Nomor/rekening tujuan tidak valid");
                    }
                }

                return false;
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        list.setLayoutManager(linearLayoutManager);
        //list.setLayoutManager(gridLayoutManager);
        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);
//        txt_nominal.setVisibility(View.GONE);

        parent_product = realm.where(Product.class)
                .equalTo("product_name", (String) voucher.get(0))
                .findFirst();

        RealmResults<ProductDetail> productDetails =realm.where(ProductDetail.class)
                .equalTo("product_id", parent_product.id)
                .findAll();

        if(productDetails.size()<=0){
            getNominal(parent_product.id, true);

        }else{
//            getNominalOffline(parent_product.id);
            getNominal(parent_product.id,true);
        }

        spinner_jenis_voucher.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                parent_product = realm.where(Product.class)
                        .equalTo("product_name", (String) item)
                        .findFirst();
//                getNominal(parent_product.id, true);

                RealmResults<ProductDetail> productDetails =realm.where(ProductDetail.class)
                        .equalTo("product_id", parent_product.id)
                        .findAll();

                if(productDetails.size()<=0){
                   // getNominal(parent_product.id, true);
                }else{
                    //getNominalOffline(parent_product.id);
                }
                if(Helper.isNowConnected(getContext())){
                    getNominal(parent_product.id, true);
                }else{
                    getNominalOffline(parent_product.id);
                    Helper.show_alert("Koneksi internet bermasalah","Koneksi internet anda sedang bermasalah, mohon periksa settingan internet anda.",getActivity());
                }

            }
        });
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon;
        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_help)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        menu.add(0, 1, Menu.NONE, "Help").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @OnClick(R.id.btn_add_number)
    public void show_add_dialog() {
        Helper.hideSoftKeyboard(getActivity());
        Holder holder = new ListHolder();
        List<String> menu = new ArrayList<>();
        menu.add("Nomor Pribadi");
        menu.add("Dari Kontak");
        menu.add("Riwayat Transaksi");
        menu.add("Nomor Favorite");

        AddNumberAdapter addNumberAdapter = new AddNumberAdapter(getActivity(), menu);

        DialogPlus dialogPlus = DialogPlus.newDialog(getContext())
                .setAdapter(addNumberAdapter)
                .setContentHolder(holder)
                .setHeader(R.layout.dialog_header_plus)
                .setOverlayBackgroundResource(R.color.grey_transparent)
                .setCancelable(true)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        dialog.dismiss();
                        switch (position){
                            case 0:
                                txt_no_hp.setText(user.phone_number);
                                break;
                            case 1:
                                askForContactPermission();

                                break;
                            case 2:
                                add_number("history");
                                break;
                            case 3:
                                add_number("favorite");
                                break;
                            default:
                                break;
                        }
                    }
                })

                .create();

        dialogPlus.show();
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getContact();
            }
        } else {
            getContact();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContact();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                    ;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private void getContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == getActivity().RESULT_OK || resultCode == -1) {

                Uri contactData = data.getData();

                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                Log.i("phoneNUmber", "The phone number is " + number);


                number = number.replace("+", "");
                number = number.replace(" ", "");
                number = number.replace("-", "");

                if (number.substring(0, 2).equals("62")) {
                    number = "0" + number.substring(2);
                }

                if (number.substring(0, 1).equals("8")) {
                    number = "0" + number;
                }
                txt_no_hp.setText(number);
                txt_no_hp.setText("");
                if (!TextUtils.isEmpty(number)) {
                    txt_no_hp.append(number);
                }
                list.requestFocus();

                // Do something with the phone number...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (data != null) {
            String result = data.getStringExtra("result");

            txt_no_hp.setText(result);
            txt_no_hp.setText("");
            if (!TextUtils.isEmpty(result)) {
                txt_no_hp.append(result);
            }

            list.requestFocus();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                Helper.ShowFaq(getActivity(),"voucher_game","");
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void getProduct() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);

            BisatopupApi.get("/product/all",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);


                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt("product_id");
                                        product.parent_id = obj.getInt("parent_id");
                                        product.product_name = obj.getString("product_name");
                                        product.product = obj.getString("product");
                                        product.code = obj.getString("code");
                                        product.prefix = obj.getString("prefix");
                                        product.description = obj.getString("description");
                                        product.img_url = obj.getString("img_url");

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction was a success.
                                if (parent_id == 2) {
                                    result = realm.where(Product.class)
                                            .equalTo("parent_id", parent_id)
                                            .findFirst();

                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getNominal(final int id, final boolean showloading) {

        if (parent_product.description != null && !parent_product.description.isEmpty() && !parent_product.description.trim().equals("") && !parent_product.description.equals("null")) {
            layout_info.setVisibility(View.VISIBLE);
            txt_info.setText(Helper.fromHtml("Info : <br> " + parent_product.description));
        } else {
            layout_info.setVisibility(View.GONE);
        }
        fastItemAdapter.clear();
        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();


            BisatopupApi.get("/product/detail/" + id,  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    android_empty.setVisibility(View.GONE);

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    try {


                        JSONArray data = new JSONArray(responseString);

                        List<NominalItem> nominalItems = new ArrayList<>();


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Nominal nominal = new Nominal();
                            nominal.product_name = obj.getString("name");
                            nominal.price = obj.getString("price");
                            if (user.is_agent == 1) {
                                nominal.price = obj.getString("base_price");
                            }
                            nominal.id = obj.getString("product_detail_id");
                            nominal.desc = obj.getString("desc");
                            nominal.is_gangguan = obj.getInt("is_gangguan");

                            nominalItems.add(new NominalItem(nominal));

                            ProductDetail productDetail = new ProductDetail();
                            productDetail.product_id = obj.getInt("product_id");
                            productDetail.id = Integer.parseInt(nominal.id);
                            productDetail.is_gangguan = nominal.is_gangguan;
                            productDetail.price = obj.getDouble("price");
                            productDetail.base_price = obj.getDouble("base_price");
                            productDetail.product_name = obj.getString("name");
                            productDetail.is_active = obj.getInt("is_active");
                            productDetail.desc = nominal.desc;
                            ProductDetail.AddOrUpdate(productDetail);
                        }

                        fastItemAdapter.add(nominalItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        android_empty.setVisibility(View.VISIBLE);
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getNominalOffline(int id){
        fastItemAdapter.clear();

        String fieldNames[]={"is_gangguan","grup","price"};
        Sort sort[]={Sort.ASCENDING,Sort.ASCENDING,Sort.ASCENDING};
        RealmResults<ProductDetail> productDetails =realm.where(ProductDetail.class)
                .equalTo("product_id", parent_product.id)
                .equalTo("is_active",1)
                .sort(fieldNames,sort).findAll();

        List<NominalItem> nominalItems = new ArrayList<>();

        for (ProductDetail productDetail :
                productDetails) {

            Nominal nominal = new Nominal();
            nominal.product_name = productDetail.product_name;
            nominal.price = String.valueOf(productDetail.price);
            if (user.is_agent == 1) {
                nominal.price = String.valueOf(productDetail.base_price);
            }
            nominal.id = String.valueOf(productDetail.id);
            nominal.desc = productDetail.desc;
            nominal.is_gangguan = productDetail.is_gangguan;

            nominalItems.add(new NominalItem(nominal));
        }

        fastItemAdapter.add(nominalItems);
        fastItemAdapter.notifyAdapterDataSetChanged();
    }
}
