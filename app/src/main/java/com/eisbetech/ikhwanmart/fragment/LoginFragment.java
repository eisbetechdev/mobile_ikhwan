package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.crashlytics.android.Crashlytics;
import com.freshchat.consumer.sdk.Freshchat;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 8/23/2016.
 */
public class LoginFragment extends Fragment {

    @BindView(R.id.txt_username)
    EditText txt_username;

    @BindView(R.id.txt_password)
    EditText txt_password;

    SessionManager session;
    HashMap<String, String> user;
    private FirebaseAuth mAuth;

    String TAG = "LoginFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, root);

        Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user_md)
                .color(Color.parseColor("#007c70")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_username.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#007c70")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password.setCompoundDrawables(img, null, null, null);
        session = new SessionManager(getContext(), getActivity());
        mAuth = FirebaseAuth.getInstance();

        return root;
    }

    @OnClick(R.id.btn_login)
    void login_acc() {
        login();
    }

    @OnClick(R.id.txt_lupa_password)
    public void lupa_pwd() {
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bisatopup.co.id/password/reset"));
//        startActivity(browserIntent);
        Intent inte = new Intent(getActivity(), MainLayout.class);
        inte.setAction("reset_password");
        startActivity(inte);
    }

    @OnClick(R.id.btn_bantuan)
    public void bantuan() {
        Freshchat.showConversations(getContext());
    }

    private void login() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("username", txt_username.getText());
            params.put("password", txt_password.getText());
            params.put("device_id", Helper.getDeviceId(getActivity()));

            BisatopupApi.post("/home/login", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            Toast.makeText(getActivity(), "Login berhasil", Toast.LENGTH_SHORT).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), object.getString("key"));
                            session.createLoginSession(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"), "");
                            loginFirebase(object.getString("token"));
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            if (object.getJSONObject("data").getInt("is_verified") == 0) {
                                Intent i = new Intent(getActivity(), MainLayout.class);
                                i.setAction("phone_number_input");
                                startActivity(i);
                            } else {
                                int need_verify = object.getInt("need_verify");
                                if (need_verify == 1) {
                                    getActivity().finish();
                                    Toast.makeText(getActivity(), "Verifikasi diperlukan.", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(getActivity(), MainLayout.class);
                                    i.setAction("verifikasi_phone");
                                    startActivity(i);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void registerUser() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        final User user = User.getUser();
        String token = Helper.getFirebaseInstanceId(getContext());
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("name", user.fullname);
            params.put("token", token);
            params.put("device_id", Helper.getDeviceId(getActivity()));

            BisatopupApi.post("/user/register-user", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {

                        getActivity().finish();
                        session.checkLogin();

                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loginFirebase(String mCustomToken) {
        mAuth.signInWithCustomToken(mCustomToken)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCustomToken:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                            registerUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }
                    }
                });
    }

    private void afterLogin(){

    }
}
