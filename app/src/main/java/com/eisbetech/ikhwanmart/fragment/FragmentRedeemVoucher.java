package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 6/23/2017.
 */

public class FragmentRedeemVoucher extends BaseFragment {

    @BindView(R.id.btn_redeem)
    protected FancyButton btn_redeem;

    @BindView(R.id.txt_kode_voucher)
    protected TextView txt_kode_voucher;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_redeem_voucher, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @OnClick(R.id.btn_redeem)
    public void prosess_redeem() {
        Helper.hideSoftKeyboard(getActivity());
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("kode_voucher", txt_kode_voucher.getText());
            params.put("device_id", Helper.getDeviceId(getActivity()));


            BisatopupApi.get("/transaksi/redeem-voucher",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            JSONObject user = object.getJSONObject("user");

                            Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_check_circle).sizeDp(32).color(Color.WHITE);

                            new LovelyStandardDialog(getActivity())
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                    .setTitle("Proses Redeem Berhasil")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getActivity().finish();
                                        }
                                    })
                                    .setMessage(message)
                                    .show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
