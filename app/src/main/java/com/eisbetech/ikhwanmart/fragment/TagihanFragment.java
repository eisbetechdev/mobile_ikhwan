package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.AddNumberAdapter;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Sort;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 9/17/2016.
 */
public class TagihanFragment extends BaseFragment {

    public static final String MESSAGE_NOTIFIER = "com.amanahah.bisatopup.tagihan.detail_notif";


    @BindView(R.id.layout_tagihan)
    protected CardView layout_tagihan;

    @BindView(R.id.layout_continue)
    protected LinearLayout layout_continue;

    @BindView(R.id.layout_info_tag)
    protected LinearLayout layout_info_tag;

    @BindView(R.id.layout_cek_tagihan)
    protected LinearLayout layout_cek_tagihan;

    @BindView(R.id.layout_loading)
    protected LinearLayout layout_loading;

    @BindView(R.id.txt_norek)
    protected MaterialEditText txt_norek;

    @BindView(R.id.spinner_tagihan)
    protected MaterialSpinner spinner_tagihan;

    @BindView(R.id.spinner_jenis_tagihan)
    protected MaterialSpinner spinner_jenis_tagihan;

    @BindView(R.id.txt_tagihan_nama)
    protected TextView txt_tagihan_nama;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.txt_tagihan_periode)
    protected TextView txt_tagihan_periode;

    @BindView(R.id.txt_tagihan_jumlah)
    protected TextView txt_tagihan_jumlah;

    @BindView(R.id.txt_tagihan_adm)
    protected TextView txt_tagihan_adm;

    @BindView(R.id.txt_tagihan_total)
    protected TextView txt_tagihan_total;

    @BindView(R.id.txt_potongan)
    protected TextView txt_potongan;

    @BindView(R.id.txt_total_bayar)
    protected TextView txt_total_bayar;

    @BindView(R.id.btn_cek_tagihan)
    protected FancyButton btn_cek_tagihan;

    @BindView(R.id.txt_struk)
    protected TextView txt_struk;

    @BindView(R.id.txt_no_hp_pelanggan)
    protected TextView txt_no_hp_pelanggan;

    @BindView(R.id.scrollView2)
    protected ScrollView scrollView2;

    protected List<Product> parent_produt;
    private int parent_id;
    String phone_number;
    String product_name;
    private String jumlah;
    int tagihan_id;
    User user;
    public static int PICK_CONTACT = 10;
    public static final int PERMISSION_REQUEST_CONTACT = 11;


    public void add_number(String type) {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", true);
        i.putExtra("parent_id", parent_id);
        i.putExtra("type", type);
        startActivityForResult(i, 1);
    }


    @OnClick(R.id.btn_add_number)
    public void show_add_dialog() {
        Helper.hideSoftKeyboard(getActivity());
        Holder holder = new ListHolder();
        List<String> menu = new ArrayList<>();
        menu.add("Nomor Pribadi");
        menu.add("Dari Kontak");
        menu.add("Riwayat Transaksi");
        menu.add("Nomor Favorite");

        AddNumberAdapter addNumberAdapter = new AddNumberAdapter(getActivity(), menu);

        DialogPlus dialogPlus = DialogPlus.newDialog(getContext())
                .setAdapter(addNumberAdapter)
                .setContentHolder(holder)
                .setHeader(R.layout.dialog_header_plus)
                .setOverlayBackgroundResource(R.color.grey_transparent)
                .setCancelable(true)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        dialog.dismiss();
                        switch (position){
                            case 0:
                                txt_norek.setText(user.phone_number);
                                break;
                            case 1:
                                askForContactPermission();

                                break;
                            case 2:
                                add_number("history");
                                break;
                            case 3:
                                add_number("favorite");
                                break;
                            default:
                                break;
                        }
                    }
                })

                .create();

        dialogPlus.show();
    }

    @OnClick(R.id.txt_struk)
    public void copy_struk() {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("struk_cek_tagihan", txt_struk.getText().toString());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getActivity(), "Informasi tagihan berhasil dicopy", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_cek_tagihan)
    public void cek_tagihan() {
        if (txt_no_hp_pelanggan.getText().toString().trim().isEmpty()) {
            Toast.makeText(getActivity(), "Nomor HP Pelanggan tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        else if (txt_norek.getText().toString().trim().isEmpty()) {
            Toast.makeText(getActivity(), "Nomor Rekening tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        layout_cek_tagihan.setVisibility(View.GONE);
        layout_loading.setVisibility(View.VISIBLE);
        btn_cek_tagihan.setEnabled(false);
        Helper.hideSoftKeyboard(getActivity());
        layout_tagihan.setVisibility(View.GONE);
        layout_continue.setVisibility(View.GONE);
        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", spinner_jenis_tagihan.getText().toString())
                .findFirst();

        User user = User.getUser();
        System.out.println(parent_product.code);
//        showProgressDialog();
        rotateloading.start();
        try {

            RequestParams params = new RequestParams();
            params.add("nomor_rekening", txt_norek.getText().toString());
            params.add("phone_number", txt_no_hp_pelanggan.getText().toString().trim());
            params.add("product", parent_product.code);
            params.add("product_name", product_name);

            BisatopupApi.post("/tagihan/cek-tagihan-mobile",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    rotateloading.stop();
//                    hideProgressDialog();
                    btn_cek_tagihan.setEnabled(true);
                    layout_loading.setVisibility(View.GONE);
                    layout_cek_tagihan.setVisibility(View.VISIBLE);
                    try {
                        JSONObject data = new JSONObject(responseString);
                        boolean error = data.getBoolean("error");
                        String message = data.getString("message");
                        if (error) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

//                    hideProgressDialog();
                    btn_cek_tagihan.setEnabled(true);
                    try {
                        JSONObject data = new JSONObject(responseString);
                        boolean error = data.getBoolean("error");
                        boolean is_process = data.getBoolean("is_process");
                        String message = data.getString("message");
                        if (error) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        } else {

                            if (is_process) {

                            } else {
                                rotateloading.stop();
                                layout_loading.setVisibility(View.GONE);
                                layout_cek_tagihan.setVisibility(View.VISIBLE);
                                JSONObject data_tagihan = data.getJSONObject("data");

                                if (message.contains("Jumlah Bayar")) {
                                    tagihan_id = data_tagihan.getInt("tagihan_id");
                                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                                    Product parent_product = realm.where(Product.class)
                                            .equalTo("product_name", spinner_jenis_tagihan.getText().toString())
                                            .findFirst();

                                    Intent i = new Intent(getActivity(), MainLayout.class);
                                    i.setAction("input_tagihan");
                                    i.putExtra("product", "PPOB");
                                    i.putExtra("product_id", parent_product.id);
                                    i.putExtra("nominal", parent_product.product_name);
                                    i.putExtra("price", jumlah);
                                    i.putExtra("is_tagihan", true);
                                    i.putExtra("no_tujuan", txt_norek.getText().toString());
                                    i.putExtra("img", "");
                                    i.putExtra("tagihan_id", tagihan_id);
                                    startActivity(i);
                                } else {


                                    double juml_tag = data_tagihan.getDouble("jumlah_tagihan");
                                    double admin = data_tagihan.getDouble("admin");
                                    double jml_bayar = data_tagihan.getDouble("jumlah_bayar");

                                    if (jml_bayar<juml_tag) {
                                        juml_tag = juml_tag - admin;
                                    }

                                    double total_tag = juml_tag+admin;

                                    txt_tagihan_nama.setText(data_tagihan.getString("nama"));
                                    txt_tagihan_periode.setText(data_tagihan.getString("periode"));
                                    txt_tagihan_total.setText(Helper.format_money(String.valueOf(total_tag)));
                                    txt_potongan.setText(Helper.format_money(String.valueOf((total_tag-jml_bayar)*-1)));
                                    txt_total_bayar.setText(Helper.format_money(data_tagihan.getString("jumlah_bayar")));
                                    txt_tagihan_adm.setText(Helper.format_money(data_tagihan.getString("admin")));
                                    txt_tagihan_jumlah.setText(Helper.format_money(String.valueOf(juml_tag)));
                                    jumlah = data_tagihan.getString("jumlah_bayar");
                                    layout_tagihan.setVisibility(View.VISIBLE);
                                    layout_continue.setVisibility(View.VISIBLE);
                                    tagihan_id = data_tagihan.getInt("tagihan_id");

                                    String struk_tagihan = data.getString("struk_data");


                                    if (!struk_tagihan.isEmpty()) {
                                        txt_struk.setText(struk_tagihan);
                                        txt_struk.setVisibility(View.VISIBLE);
                                        layout_info_tag.setVisibility(View.GONE );

                                    } else {
                                        txt_struk.setVisibility(View.GONE);
                                        layout_info_tag.setVisibility(View.VISIBLE);
                                    }

                                    scrollView2.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            scrollView2.fullScroll(ScrollView.FOCUS_DOWN);
                                        }
                                    });

                                }
                            }


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_buy)
    public void bayar() {
        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", spinner_jenis_tagihan.getText().toString())
                .findFirst();

        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("checkout");
        i.putExtra("product", "PPOB");
        i.putExtra("product_id", parent_product.id);
        i.putExtra("nominal", parent_product.product_name);
        i.putExtra("price", jumlah);
        i.putExtra("is_tagihan", true);
        i.putExtra("no_tujuan", txt_norek.getText().toString());
        i.putExtra("img", "");
        i.putExtra("tagihan_id", tagihan_id);
        i.putExtra("category", product_name);
        startActivity(i);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tagihan, container, false);
        ButterKnife.bind(this, root);

        layout_tagihan.setVisibility(View.GONE);
        Bundle bundle = getArguments();
        phone_number = bundle.getString("phone_number");

        txt_struk.setTypeface(Typeface.MONOSPACE);

        product_name = bundle.getString("product");

        parent_id = bundle.getInt("id", 1);
        parent_produt = realm.where(Product.class)
                .equalTo("parent_id", parent_id)
                .sort("product_name",Sort.ASCENDING)
                .findAll();

        user = User.getUser();

        List<String> tagihan_list = new ArrayList<>();

        for (Product product : parent_produt) {
            tagihan_list.add(product.product_name);

        }
//        setJenisTagihan((String) tagihan_list.get(0));
        if (tagihan_list.size()>0) spinner_tagihan.setItems(tagihan_list);

        spinner_tagihan.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                //setJenisTagihan((String) item);

            }
        });

        List<Product> products = realm.where(Product.class)
                .findAll();

        int[] data_tagihan = new int[1];

        if (product_name.equals("pln")) {
            data_tagihan = new int[2];
            data_tagihan[0] = 31;
            data_tagihan[1] = 258;
        } else if (product_name.equals("asuransi")) {
            data_tagihan = new int[5];
            data_tagihan[0] = 93;
            data_tagihan[1] = 152;
            data_tagihan[2] = 153;
            data_tagihan[3] = 154;
            data_tagihan[4] = 155;
        } else if (product_name.equals("tv")) {
            data_tagihan = new int[13];
            data_tagihan[0] = 33;
            data_tagihan[1] = 34;
            data_tagihan[2] = 35;
            data_tagihan[3] = 41;
            data_tagihan[4] = 88;
            data_tagihan[5] = 89;
            data_tagihan[6] = 146;
            data_tagihan[7] = 147;
            data_tagihan[8] = 148;
            data_tagihan[9] = 150;
            data_tagihan[10] = 151;
            data_tagihan[11] = 241;
            data_tagihan[12] = 246;
        } else if (product_name.equals("pdam")) {
            data_tagihan = new int[0];
            products = realm.where(Product.class)
                    .equalTo("parent_id", 53)
                    .findAll();
        } else if (product_name.equals("telpon_pasca")) {
            data_tagihan = new int[7];
            data_tagihan[0] = 42;
            data_tagihan[1] = 43;
            data_tagihan[2] = 44;
            data_tagihan[3] = 45;
            data_tagihan[4] = 46;
            data_tagihan[5] = 47;
            data_tagihan[6] = 48;
        } else if (product_name.equals("telkom")) {
            data_tagihan = new int[5];
            data_tagihan[0] = 32;
            data_tagihan[1] = 36;
            data_tagihan[2] = 37;
            data_tagihan[3] = 38;
            data_tagihan[4] = 146;
        } else if (product_name.equals("multifinance")) {
            data_tagihan = new int[0];
            products = realm.where(Product.class)
                    .equalTo("parent_id", 39)
                    .findAll();
        } else if (product_name.equals("internet")) {
            data_tagihan = new int[4];
            data_tagihan[0] = 147;
            data_tagihan[1] = 149;
            data_tagihan[2] = 150;
            data_tagihan[3] = 151;
        } else if (product_name.equals("kartu_kredit")) {
            data_tagihan = new int[0];
            products = realm.where(Product.class)
                    .equalTo("parent_id", 156)
                    .findAll();
        } else if (product_name.equals("gas")) {
            data_tagihan = new int[1];
            data_tagihan[0] = 90;
        }


        List<String> jenis_tagihan = new ArrayList<>();

        for (Product product : products) {
            if (data_tagihan.length > 0) {
                if (cek_in_array(data_tagihan, product.id)) {
                    jenis_tagihan.add(product.product_name);
                }
            } else {
                jenis_tagihan.add(product.product_name);
            }

        }
        if (jenis_tagihan.size() > 0) {
            spinner_jenis_tagihan.setItems(jenis_tagihan);

        }

        if (jenis_tagihan.size() == 1) {
            spinner_jenis_tagihan.setEnabled(false);
        }

        if (phone_number != null && phone_number != "") {
//            txt_norek.setText(phone_number);
            txt_norek.setText("");
            if (!TextUtils.isEmpty(phone_number)) {
                txt_norek.append(phone_number);
            }
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        }

        User user = User.getUser();
        txt_no_hp_pelanggan.setText(user.phone_number);
        layout_loading.setVisibility(View.GONE);

        String no_hp = bundle.getString("no_hp");
        String no_rek = bundle.getString("no_rek");

        String produk = bundle.getString("product_name");

        if (produk != null) {
            spinner_jenis_tagihan.setText(produk);
        }

        if (no_hp != null) {
            txt_norek.setText(no_rek);
            txt_no_hp_pelanggan.setText(no_hp);
            layout_cek_tagihan.setFocusable(true);
            layout_cek_tagihan.requestFocus();
            txt_norek.setFocusable(false);
            txt_no_hp_pelanggan.setFocusable(false);
            cek_tagihan();
        }


        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon;
        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_help)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        menu.add(0, 1, Menu.NONE, "Help").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 1:
                Helper.ShowFaq(getActivity(),"cek_tagihan","");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean cek_in_array(int[] data, int value) {
        for (int a : data) {
            if (a == value) {
                return true;
            }
        }
        return false;
    }

    private void setJenisTagihan(String tagihan) {
        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", tagihan)
                .findFirst();

        List<Product> products = realm.where(Product.class)
                .equalTo("parent_id", parent_product.id)
                .findAll();

        List<String> jenis_tagihan = new ArrayList<>();

        for (Product product : products) {
            jenis_tagihan.add(product.product_name);

        }
        spinner_jenis_tagihan.setItems(jenis_tagihan);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == getActivity().RESULT_OK || resultCode == -1) {

                Uri contactData = data.getData();

                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);

                Log.i("phoneNUmber", "The phone number is " + number);


                number = number.replace("+", "");
                number = number.replace(" ", "");
                number = number.replace("-", "");

                if (number.substring(0, 2).equals("62")) {
                    number = "0" + number.substring(2);
                }

                if (number.substring(0, 1).equals("8")) {
                    number = "0" + number;
                }
                txt_norek.setText(number);
                txt_norek.setText("");
                if (!TextUtils.isEmpty(number)) {
                    txt_norek.append(number);
                }
                layout_continue.requestFocus();

                // Do something with the phone number...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (data != null) {
            String result = data.getStringExtra("result");

            txt_norek.setText(result);
            txt_norek.setText("");
            if (!TextUtils.isEmpty(result)) {
                txt_norek.append(result);
            }

            layout_continue.requestFocus();
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // DO YOUR STUFF
            String action = intent.getAction();
            String no_rek = intent.getStringExtra("no_rek");
            String no_hp = intent.getStringExtra("no_hp");
            txt_no_hp_pelanggan.setText(no_hp);
            txt_norek.setText(no_rek);
            cek_tagihan();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(MESSAGE_NOTIFIER));
    }


    @Override
    public void onStop() {
        super.onStop();

        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onPause() {
        super.onPause();
//        getActivity().unregisterReceiver(receiver);
    }

    private void getContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getContact();
            }
        } else {
            getContact();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContact();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                    ;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}