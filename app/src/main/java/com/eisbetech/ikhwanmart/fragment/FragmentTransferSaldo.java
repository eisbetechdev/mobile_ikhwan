package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.LockPinActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 6/21/2017.
 */

public class FragmentTransferSaldo extends BaseFragment {

    @BindView(R.id.txt_saldo)
    protected TextView txt_saldo;

    @BindView(R.id.txt_nominal)
    protected TextView txt_nominal;

    @BindView(R.id.txt_phone_number)
    protected MaterialEditText txt_phone_number;

    public static int PICK_CONTACT = 10;
    public static final int PERMISSION_REQUEST_CONTACT = 11;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_transfer_saldo, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        User user = User.getUser();

        txt_saldo.setText("Sisa saldo anda adalah " + user.wallet);

        return root;
    }

    @OnClick(R.id.btn_add_number)
    public void add_number() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", false);
        i.putExtra("parent_id", 1);
        startActivityForResult(i, 1);
    }

    @OnClick(R.id.btn_add_contact)
    public void addContact() {
        askForContactPermission();
    }

    private void getContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(intent, PICK_CONTACT);
    }

    public void askForContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(
                                    new String[]
                                            {Manifest.permission.READ_CONTACTS}
                                    , PERMISSION_REQUEST_CONTACT);
                        }
                    });
                    builder.show();
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CONTACT);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                getContact();
            }
        } else {
            getContact();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContact();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(getActivity(), "No permission for contacts", Toast.LENGTH_SHORT).show();
                    ;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT) {
            if (resultCode == getActivity().RESULT_OK || resultCode == -1) {

                Uri contactData = data.getData();

                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getActivity().getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(column);


                number = number.replace("+", "");
                number = number.replace(" ", "");
                number = number.replace("-", "");

                if (number.substring(0, 2).equals("62")) {
                    number = "0" + number.substring(2);
                }

                if (number.substring(0, 1).equals("8")) {
                    number = "0" + number;
                }
                txt_phone_number.setText(number);
                // Do something with the phone number...
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else if (data != null) {

            if (resultCode == LockPinActivity.RESULT_PIN_INPUT_VALIDATE) {
                boolean validate = data.getBooleanExtra("validate", false);
                if (validate) {
                    transfer_saldo();
                }
            } else {
                String result = data.getStringExtra("result");
                txt_phone_number.setText(result);
            }

        }




    }

    @OnClick(R.id.btn_buy)
    public void cek_transfer() {
        Helper.hideSoftKeyboard(getActivity());
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());


        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("no_hp", txt_phone_number.getText());
            params.put("nominal", txt_nominal.getText());


            BisatopupApi.get("/transaksi/cek-transfer",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            JSONObject user = object.getJSONObject("user");
                            Helper.hideSoftKeyboard(getActivity());
                            Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                            new LovelyStandardDialog(getActivity())
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    .setTitle("Konfirmasi Transfer")
                                    .setMessage("Anda akan transfer sejumlah " + Helper.format_money(txt_nominal.getText().toString()) + " ke nomor " + user.getString("phone_number") + " atas nama " + user.getString("name") + ", apakah anda yakin?")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            User user = User.getUser();
                                            if (user!=null && user.lock_pin_enable!=null && user.lock_pin_enable) {
                                                Intent intent = new Intent(getActivity(), LockPinActivity.class);
                                                intent.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                                                intent.setAction("Masukkan PIN");
                                                startActivityForResult(intent, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                                            } else {
                                                transfer_saldo();
                                            }
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, null)
                                    .show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void transfer_saldo() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());


        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("no_hp", txt_phone_number.getText());
            params.put("nominal", txt_nominal.getText());


            BisatopupApi.get("/transaksi/transfer-saldo",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");

                            Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_check_circle).sizeDp(32).color(Color.WHITE);

                            new LovelyStandardDialog(getActivity())
                                    .setTopColorRes(R.color.accent)
                                    .setButtonsColorRes(R.color.accent)
                                    .setIcon(img)
                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                    .setTitle("Transaksi Sukses")
                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getActivity().finish();
                                        }
                                    })
                                    .setMessage(message)
                                    .show();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
