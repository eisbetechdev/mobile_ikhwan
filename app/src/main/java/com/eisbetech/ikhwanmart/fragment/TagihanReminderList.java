package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.TagihanReminder;
import com.eisbetech.ikhwanmart.item.TagihanReminderItem;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 3/25/2017.
 */

public class TagihanReminderList extends BaseFragment {

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    private FastItemAdapter<TagihanReminderItem> fastItemAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_reminder_list, container, false);
        ButterKnife.bind(this, root);
        android_empty.setVisibility(View.GONE);

        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        fastItemAdapter.withSelectable(true);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<TagihanReminderItem>() {
            @Override
            public boolean onClick(View v, IAdapter<TagihanReminderItem> adapter, final TagihanReminderItem itemd, int position) {

                String action_disable = "Disable";
                if (itemd.tagihanReminder.is_enable != 1) {
                    action_disable = "Enable";
                }

                DialogPlus dialogPlus = DialogPlus.newDialog(getActivity())
                        .setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,
                                new String[]{"Cek Tagihan Sekarang", "Edit", action_disable, "Delete"}
                        ))
                        .setCancelable(true)
                        .setGravity(Gravity.BOTTOM)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                dialog.dismiss();

                                if (position == 0) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            cek_tagihan(itemd.tagihanReminder.product, itemd.tagihanReminder.nomor_rekening);
                                        }
                                    }, 500);
                                } else if (position == 1) {
                                    Intent intent = new Intent(getActivity(), MainLayout.class);
                                    intent.setAction("tagihan_reminder");
                                    intent.putExtra("state","edit");
                                    intent.putExtra("reminder",itemd.tagihanReminder);
                                    startActivity(intent);

                                } else if (position == 2) {
                                    if (itemd.tagihanReminder.is_enable == 1) {
                                        Drawable img = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_warning).sizeDp(32).color(Color.WHITE);

                                        new LovelyStandardDialog(getActivity())
                                                .setTopColorRes(R.color.accent)
                                                .setButtonsColorRes(R.color.accent)
                                                .setIcon(img)
                                                .setTitle("Disable Pengingat Tagihan")
                                                .setMessage("Apakah anda yakin akan menonaktifkan data untuk pengingat tagihan ini?")
                                                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        set_status_reminder(itemd.tagihanReminder.id, 0);
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, null)
                                                .show();
                                    } else {
                                        set_status_reminder(itemd.tagihanReminder.id, 1);
                                    }
                                } else if (position == 3) {
                                    Drawable img = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_warning).sizeDp(32).color(Color.WHITE);

                                    new LovelyStandardDialog(getActivity())
                                            .setTopColorRes(R.color.accent)
                                            .setButtonsColorRes(R.color.accent)
                                            .setIcon(img)
                                            .setTitle("Delete Pengingat Tagihan")
                                            .setMessage("Apakah anda yakin akan mendelete data untuk pengingat tagihan ini?")
                                            .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    delete_reminder(itemd.tagihanReminder.id);
                                                }
                                            })
                                            .setNegativeButton(android.R.string.no, null)
                                            .show();
                                }
                            }
                        })
                        .create();

                dialogPlus.show();
//cek_tagihan();
                return false;
            }
        });

        Bundle bundle = getArguments();

        //getTagihanList(1,true);
        return root;
    }

    public void cek_tagihan(String jenis_tagihan, final String no_rek) {


        final Product parent_product = realm.where(Product.class)
                .equalTo("product_name", jenis_tagihan)
                .findFirst();

        User user = User.getUser();
        System.out.println(parent_product.code);
        showProgressDialog();
        try {

            RequestParams params = new RequestParams();
            params.add("nomor_rekening", no_rek);
            params.add("phone_number", user.phone_number);
            params.add("product", parent_product.code);

            BisatopupApi.post("/tagihan/cek",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog();

                    try {
                        JSONObject data = new JSONObject(responseString);
                        boolean error = data.getBoolean("error");
                        String message = data.getString("message");
                        if (error) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    hideProgressDialog();
                    try {
                        JSONObject data = new JSONObject(responseString);
                        boolean error = data.getBoolean("error");
                        String message = data.getString("message");
                        if (error) {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        } else {
                            JSONObject data_tagihan = data.getJSONObject("data");

                            double juml_tag = data_tagihan.getDouble("jumlah_tagihan");
                            double admin = data_tagihan.getDouble("admin");
                            double jml_bayar = data_tagihan.getDouble("jumlah_bayar");

                            if (jml_bayar<juml_tag) {
                                juml_tag = juml_tag - admin;
                            }

                            double total_tag = juml_tag+admin;

                            final String jumlah = data_tagihan.getString("jumlah_bayar");
                            final int tagihan_id = data_tagihan.getInt("tagihan_id");

                            ViewHolder viewHolder = new ViewHolder(R.layout.dialog_cek_tagihan);


                            final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                                    .setContentHolder(new ViewHolder(R.layout.dialog_cek_tagihan))
                                    .setGravity(Gravity.BOTTOM)
//                .setHeader(R.layout.dialog_header)
                                    .setFooter(R.layout.dialog_footer_cek_tagihan)
                                    .setExpanded(true)
//                                    .setContentHeight(600)
                                    .setCancelable(true)
                                    .setOnCancelListener(new OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogPlus dialog) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(DialogPlus dialog, View view) {
                                            System.out.println("test");
                                            if (view.getId() == R.id.footer_close_button) {
                                                dialog.dismiss();
                                            } else if (view.getId() == R.id.footer_confirm_button) {
                                                Intent i = new Intent(getActivity(), MainLayout.class);
                                                i.setAction("checkout");
                                                i.putExtra("product", "PPOB");
                                                i.putExtra("product_id", parent_product.id);
                                                i.putExtra("nominal", parent_product.product_name);
                                                i.putExtra("price", jumlah);
                                                i.putExtra("is_tagihan", true);
                                                i.putExtra("no_tujuan", no_rek);
                                                i.putExtra("img", "");
                                                i.putExtra("tagihan_id", tagihan_id);
                                                startActivity(i);
                                            }

                                        }
                                    })
                                    .create();

                            // View view = viewHolder.getInflatedView();
                            // LayoutInflater inflater = LayoutInflater.from(getActivity());
//                            View view = inflater.inflate(com.orhanobut.dialogplus.R.layout.dialog_view, parent, false);

                            TextView txt_tagihan_nama = (TextView) dialog.findViewById(R.id.txt_tagihan_nama);
                            TextView txt_tagihan_periode = (TextView) dialog.findViewById(R.id.txt_tagihan_periode);
                            TextView txt_tagihan_total = (TextView) dialog.findViewById(R.id.txt_tagihan_total);
                            TextView txt_tagihan_adm = (TextView) dialog.findViewById(R.id.txt_tagihan_adm);
                            TextView txt_potongan = (TextView) dialog.findViewById(R.id.txt_potongan);
                            TextView txt_tagihan_jumlah = (TextView) dialog.findViewById(R.id.txt_tagihan_jumlah);
                            TextView txt_header_info = (TextView) dialog.findViewById(R.id.txt_header_info);
                            TextView txt_nomor_rek = (TextView) dialog.findViewById(R.id.txt_nomor_rek);
                            TextView txt_total_bayar = (TextView) dialog.findViewById(R.id.txt_total_bayar);

                            txt_nomor_rek.setText(data_tagihan.getString("no_pelanggan"));
                            txt_tagihan_nama.setText(data_tagihan.getString("nama"));
                            txt_header_info.setText("Informasi tagihan " + data_tagihan.getString("product_name") + " anda");
                            txt_tagihan_periode.setText(data_tagihan.getString("periode"));

                            txt_tagihan_total.setText(Helper.format_money(String.valueOf(total_tag)));

                            txt_potongan.setText(Helper.format_money(String.valueOf((total_tag-jml_bayar)*-1)));

                            txt_tagihan_adm.setText(Helper.format_money(data_tagihan.getString("admin")));
                            txt_tagihan_jumlah.setText(Helper.format_money(String.valueOf(juml_tag)));
                            txt_total_bayar.setText(Helper.format_money(data_tagihan.getString("jumlah_bayar")));

                            dialog.show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cek_tagihan2() {
        ViewHolder viewHolder = new ViewHolder(R.layout.dialog_cek_tagihan);
        View view = viewHolder.getInflatedView();

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.dialog_cek_tagihan))
                .setGravity(Gravity.BOTTOM)
//                .setHeader(R.layout.dialog_header)
                .setFooter(R.layout.dialog_footer_cek_tagihan)
                .setExpanded(true)
                .setCancelable(true)
                .create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getTagihanList(1, true);
    }

    @OnClick(R.id.btn_add)
    public void add_reminder() {
        Intent intent = new Intent(getActivity(), MainLayout.class);
        intent.setAction("tagihan_reminder");
        intent.putExtra("state","add");

        startActivity(intent);
    }

    private void getTagihanList(final int page, final boolean showloading) {

        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();


            BisatopupApi.get("/tagihan/reminder",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONArray data = response.getJSONArray("data");

                        List<TagihanReminderItem> tagihanReminderItems = new ArrayList<>();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
//                            Transaksi transaksi = new Transaksi();
                            TagihanReminder tagihanReminder = new TagihanReminder();

                            tagihanReminder.id = obj.getInt("id");
                            tagihanReminder.product_detail_id = obj.getInt("product_detail_id");
                            tagihanReminder.product_parent = obj.getString("product_parent");
                            tagihanReminder.tanggal_reminder = obj.getInt("tanggal_reminder");
                            tagihanReminder.is_enable = obj.getInt("is_enable");
                            tagihanReminder.note = obj.getString("note");
                            tagihanReminder.nomor_rekening = obj.getString("nomor_rekening");
                            tagihanReminder.product = obj.getString("product");

                            tagihanReminderItems.add(new TagihanReminderItem(tagihanReminder));
                        }
                        fastItemAdapter.clear();
                        fastItemAdapter.add(tagihanReminderItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                        if (page == 1 && tagihanReminderItems.size()== 0) android_empty.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (page == 1) android_empty.setVisibility(View.VISIBLE);

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void delete_reminder(int id) {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("id", id);

            BisatopupApi.post("/tagihan/delete-reminder",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            //  getActivity().finish();
                            getTagihanList(1, true);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void set_status_reminder(int id, int status) {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("id", id);
            params.put("status", status);

            BisatopupApi.post("/tagihan/status-reminder", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            //getActivity().finish();
                            getTagihanList(1, true);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
