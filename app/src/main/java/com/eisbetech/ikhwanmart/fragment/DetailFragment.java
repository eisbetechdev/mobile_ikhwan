package com.eisbetech.ikhwanmart.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.GlideApp;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.AppsUtil;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.VaBankActivity;
import com.eisbetech.ikhwanmart.VaResultBankTransfer;
import com.eisbetech.ikhwanmart.VaResultMinimarket;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.PermissionUtil;
import com.eisbetech.ikhwanmart.contants.I;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.Payment;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.eisbetech.ikhwanmart.printer_command.Command;
import com.eisbetech.ikhwanmart.printer_command.PrinterCommand;
import com.eisbetech.ikhwanmart.service.BluetoothService;
import com.crashlytics.android.Crashlytics;
import com.doku.sdkocov2.DirectSDK;
import com.doku.sdkocov2.interfaces.iPaymentCallback;
import com.doku.sdkocov2.model.LayoutItems;
import com.doku.sdkocov2.model.PaymentItems;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 9/11/2016.
 */
public class DetailFragment extends BaseFragment {
    byte FONT_TYPE;
    private static BluetoothSocket btsocket;
    private static OutputStream btoutputstream;

    String responseToken, Challenge1, Challenge2, Challenge3, debitCard;
    int PayChanChoosed = 0;
    private static final int REQUEST_PHONE = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 12;
    private static String[] PERMISSION_PHONE = {Manifest.permission.READ_PHONE_STATE};
    String tokenPayment = null;
    String customerID = null;

    private String trans_id;
    public Transaksi transaksi;

    @BindView(R.id.txt_product)
    protected TextView txt_product;

    @BindView(R.id.img_product)
    protected ImageView img_product;

    @BindView(R.id.txt_no_tujuan)
    protected TextView txt_no_tujuan;

    @BindView(R.id.txt_product_detail)
    protected TextView txt_product_detail;

    @BindView(R.id.txt_order_id)
    protected TextView txt_order_id;

    @BindView(R.id.txt_kode_unik)
    protected TextView txt_kode_unik;

    @BindView(R.id.txt_biaya_admin)
    protected TextView txt_biaya_admin;

    @BindView(R.id.txt_status)
    protected TextView txt_status;

    @BindView(R.id.txt_total)
    protected TextView txt_total;

    @BindView(R.id.txt_token)
    protected TextView txt_token;

    @BindView(R.id.txt_doku_tanggal)
    protected TextView txt_doku_tanggal;

    @BindView(R.id.txt_payment)
    protected TextView txt_payment;

    @BindView(R.id.txt_no_hp_pelanggan)
    protected TextView txt_no_hp_pelanggan;

    @BindView(R.id.txt_note)
    protected TextView txt_note;

    @BindView(R.id.txt_jumlah)
    protected TextView txt_jumlah;

    @BindView(R.id.btn_batal)
    protected FancyButton btn_batal;


    @BindView(R.id.txt_tanggal)
    protected TextView txt_tanggal;

    @BindView(R.id.layout_token)
    protected LinearLayout layout_token;

    @BindView(R.id.card_payment)
    protected CardView card_payment;

    @BindView(R.id.card_repeat_order)
    protected CardView card_repeat_order;

    @BindView(R.id.layout_kode_unik)
    protected LinearLayout layout_kode_unik;

    @BindView(R.id.layout_biaya_admin)
    protected LinearLayout layout_biaya_admin;

    @BindView(R.id.layout_note)
    protected LinearLayout layout_note;

    @BindView(R.id.layout_no_hp_pelanggan)
    protected LinearLayout layout_no_hp_pelanggan;

    @BindView(R.id.layout_jumlah)
    protected LinearLayout layout_jumlah;

    @BindView(R.id.layout_doku)
    protected LinearLayout layout_doku;

    @BindView(R.id.layout_lanjut_bayar)
    protected LinearLayout layout_lanjut_bayar;

    @BindView(R.id.img_doku)
    protected ImageView img_doku;

    @BindView(R.id.layout_nomor_rekening)
    protected LinearLayout layout_nomor_rekening;

    @BindView(R.id.btn_share_token)
    protected FancyButton btn_share_token;

    @BindView(R.id.swiperefresh)
    protected SwipeRefreshLayout swiperefresh;

    private BluetoothService mService = null;

    public static final String MESSAGE_NOTIFIER = "com.amanahah.bisatopup.detail.messagenotif";

    @OnClick(R.id.btn_help)
    public void help_click() {
//        Intent i = new Intent(getActivity(), MainLayout.class);
//        i.setAction("Support");
//        i.putExtra("subject", "Komplain untuk transaksi #" + transaksi.trans_id);
//        startActivity(i);

        MainLayout activity = (MainLayout) getActivity();
        activity.open_hotline();
    }

    @OnClick(R.id.btn_bantuan)
    public void bantuan() {
        MainLayout activity = (MainLayout) getActivity();
        activity.open_hotline();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();
        transaksi = bundle.getParcelable(K.trans_id);

        btn_share_token.setVisibility(View.GONE);
        layout_note.setVisibility(View.GONE);
        layout_kode_unik.setVisibility(View.GONE);
        layout_biaya_admin.setVisibility(View.GONE);

        if (transaksi != null) {
            refresh_view();
        } else {
            trans_id = bundle.getString(K.transaction_id);
            get_detail(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Toast.makeText(getActivity(), "Permission checking", Toast.LENGTH_SHORT).show();
            checkPermission();
        }

        if(Build.VERSION.SDK_INT>=24){
            try{
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        mService = new BluetoothService(getActivity(), new Handler());

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        get_detail(true);
                        swiperefresh.setRefreshing(false);
                    }
                }, 500);
            }
        });

        handler = new Handler();
        refresh = new Runnable() {
            @Override
            public void run() {
                try {
                    if (Helper.isNowConnected(getContext()) && !is_pause) {
                        get_detail(false);
                    }
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }
        };
//        handler.postDelayed(refresh, 5 * 1000);
        return root;
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) { //Can add more as per requirement

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
        }
    }

    protected void connect() {
        if (btsocket == null) {
            Intent BTIntent = new Intent(getActivity(), MainLayout.class);
            BTIntent.setAction(S.action_printer_list);
            this.startActivityForResult(BTIntent, FragmentPrinterList.REQUEST_CONNECT_BT);
        } else {
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            btoutputstream = opstream;
            print_bt();
        }
    }

    private void SendDataString(String data) {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Helper.showMessage(getActivity(), S.printer_not_connected);
            return;
        }
        if (data.length() > 0) {
            try {
                mService.write(data.getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void SendDataByte(byte[] data) {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Helper.showMessage(getActivity(), S.printer_not_connected);
            return;
        }
        mService.write(data);
    }

    private void print_bt() {
        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Helper.showMessage(getActivity(), S.printer_not_connected);
            return;
        }
        try {
            SendDataByte(PrinterCommand.byteCommands[3]);

            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte("IKHWAN MART\n".getBytes("GBK"));

            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte("PT AMANAH KARYA INDONESIA\n\n".getBytes("GBK"));

            Command.ESC_Align[2] = 0x00;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

//                String msg = "Congratulations!\n\n";
            String dataf = "INFO LISTRIK PRABAYAR\n" +
                    "\n" +
                    "\n" +
                    "MEMBER          : 1221252/6285277677761\n" +
                    "NO METER        : 56600291167\n" +
                    "ID PEL          : 523040863017\n" +
                    "NAMA            : KRISDIYANTO\n" +
                    "TARIF/DAYA      : R1/000001300 VA\n" +
                    "NOMINAL         : Rp     20.000,00 \n" +
                    "ADMIN BANK      : Rp      2.500,00 \n" +
                    "\n" +
                    "PERHATIAN: \n" +
                    "CETAK INI BUKAN BUKTI KWITANSI PEMBAYARAN";
//                SendDataByte(PrinterCommand.POS_Print_Text(msg, "GBK", 0, 1, 1, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(dataf, "GBK", 0, 0, 0, 0));
            SendDataByte(dataf.getBytes("GBK"));
            SendDataString("\n\n\n");
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(48));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
//            SendDataByte(Command.GS_V_m_n);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean is_pause = false;

    @Override
    public void onStop() {
        super.onStop();
        is_pause = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        is_pause = false;
        getActivity().registerReceiver(receiver, new IntentFilter(MESSAGE_NOTIFIER));

        if (transaksi != null) {
            if (transaksi.status_id != 1) {
                get_detail(true);
            } else {
                refresh_view();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        is_pause = true;
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        Drawable icon;
        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_cloud_download)
                .color(Color.parseColor("#ffffff")).sizeDp(20);

        if (transaksi != null) {
            if (transaksi.is_tagihan == 1 || transaksi.product_id == 7) {
                menu.add(0, 2, Menu.NONE, S.download_receipt).setIcon(icon)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
            if (transaksi.status_id == 4) {
                icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_print)
                        .color(Color.parseColor("#ffffff")).sizeDp(20);
                menu.add(0, 3, Menu.NONE, S.print).setIcon(icon)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
        }
    }

    Handler handler;
    Runnable refresh;

    public void refresh_view() {
        getActivity().invalidateOptionsMenu();
        if (transaksi.image_url != null) {
//            Glide.with(getActivity().getApplicationContext()).load(transaksi.image_url)
////                    .animate(R.anim.alpha_on)
//                    .placeholder(R.drawable.android_launcher)
//                    .into(img_product);

            GlideApp.with(getActivity().getApplicationContext())
                    .load(transaksi.image_url)
                    .placeholder(R.drawable.logo)
                    .into(img_product);
        } else {
//            Glide.clear(img_product);
            GlideApp.with(getActivity().getApplicationContext()).clear(img_product);
        }

        txt_tanggal.setText(transaksi.tanggal + " " + transaksi.time);
        txt_product.setText(transaksi.product_name);
        txt_product_detail.setText(transaksi.product_detail);
        txt_order_id.setText(transaksi.trans_id);
        if (Helper.isExist(transaksi.kode_unik)) {
            layout_kode_unik.setVisibility(View.VISIBLE);
            txt_kode_unik.setText(transaksi.kode_unik);
        } else {
            layout_kode_unik.setVisibility(View.GONE);
        }

        txt_no_tujuan.setText(transaksi.no_pelanggan);
        txt_status.setText(transaksi.status);
        if (Helper.isExist(transaksi.token)) {
            layout_token.setVisibility(View.VISIBLE);
            txt_token.setText(transaksi.token);
        } else {
            layout_token.setVisibility(View.GONE);
        }
        card_repeat_order.setVisibility(View.GONE);

        if (transaksi.status_id == 1) {
            btn_batal.setVisibility(View.VISIBLE);
        } else {
            btn_batal.setVisibility(View.GONE);
        }

        if (transaksi.status_id != 1) {
            card_payment.setVisibility(View.GONE);
            layout_doku.setVisibility(View.GONE);
            layout_lanjut_bayar.setVisibility(View.GONE);
        }

        if (Helper.isExist(transaksi.note)) {
            layout_note.setVisibility(View.VISIBLE);
            txt_note.setText(Helper.fromHtml(transaksi.note));
        } else {
            layout_note.setVisibility(View.GONE);
        }

        if (transaksi.status_id == 4 && transaksi.product_id != 91 && transaksi.is_tagihan == 0) {
            card_repeat_order.setVisibility(View.VISIBLE);
        }

        if (transaksi.status_id == 4 && transaksi.product_id == 7) {
            btn_share_token.setVisibility(View.VISIBLE);
        }

        if (Helper.isExist(transaksi.no_hp_pelanggan)) {
            layout_no_hp_pelanggan.setVisibility(View.VISIBLE);
            txt_no_hp_pelanggan.setText(transaksi.no_hp_pelanggan);
        } else {
            layout_no_hp_pelanggan.setVisibility(View.GONE);
        }

        if (transaksi.product_id == 91) {
            layout_no_hp_pelanggan.setVisibility(View.GONE);
            layout_nomor_rekening.setVisibility(View.GONE);
            layout_jumlah.setVisibility(View.GONE);
            layout_kode_unik.setVisibility(View.GONE);
        }

        int pmId = transaksi.payment_method.id;


        if(transaksi.payment_method.image_url !=null){
            GlideApp.with(getActivity().getApplicationContext())
                    .load(transaksi.payment_method.image_url)
                    .placeholder(R.drawable.logo)
                    .into(img_doku);
        }


        switch (pmId) {
            case I.payment_doku:
                img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                        R.drawable.ico_pc_doku));
                break;
            case I.payment_minimarket:
                img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                        R.drawable.paychan_alfagroup));
                break;
            case I.payment_atm:
                img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(),
                        R.drawable.paychan_atm));
                break;
            default:
                String pmBank = transaksi.payment_method.bank;
                if (pmBank.contains(S.bca)) {
                    img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_bca));
                } else if (pmBank.contains(S.bni)) {
                    img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_bni));
                } else if (pmBank.contains(S.bri)) {
                    img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_bri));
                } else if (pmBank.contains(S.mandiri_syariah)) {
                    img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_bsm));
                } else if (pmBank.contains(S.mandiri)) {
                    img_doku.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.logo_mandiri));
                }
        }

        if (Helper.isExist(transaksi.admin_fee)) {
            layout_biaya_admin.setVisibility(View.VISIBLE);
            txt_biaya_admin.setText(Helper.format_money(transaksi.admin_fee));
            layout_jumlah.setVisibility(View.VISIBLE);
        }

        txt_payment.setText(transaksi.payment);
        txt_total.setText(Helper.format_money(transaksi.harga));
        txt_jumlah.setText(Helper.format_money(transaksi.base_price));
        trans_id = transaksi.trans_id;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = formatter.parse(transaksi.expired_date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy HH:mm");
            txt_doku_tanggal.setText(format2.format(calendar.getTime()) + " WIB");
            txt_doku_tanggal.setVisibility(View.VISIBLE);
        } catch (ParseException e) {
            e.printStackTrace();
            txt_doku_tanggal.setVisibility(View.GONE);
        }


        String color = Helper.GetStatusColor(transaksi.status);
        txt_status.setTextColor(Color.parseColor(color));

        try{
            color = transaksi.status_color;
            txt_status.setTextColor(Color.parseColor(color));
        }catch (Exception ec){

        }
    }

    @OnClick(R.id.btn_share_token)
    public void share_token() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Pembelian " + transaksi.product_name + " " +
                transaksi.product_detail + " berhasil. Token : " + transaksi.token + ", Terimakasih");
        sendIntent.setType(S.text_plain);
        startActivity(sendIntent);
    }

    @OnClick(R.id.btn_lanjut_bayar)
    public void lanjut_doku() {
        switch (transaksi.payment_method.id) {
            case I.payment_doku:
                set_payment(2);
                break;
            case I.payment_minimarket:
            case I.payment_atm:
                request_pay_va();
                break;
            default:
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(K.res_pay_code, S.empty);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(Helper.IntArrayCheck(I.VA_DATA,transaksi.payment_method.id)){
                    Intent intent = new Intent(getActivity(), VaBankActivity.class);
                    intent.putExtra(K.data, jsonObject.toString());
                    intent.putExtra(K.transaction, transaksi);
                    startActivityForResult(intent, I.req_confirm_payment);
                    return;
                }

                Intent intent = new Intent(getActivity(), VaResultBankTransfer.class);
                intent.putExtra(K.data, jsonObject.toString());
                intent.putExtra(K.transaction, transaksi);
                startActivityForResult(intent, I.req_confirm_payment);
        }
    }

    public void get_detail(final boolean is_show_loading) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        if (is_show_loading) {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(S.loading_detail);
            progressDialog.show();
        }

        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.trans_id, trans_id);

            BisatopupApi.post("/transaksi/detail-mobile/" + trans_id, params, getContext(),
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            if (is_show_loading) {
                                progressDialog.dismiss();
                            }

                            try {
                                Helper.showMessage(getActivity(), throwable.getMessage());

                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Crashlytics.logException(e);
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            if (is_show_loading) progressDialog.dismiss();
                            try {
                                JSONObject object = new JSONObject(responseString);

                                boolean error = object.getBoolean(K.error);

                                if (!error) {
                                    JSONObject obj = object.getJSONObject(K.transaction);

                                    if (transaksi == null) {
                                        transaksi = new Transaksi();
                                    }

                                    String status = obj.getString(K.status);
                                    transaksi.trans_id = obj.getString(K.trans_id);
                                    transaksi.product_id = obj.getInt(K.product_id);
                                    transaksi.is_tagihan = obj.getInt(K.is_tagihan);

                                    transaksi.tanggal = obj.getString(K.date);
                                    transaksi.time = obj.getString(K.time);
                                    transaksi.harga = obj.getString(K.harga);
                                    transaksi.base_price = obj.getString(K.base_price);
                                    transaksi.product = obj.getString(K.product);
                                    transaksi.product_name = obj.getString(K.product_name);
                                    transaksi.product_detail = obj.getString(K.product_detail);
                                    transaksi.product_detail_id = obj.getString(K.product_detail_id);
                                    transaksi.no_pelanggan = obj.getString(K.customer_no);
                                    transaksi.expired_date = obj.getString(K.expired_date);
                                    transaksi.note = obj.getString(K.note);
                                    transaksi.status_color = obj.getString("status_color");

                                    transaksi.token = obj.getString(K.token);
                                    transaksi.payment = obj.getString(K.payment);
                                    transaksi.status_id = obj.getInt(K.status_id);
                                    transaksi.kode_unik = obj.getString(K.unique_code);

                                    JSONObject pay_ojb = obj.getJSONObject(K.pembayaran);
                                    Payment payment = new Payment();
                                    payment.rekening = pay_ojb.getString(K.rekening);
                                    payment.name = pay_ojb.getString(K.name);
                                    payment.bank = pay_ojb.getString(K.bank);
                                    payment.id = pay_ojb.getInt("payment_id");
                                    payment.image_url = pay_ojb.getString("image_url");

                                    transaksi.payment_method = payment;
                                    if (!status.equals(transaksi.status)) {
                                        transaksi.status = obj.getString(K.status);
                                        transaksi.status_id = obj.getInt(K.status_id);
                                        refresh_view();

                                        if (transaksi.status_id == 4) {
                                            Drawable img = new IconicsDrawable(getActivity(),
                                                    FontAwesome.Icon.faw_check_circle).sizeDp(32)
                                                    .color(Color.WHITE);
//                                   new LovelyStandardDialog(getActivity())
//                                           .setTopColorRes(R.color.primary_dark)
//                                           .setButtonsColorRes(R.color.primary_dark)
//                                           .setIcon(img)
//                                           .setTitle("Transaksi Sukses")
//                                           .setMessage("Selamat transaksi anda sudah sukses.")
//                                           .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
//                                               @Override
//                                               public void onClick(View v) {
//                                               }
//                                           })
//                                           .setNegativeButton(android.R.string.no, null)
//                                           .show();

                                            new LovelyInfoDialog(getActivity())
                                                    .setTopColorRes(R.color.primary_dark)
                                                    .setIcon(img)
                                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
//                                           .setNotShowAgainOptionEnabled(0)
                                                    .setTitle(S.transaction_success)
                                                    .setMessage(S.congrats_ur_transaction_has_been_successfull)
                                                    .show();
                                        }
                                    }

                                    if (transaksi.status_id != 4) {
//                                handler.postDelayed(refresh, 5 * 1000);
                                    }
                                } else {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Crashlytics.logException(e);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_repeat_order)
    public void repead_order() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction(K.checkout);
        i.putExtra(K.product, transaksi.product_name);
        i.putExtra(K.product_id, transaksi.product_detail_id);
        i.putExtra(K.nominal, transaksi.product_detail);
        i.putExtra(K.price, transaksi.harga);
        i.putExtra(K.is_tagihan, transaksi.is_tagihan != 0);
        i.putExtra(K.no_tujuan, transaksi.no_pelanggan);
        if (transaksi.image_url != null) {
            i.putExtra(K.img, transaksi.image_url);
        }
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:

                PermissionUtil.checkPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        new PermissionUtil.PermissionAskListener() {
                            @Override
                            public void onNeedPermission() {
                                ActivityCompat.requestPermissions(
                                        getActivity(),
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        REQUEST_EXTERNAL_STORAGE
                                );
                            }
                            @Override
                            public void onPermissionPreviouslyDenied() {
                                //show a dialog explaining permission and then request permission
                            }
                            @Override
                            public void onPermissionDisabled() {
                                Toast.makeText(getContext(), "Permission Disabled.", Toast.LENGTH_SHORT).show();
                            }
                            @Override
                            public void onPermissionGranted() {
                                try {
                                    new DownloadFileFromURL().execute("https://ikhwan.eisbetech.com/transaksi/download-struk/"
                                            + transaksi.trans_id, transaksi.no_pelanggan + S.pdf);
                                } catch (Exception e) {
                                    Log.e("Error: ", e.getMessage());
                                    Helper.showMessage(getActivity(), "Download struk error.");
                                }
                            }
                        });

//                downloadFile("https://bisatopup.co.id/transaksi/download-struk/"+transaksi.trans_id,transaksi.no_pelanggan+".pdf");

                break;
            case 3:
//                connect();
                Intent BTIntent = new Intent(getActivity(), MainLayout.class);
                BTIntent.setAction("print_trx");
                BTIntent.putExtra(K.trans_id, trans_id);
                this.startActivity(BTIntent);
                break;
        }
        return true;
    }

    ProgressBar pb;
    Dialog dialog;
    int downloadedSize = 0;
    int totalSize = 0;

    void downloadFile(String url_text, String filename) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            URL url = new URL(url_text);
            HttpURLConnection urlConnection = (HttpURLConnection)
                    url.openConnection();

            urlConnection.setRequestMethod(S.get);
            urlConnection.setDoOutput(true);

            //connect
            urlConnection.connect();

            //set the path where we want to save the file
            File SDCardRoot = Environment.getExternalStorageDirectory();
            //create a new file, to save the downloaded file
            File file = new File(SDCardRoot, filename);

            FileOutputStream fileOutput = new FileOutputStream(file);

            //Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            //this is the total size of the file which we are downloading
            totalSize = urlConnection.getContentLength();

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    pb.setMax(totalSize);
                }
            });

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                // update the progressbar //
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        pb.setProgress(downloadedSize);
                        float per = ((float) downloadedSize / totalSize) * 100;
//                        cur_val.setText("Downloaded " + downloadedSize +
//
//                                "KB / " + totalSize + "KB (" + (int)per + "%)" );
                    }
                });
            }
            //close the output stream when complete //
            fileOutput.close();
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    // pb.dismiss(); // if you want close it..
                    progressDialog.dismiss();
                }
            });

        } catch (final MalformedURLException e) {
            progressDialog.dismiss();

            showError(S.error_malformed_url + e);
            e.printStackTrace();
        } catch (final IOException e) {
            progressDialog.dismiss();

            showError(S.error_io_exception + e);
            e.printStackTrace();
        } catch (final Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
            showError(S.error_pls_check_internet_connection + e);
        }
    }

    void showError(final String err) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Helper.showMessage(getActivity(), err);
            }
        });
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(S.loading);
            progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                        .getPath() + "/struk_" + f_url[1]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                        .getPath() + "/struk_" + f_url[1];
            } catch (Exception e) {
                Log.e(S.error, e.getMessage());
                Crashlytics.logException(e);
                Helper.showMessage(getActivity(), S.error_download_receipt);
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
//            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            try {
                // dismiss the dialog after the file was downloaded
//            dismissDialog(progress_bar_type);
                progressDialog.dismiss();
                // Displaying downloaded image into image view
                // Reading image path from sdcard
//            String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
                // setting downloaded into image view
//            my_image.setImageDrawable(Drawable.createFromPath(imagePath));
                if (file_url != null) {
                    File temp_file = new File(file_url);
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(temp_file), getMimeType(temp_file.getAbsolutePath()));
                    startActivity(intent);
                }
            } catch (Exception e) {
                Log.e(S.error, e.getMessage());
                Crashlytics.logException(e);
                Helper.showMessage(getActivity(), S.error_download_receipt);
            }
        }
    }

    private String getMimeType(String url) {
        String parts[] = url.split("\\.");
        String extension = parts[parts.length - 1];
        String type = null;
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (btsocket != null) {
                btoutputstream.close();
                btsocket.close();
                btsocket = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == I.req_confirm_payment) {
            get_detail(true);
        } else {
            try {
                btsocket = FragmentPrinterList.getSocket();
                if (btsocket != null) {
                    mService.connect(btsocket);
                    print_bt();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // DO YOUR STUFF
            String action = intent.getAction();
            get_detail(true);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (PayChanChoosed != 0) {
                    if (PayChanChoosed == 1 || PayChanChoosed == 2) {

                        if (tokenPayment != null) {
                            // connectSecondPay(PayChanChoosed);
                        } else if (customerID != null) {
                            // connectFirstPay(PayChanChoosed);
                        } else {
                            set_payment(PayChanChoosed);
                        }
                    } else if (PayChanChoosed == 3) {
                        //  new MandiriPayment().execute();
                    }
                } else {
                    Helper.showMessage(getContext(), S.error_paychan);
                }
            } else {
                Helper.showMessage(getContext(), S.permission_failed);
            }
        }
    }

    private PaymentItems InputCard() {
        User user = User.getUser();

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

//        String merchan_code = "4837";
        //prod
        String merchan_code = "2444";
        String shared_key = "xxx";
        String invoiceNumber = trans_id;

        PaymentItems paymentItems = new PaymentItems();
        paymentItems.setDataAmount(AppsUtil.generateMoneyFormat(transaksi.harga));
        paymentItems.setDataBasket("[{\"name\": \"" + transaksi.product_name + "-" +
                transaksi.product_detail + "\",\"amount\": \"" + transaksi.harga + ".00" +
                "\",\"quantity\": \"1\",\"subtotal\": \"" + transaksi.harga + ".00" + "\"}]");
        paymentItems.setDataCurrency("360");
        paymentItems.setDataWords(AppsUtil.SHA1(AppsUtil.generateMoneyFormat(transaksi.harga) +
                merchan_code + shared_key + invoiceNumber + 360 + telephonyManager.getDeviceId()));
        paymentItems.setDataMerchantChain("NA");
        paymentItems.setDataSessionID(String.valueOf(AppsUtil.nDigitRandomNo(9)));
        paymentItems.setDataTransactionID(invoiceNumber);
        paymentItems.setDataMerchantCode(merchan_code);
        paymentItems.setDataImei(telephonyManager.getDeviceId());
        paymentItems.setMobilePhone(user.phone_number);
        paymentItems.setCustomerID(String.valueOf(user.user_id));
        paymentItems.setPublicKey("xxx");
        paymentItems.isProduction(true);
        return paymentItems;
    }


    private void set_payment(int menuPaymentChannel) {

        String invoiceNumber = String.valueOf(AppsUtil.nDigitRandomNo(10));

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        DirectSDK directSDK = new DirectSDK();

        PaymentItems cardDetails = null;
        cardDetails = InputCard();
//        paymentItems.setTokenPayment(tokenPayment);
        directSDK.setCart_details(cardDetails);

        LayoutItems layoutItems = new LayoutItems();
        layoutItems.setFontPath("fonts/muli.ttf");
        layoutItems.setToolbarColor("#00544a");
        layoutItems.setToolbarTextColor("#FFFFFF");
        layoutItems.setFontColor("#121212");
        layoutItems.setBackgroundColor("#eaeaea");
        layoutItems.setLabelTextColor("#9a9a9a");
        layoutItems.setButtonBackground(getResources().getDrawable(R.drawable.button_orange));
        layoutItems.setButtonTextColor("#FFFFFF");
        directSDK.setLayout(layoutItems);
//        directSDK.setPaymentChannel(04);
        directSDK.setPaymentChannel(menuPaymentChannel);

        directSDK.getResponse(new iPaymentCallback() {
            @Override
            public void onSuccess(final String text) {
                try {
                    JSONObject respongetTokenSDK = new JSONObject(text);
                    if (respongetTokenSDK.getString(K.res_response_code).equalsIgnoreCase("0000")) {
//                      do your background AsyncTask service to merchant server handler here
                        //System.out.println(respongetTokenSDK.toString());
                        send_doku_notif(text);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(final String text) {
                Log.d("onError", text);
                Helper.showMessage(getContext(), text);
            }

            @Override
            public void onException(Exception eSDK) {
                eSDK.printStackTrace();
            }
        }, getContext());
    }

    private void send_doku_notif(String json_response) {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.please_wait);
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);
            params.put(K.data, json_response);

            BisatopupApi.post("/payment/doku-notif", params, getContext(),
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();

                            try {
                                JSONObject object = new JSONObject(responseString);

                                boolean error = object.getBoolean(K.error);

                                if (error) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Helper.showMessage(getActivity(),
                                        S.your_transaction_cant_be_processed_pls_contact_our_cs_tq);
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            progressDialog.dismiss();

                            System.out.println("Doku : " + responseString);
                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                final JSONObject response = new JSONObject(responseString);
                                final JSONObject data = response.getJSONObject(K.data);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void request_pay_va() {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().
                getSystemService(Context.TELEPHONY_SERVICE);
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.mohon_menunggu);
        progressDialog.show();
        try {
            JSONObject jGroup = new JSONObject();// /main Object

            try {
                jGroup.put(K.req_device_id, telephonyManager.getDeviceId());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);
            params.put(K.trans_id, trans_id);
            params.put(K.payment_id, transaksi.payment_method.id);
            params.put(K.data, jGroup.toString());

            BisatopupApi.post("/payment/doku-payment", params, getContext(),
                    new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {
                            progressDialog.dismiss();

                            try {
                                JSONObject object = new JSONObject(responseString);
                                if (object.getBoolean(K.error)) {
                                    Helper.showMessage(getActivity(), object.getString(K.message));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Helper.showMessage(getActivity(),
                                        S.your_transaction_cant_be_processed_pls_contact_our_cs_tq);
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                            progressDialog.dismiss();

                            System.out.println("Doku VA: " + responseString);
                            try {
                                JSONObject json = new JSONObject(responseString);

                                if (json.getString(K.res_response_code).equalsIgnoreCase("0000")) {
                                    Intent intent = new Intent(getActivity(), VaResultMinimarket.class);
                                    if (transaksi.payment_method.id == I.payment_atm) {
                                        intent = new Intent(getActivity(), VaResultBankTransfer.class);
                                    }
                                    intent.putExtra(K.data, json.toString());
                                    intent.putExtra(K.transaction, transaksi);
                                    startActivity(intent);
                                } else {
                                    Helper.showMessage(getActivity(), S.payment_cant_be_processed);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_batal)
    public void confirm_batal(){
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_question).sizeDp(32).color(Color.WHITE);
        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.accent)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda yakin akan membatalkan transaksi ini?")
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        batal();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void batal() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("trans_id", trans_id);
            params.put("device_id", Helper.getDeviceId(getActivity()));

            BisatopupApi.post("/transaksi/batal", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            get_detail(true);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
