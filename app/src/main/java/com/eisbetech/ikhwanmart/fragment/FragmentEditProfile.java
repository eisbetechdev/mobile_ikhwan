package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 7/9/2017.
 */

public class FragmentEditProfile extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.txt_nama)
    protected MaterialEditText txt_nama;


    @BindView(R.id.txt_email)
    protected MaterialEditText txt_email;

    @BindView(R.id.txt_nama_agen)
    protected MaterialEditText txt_nama_agen;

    @BindView(R.id.txt_tgl_lahir)
    protected MaterialEditText txt_tgl_lahir;

    @BindView(R.id.rd_male)
    protected RadioButton rd_male;

    @BindView(R.id.rd_female)
    protected RadioButton rd_female;

    User user;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, root);

        user = User.getUser();

        txt_nama.setText(user.fullname);
        txt_email.setText(user.email);

        if (user.is_agent == 1) {
            txt_nama_agen.setVisibility(View.VISIBLE);
            txt_nama_agen.setText(user.nama_agen);
        } else {
            txt_nama_agen.setVisibility(View.GONE);
        }

        if (!user.tgl_lahir.isEmpty() && !user.tgl_lahir.equals("null")) {
            txt_tgl_lahir.setText(user.tgl_lahir);
        }

        if (user.jenis_kelamin.equals("male")) {
            rd_male.setChecked(true);
        } else {
            rd_female.setChecked(true);
        }

        return root;
    }

    @OnClick(R.id.btn_submit)
    public void validasi() {
        Helper.hideSoftKeyboard(getActivity());
        String name = txt_nama.getText().toString();
        String email = txt_email.getText().toString().trim();

        if (name.equals("")) {
            Toast.makeText(getActivity(), "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.equals("")) {
            Toast.makeText(getActivity(), "Email tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (!Helper.isValidEmail(email)) {
            Toast.makeText(getActivity(), "Email tidak valid", Toast.LENGTH_SHORT).show();
        } else {
            update();
        }
    }

    public void update() {


        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", txt_email.getText().toString().trim());
            params.put("nama", txt_nama.getText().toString().trim());
            params.put("nama_agen", txt_nama_agen.getText().toString().trim());
            params.put("tgl_lahir", txt_tgl_lahir.getText().toString().trim());

            if (rd_male.isChecked()) {
                params.put("jenis_kelamin", "male");

            } else {
                params.put("jenis_kelamin", "female");

            }

            BisatopupApi.post("/user/update-profile",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txt_tgl_lahir)
    public void show_date() {
        Calendar now = Calendar.getInstance();
        String date = txt_tgl_lahir.getText().toString().trim();

        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (!date.isEmpty()) {
            String[] dateArr = date.split("-");
            try {
                dpd = DatePickerDialog.newInstance(
                        this,
                        Integer.parseInt(dateArr[2]),
                        Integer.parseInt(dateArr[1]) - 1,
                        Integer.parseInt(dateArr[0])
                );
            } catch (Exception Ec) {
            }
        }

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
        txt_tgl_lahir.setText(date);
    }
}
