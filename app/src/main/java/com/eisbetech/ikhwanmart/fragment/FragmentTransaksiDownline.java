package com.eisbetech.ikhwanmart.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.entity.TransaksiDownline;
import com.eisbetech.ikhwanmart.item.TransaksiDownlineItem;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 6/24/2017.
 */

public class FragmentTransaksiDownline extends BaseFragment {

    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    private FooterAdapter<TransaksiDownlineItem> footerAdapter;

    private FastItemAdapter<TransaksiDownlineItem> fastItemAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_transaksi_downline, container, false);
        ButterKnife.bind(this, root);
        android_empty.setVisibility(View.GONE);

        fastItemAdapter = new FastItemAdapter<>();
        footerAdapter = new FooterAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        list.addOnScrollListener(new EndlessRecyclerOnScrollListener(footerAdapter) {

            @Override
            public void onLoadMore(final int currentPage) {
//                footerAdapter.clear();
//                footerAdapter.add(new ProgressItem().withEnabled(false));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        get_data(currentPage,true);
                    }
                }, 500);
                System.out.println(currentPage);
            }
        });

        fastItemAdapter.withSelectable(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                get_data(1,true);
            }
        }, 500);


        return root;
    }

    private void get_data(final int page, final boolean showloading) {

        if (showloading)
            rotateloading.start();

        try {
            JSONObject requestJson = new JSONObject();


            RequestParams params = new RequestParams();
            params.put("page",page);

            BisatopupApi.get("/profile/afiliasi",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (showloading) rotateloading.stop();
                    // footerAdapter.clear();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    if (showloading) rotateloading.stop();
                    //   footerAdapter.clear();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONArray data = response.getJSONArray("data");

                        List<TransaksiDownlineItem> transaksiDownlineItems = new ArrayList<>();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            TransaksiDownline transaksiDownline = new TransaksiDownline();
                            transaksiDownline.jumlah = obj.getString("jumlah");
                            transaksiDownline.id_transaksi = obj.getInt("id_transaksi");
                            transaksiDownline.note = obj.getString("note");
                            transaksiDownline.tanggal = obj.getString("created_at");
                            transaksiDownlineItems.add(new TransaksiDownlineItem(transaksiDownline));
                        }

                        fastItemAdapter.add(transaksiDownlineItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();

                        if (page == 1 && transaksiDownlineItems.size() == 0) android_empty.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (page == 1) android_empty.setVisibility(View.VISIBLE);

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
