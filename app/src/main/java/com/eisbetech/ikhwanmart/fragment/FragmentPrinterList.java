package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.Device;
import com.eisbetech.ikhwanmart.item.DeviceItem;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by khadafi on 7/8/2017.
 */

public class FragmentPrinterList extends BaseFragment {


    static public final int REQUEST_CONNECT_BT = 0x2300;

    static private final int REQUEST_ENABLE_BT = 0x1000;

    static private BluetoothAdapter mBluetoothAdapter = null;

    static private ArrayAdapter<String> mArrayAdapter = null;

    static private ArrayAdapter<BluetoothDevice> btDevices = null;

//    private static final UUID SPP_UUID = UUID
//            .fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    // UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    static private BluetoothSocket mbtSocket = null;
    static private BluetoothDevice mbDevice = null;

    List<DeviceItem> deviceItems;
    @BindView(R.id.list)
    protected RecyclerView list;

    private FastItemAdapter<DeviceItem> fastItemAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_device, container, false);
        ButterKnife.bind(this, root);

        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);


        try {
            if (initDevicesList() != 0) {
                getActivity().finish();
            }

        } catch (Exception ex) {
            getActivity().finish();
        }

        IntentFilter btIntentFilter = new IntentFilter(
                BluetoothDevice.ACTION_FOUND);
        getActivity().registerReceiver(mBTReceiver, btIntentFilter);


        return root;
    }

    public static BluetoothSocket getSocket() {
        return mbtSocket;

    }

    public static BluetoothDevice getDevice() {
        return mbDevice;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mBTReceiver);
    }

    private void flushData() {
        try {
            if (mbtSocket != null) {
                mbtSocket.close();
                mbtSocket = null;
            }

            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.cancelDiscovery();
            }

            if (btDevices != null) {
                btDevices.clear();
                btDevices = null;
            }

            if (mArrayAdapter != null) {
                mArrayAdapter.clear();
                mArrayAdapter.notifyDataSetChanged();
                mArrayAdapter.notifyDataSetInvalidated();
                mArrayAdapter = null;
                fastItemAdapter.clear();
                fastItemAdapter.notifyAdapterDataSetChanged();
                fastItemAdapter.notifyDataSetChanged();
                fastItemAdapter = null;
            }

            finalize();

        } catch (Exception ex) {
        } catch (Throwable e) {
        }

    }


    @OnClick(R.id.btn_refresh)
    public void refresh() {
        Toast.makeText(getActivity(), "Refresh device...", Toast.LENGTH_SHORT).show();
        initDevicesList();
    }

    private int initDevicesList() {
        deviceItems = new ArrayList<>();
        flushData();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getContext(),
                    "Bluetooth not supported!!", Toast.LENGTH_LONG).show();
            return -1;
        }

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        fastItemAdapter = new FastItemAdapter<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<DeviceItem>() {
            @Override
            public boolean onClick(View v, IAdapter<DeviceItem> adapter, DeviceItem item, final int position) {
                if (mBluetoothAdapter == null) {
                    return false;
                }

                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Koneksi ke printer...");
                progressDialog.show();
                Toast.makeText(
                        getContext(),
                        "Connecting to " + btDevices.getItem(position).getName() + ","
                                + btDevices.getItem(position).getAddress(),
                        Toast.LENGTH_SHORT).show();

                Thread connectThread = new Thread(new Runnable() {

                    @Override
                    public void run() {

                        int count = 0;

                        try {
                            boolean gotuuid = btDevices.getItem(position)
                                    .fetchUuidsWithSdp();
                            UUID uuid = btDevices.getItem(position).getUuids()[0]
                                    .getUuid();
                            mbtSocket = btDevices.getItem(position)
                                    .createRfcommSocketToServiceRecord(uuid);

                            boolean is_connected = false;
                            while (count < 3) {
                                try {
                                    try {
                                        Thread.sleep(2000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    System.out.println("Trying....");
                                    mbtSocket.connect();

                                } catch (Exception ec) {
                                    ec.printStackTrace();

                                } finally {
                                    count = 3;
                                    is_connected = true;
                                }
                                count++;
                            }

                            if (!is_connected) {
                                mbtSocket.connect();
                            }

                        } catch (IOException ex) {
                            ex.printStackTrace();
                            progressDialog.hide();
                            if (socketErrorRunnable != null) {
                                getActivity().runOnUiThread(socketErrorRunnable);
                            } else {
                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                    }
                                });
                            }

                            try {
                                mbtSocket.close();
                            } catch (IOException e) {
                                // e.printStackTrace();
                            }
                            mbtSocket = null;
                            return;
                        } finally {
                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    progressDialog.hide();

                                    getActivity().finish();
                                    return;
                                }
                            });
                        }
                    }
                });

                connectThread.start();

                return false;
            }
        });
//        mArrayAdapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_list_item_1);

        // setListAdapter(mArrayAdapter);

        Intent enableBtIntent = new Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE);
        try {
            if (btDevices == null) {
                btDevices = new ArrayAdapter<BluetoothDevice>(
                        getContext(), android.R.layout.simple_list_item_1, android.R.id.text1);
            }
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.startDiscovery();
                // Do whatever you want to do with your bluetoothAdapter
                Set<BluetoothDevice> all_devices = mBluetoothAdapter.getBondedDevices();
                if (all_devices.size() > 0) {
                    for (BluetoothDevice currentDevice : all_devices) {

                        Device device1 = new Device();
                        device1.name = currentDevice.getName();
                        device1.address = currentDevice.getAddress();
                        deviceItems.add(new DeviceItem(device1));
                        btDevices.add(currentDevice);


                    }
                    fastItemAdapter.add(deviceItems);
                    fastItemAdapter.notifyAdapterDataSetChanged();
                }

            } else {
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } catch (Exception ex) {
            return -2;
        }

        Toast.makeText(getContext(),
                "Getting all available Bluetooth Devices", Toast.LENGTH_SHORT)
                .show();

        return 0;

    }

    private final BroadcastReceiver mBTReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                try {
                    if (btDevices == null) {
                        btDevices = new ArrayAdapter<BluetoothDevice>(
                                getContext(), android.R.layout.simple_list_item_1, android.R.id.text1);
                    }

                    if (btDevices.getPosition(device) < 0) {
                        btDevices.add(device);
//                        mArrayAdapter.add(device.getName() + "\n"
//                                + device.getAddress() + "\n" );
//                        mArrayAdapter.notifyDataSetInvalidated();

                        List<DeviceItem> deviceItems = new ArrayList<>();

                        Device device1 = new Device();
                        device1.name = device.getName();
                        device1.address = device.getAddress();
                        deviceItems.add(new DeviceItem(device1));

                        fastItemAdapter.add(deviceItems);
                        fastItemAdapter.notifyAdapterDataSetChanged();
                    }
                } catch (Exception ex) {
                    // ex.fillInStackTrace();
                }
            }
        }
    };

    private Runnable socketErrorRunnable = new Runnable() {

        @Override
        public void run() {
            Toast.makeText(getContext(),
                    "Cannot establish connection", Toast.LENGTH_SHORT).show();
            mBluetoothAdapter.startDiscovery();

        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.add(0, Menu.FIRST, Menu.NONE, "Refresh Scanning");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case Menu.FIRST:
                initDevicesList();
                break;
        }

        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT:

                if (resultCode == HomeActivity.RESULT_OK) {
                    Set<BluetoothDevice> btDeviceList = mBluetoothAdapter
                            .getBondedDevices();
                    try {
                        if (btDeviceList.size() > 0) {

                            for (BluetoothDevice device : btDeviceList) {


//                                    mArrayAdapter.add(device.getName() + "\n"
//                                            + device.getAddress());
//                                    mArrayAdapter.notifyDataSetInvalidated();
//                                List<DeviceItem> deviceItems = new ArrayList<>();

                                Device device1 = new Device();
                                device1.name = device.getName();
                                device1.address = device.getAddress();
                                deviceItems.add(new DeviceItem(device1));
                                btDevices.add(device);

                            }
                            fastItemAdapter.add(deviceItems);
                            fastItemAdapter.notifyAdapterDataSetChanged();

                        }
                    } catch (Exception ex) {
                    }
                }

                break;
        }

        mBluetoothAdapter.startDiscovery();
    }
}
