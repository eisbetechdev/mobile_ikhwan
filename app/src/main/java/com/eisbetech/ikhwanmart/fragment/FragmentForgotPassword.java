package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 8/4/2017.
 */

public class FragmentForgotPassword extends BaseFragment {

    @BindView(R.id.txt_no_hp)
    EditText txt_no_hp;

    @BindView(R.id.txt_pwd)
    MaterialEditText txt_pwd;


    @BindView(R.id.txt_pwd_conf)
    MaterialEditText txt_pwd_conf;


    @BindView(R.id.txt_tunggu)
    TextView txt_tunggu;

    @BindView(R.id.txt_code)
    protected MaterialEditText txt_code;

    @BindView(R.id.btn_ulangi)
    protected FancyButton btn_ulangi;

    @BindView(R.id.btn_kirim_ulangi)
    protected FancyButton btn_kirim_ulangi;

    @BindView(R.id.layout_verifikasi)
    protected LinearLayout layout_verifikasi;

    @BindView(R.id.layout_ubah_password)
    protected LinearLayout layout_ubah_password;

    @BindView(R.id.layout_input_number)
    protected LinearLayout layout_input_number;

    String kode_verifikasi;

    CountDownTimer countDownTimer;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this, root);

        layout_verifikasi.setVisibility(View.GONE);
        layout_ubah_password.setVisibility(View.GONE);
        return root;
    }

    public void setCoundown() {
        if(countDownTimer != null){
            countDownTimer.cancel();
        }
        btn_kirim_ulangi.setVisibility(View.GONE);
        txt_tunggu.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_tunggu.setText("Mohon tunggu ( " + millisUntilFinished / 1000 + " ) untuk kirim ulang.");
            }

            public void onFinish() {
                txt_tunggu.setVisibility(View.GONE);
                btn_kirim_ulangi.setVisibility(View.VISIBLE);

            }
        }.start();

    }

    @OnClick(R.id.btn_kirim_ulangi)
    public void kirim_ulangi() {
        verifikasi();
        ;
    }

    @OnClick(R.id.btn_submit)
    public void verifikasi() {
        if (txt_no_hp.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Nomor Handphone tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("phone_number", txt_no_hp.getText().toString().trim());

            BisatopupApi.post("/user/kirim-kode-verifikasi", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            String kode = object.getString("kode");
//
//                            kode_verifikasi = kode;

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            layout_verifikasi.setVisibility(View.VISIBLE);
                            btn_ulangi.setVisibility(View.VISIBLE);

                            layout_input_number.setVisibility(View.GONE);
                            layout_verifikasi.setVisibility(View.VISIBLE);
                            setCoundown();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_ulangi)
    public void ulangi() {
        layout_verifikasi.setVisibility(View.GONE);
        layout_input_number.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_submit_code)
    public void verifikasi_kodes() {
//        if (txt_code.getText().toString().trim().equals(kode_verifikasi)) {
//
//            layout_verifikasi.setVisibility(View.GONE);
//            layout_ubah_password.setVisibility(View.VISIBLE);
//        } else {
//            Helper.show_alert("Kode Salah", "Kode verifikasi salah", getActivity());
//        }
        verifikasi_kode();
    }

    public void verifikasi_kode() {
        Helper.hideSoftKeyboard(getActivity());
        if (txt_code.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Kode tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("no_hp", txt_no_hp.getText());
            params.put("kode", txt_code.getText());


            BisatopupApi.post("/user/verify-code", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            layout_verifikasi.setVisibility(View.GONE);
                            layout_ubah_password.setVisibility(View.VISIBLE);
                        } else {
                            String message = object.getString("message");
                            Helper.show_alert("Error", message, getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.btn_submit_pwd)
    public void ubah_password() {
        Helper.hideSoftKeyboard(getActivity());
        if (txt_pwd.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (txt_pwd_conf.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Password konfimasi tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }else if (!txt_pwd_conf.getText().toString().equals(txt_pwd.getText().toString())) {
            Toast.makeText(getActivity(), "Password harus sama", Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("no_hp", txt_no_hp.getText());
            params.put("password", txt_pwd.getText());
            params.put("password_confirm", txt_pwd_conf.getText());
            params.put("kode", txt_code.getText());


            BisatopupApi.post("/user/ubah-password", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void submit_data() {
        Helper.hideSoftKeyboard(getActivity());
        if (txt_no_hp.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Nomor Hp tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Mohon menunggu...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("no_hp", txt_no_hp.getText());


            BisatopupApi.post("/user/reset-password", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
