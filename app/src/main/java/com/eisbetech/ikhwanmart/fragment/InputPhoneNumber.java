package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 3/19/2017.
 */

public class InputPhoneNumber extends BaseFragment {

    @BindView(R.id.txt_nomor_handphone)
    protected TextView txt_nomor_handphone;

    User user;
    SessionManager session;
    String email = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_input_number, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        if (txt_nomor_handphone.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        user = User.getUser();
        session = new SessionManager(getContext(), getActivity());

        if(user != null){
            email = user.email;
        }else{
            try{
                email = bundle.getString("email");
            }catch (Exception ec){

            }
        }

        return root;
    }

    @OnClick(R.id.btn_continue)
    public void btn_continue() {
        String phone = txt_nomor_handphone.getText().toString();
        if (phone.trim().equals("")) {
            Toast.makeText(getActivity(), "Nomor Handphone tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_phone).sizeDp(24).color(getResources().getColor(R.color.white));

            new LovelyStandardDialog(getActivity())
                    .setTopColorRes(R.color.accent)
                    .setButtonsColorRes(R.color.accent)
                    .setIcon(img)
                    .setTitle("Konfirmasi")
                    .setMessage("Apakah Nomor handphone anda dengan nomor " + "+62" + phone + " sudah benar?")
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            register_no_handphone();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .show();
        }

    }

    public void register_no_handphone() {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            if (user != null) {
                params.put("email", user.email);
            }
            if(email != null){
                params.put("email", email);
            }
            params.put("phone", txt_nomor_handphone.getText().toString());

            BisatopupApi.post("/user/register-phone",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            realm.beginTransaction();
                            if(email != null){
                                user.email = email;
                            }
                            user.phone_number = "0"+txt_nomor_handphone.getText().toString();
                            realm.copyToRealmOrUpdate(user);
                            realm.commitTransaction();
//                                Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(getActivity(), MainLayout.class);
                            i.setAction("verifikasi_phone");
                            startActivity(i);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
