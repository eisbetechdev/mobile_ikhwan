package com.eisbetech.ikhwanmart.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.RotateLoading;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.database.Favorite;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.item.FavoriteItem;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by khadafi on 12/22/2016.
 */

public class FavoriteFragment extends BaseFragment {
    @BindView(R.id.android_empty)
    protected TextView android_empty;

    @BindView(R.id.list)
    protected RecyclerView list;

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    private RealmChangeListener<RealmResults<Favorite>> changeListener;
    RealmResults<Favorite> results;

    @OnClick(R.id.btn_add_favorite)
    public void add_favorite() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("add_favorite");
        startActivityForResult(i, 2);
    }


    private FastItemAdapter<FavoriteItem> fastItemAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_favorit, container, false);
        ButterKnife.bind(this, root);
        android_empty.setVisibility(View.GONE);

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);

        list.setLayoutManager(linearLayoutManager);
//        list.setItemAnimator(new SlideDownAlphaAnimator());
        list.setAdapter(fastItemAdapter);
        list.setNestedScrollingEnabled(false);
        android_empty.setVisibility(View.GONE);

        final List<FavoriteItem> favoriteItems = new ArrayList<>();

        results = realm.where(Favorite.class)
                .findAllAsync();


        results.addChangeListener(new RealmChangeListener<RealmResults<Favorite>>() {
            @Override
            public void onChange(RealmResults<Favorite> element) {
                favoriteItems.clear();
                fastItemAdapter.clear();
                element = element.sort("id", Sort.DESCENDING);

                for (Favorite favorite : element) {
                    favoriteItems.add(new FavoriteItem(favorite));
                    if (favorite.is_sync == 0) {
                        simpan(favorite);
                    }
                }

                fastItemAdapter.add(favoriteItems);
                fastItemAdapter.notifyAdapterDataSetChanged();

                if (element.size() <= 0) {
                    android_empty.setVisibility(View.VISIBLE);
                } else {
                    android_empty.setVisibility(View.GONE);
                }
            }
        });

        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<FavoriteItem>() {
            @Override
            public boolean onClick(View v, IAdapter<FavoriteItem> adapter, FavoriteItem item, int position) {
                Intent intent = new Intent(getActivity(), MainLayout.class);
                intent.putExtra("id", item.favorite.product_id);
                intent.putExtra("phone_number", item.favorite.phone_number);

                if (item.favorite.category.equals("pulsa") || item.favorite.category.equals("") || item.favorite.product_id == 2) {
                    intent.putExtra("id", 1);
                } else if (item.favorite.category.equals("token") || item.favorite.product_id == 2) {
                    intent.putExtra("id", 2);
                    intent.setAction("tagihan");
                }else if (item.favorite.category.equals("paket_data") || item.favorite.product_id == 84) {
                    intent.putExtra("id", 84);
                    intent.setAction("tagihan");
                } else if (item.favorite.category != null && item.favorite.product_id != 1 && item.favorite.product_id != 2 && item.favorite.product_id != 84) {
                    intent.setAction("tagihan");
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, item.favorite.product_id);
                    intent.putExtra(K.product, item.favorite.category);
                }

                startActivity(intent);
                return false;
            }
        });

        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                get();
            }
        }, 2000);

        return root;
    }

    private void simpan(final Favorite favorite) {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("nama", favorite.nama);
            params.put("nomor_hp", favorite.phone_number);
            params.put("kategori", favorite.category);
            params.put("product_id", favorite.product_id);

            BisatopupApi.post("/favorite/add", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                            realm.beginTransaction();
                            favorite.is_sync = 1;
                            realm.copyToRealmOrUpdate(favorite);
                            realm.commitTransaction();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void get() {
        final User user = User.getUser();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);


            BisatopupApi.get("/favorite/data", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {


                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");
                        if (!error) {

                            JSONArray data = object.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject dt = data.getJSONObject(i);
                                String nama = dt.getString("nama");
                                String kategori = dt.getString("kategori");
                                String nomor_hp = dt.getString("nomor_hp");
                                int product_id = dt.getInt("product_id");

                                Favorite favorite = realm.where(Favorite.class).equalTo("nama", nama)
                                        .equalTo("category", kategori)
                                        .equalTo("phone_number", nomor_hp)
                                        .findFirst();

                                if (favorite == null) {
                                    favorite = new Favorite();
                                    favorite.id = favorite.getNextPrimaryKey(realm);
                                    favorite.nama = nama;
                                    favorite.category = kategori;
                                    favorite.phone_number = nomor_hp;
                                    favorite.product_id = product_id;
                                    favorite.is_sync = 1;
                                    realm.beginTransaction();
                                    realm.copyToRealm(favorite);
                                    realm.commitTransaction();
                                }
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
