package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.entity.TagihanReminder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by khadafi on 9/17/2016.
 */
public class TagihanReminderFragment extends BaseFragment {
    TagihanReminder tagihanReminder;

    String state;

    @BindView(R.id.txt_norek)
    protected MaterialEditText txt_norek;

    @BindView(R.id.spinner_tagihan)
    protected MaterialSpinner spinner_tagihan;

    @BindView(R.id.spinner_jenis_tagihan)
    protected MaterialSpinner spinner_jenis_tagihan;

    @BindView(R.id.spinner_bulan)
    protected MaterialSpinner spinner_bulan;


    @BindView(R.id.btn_add)
    protected FancyButton btn_add;

    protected List<Product> parent_produt;
    private int parent_id;

    private String jumlah;
    int tagihan_id;

    @OnClick(R.id.btn_add_number)
    public void add_number() {
        Intent i = new Intent(getActivity(), MainLayout.class);
        i.setAction("daftar");
        i.putExtra("is_tagihan", true);
        startActivityForResult(i, 1);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tagihan_reminder_add, container, false);
        ButterKnife.bind(this, root);

        Bundle bundle = getArguments();

        state = bundle.getString("state");
        if (state.equals("edit")) {
            tagihanReminder = (TagihanReminder) bundle.getParcelable("reminder");
            btn_add.setText("Simpan data");
        } else {
            btn_add.setText("Tambahkan");
        }

        parent_id = 20;
        parent_produt = realm.where(Product.class)
                .equalTo("parent_id", parent_id)
                .findAll();

        List<String> tagihan_list = new ArrayList<>();

        for (Product product : parent_produt) {
            tagihan_list.add(product.product_name);

        }
        setJenisTagihan((String) tagihan_list.get(0));
        spinner_tagihan.setItems(tagihan_list);

        spinner_tagihan.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                setJenisTagihan((String) item);

            }
        });

        List<String> bulan = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            bulan.add(String.valueOf(i));
        }
        spinner_bulan.setItems(bulan);


        if (state.equals("edit")) {
            txt_norek.setText(tagihanReminder.nomor_rekening);
            spinner_bulan.setText(String.valueOf(tagihanReminder.tanggal_reminder));
            spinner_jenis_tagihan.setText(tagihanReminder.product);
            spinner_tagihan.setText(tagihanReminder.product_parent);
        }
        return root;
    }

    @OnClick(R.id.btn_add)
    public void add() {
        if (state.equals("edit")) {
            update_reminder();
        } else {
            add_reminder();

        }
    }

    private void add_reminder() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Tambah Data...");
        progressDialog.show();

        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", spinner_jenis_tagihan.getText().toString())
                .findFirst();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("product_detail", parent_product.id);
            params.put("nomor_rekening", txt_norek.getText().toString().trim());
            params.put("bulan", spinner_bulan.getText().toString());

            BisatopupApi.post("/tagihan/add-reminder",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void update_reminder() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Update Data...");
        progressDialog.show();

        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", spinner_jenis_tagihan.getText().toString())
                .findFirst();

        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", user.email);
            params.put("id", tagihanReminder.id);
            params.put("product_detail", parent_product.id);
            params.put("nomor_rekening", txt_norek.getText().toString().trim());
            params.put("bulan", spinner_bulan.getText().toString());

            BisatopupApi.post("/tagihan/edit-reminder",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setJenisTagihan(String tagihan) {
        Product parent_product = realm.where(Product.class)
                .equalTo("product_name", tagihan)
                .findFirst();

        List<Product> products = realm.where(Product.class)
                .equalTo("parent_id", parent_product.id)
                .findAll();

        List<String> jenis_tagihan = new ArrayList<>();

        for (Product product : products) {
            jenis_tagihan.add(product.product_name);

        }
        spinner_jenis_tagihan.setItems(jenis_tagihan);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String result = data.getStringExtra("result");
            txt_norek.setText(result);

            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
}