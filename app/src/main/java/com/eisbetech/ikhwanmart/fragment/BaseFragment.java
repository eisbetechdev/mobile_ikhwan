package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.api.Mcrypt;
import com.eisbetech.ikhwanmart.database.RealmController;

import io.realm.Realm;

/**
 * Created by khadafi on 7/31/2016.
 */
public class BaseFragment extends Fragment {
    private ProgressDialog mProgressDialog;
    public Realm realm;

    public Mcrypt mcrypt;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        realm =  RealmController.with(this).getRealm();

        mcrypt = new Mcrypt();

    }

    @Override
    public void onResume() {
        super.onResume();
        realm =  RealmController.with(this).getRealm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
