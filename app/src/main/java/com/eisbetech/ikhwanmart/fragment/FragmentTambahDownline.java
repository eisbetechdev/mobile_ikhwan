package com.eisbetech.ikhwanmart.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 7/8/2017.
 */

public class FragmentTambahDownline extends BaseFragment {


    @BindView(R.id.txt_nama)
    protected MaterialEditText txt_nama;

    @BindView(R.id.txt_email)
    protected MaterialEditText txt_email;

    @BindView(R.id.txt_no_hp)
    protected MaterialEditText txt_no_hp;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tambah_downline, container, false);
        ButterKnife.bind(this, root);


        return root;

    }

    @OnClick(R.id.btn_kirim_data)
    public void validasi() {
        Helper.hideSoftKeyboard(getActivity());
        String name = txt_nama.getText().toString();
        String email = txt_email.getText().toString().trim();
        String no_hp = txt_no_hp.getText().toString().trim();

        if (name.equals("")) {
            Toast.makeText(getActivity(), "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.equals("")) {
            Toast.makeText(getActivity(), "Email tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (txt_no_hp.equals("")) {
            Toast.makeText(getActivity(), "No tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (!Helper.isValidEmail(email)) {
            Toast.makeText(getActivity(), "Email tidak valid", Toast.LENGTH_SHORT).show();
        } else {
            tambah();
        }
    }

    public void tambah() {


        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("email", txt_email.getText().toString().trim());
            params.put("nama", txt_nama.getText().toString().trim());
            params.put("nohp", txt_no_hp.getText().toString().trim());

            BisatopupApi.post("/user/tambah-downline",  params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {

                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
