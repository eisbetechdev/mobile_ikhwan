package com.eisbetech.ikhwanmart.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.components.RotateLoading;

import butterknife.BindView;

/**
 * Created by khadafi on 8/23/2017.
 */

public class FragmentLoadWeb extends BaseFragmentNew {

    @BindView(R.id.rotateloading)
    protected RotateLoading rotateloading;

    @BindView(R.id.webview)
    protected WebView myWebView;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_load_web;
    }

    @Override
    protected void LoadView() {

        Bundle bundle = getArguments();

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (rotateloading != null) {
                    rotateloading.start();
                }
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (rotateloading.isStart()) {
                    rotateloading.stop();
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });

        myWebView.loadUrl(bundle.getString("url"));
    }
}
