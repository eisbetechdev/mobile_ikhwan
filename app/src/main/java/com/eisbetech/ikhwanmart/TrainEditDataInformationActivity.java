package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.InfoPenumpang;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrainEditDataInformationActivity extends AppCompatActivity {
    @BindView(R.id.titles)
    protected Spinner titles;
    
    @BindView(R.id.idTypes)
    protected Spinner idTypes;

    @BindView(R.id.name)
    protected TextView name;

    @BindView(R.id.userId)
    protected TextView userId;

    @BindView(R.id.layout_idTypes)
    protected LinearLayout layout_idTypes;

    @BindView(R.id.layout_idPenumpang)
    protected LinearLayout layout_idPenumpang;

    InfoPenumpang infoPenumpang ;

    int DATA_PEMESAN = 0;
    int DATA_PENUMPANG = 1;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.train_edit_data_information);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(S.customer_info);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        loadData();
    }

    private void loadData() {
        Intent intent = getIntent();

        infoPenumpang = new InfoPenumpang();
        infoPenumpang = intent.getParcelableExtra(K.selected_passenger);
        String flag = intent.getStringExtra(K.flag);

        if (flag != null) {
            DATA_PEMESAN = Integer.parseInt(flag);
        }
        ArrayAdapter<CharSequence> adapter;
        if  (infoPenumpang != null && infoPenumpang.getTipePenumpang().contains(S.infant)) {
            layout_idTypes.setVisibility(View.GONE);
            layout_idPenumpang.setVisibility(View.GONE);
            adapter = ArrayAdapter.createFromResource(this,
                    R.array.title_bayi, android.R.layout.simple_spinner_item);
        } else {
            adapter = ArrayAdapter.createFromResource(this,
                    R.array.title_dewasa, android.R.layout.simple_spinner_item);
            userId.setText(infoPenumpang.getUserId());
        }
        name.setText(infoPenumpang.getName());

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        titles.setAdapter(adapter);
        titles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) titles.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ((TextView) titles.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }
        });

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.idType_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        idTypes.setAdapter(adapter2);
        idTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) idTypes.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ((TextView) idTypes.getSelectedView()).setTextColor(getResources()
                        .getColor(R.color.black));
            }
        });
    }

    @OnClick(R.id.simpanData)
    protected void simpanData() {
        Intent returnIntent = new Intent();

        infoPenumpang.setName(name.getText().toString());
        infoPenumpang.setIdType(idTypes.getSelectedItem().toString());
        infoPenumpang.setUserId(userId.getText().toString());

        returnIntent.putExtra(K.selected_passenger, infoPenumpang);
        if (DATA_PEMESAN == 2) {
            setResult(DATA_PEMESAN, returnIntent);
        } else {
            setResult(DATA_PENUMPANG, returnIntent);
        }
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
