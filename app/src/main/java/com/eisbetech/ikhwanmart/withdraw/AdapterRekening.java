package com.eisbetech.ikhwanmart.withdraw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.item.Rekening;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRekening extends RecyclerView.Adapter<AdapterRekening.holder> {
    Context context;
    List<Rekening> data;
    OnClick listener;

    public AdapterRekening(Context context, List<Rekening> data, OnClick onClick) {
        this.context = context;
        this.data = data;
        this.listener = onClick;
    }

    @Override
    public holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rekening, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(holder holder, int position) {
        holder.mNamaRekening.setText(data.get(position).getNama());
        holder.mNomerRekening.setText(data.get(position).getNomer() + " ("+data.get(position).getBank()+")");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnClick{
        public void OnClickRekening(Rekening rekening);
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.nama_rekening)
        TextView mNamaRekening;
        @BindView(R.id.nomer_rekening)
        TextView mNomerRekening;
        @BindView(R.id.background)
        LinearLayout mBackground;
        public holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null) listener.OnClickRekening(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
