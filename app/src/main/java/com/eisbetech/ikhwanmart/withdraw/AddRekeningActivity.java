package com.eisbetech.ikhwanmart.withdraw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.eisbetech.ikhwanmart.R;

import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.eisbetech.ikhwanmart.item.Rekening;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class AddRekeningActivity extends AppCompatActivity {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.spinner_bank)
    SearchableSpinner mSpinnerBank;
    @BindView(R.id.nama_rekening)
    EditText mNamaRekening;
    @BindView(R.id.nomer_rekening)
    EditText mNomerRekening;
    @BindView(R.id.add_rekening)
    Button mAddRekening;
    private ArrayList<String> bankSpinner;
    private ArrayAdapter<String> bankAdapter;
    private List<Rekening> dataRekening;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank);
        ButterKnife.bind(this);

        realm =  RealmController.with(AddRekeningActivity.this).getRealm();

        dataRekening = new ArrayList<>();

        bankSpinner = new ArrayList<String>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("bank_name.csv")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                String[] stringSplits = mLine.split(",");

                bankSpinner.add(stringSplits[0]);
            }

            bankAdapter = new ArrayAdapter<String>(AddRekeningActivity.this,
                    android.R.layout.simple_spinner_item, bankSpinner);
            bankAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerBank.setAdapter(bankAdapter);
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public boolean validator(){
        if (mNamaRekening.getText().toString().isEmpty()){
            mNamaRekening.setError("Tidak Boleh Kosong");
            return false;
        } else if (mNomerRekening.getText().toString().isEmpty()){
            mNomerRekening.setError("Tidak Boleh Kosong");
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick({R.id.back_button, R.id.add_rekening})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                break;
            case R.id.add_rekening:
                if (validator()){
                    if (!realm.isInTransaction()) {
                        realm.beginTransaction();
                    }
                    Rekening rekening = realm.createObject(Rekening.class,System.currentTimeMillis());
                    rekening.bank = mSpinnerBank.getSelectedItem().toString();
                    rekening.nama = mNamaRekening.getText().toString();
                    rekening.nomer = mNomerRekening.getText().toString();

                    realm.commitTransaction();
                    setResult(RESULT_OK);
                    finish();
                }
                break;
        }
    }
}
