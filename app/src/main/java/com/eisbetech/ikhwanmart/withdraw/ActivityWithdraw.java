package com.eisbetech.ikhwanmart.withdraw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.item.Rekening;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ActivityWithdraw extends AppCompatActivity implements AdapterRekening.OnClick {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.nominal_withdraw)
    EditText mNominalWithdraw;
    @BindView(R.id.list_rekening)
    RecyclerView mListRekening;
    @BindView(R.id.add_rekening)
    FloatingActionButton mAddRekening;
    @BindView(R.id.rekening_kosong)
    TextView mRekeningKosong;
    @BindView(R.id.saldo_wallet)
    TextView mSaldoWallet;
    private List<Rekening> dataRekening;
    private AdapterRekening mAdapterRekening;
    private SessionManager session;
    private HashMap<String, String> user;
    private Realm realm;
    private User users;
    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        ButterKnife.bind(this);

        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading...");

        realm =  RealmController.with(ActivityWithdraw.this).getRealm();
        session = new SessionManager(getApplicationContext(), this);

        dataRekening = new ArrayList<>();
        mAdapterRekening = new AdapterRekening(this, dataRekening,this);
        mListRekening.setLayoutManager(new LinearLayoutManager(this));
        mListRekening.setAdapter(mAdapterRekening);

        dataRekening.clear();
        RealmResults<Rekening> rekening = realm.where(Rekening.class).findAll();

        dataRekening.addAll(rekening);
        mAdapterRekening.notifyDataSetChanged();

        if (mAdapterRekening.getItemCount() == 0) {
            mRekeningKosong.setVisibility(View.VISIBLE);
        } else {
            mRekeningKosong.setVisibility(View.GONE);
        }

        getProfile();
    }


    @OnClick({R.id.back_button, R.id.add_rekening})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.add_rekening:
                startActivityForResult(new Intent(ActivityWithdraw.this, AddRekeningActivity.class), 0);
                break;
        }
    }

    private void getProfile() {
        final User userdb = User.getUser();
        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        mSaldoWallet.setText(S.loading);
        try {
            String token = Helper.getFirebaseInstanceId(getApplicationContext());

            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, email);
            params.put(K.token, token);
            params.put("model", Helper.getDeviceName());
            params.put("os_version", Helper.getAndroidVersion());

            String device_id = Helper.getDeviceId(ActivityWithdraw.this);
            //  params.put("secret", AesCrypto.encrypt(String.format("%s:%s:%s",userdb.phone_number,userdb.email,device_id)));


            BisatopupApi.post("/profile/index", params, getApplicationContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(ActivityWithdraw.this, object.getString(K.message));
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(ActivityWithdraw.this, object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject response = new JSONObject(responseString);
                        realm =  RealmController.with(ActivityWithdraw.this).getRealm();
                        JSONObject data = response.getJSONObject(K.data);
                        users = realm.where(User.class).equalTo(K.email, email).findFirst();

                        Log.d("user_id",data.getInt(K.user_id)+"");

                        if (!realm.isInTransaction()) {
                            realm.beginTransaction();
                        }
                        if (users == null) {
                            users = realm.createObject(User.class, 1);
                        }
                        users.email = email;
                        users.user_id = data.getInt(K.user_id);
                        users.fullname = data.getString(K.name);
                        users.phone_number = data.getString(K.phone_number);
                        users.role = data.getString(K.role);
                        users.poin = data.getInt(K.poin);
                        users.wallet = data.getString(K.wallet);
                        //  users.sponsored_by = data.getString("sponsored_by");
                        users.kode_affiliasi = data.getString(K.affiliation_code);
                        users.total_affiliasi = data.getString(K.total_affiliate);
                        users.total_pendapatan = data.getString(K.total_income);
                        users.profile_url = data.getString(K.profile_url);
                        users.is_agent = data.getInt(K.is_agent);
                        users.nama_agen = data.getString(K.agent_name);
                        users.jenis_kelamin = data.getString(K.gender);
                        users.tgl_lahir = data.getString(K.birth_date);

                        if (data.has("key")) {
                            users.token = data.getString("key");
                        }

                        realm.commitTransaction();

                        mSaldoWallet.setText(users.wallet);

                        int need_verify = data.getInt(K.need_verify);
                        if (need_verify == 1) {
                            session.clearSession();
//                            try {
//                                User.ClearUser();
//                            } catch (Exception ec) {
//                                ec.printStackTrace();
//                            }
                            ActivityWithdraw.this.finish();
                            Helper.showMessage(ActivityWithdraw.this, S.verification_needed);
                            Intent i = new Intent(ActivityWithdraw.this, MainLayout.class);
                            i.setAction(S.action_phone_verification);
                            startActivity(i);
                        }
                        int force_logout = data.getInt(K.force_logout);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            dataRekening.clear();
            RealmResults<Rekening> rekening = realm.where(Rekening.class).findAll();

            dataRekening.addAll(rekening);
            mAdapterRekening.notifyDataSetChanged();

            if (mAdapterRekening.getItemCount() == 0) {
                mRekeningKosong.setVisibility(View.VISIBLE);
            } else {
                mRekeningKosong.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void OnClickRekening(Rekening rekening) {
        String regex = "[-\\\\[\\\\]^/,'*:.!><~@#$%+=?|\\\"\\\\\\\\()]+";
        if (mNominalWithdraw.getText().toString().length()==0){
            mNominalWithdraw.setError("Tidak Boleh Kosong");
        } else {
            if (Long.parseLong(mNominalWithdraw.getText().toString().replaceAll(regex,""))>Long.parseLong(mSaldoWallet.getText().toString().replace("Rp","").replaceAll(" ","").replaceAll(regex,""))){
                mNominalWithdraw.setError("Saldo Anda Tidak Mencukupi");
            } else {
                progress.show();
                final OkHttpClient client = new OkHttpClient();
                String url = "http://api.trans.ikhwan.eisbetech.com/api/v1/withdraw";

                RequestBody formBody = new FormBody.Builder()
                        .add("bank_name", rekening.getBank())
                        .add("bank_account",rekening.getNomer())
                        .add("bank_username",rekening.getNama())
                        .add("nominal",mNominalWithdraw.getText().toString().replaceAll(regex,""))
                        .add("user_id",users.user_id+"")
                        .build();

                final Request request = new Request.Builder()
                        .url(url)
                        .post(formBody)
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override public void onFailure(Call call, IOException e) {
                        Log.d("unik",e.getLocalizedMessage()+"");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.dismiss();
                            }
                        });
                    }

                    @Override public void onResponse(Call call, Response response) throws IOException {
                        final String textResponse = response.body().string();
                        Log.d("unik",textResponse+"");
                        try {
                            final JSONObject jsonObject = new JSONObject(textResponse);

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    progress.dismiss();
                                    Drawable img = new IconicsDrawable(ActivityWithdraw.this, FontAwesome.Icon.faw_sign_out).sizeDp(32).color(Color.WHITE);
                                    try {
                                        new LovelyStandardDialog(ActivityWithdraw.this)
                                                .setTopColorRes(R.color.accent)
                                                .setButtonsColorRes(R.color.accent)
                                                .setIcon(img)
                                                .setTitle("Berhasil")
                                                .setMessage(jsonObject.getString("message"))
                                                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                    }
                                                })
                                                .show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }
}
