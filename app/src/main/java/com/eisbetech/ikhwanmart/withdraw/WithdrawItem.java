package com.eisbetech.ikhwanmart.withdraw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.item.Withdraw.WithItem;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class WithdrawItem extends AbstractItem<WithdrawItem, WithdrawItem.ViewHolder> {
    public WithItem withdrawItem;

    public WithdrawItem(WithItem transaksi) {
        this.withdrawItem = transaksi;
    }

    @Override
    public int getType() {
        return R.id.transaksi_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.transaksi_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);

        Context ctx = holder.itemView.getContext();

        holder.txt_date.setText(withdrawItem.getCreatedAt());
        holder.txt_time.setVisibility(View.GONE);
        holder.txt_no_pelanggan.setText(withdrawItem.getBankUsername()+" ("+withdrawItem.getBankAccount()+")");
        holder.txt_status.setText(withdrawItem.getStatus()+"");
        holder.txt_price.setText(Helper.format_money(withdrawItem.getNominal()+""));
        holder.txt_product.setText("Withdraw");
        holder.txt_pembayaran.setText(withdrawItem.getBankName());
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected CircleImageView image;

        @BindView(R.id.txt_date)
        protected TextView txt_date;
        @BindView(R.id.txt_product)
        protected TextView txt_product;

        @BindView(R.id.txt_time)
        protected TextView txt_time;

        @BindView(R.id.txt_no_pelanggan)
        protected TextView txt_no_pelanggan;

        @BindView(R.id.txt_status)
        protected TextView txt_status;

        @BindView(R.id.txt_price)
        protected TextView txt_price;

        @BindView(R.id.txt_no_order)
        protected TextView txt_no_order;

        @BindView(R.id.txt_pembayaran)
        protected TextView txt_pembayaran;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}