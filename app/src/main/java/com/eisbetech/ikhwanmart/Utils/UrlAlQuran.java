package com.eisbetech.ikhwanmart.Utils;

public class UrlAlQuran {
    public static final String LIST_SURAH = "surah";
    public static final String DETAIL_SURAH_ARABIC = "surah/{id}";
    public static final String DETAIL_SURAH_TRANSLATE = "surah/{id}/id.indonesian";

    public static final String DETAIL_JUZ_ARABIC = "juz/{id}/ar.alafasy";
    public static final String DETAIL_JUZ_TRANSLATE = "juz/{id}/id.indonesian";
}
