package com.eisbetech.ikhwanmart.Utils;

import android.app.Activity;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.NumberFormat;
import java.util.Locale;

public class Constant {

    public static final String API_URL = "https://api.rajaongkir.com/starter/";
    public static final String API_KEY = "a3ed238f1687d7f82e9758a5efcfd973";
    public static final String CART_KEY = "cart_key";
    public static final String NEWS_KEY = "news_key";
    public static final String REKENING_KEY = "rekening_key";

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpaceItemDecoration(int space) {
            this.space = space;
        }


        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view);

            StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            int spanIndex = lp.getSpanIndex();

            if (position > 0) {
                outRect.left = space;
                outRect.bottom = space * 2;
            }
        }
    }

    public static String toRupiah(long value) {
        Locale locale = new Locale("in", "ID");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);

        return numberFormat.format(value).replace("Rp", "Rp. ");
    }

    public static String toTimeFormat(long start, long end) {
        long lama = Math.abs((end - start));

        return ((lama / 60) > 0 ? lama / 60 + "j " : "") + ((lama % 60) > 0 ? lama % 60 + "m" : "") + "";
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    public static class EdgeDecorator extends RecyclerView.ItemDecoration {

        private final int edgePadding;

        /**
         * EdgeDecorator
         *
         * @param edgePadding padding set on the left side of the first item and the right side of the last item
         */
        public EdgeDecorator(int edgePadding) {
            this.edgePadding = edgePadding;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            int itemCount = state.getItemCount();

            final int itemPosition = parent.getChildAdapterPosition(view);

            // no position, leave it alone
            if (itemPosition == RecyclerView.NO_POSITION) {
                return;
            }

            // first item
            if (itemPosition == 0 || itemPosition == 1) {
                outRect.set(0, edgePadding, 0, 0);
            } else if (itemPosition == itemCount - 1 || itemPosition == itemCount - 2) {
                outRect.set(0, 0, 0, edgePadding);
            } else {
                outRect.set(0, 0, 0, 0);
            }
        }
    }

    public static class LeftItemDecotaion extends RecyclerView.ItemDecoration {
        private int spacing;

        public LeftItemDecotaion(int spacing) {
            this.spacing = spacing;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            if (position == 0) {
                outRect.left = spacing;
            }
        }
    }

    public static class TopItemDecoration extends RecyclerView.ItemDecoration {
        private int spacing;

        public TopItemDecoration(int spacing) {
            this.spacing = spacing;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            if (position == 0) {
                outRect.top = spacing;
            }
        }
    }
}
