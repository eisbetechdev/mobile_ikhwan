package com.eisbetech.ikhwanmart.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.Login;
import com.eisbetech.ikhwanmart.R;

import com.freshchat.consumer.sdk.FaqOptions;
import com.freshchat.consumer.sdk.Freshchat;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.tapadoo.alerter.Alerter;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by khadafi on 8/25/2016.
 */
public class Helper {
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public static void newIntent(Activity activity, Class newClasss, @Nullable Bundle bundle){
        Intent intent = new Intent(activity, newClasss);

        if(bundle != null){
            intent.putExtras(bundle);
        }

        activity.startActivity(intent);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void addFragment(Fragment fragment1, Fragment fragment2, @Nullable Bundle bundle) {
        if (bundle != null) {
            fragment2.setArguments(bundle);
        }
        if (!fragment2.isAdded()) {
            try {
                HomeActivity.getInstance().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .hide(fragment1)
                        .add(R.id.fragment, fragment2, null)
                        .addToBackStack("tag")
                        .commit();
            } catch (Exception e) {
                System.out.println("Error fragment: " + e.getMessage());
            }
        } else {
            return;
        }
    }

    public static void changeFragment(Fragment fragment, @Nullable Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        HomeActivity.getInstance().getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.fragment, fragment, null)
                .addToBackStack("tag")
                .commit();
    }

    public static void sendMail(Activity activity, String email, String subject, String body) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        activity.startActivity(Intent.createChooser(intent, ""));
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.
                    getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static String DeviceId(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }
        return uniqueID;
    }

    public static void setFirebaseInstanceId(Context context, String InstanceId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor;
        editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.pref_firebase_instance_id_key), InstanceId);
        editor.apply();
    }

    public static String getFirebaseInstanceId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String key = context.getString(R.string.pref_firebase_instance_id_key);
        String default_value = context.getString(R.string.pref_firebase_instance_id_default_key);
        return sharedPreferences.getString(key, default_value);
    }

    public static String getDeviceId(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, Login.REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice = tm.getDeviceId();
            String androidId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
            String serial = null;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) serial = Build.SERIAL;

            if (tmDevice != null) return "01" + tmDevice;
            if (androidId != null) return "02" + androidId;
            if (serial != null) return "03" + serial;
            // other alternatives (i.e. Wi-Fi MAC, Bluetooth MAC, etc.)
        }

        return null;
    }

    public static void show_message(Context context,String message,Activity activity){
        SnackbarManager.show(
                Snackbar.with(context) // context
                        .type(SnackbarType.MULTI_LINE) // Set is as a multi-line snackbar
                        .text(message) // text to be displayed
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                , activity); // where it is displayed
    }
    public static void show_alert(String title,String message,Activity activity){
        Alerter.create(activity)
                .setTitle(title)
                .setText(message)
                .setBackgroundColorRes(R.color.alert) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }
    public static void show_info(String title,String message,Activity activity){
        Alerter.create(activity)
                .setTitle(title)
                .setText(message)
                .setBackgroundColorRes(R.color.accent) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }
    public static String format_money(String money) {
//        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
//        format.setMinimumFractionDigits(0);
//        format.setCurrency(Currency.getInstance("IDR"));
//        return format.format(Float.parseFloat(money));

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setMinimumFractionDigits(0);

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(Float.parseFloat(money));
    }

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//        Snackbar.make(context, message, Snackbar.LENGTH_LONG).show();
    }

    public static String getTimeAgo(String tanggal) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date parsedDate = dateFormat.parse(tanggal);

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(parsedDate.getTime()));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) {
            return null;
        }

        long time = date.getTime();

        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int timeDIM = getTimeDistanceInMinutes(time);

        String timeAgo;

        if (timeDIM == 0) {
            timeAgo = "less than a minute";
        } else if (timeDIM == 1) {
            return "1 minute";
        } else if (timeDIM >= 2 && timeDIM <= 44) {
            timeAgo = timeDIM + " minutes";
        } else if (timeDIM >= 45 && timeDIM <= 89) {
            timeAgo = "about an hour";
        } else if (timeDIM >= 90 && timeDIM <= 1439) {
            timeAgo = "about " + (Math.round(timeDIM / 60)) + " hours";
        } else if (timeDIM >= 1440 && timeDIM <= 2519) {
            timeAgo = "1 day";
        } else if (timeDIM >= 2520 && timeDIM <= 43199) {
            timeAgo = (Math.round(timeDIM / 1440)) + " days";
        } else if (timeDIM >= 43200 && timeDIM <= 86399) {
            timeAgo = "about a month";
        } else if (timeDIM >= 86400 && timeDIM <= 525599) {
            timeAgo = (Math.round(timeDIM / 43200)) + " months";
        } else if (timeDIM >= 525600 && timeDIM <= 655199) {
            timeAgo = "about a year";
        } else if (timeDIM >= 655200 && timeDIM <= 914399) {
            timeAgo = "over a year";
        } else if (timeDIM >= 914400 && timeDIM <= 1051199) {
            timeAgo = "almost 2 years";
        } else {
            timeAgo = "about " + (Math.round(timeDIM / 525600)) + " years";
        }

        return timeAgo + " ago";
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String GetStatusColor(String status) {
        String color = "";

        switch (status) {
            case "Completed":
                color = "#1BA39C";
                break;
            case "On processing":
                color = "#5E738B";
                break;
            case "Terbayar":
                color = "#5C9BD1";
                break;
            case "Menunggu":
                color = "#22313F";
                break;
            case "Cancelled":
                color = "#E7505A";
                break;

            case "Refund":
                color = "#1BA39C";
                break;
            case "On Hold":
                color = "#E7505A";
                break;
            case "Pending":
                color = "#E7505A";
                break;
            case "Confirmed":
                color = "#1BA39C";
                break;
            case "Belum Bayar":
                color = "#22313F";
                break;
            case "Validasi":
                color = "#5E738B";
                break;
            case "Failed":
                color = "#E7505A";
                break;
        }
        return color;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static String format_tanggal(String datestr) {
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = (Date)formatter.parse(datestr);
            return new SimpleDateFormat("d MMM YYYY, HH:mm").format(date);
        } catch (Exception e) {

        }
        return datestr;
    }

    public static boolean isNowConnected(Context context) {
        if (context == null) {
            return true;
        }
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
        return nwInfo != null && nwInfo.isConnectedOrConnecting();
    }

    public static String getStringResourceByName(String aString,Context context) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }

    public static  void checkPermission(Activity context) {
        int permissionCheck = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
        } else {
           // callMethod();
        }
    }

    public static void ShowFaq(Activity activity, String tag, String title) {
        List<String> tags = new ArrayList<>();
        tags.add(tag);

        FaqOptions faqOptions = new FaqOptions()
                .showFaqCategoriesAsGrid(false)
                .showContactUsOnAppBar(true)
                .showContactUsOnFaqScreens(true)
                .showContactUsOnFaqNotHelpful(true)
                .filterByTags(tags,"ARTICLE");

        Freshchat.showFAQs(activity, faqOptions);
    }

    public static boolean isExist (Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof String) {
            String s = (String) obj;
            return !s.isEmpty() && !s.equals("null") && !s.equals("0");
        } else if (obj instanceof Integer) {
            return (int) obj != 0;
        } else if (obj instanceof List) {
            return ((List) obj).size() != 0;
        } else if (obj instanceof Map) {
            return ((Map) obj).size() != 0;
        }
        return true;
    }

    public static boolean IntArrayCheck(int[] data, int value){
        for (int a :
                data) {
            if(a == value){
                return true;
            }
        }
        return false;
    }

    public static void copy(String txt, String val, Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.
                CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(txt, val);
        clipboard.setPrimaryClip(clip);
        Helper.showMessage(context, txt);
    }

    public static TextView createLink(TextView targetTextView, String completeString,
                                      String partToClick, ClickableSpan clickableAction) {

        SpannableString spannableString = new SpannableString(completeString);

        // make sure the String is exist, if it doesn't exist
        // it will throw IndexOutOfBoundException
        int startPosition = completeString.indexOf(partToClick);
        int endPosition = completeString.lastIndexOf(partToClick) + partToClick.length();

        spannableString.setSpan(clickableAction, startPosition, endPosition,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        targetTextView.setText(spannableString);
        targetTextView.setMovementMethod(LinkMovementMethod.getInstance());

        return targetTextView;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static int getAndroidVersion(){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        return currentapiVersion;
    }
}
