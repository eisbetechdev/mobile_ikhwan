package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.Menu;

import java.util.List;

/**
 * Created by khadafi on 8/25/2016.
 */
public class HomeMenuAdapter extends BaseAdapter {
    private LayoutInflater layoutinflater;
    private List<Menu> menuList;
    private Context context;

    public HomeMenuAdapter(List<Menu> menuList, Context context) {
        this.menuList = menuList;
        this.context = context;
        this.layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder listViewHolder;
        if (view == null) {
            listViewHolder = new ViewHolder();
            view = layoutinflater.inflate(R.layout.menu_item, viewGroup, false);
            listViewHolder.txt_title = (TextView)view.findViewById(R.id.txt_title);
            listViewHolder.image = (ImageView)view.findViewById(R.id.image);
            listViewHolder.txt_info = (TextView)view.findViewById(R.id.txt_info);
            view.setTag(listViewHolder);
        } else {
            listViewHolder = (ViewHolder)view.getTag();
        }

        listViewHolder.txt_title.setText(menuList.get(i).getTitle());

        listViewHolder.image.setImageDrawable(menuList.get(i).getImage());

        if (menuList.get(i).is_new())
        {


            listViewHolder.txt_info.setVisibility(View.VISIBLE);
        } else {
            listViewHolder.txt_info.setVisibility(View.GONE);
        }


        return view;
    }

    static class ViewHolder{
        TextView txt_title;
        ImageView image;
        TextView txt_info;
    }

}
