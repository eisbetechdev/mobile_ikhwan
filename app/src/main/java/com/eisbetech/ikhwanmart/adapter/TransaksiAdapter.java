package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.item.Transaksi;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransaksiAdapter extends RecyclerView.Adapter<TransaksiAdapter.holder> {
    Context context;
    List<Transaksi> data;

    public TransaksiAdapter(Context context, List<Transaksi> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_history, viewGroup, false);
        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int i) {
        Transaksi transaksi = data.get(i);

        Glide.with(context.getApplicationContext())
                .load(transaksi.getTransaction_cart().get(0).getCart_item_produk().getGambar().get(0))
                .into(holder.mImageProduct);

        if (transaksi.getTransaction_cart().size() == 1) {
            holder.mNamaProduct.setText(transaksi.getTransaction_cart().get(0).getCart_item_qnt() + " x " + transaksi.getTransaction_cart().get(0).getCart_item_produk().getNama());
        } else {
            holder.mNamaProduct.setText(transaksi.getTransaction_cart().size() + " Item");
        }

        holder.mHargaTotal.setText(Constant.toRupiah(transaksi.getTransaction_total()));
        holder.mTransactionStatus.setText(transaksi.getTransaction_status());

        holder.btnJumlah.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.nama_product)
        TextView mNamaProduct;
        @BindView(R.id.harga_total)
        TextView mHargaTotal;
        @BindView(R.id.parent)
        CardView mParent;
        @BindView(R.id.image_product)
        ImageView mImageProduct;
        @BindView(R.id.transaction_status)
        TextView mTransactionStatus;
        @BindView(R.id.btn_jumlah)
        LinearLayout btnJumlah;

        public holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

}
