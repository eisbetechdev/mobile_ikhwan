package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;

import java.util.List;

public class MyPagerAdapter extends PagerAdapter {
    Context context;
    List<String> listItems;
    int adapterType;

    public MyPagerAdapter(Context context, List<String> listItems) {
        this.context = context;
        this.listItems = listItems;
        this.adapterType = adapterType;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image, null);
        ImageView mImage = (ImageView) view.findViewById(R.id.image);

        Glide.with(context.getApplicationContext()).load(listItems.get(position)).into(mImage);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((CardView) object);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }
}