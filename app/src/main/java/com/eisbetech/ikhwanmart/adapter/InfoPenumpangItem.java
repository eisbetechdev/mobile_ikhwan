package com.eisbetech.ikhwanmart.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.InfoPenumpang;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hari on 1/8/2017.
 *
 */
public class InfoPenumpangItem extends AbstractItem<InfoPenumpangItem, InfoPenumpangItem.ViewHolder> {

    public InfoPenumpang infoPenumpang ;

    public InfoPenumpangItem(InfoPenumpang infoPenumpang) {
        this.infoPenumpang = infoPenumpang;
    }

    public InfoPenumpangItem() {
        this.infoPenumpang = new InfoPenumpang();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.info_penumpang_kereta;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        // Context ctx = holder.itemView.getContext();
        if (Helper.isExist(infoPenumpang.getName())) {
            holder.emptyView.setVisibility(View.VISIBLE);
            holder.dataView.setVisibility(View.GONE);
            holder.tipePenumpang.setText(infoPenumpang.getTipePenumpang());
        } else {
            holder.emptyView.setVisibility(View.GONE);
            holder.dataView.setVisibility(View.VISIBLE);
            holder.name.setText(infoPenumpang.getName());
            holder.idType.setText(infoPenumpang.getIdType());
            holder.userId.setText(infoPenumpang.getUserId());
        }
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.name)
        protected TextView name;

        @BindView(R.id.idType)
        protected TextView idType;

        @BindView(R.id.userId)
        protected TextView userId;

        @BindView(R.id.tipePenumpang)
        protected TextView tipePenumpang;

        @BindView(R.id.emptyView)
        protected LinearLayout emptyView;

        @BindView(R.id.dataView)
        protected LinearLayout dataView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
