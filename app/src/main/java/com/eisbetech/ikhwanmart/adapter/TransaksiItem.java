package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.Transaksi;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by khadafi on 8/25/2016.
 */
public class TransaksiItem extends AbstractItem<TransaksiItem, TransaksiItem.ViewHolder> {
    public Transaksi transaksi;

    public TransaksiItem(Transaksi transaksi) {
        this.transaksi = transaksi;
    }

    @Override
    public int getType() {
        return R.id.transaksi_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.transaksi_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);

        Context ctx = holder.itemView.getContext();

        holder.txt_date.setText(transaksi.tanggal);
        holder.txt_time.setText(transaksi.time);
        holder.txt_no_pelanggan.setText(transaksi.no_pelanggan);
        holder.txt_status.setText(transaksi.status);
        holder.txt_price.setText(Helper.format_money(transaksi.harga));
        holder.txt_product.setText(transaksi.product);
        holder.txt_pembayaran.setText(transaksi.payment_method.bank);

        String color = transaksi.status_color;
        holder.txt_status.setTextColor(Color.parseColor(color));
        holder.txt_no_order.setText("ID #"+transaksi.trans_id);
//        holder.txt_status.setBackgroundColor(Color.parseColor(color));

//        if (transaksi.image_url != null) {
//            Glide.clear(holder.image);
//            Glide.with(ctx).load(transaksi.image_url)
//                    .animate(R.anim.alpha_on)
//                    .placeholder(R.drawable.android_launcher)
//                    .into(holder.image);
//
//        } else {
//            Glide.clear(holder.image);
//        }
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }
    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected CircleImageView image;

        @BindView(R.id.txt_date)
        protected TextView txt_date;
        @BindView(R.id.txt_product)
        protected TextView txt_product;

        @BindView(R.id.txt_time)
        protected TextView txt_time;

        @BindView(R.id.txt_no_pelanggan)
        protected TextView txt_no_pelanggan;

        @BindView(R.id.txt_status)
        protected TextView txt_status;

        @BindView(R.id.txt_price)
        protected TextView txt_price;

        @BindView(R.id.txt_no_order)
        protected TextView txt_no_order;

        @BindView(R.id.txt_pembayaran)
        protected TextView txt_pembayaran;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
