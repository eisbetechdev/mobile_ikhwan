package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.List;

/**
 * Created by khadafi on 9/17/2017.
 */

public class AddNumberAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;


    private List<String> list;

    public AddNumberAdapter(Context context, List<String> list) {
        layoutInflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder;


        if (view == null) {
            view = layoutInflater.inflate(R.layout.add_number_layout, viewGroup, false);

            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView) view.findViewById(R.id.text_view);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.image_view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Context context = viewGroup.getContext();

        String desc = list.get(i);
        viewHolder.textView.setText(desc);
        Drawable icon;
        switch (i) {
            case 0:
                icon = new IconicsDrawable(context, GoogleMaterial.Icon.gmd_smartphone)
                        .color(Color.parseColor("#171e3b")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
            case 1:
                icon = new IconicsDrawable(context, GoogleMaterial.Icon.gmd_contact_phone)
                        .color(Color.parseColor("#171e3b")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
            case 2:
                icon = new IconicsDrawable(context, GoogleMaterial.Icon.gmd_history)
                        .color(Color.parseColor("#171e3b")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
            case 3:
                icon = new IconicsDrawable(context, GoogleMaterial.Icon.gmd_favorite)
                        .color(Color.parseColor("#171e3b")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
            case 4:
                icon = new IconicsDrawable(context, FontAwesome.Icon.faw_barcode)
                        .color(Color.parseColor("#171e3b")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
            default:
                icon = new IconicsDrawable(context, GoogleMaterial.Icon.gmd_help)
                        .color(Color.parseColor("#00544a")).sizeDp(20);
                viewHolder.imageView.setImageDrawable(icon);
                break;
        }

        return view;
    }

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}
