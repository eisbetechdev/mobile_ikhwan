package com.eisbetech.ikhwanmart.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.Train;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 *
 */
public class TrainItem extends AbstractItem<TrainItem, TrainItem.ViewHolder> {
    public Train train;

    public TrainItem(Train train) {
        this.train = train;
    }


    public TrainItem() {
        this.train = new Train();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }


    @Override
    public int getType() {
        return R.id.train_type;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.train_item;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.txt_train_name.setText(train.getTrain_name());
        holder.train_type.setText(train.getTrain_type());
        holder.txt_waktu.setText(train.getWaktu());
        holder.txt_price.setText(Helper.format_money(train.getAdultFare()));
        holder.txt_keterangan_waktu.setText(train.getKeterangan_waktu());
        holder.sisa_kursi.setText(train.getSisa_kursi());
        if(train.getSisa_kursi().equals("Habis"))
            holder.sisa_kursi.setTextColor(Color.parseColor("#FF0000"));
        else
            holder.sisa_kursi.setTextColor(Color.parseColor("#878585"));
    }


    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected ImageView image;

        @BindView(R.id.txt_train_name)
        protected TextView txt_train_name;

        @BindView(R.id.train_type)
        protected TextView train_type;

        @BindView(R.id.txt_waktu)
        protected TextView txt_waktu;

        @BindView(R.id.txt_keterangan_waktu)
        protected TextView txt_keterangan_waktu;

        @BindView(R.id.txt_price)
        protected TextView txt_price;

        @BindView(R.id.sisa_kursi)
        protected TextView sisa_kursi;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
        }
    }
}
