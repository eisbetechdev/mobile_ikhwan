package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.e_commerce.DetailProdukActivity;
import com.eisbetech.ikhwanmart.item.Produk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.holder> {
    Context context;
    List<Produk> data;
    int jumlah = 0;
    int mode = 0;

    public ProdukAdapter(Context context, List<Produk> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_produk, viewGroup, false);
        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int i) {
        Produk produk = data.get(i);
        /*if ((Integer)holder.mCardView.getTag()==0){
            holder.mCardView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,500));
        } else {
            holder.mCardView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,300));
        }*/

        holder.mHargaProduct.setText(Constant.toRupiah(produk.getHarga()));
        holder.mNamaProduct.setText(produk.getNama());

        Glide.with(context.getApplicationContext()).load(produk.getGambar() != null ? produk.getGambar().get(0) : context.getResources().getDrawable(R.drawable.logo)).into(holder.mThumbnail);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView mThumbnail;
        @BindView(R.id.nama_product)
        TextView mNamaProduct;
        @BindView(R.id.harga_product)
        TextView mHargaProduct;

        public holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            /*mCardView.setTag(mode);
            mode = 1;
            if (jumlah<2){
                jumlah+=1;

            } else {
                jumlah = 0;
                mode = 0;
            }*/

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailProdukActivity.class);
                    intent.putExtra("produk", data.get(getAdapterPosition()));
                    context.startActivity(intent);
//                    Helper.showToast(context,"Coming Soon");
                }
            });

        }
    }
}
