package com.eisbetech.ikhwanmart.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.DetailList;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hari on 1/8/2017.
 *
 */
public class LayoutKeretaItem1 extends AbstractItem<LayoutKeretaItem1, LayoutKeretaItem1.ViewHolder> {
    private DetailList detailList;
    public LayoutKeretaItem1(DetailList detailList) {
        this.detailList = detailList;
    }
    public LayoutKeretaItem1() {
        this.detailList = new DetailList();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.layout_list_detail_kereta1;
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);

        holder.key.setText(detailList.getKey());
        holder.value.setText(detailList.getValue());
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.key)
        protected TextView key;

        @BindView(R.id.value)
        protected TextView value;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;
        }
    }
}
