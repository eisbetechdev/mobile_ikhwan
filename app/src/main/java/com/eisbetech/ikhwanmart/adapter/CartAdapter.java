package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.e_commerce.CartFragment;
import com.eisbetech.ikhwanmart.item.CartItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.holder> {
    Context context;
    List<CartItem> data;
    CartFragment fragment;

    public CartAdapter(Context context, List<CartItem> data, CartFragment fragment) {
        this.context = context;
        this.data = data;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_history, viewGroup, false);
        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final holder holder, int i) {
        final CartItem cartItem = data.get(i);

        Glide.with(context.getApplicationContext())
                .load(cartItem.getCart_item_produk().getGambar().get(0))
                .into(holder.mImageProduct);

        holder.mNamaProduct.setText(cartItem.getCart_item_produk().getNama());
        holder.tvJml.setText(cartItem.getCart_item_qnt()+"");

        holder.mHargaTotal.setText(Constant.toRupiah(cartItem.getCart_item_produk().getHarga() * cartItem.getCart_item_qnt()));
        holder.mTransactionStatus.setVisibility(View.GONE);

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int jumlah = cartItem.getCart_item_qnt() - 1;
                cartItem.setCart_item_qnt(jumlah);
                holder.tvJml.setText(cartItem.getCart_item_qnt()+"");
                if(cartItem.getCart_item_qnt() == 0){
                    holder.btnMinus.setEnabled(false);
                }else {
                    holder.btnMinus.setEnabled(true);
                }
                fragment.setDataChange(data);
            }
        });

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int jumlah = cartItem.getCart_item_qnt() + 1;
                cartItem.setCart_item_qnt(jumlah);
                holder.tvJml.setText(cartItem.getCart_item_qnt()+"");
                fragment.setDataChange(data);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.nama_product)
        TextView mNamaProduct;
        @BindView(R.id.harga_total)
        TextView mHargaTotal;
        @BindView(R.id.parent)
        CardView mParent;
        @BindView(R.id.image_product)
        ImageView mImageProduct;
        @BindView(R.id.transaction_status)
        TextView mTransactionStatus;
        @BindView(R.id.btn_minus)
        RelativeLayout btnMinus;
        @BindView(R.id.btn_plus)
        RelativeLayout btnPlus;
        @BindView(R.id.btn_jumlah)
        LinearLayout btnJumlah;
        @BindView(R.id.tv_jml)
        TextView tvJml;

        public holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

}
