package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.entity.Menu;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 */
public class MenuItemAdapter extends AbstractItem<MenuItemAdapter, MenuItemAdapter.ViewHolder> {
    public Menu menu;

    public MenuItemAdapter(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int getType() {
        return R.id.menu_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.menu_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);


        Context ctx = holder.itemView.getContext();

        holder.txt_title.setText(menu.getTitle());

        Drawable default_im = new IconicsDrawable(ctx, GoogleMaterial.Icon.gmd_change_history)
                .color(Color.LTGRAY).sizeDp(120);
        holder.image.setImageDrawable(default_im);
    }

    @Override
    public ViewHolder getViewHolder(View v) {
            return new ViewHolder(v);
    }


    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected ImageView image;

        @BindView(R.id.txt_title)
        protected TextView txt_title;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
