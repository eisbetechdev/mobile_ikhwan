package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.DetailNewsActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.item.News.DataItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Centaury on 09/10/2018.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.viewHolder> {

    private Context context;
    private List<DataItem> dataItemList;

    public NewsAdapter(Context context, List<DataItem> dataItemList) {
        this.context = context;
        this.dataItemList = dataItemList;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_berita, parent, false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        DataItem dataItem = dataItemList.get(position);

        Glide.with(context.getApplicationContext())
                .load(dataItem.getCover())
                .into(holder.mImageCover);

        holder.mJudulBerita.setText(dataItem.getTitle());
        holder.mKategoriBerita.setText(dataItem.getCategory());
        holder.mTanggalBerita.setText(dataItem.getPostDate());

    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_cover)
        ImageView mImageCover;
        @BindView(R.id.kategori_berita)
        TextView mKategoriBerita;
        @BindView(R.id.judul_berita)
        TextView mJudulBerita;
        @BindView(R.id.tanggal_berita)
        TextView mTanggalBerita;
        @BindView(R.id.container_berita)
        CardView mContainerBerita;

        public viewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context,DetailNewsActivity.class);
                    intent.putExtra(Constant.NEWS_KEY, dataItemList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
