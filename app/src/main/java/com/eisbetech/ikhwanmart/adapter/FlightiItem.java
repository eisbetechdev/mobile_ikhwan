package com.eisbetech.ikhwanmart.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.entity.Flight;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 */
public class FlightiItem extends AbstractItem<FlightiItem, FlightiItem.ViewHolder> {
    public Flight flight;

    public FlightiItem(Flight flight) {
        this.flight = flight;
    }


    public FlightiItem() {
        this.flight = new Flight();
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
    }

    public FlightiItem withMaskapai(Drawable image) {
        flight.setImage(image);
        return this;
    }

    public FlightiItem withImage(Drawable image) {
        flight.setImage(image);
        return this;
    }

    @Override
    public int getType() {
        return R.id.flight_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.flight_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);
        Context ctx = holder.itemView.getContext();

        holder.image.setImageDrawable(flight.getImage());
        holder.txt_maskapai_name.setText(flight.getMaskapai_name());
        holder.txt_nomor.setText(flight.getKode_penerbangan());
        holder.txt_waktu.setText(flight.getWaktu());
        holder.txt_price.setText(Helper.format_money(flight.getPrice()));
    }


    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;

        @BindView(R.id.image)
        protected ImageView image;

        @BindView(R.id.txt_maskapai_name)
        protected TextView txt_maskapai_name;

        @BindView(R.id.txt_nomor)
        protected TextView txt_nomor;

        @BindView(R.id.txt_waktu)
        protected TextView txt_waktu;

        @BindView(R.id.txt_keterangan_waktu)
        protected TextView txt_keterangan_waktu;

        @BindView(R.id.txt_price)
        protected TextView txt_price;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
