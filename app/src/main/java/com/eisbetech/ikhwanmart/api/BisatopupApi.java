package com.eisbetech.ikhwanmart.api;

import android.content.Context;

import com.eisbetech.ikhwanmart.BuildConfig;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.components.TLSSocketFactory;
import com.eisbetech.ikhwanmart.database.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

/**
 * Created by khadafi on 8/27/2016.
 *
 */
public class BisatopupApi {
    private static final String BASE_URL = BuildConfig.BASEURL;
    private static String user_key = BuildConfig.TOKEN;
    final static int DEFAULT_TIMEOUT = 60 * 1000;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, Context context,
                           AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(DEFAULT_TIMEOUT);
        client.setMaxRetriesAndTimeout(3,1500);

        User user = User.getUser();

        String token = user_key;

        if (user != null) {
            token  = user.token;
        }
        if (token == null) {
            token = user_key;
        }
        try {
            SSLContext.getInstance("TLSv1.2");
            SSLContext contextd = SSLContext.getInstance("TLS");

            TLSSocketFactory noSSLv3Factory = null;


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        client.addHeader("X-Authorization", token);
        params.put("version", BuildConfig.VERSION_CODE);

//        params.put("secret",AesCrypto.encrypt(params.toString()));
        if(user != null){
            params.put("device_id",user.device_id);
        }
        String sc = AesCrypto.encrypt(params.toString());
        RequestParams newparam = new RequestParams();
        newparam.put("secret",sc);


        if (Helper.isNowConnected(context)) {
            client.get(getAbsoluteUrl(url), newparam, responseHandler);
        } else {
            Helper.showMessage(context, "Your phone is not connect, Please check your network connection");
        }
    }

    public static void post(String url, RequestParams params, Context context,
                            AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(DEFAULT_TIMEOUT);
        client.setMaxRetriesAndTimeout(0,1500);

        User user = User.getUser();

        String token = user_key;
        if (user != null) {
            token = user.token;
        }
        if (token == null) {
            token = user_key;
        }

        client.addHeader("X-Authorization", token);
        params.put("version", BuildConfig.VERSION_CODE);
        if(user != null){
            params.put("device_id",user.device_id);
        }
//        params.put("secret",AesCrypto.encrypt(params.toString()));
        String sc = AesCrypto.encrypt(params.toString());
        RequestParams newparam = new RequestParams();
        newparam.put("secret",sc);

        if (Helper.isNowConnected(context)) {
            client.post(getAbsoluteUrl(url), newparam, responseHandler);
        } else {
            Helper.showMessage(context, "Your phone is not connect, Please check your network connection");
        }
    }

//    public static  OkHttpClient getOkHttpClient() {
//        final User user = User.getUser();
//        String token = user_key;
//
//        return new OkHttpClient.Builder()
//                .followRedirects(true)
//                .followSslRedirects(true)
//                .retryOnConnectionFailure(true)
//                .connectTimeout(15, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .readTimeout(30, TimeUnit.SECONDS)
//
//                // you can add your own request interceptors to add authorization headers.
//                // do not modify the body or the http method here, as they are set and managed
//                // internally by Upload Service, and tinkering with them will result in strange,
//                // erroneous and unpredicted behaviors
//                .addNetworkInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//                        Request.Builder request = chain.request().newBuilder()
//                                .addHeader("X-Authorization",user.token)
//                                .addHeader("mysecondheader", "mysecondvalue");
//
//                        return chain.proceed(request.build());
//                    }
//                })
//
//                .cache(null)
//                .build();
//    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler
            responseHandler) {
        client.setTimeout(DEFAULT_TIMEOUT);
        User user = User.getUser();
        String token = user_key;
        if (user != null) {
            token  = user.token;
        }
        client.addHeader("X-Authorization", token);
        params.put("version", "4.24");
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }
}
