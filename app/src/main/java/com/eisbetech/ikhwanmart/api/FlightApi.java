package com.eisbetech.ikhwanmart.api;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by khadafi on 8/27/2016.
 */
public class FlightApi {
    private static final String BASE_URL = "http://api.amanahcorp.com/api/v1";
    private static String user_key = "d93122baa5b3fee3d210c90b761b3d60377f2085";
    private static String token = "2f13b25b1f5c93212a0dbef71f5a5597";
    final static int DEFAULT_TIMEOUT = 600 * 1000;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(DEFAULT_TIMEOUT);

        client.addHeader("X-Authorization",user_key);
        client.addHeader("Token",token);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(DEFAULT_TIMEOUT);
        client.addHeader("X-Authorization",user_key);
        client.addHeader("Token",token);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
