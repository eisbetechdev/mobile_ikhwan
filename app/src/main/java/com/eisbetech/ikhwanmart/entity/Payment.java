package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 9/12/2016.
 */
public class Payment implements Parcelable {
    public String name;
    public String rekening;
    public String bank;
    public String image_url;
    public int id;



    public Payment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.rekening);
        dest.writeString(this.bank);
        dest.writeInt(this.id);
        dest.writeString(this.image_url);
    }

    protected Payment(Parcel in) {
        this.name = in.readString();
        this.rekening = in.readString();
        this.bank = in.readString();
        this.id = in.readInt();
        this.image_url = in.readString();
    }

    public static final Creator<Payment> CREATOR = new Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel source) {
            return new Payment(source);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };


}
