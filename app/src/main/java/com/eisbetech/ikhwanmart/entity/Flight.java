package com.eisbetech.ikhwanmart.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by khadafi on 8/27/2016.
 */
public class Flight {
    protected int id;
    protected String maskapai_id;
    protected String maskapai_name;
    protected String kode_penerbangan;
    protected String waktu;
    protected String keterangan_waktu;
    protected String price;
    protected double harga;
    protected Drawable image;


    public Flight(String maskapai_id, String maskapai_name, String kode_penerbangan, String waktu, String keterangan_waktu, String price) {
        this.maskapai_id = maskapai_id;
        this.maskapai_name = maskapai_name;
        this.kode_penerbangan = kode_penerbangan;
        this.waktu = waktu;
        this.keterangan_waktu = keterangan_waktu;
        this.price = price;
    }

    public Flight() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getMaskapai_id() {
        return maskapai_id;
    }

    public void setMaskapai_id(String maskapai_id) {
        this.maskapai_id = maskapai_id;
    }

    public String getMaskapai_name() {
        return maskapai_name;
    }

    public void setMaskapai_name(String maskapai_name) {
        this.maskapai_name = maskapai_name;
    }

    public String getKode_penerbangan() {
        return kode_penerbangan;
    }

    public void setKode_penerbangan(String kode_penerbangan) {
        this.kode_penerbangan = kode_penerbangan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getKeterangan_waktu() {
        return keterangan_waktu;
    }

    public void setKeterangan_waktu(String keterangan_waktu) {
        this.keterangan_waktu = keterangan_waktu;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
