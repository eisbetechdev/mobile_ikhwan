package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 8/27/2016.
 * 
 */
public class InfoPenumpang implements Parcelable {
    private String name;
    private String idType;
    private String userId;
    private String tipePenumpang;
    private int posisiPenumpang;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTipePenumpang() {
        return tipePenumpang;
    }

    public void setTipePenumpang(String tipePenumpang) {
        this.tipePenumpang = tipePenumpang;
    }

    public int getPosisiPenumpang() {
        return posisiPenumpang;
    }

    public void setPosisiPenumpang(int posisiPenumpang) {
        this.posisiPenumpang = posisiPenumpang;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.idType);
        dest.writeString(this.userId);
        dest.writeString(this.tipePenumpang);
        dest.writeInt(this.posisiPenumpang);
    }

    public InfoPenumpang() {
    }

    private InfoPenumpang(Parcel in) {
        this.name = in.readString();
        this.idType = in.readString();
        this.userId = in.readString();
        this.tipePenumpang = in.readString();
        this.posisiPenumpang = in.readInt();
    }

    public static final Creator<InfoPenumpang> CREATOR = new Creator<InfoPenumpang>() {
        @Override
        public InfoPenumpang createFromParcel(Parcel source) {
            return new InfoPenumpang(source);
        }

        @Override
        public InfoPenumpang[] newArray(int size) {
            return new InfoPenumpang[size];
        }
    };
}
