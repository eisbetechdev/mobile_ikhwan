package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 8/27/2016.
 * 
 */
public class Train implements Parcelable {
    private int id;
    private String train_id;
    private String train_name;
    private String train_type;
    private String waktu;
    private String keterangan_waktu;
    private String stasiunAsalCode;
    private String stasiunAsalName;
    private String stasiunTujuanCode;
    private String stasiunTujuanName;
    private double harga;
    private String tglBerangkat;
    private String tglPulang;
    private String jumlahDewasa;
    private String jumlahBayi;
    private String sisa_kursi;
    private String adultFare;
    private String childFare;
    
    public Train(String train_id, String train_name, String train_type, String waktu,
                 String keterangan_waktu, String stasiunAsalCode, String stasiunAsalName,
                 String stasiunTujuanCode, String stasiunTujuanName, String tglBerangkat,
                 String tglPulang, String jumlahBayi, String jumlahDewasa, String sisa_kursi,
                 String adultFare, String childFare ) {
        this.train_id = train_id;
        this.train_name = train_name;
        this.train_type = train_type;
        this.waktu = waktu;
        this.keterangan_waktu = keterangan_waktu;
        this.stasiunAsalCode = stasiunAsalCode;
        this.stasiunAsalName = stasiunAsalName;
        this.stasiunTujuanCode = stasiunTujuanCode;
        this.stasiunTujuanName = stasiunTujuanName;
        this.tglBerangkat = tglBerangkat;
        this.tglPulang = tglPulang;
        this.jumlahDewasa = jumlahDewasa;
        this.jumlahBayi = jumlahBayi;
        this.sisa_kursi = sisa_kursi;
        this.adultFare = adultFare;
        this.childFare = childFare;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrain_id() {
        return train_id;
    }

    public void setTrain_id(String train_id) {
        this.train_id = train_id;
    }

    public String getTrain_name() {
        return train_name;
    }

    public void setTrain_name(String train_name) {
        this.train_name = train_name;
    }

    public String getTrain_type() {
        return train_type;
    }

    public void setTrain_type(String train_type) {
        this.train_type = train_type;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getKeterangan_waktu() {
        return keterangan_waktu;
    }

    public void setKeterangan_waktu(String keterangan_waktu) {
        this.keterangan_waktu = keterangan_waktu;
    }

    public String getStasiunAsalCode() {
        return stasiunAsalCode;
    }

    public void setStasiunAsalCode(String stasiunAsalCode) {
        this.stasiunAsalCode = stasiunAsalCode;
    }

    public String getStasiunAsalName() {
        return stasiunAsalName;
    }

    public void setStasiunAsalName(String stasiunAsalName) {
        this.stasiunAsalName = stasiunAsalName;
    }

    public String getStasiunTujuanCode() {
        return stasiunTujuanCode;
    }

    public void setStasiunTujuanCode(String stasiunTujuanCode) {
        this.stasiunTujuanCode = stasiunTujuanCode;
    }

    public String getStasiunTujuanName() {
        return stasiunTujuanName;
    }

    public void setStasiunTujuanName(String stasiunTujuanName) {
        this.stasiunTujuanName = stasiunTujuanName;
    }
    public String getTglBerangkat() {
        return tglBerangkat;
    }

    public void setTglBerangkat(String tglBerangkat) {
        this.tglBerangkat = tglBerangkat;
    }

    public String getTglPulang() {
        return tglPulang;
    }

    public void setTglPulang(String tglPulang) {
        this.tglPulang = tglPulang;
    }

    public String getJumlahDewasa() {
        return jumlahDewasa;
    }

    public void setJumlahDewasa(String jumlahDewasa) {
        this.jumlahDewasa = jumlahDewasa;
    }

    public String getJumlahBayi() {
        return jumlahBayi;
    }

    public void setJumlahBayi(String jumlahBayi) {
        this.jumlahBayi = jumlahBayi;
    }

    public String getSisa_kursi() {
        return sisa_kursi;
    }

    public void setSisa_kursi(String sisa_kursi) {
        this.sisa_kursi = sisa_kursi;
    }

    public String getAdultFare() {
        return adultFare;
    }

    public void setAdultFare(String adultFare) {
        this.adultFare = adultFare;
    }

    public String getChildFare() {
        return childFare;
    }

    public void setChildFare(String childFare) {
        this.childFare = childFare;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.train_id);
        dest.writeString(this.train_name);
        dest.writeString(this.train_type);
        dest.writeString(this.waktu);
        dest.writeString(this.keterangan_waktu);
        dest.writeString(this.stasiunAsalCode);
        dest.writeString(this.stasiunAsalName);
        dest.writeString(this.stasiunTujuanCode);
        dest.writeString(this.stasiunTujuanName);
        dest.writeString(this.tglBerangkat);
        dest.writeString(this.tglPulang);
        dest.writeString(this.jumlahDewasa);
        dest.writeString(this.jumlahBayi);
        dest.writeString(this.adultFare);
        dest.writeString(this.childFare);
    }

    public Train() {
    }

    private Train(Parcel in) {
        this.train_id = in.readString();
        this.train_name = in.readString();
        this.train_type = in.readString();
        this.waktu = in.readString();
        this.keterangan_waktu = in.readString();
        this.stasiunAsalCode = in.readString();
        this.stasiunAsalName = in.readString();
        this.stasiunTujuanCode = in.readString();
        this.stasiunTujuanName = in.readString();
        this.tglBerangkat = in.readString();
        this.tglPulang = in.readString();
        this.jumlahDewasa = in.readString();
        this.jumlahBayi = in.readString();
        this.adultFare = in.readString();
        this.childFare = in.readString();
    }

    public static final Parcelable.Creator<Train> CREATOR = new Parcelable.Creator<Train>() {
        @Override
        public Train createFromParcel(Parcel source) {
            return new Train(source);
        }

        @Override
        public Train[] newArray(int size) {
            return new Train[size];
        }
    };
}
