package com.eisbetech.ikhwanmart.entity;

/**
 * Created by khadafi on 3/26/2017.
 */

public class Media {
    public int id;
    public String title;
    public String sub_title;
    public int parent_id;
    public int has_child;
    public String image;
    public String type;
    public String url;
}
