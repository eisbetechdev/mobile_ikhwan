package com.eisbetech.ikhwanmart.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by khadafi on 8/25/2016.
 */
public class Menu {
    private String image_url;
    private Drawable image;
    private String title;
    private boolean is_new = false;

    public Menu() {
    }
    public Menu(String image_url, Drawable image, String title) {
        this.image_url = image_url;
        this.image = image;
        this.title = title;
    }
    public Menu(String image_url, Drawable image, String title, boolean is_new) {
        this.image_url = image_url;
        this.image = image;
        this.title = title;
        this.is_new = is_new;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setIs_new(boolean is_new) {
        this.is_new = is_new;
    }

    public boolean is_new() {
        return is_new;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
