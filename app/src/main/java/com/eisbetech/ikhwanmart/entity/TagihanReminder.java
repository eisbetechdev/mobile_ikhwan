package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 3/25/2017.
 */

public class TagihanReminder implements Parcelable {
    public int id;
    public String nomor_rekening;
    public int product_detail_id;
    public String product;
    public String product_parent;
    public int tanggal_reminder;
    public int is_enable;
    public String note;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nomor_rekening);
        dest.writeInt(this.product_detail_id);
        dest.writeString(this.product);
        dest.writeString(this.product_parent);
        dest.writeInt(this.tanggal_reminder);
        dest.writeInt(this.is_enable);
        dest.writeString(this.note);
    }

    public TagihanReminder() {
    }

    protected TagihanReminder(Parcel in) {
        this.id = in.readInt();
        this.nomor_rekening = in.readString();
        this.product_detail_id = in.readInt();
        this.product = in.readString();
        this.product_parent = in.readString();
        this.tanggal_reminder = in.readInt();
        this.is_enable = in.readInt();
        this.note = in.readString();
    }

    public static final Parcelable.Creator<TagihanReminder> CREATOR = new Parcelable.Creator<TagihanReminder>() {
        @Override
        public TagihanReminder createFromParcel(Parcel source) {
            return new TagihanReminder(source);
        }

        @Override
        public TagihanReminder[] newArray(int size) {
            return new TagihanReminder[size];
        }
    };
}
