package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 8/27/2016.
 *
 */
public class DetailList implements Parcelable {
    protected String key;
    protected String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DetailList(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.value);
    }

    public DetailList() {
    }

    private DetailList(Parcel in) {
        this.key = in.readString();
        this.value = in.readString();
    }

    public static final Creator<DetailList> CREATOR = new Creator<DetailList>() {
        @Override
        public DetailList createFromParcel(Parcel source) {
            return new DetailList(source);
        }

        @Override
        public DetailList[] newArray(int size) {
            return new DetailList[size];
        }
    };
}
