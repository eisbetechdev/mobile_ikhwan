package com.eisbetech.ikhwanmart.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by khadafi on 8/25/2016.
 */
public class Transaksi implements Parcelable {
    public String trans_id;
    public int product_id;
    public int is_tagihan;
    public String tanggal;
    public String time;
    public String product;
    public String note;
    public String expired_date;
    public String product_name;
    public String product_detail;
    public String product_detail_id;
    public String harga;
    public String base_price;
    public String payment;
    public String no_pelanggan;
    public String no_hp_pelanggan;
    public String image_url;
    public String status;
    public String status_color;
    public String kode_unik;
    public String admin_fee;
    public int status_id;
    public String token;

    public Payment payment_method;

    public Transaksi() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.trans_id);
        dest.writeInt(this.product_id);
        dest.writeInt(this.is_tagihan);
        dest.writeString(this.tanggal);
        dest.writeString(this.time);
        dest.writeString(this.product);
        dest.writeString(this.note);
        dest.writeString(this.expired_date);
        dest.writeString(this.product_name);
        dest.writeString(this.product_detail);
        dest.writeString(this.product_detail_id);
        dest.writeString(this.harga);
        dest.writeString(this.base_price);
        dest.writeString(this.payment);
        dest.writeString(this.no_pelanggan);
        dest.writeString(this.no_hp_pelanggan);
        dest.writeString(this.image_url);
        dest.writeString(this.status);
        dest.writeString(this.status_color);
        dest.writeString(this.kode_unik);
        dest.writeString(this.admin_fee);
        dest.writeInt(this.status_id);
        dest.writeString(this.token);
        dest.writeParcelable(this.payment_method, flags);
    }

    protected Transaksi(Parcel in) {
        this.trans_id = in.readString();
        this.product_id = in.readInt();
        this.is_tagihan = in.readInt();
        this.tanggal = in.readString();
        this.time = in.readString();
        this.product = in.readString();
        this.note = in.readString();
        this.expired_date = in.readString();
        this.product_name = in.readString();
        this.product_detail = in.readString();
        this.product_detail_id = in.readString();
        this.harga = in.readString();
        this.base_price = in.readString();
        this.payment = in.readString();
        this.no_pelanggan = in.readString();
        this.no_hp_pelanggan = in.readString();
        this.image_url = in.readString();
        this.status = in.readString();
        this.status_color = in.readString();
        this.kode_unik = in.readString();
        this.admin_fee = in.readString();
        this.status_id = in.readInt();
        this.token = in.readString();
        this.payment_method = in.readParcelable(Payment.class.getClassLoader());
    }

    public static final Creator<Transaksi> CREATOR = new Creator<Transaksi>() {
        @Override
        public Transaksi createFromParcel(Parcel source) {
            return new Transaksi(source);
        }

        @Override
        public Transaksi[] newArray(int size) {
            return new Transaksi[size];
        }
    };
}
