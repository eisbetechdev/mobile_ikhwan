package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.DataKeretaUtils;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StasiunAsalActivity extends AppCompatActivity {
    @BindView(R.id.searchStasiunAsal)
    protected SearchView searchStasiunAsal ;

    @BindView(R.id.stasiunAsalList)
    protected ListView stasiunAsalList ;

    protected List<String> listStasiun = new ArrayList<>();
    protected ArrayAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.train_stasiun_asal);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchStasiunAsal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStasiunAsal.setIconified(false);
            }
        });
        EditText searchEditText = searchStasiunAsal.findViewById(android.support.v7.appcompat.R.id
                .search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.black));
        searchEditText.setHint(S.station_origin);

        ImageView luvIcon = searchStasiunAsal.findViewById(android.support.v7.appcompat.R.id.
                search_button);
        ImageView cancelIcon = searchStasiunAsal.findViewById(android.support.v7.appcompat.R.id.
                search_close_btn);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            luvIcon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
            cancelIcon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        }
        listStasiun = DataKeretaUtils.getListStasiunKereta();

        searchStasiunAsal.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newKey) {
                 List<String> newSearchList = new ArrayList<>();
                 for (int i = 0; i < listStasiun.size(); i++) {
                     String param = listStasiun.get(i);
                     if (param.toUpperCase().contains(newKey.toUpperCase())) {
                         newSearchList.add(param);
                     }
                 }
                 ArrayAdapter<String> listAdapter =
                     new ArrayAdapter<String>(getApplicationContext(), android.R.layout
                             .simple_list_item_1, newSearchList){
                         @NonNull
                         public View getView(int position, View convertView, @NonNull
                                 ViewGroup parent){
                             View view = super.getView(position, convertView, parent);
                             TextView tv = view.findViewById(android.R.id.text1);
                             tv.setTextColor(getResources().getColor(R.color.black));
                             return view;
                         }
                     };
                 stasiunAsalList.setAdapter(listAdapter);
                 return false;
            }
        });
        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                listStasiun) {
            @NonNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(getResources().getColor(R.color.black));

                return view;
            }
        };
        stasiunAsalList.setAdapter(listAdapter);
        stasiunAsalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(K.result, view.toString());

                    String selectedStasiun = (String) parent.getItemAtPosition(position);
                    returnIntent.putExtra(K.selected_station, selectedStasiun);
                    setResult(1, returnIntent);
                    finish();
                }
            });
    }

    @OnClick(R.id.tutupSearchAsal)
    protected void doTutupButton(){
        onBackPressed();
    }
}
