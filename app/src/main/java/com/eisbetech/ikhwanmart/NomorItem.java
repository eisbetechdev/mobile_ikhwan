package com.eisbetech.ikhwanmart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 8/25/2016.
 */
public class NomorItem extends AbstractItem<NomorItem, NomorItem.ViewHolder> {
    public String Nomor;
    public String Nama;
    public String Detail;

    public NomorItem(String nomor,String nama) {
        Nomor = nomor;
        Nama = nama;
    }

    public NomorItem(String nomor, String nama, String detail) {
        Nomor = nomor;
        Nama = nama;
        Detail = detail;
    }

    @Override
    public int getType() {
        return R.id.nomor_item;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.nomor_item;
    }

    @Override
    public void bindView(ViewHolder holder, List payloads) {
        super.bindView(holder, payloads);

        Context ctx = holder.itemView.getContext();

        holder.txt_nomor.setText(Nomor);

        if(Nama.isEmpty()){
            holder.txt_title.setVisibility(View.GONE);
        }else{
            holder.txt_title.setVisibility(View.VISIBLE);
            holder.txt_title.setText(Nama);
        }

        if(Detail == null){
            holder.txt_detail.setVisibility(View.GONE);
        }else{
            holder.txt_detail.setVisibility(View.VISIBLE);
            holder.txt_detail.setText(Detail);
        }

    }
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }
    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        protected View view;


        @BindView(R.id.txt_nomor)
        protected TextView txt_nomor;

        @BindView(R.id.txt_title)
        protected TextView txt_title;

        @BindView(R.id.txt_detail)
        protected TextView txt_detail;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
            this.view = itemView;

        }
    }
}
