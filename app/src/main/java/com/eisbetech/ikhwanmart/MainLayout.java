package com.eisbetech.ikhwanmart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.fragment.AddFavoriteFragment;
import com.eisbetech.ikhwanmart.fragment.BeliVoucherFragment;
import com.eisbetech.ikhwanmart.fragment.BeliVoucherGameFragment;
import com.eisbetech.ikhwanmart.fragment.ChangePasswordFragment;
import com.eisbetech.ikhwanmart.fragment.CheckoutFragment;
import com.eisbetech.ikhwanmart.fragment.DetailFragment;
import com.eisbetech.ikhwanmart.fragment.FlightFragment;
import com.eisbetech.ikhwanmart.fragment.FragmentAfiliasiMember;
import com.eisbetech.ikhwanmart.fragment.FragmentDaftarNo;
import com.eisbetech.ikhwanmart.fragment.FragmentDeposit;
import com.eisbetech.ikhwanmart.fragment.FragmentEditHandphone;
import com.eisbetech.ikhwanmart.fragment.FragmentEditProfile;
import com.eisbetech.ikhwanmart.fragment.FragmentForgotPassword;
import com.eisbetech.ikhwanmart.fragment.FragmentMedia;
import com.eisbetech.ikhwanmart.fragment.FragmentPrint;
import com.eisbetech.ikhwanmart.fragment.FragmentPrinterListNew;
import com.eisbetech.ikhwanmart.fragment.FragmentRedeemVoucher;
import com.eisbetech.ikhwanmart.fragment.FragmentSupport;
import com.eisbetech.ikhwanmart.fragment.FragmentTambahDownline;
import com.eisbetech.ikhwanmart.fragment.FragmentTransferSaldo;
import com.eisbetech.ikhwanmart.fragment.HargaFragment;
import com.eisbetech.ikhwanmart.fragment.InputPhoneNumber;
import com.eisbetech.ikhwanmart.fragment.MessageFragment;
import com.eisbetech.ikhwanmart.fragment.ProfileFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanInputFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanReminderFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanReminderList;
import com.eisbetech.ikhwanmart.fragment.TrainFragment;
import com.eisbetech.ikhwanmart.fragment.VerifyPhoneNumber;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatNotificationConfig;
import com.freshchat.consumer.sdk.FreshchatUser;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khadafi on 9/7/2016.
 *
 */
public class MainLayout extends AppCompatActivity {
    public static int NOMOR_ACTIVIT_OK = 10;
    public static int NOMOR_ACTIVIT_CANCEL = 11;

    private LayoutInflater mInflater;
    private boolean mAnimating;

    @BindView(R.id.root)
    ViewGroup mRootView;

    @BindView(R.id.root_container)
    View mRootContainer;
    private KillReceiver clearActivityStack;
    Fragment fr = null;
    private String action ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        mInflater = LayoutInflater.from(this);

        ButterKnife.bind(this);
        Intent intent = getIntent();
        action = intent.getAction();

        String title = intent.getStringExtra("title");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(S.buy_voucher);

            if(title != null){
                actionBar.setTitle(title);
            }
        }
        fr = new BeliVoucherFragment();
        if (action != null && actionBar != null) {
            switch (action) {
                case S.action_checkout:
                    fr = new CheckoutFragment();
                    actionBar.setTitle(S.checkout);
                    break;
                case S.action_detail:
                    fr = new DetailFragment();
                    actionBar.setTitle(S.transaction_detail);
                    break;
                case S.action_bill:
                    fr = new TagihanFragment();
                    actionBar.setTitle(S.bill);
                    if(title != null){
                        actionBar.setTitle(title);
                    }
                    break;
                case S.action_daftar:
                    fr = new FragmentDaftarNo();
                    actionBar.setTitle(S.transaction_number_history);
                    if(intent.getStringExtra("type")!= null && intent.getStringExtra("type").equals("favorite")){
                        actionBar.setTitle(S.favorite_number_title);
                    }
                    break;
                case S.action_message:
                    fr = new MessageFragment();
                    actionBar.setTitle(S.message_history);
                    break;
                case S.action_favorite:
                    fr = new MessageFragment();
                    actionBar.setTitle(S.favorite_transaction);
                    break;
                case S.action_add_favorite:
                    fr = new AddFavoriteFragment();
                    actionBar.setTitle(S.add_favorite);
                    break;
                case S.action_support:
                    fr = new FragmentSupport();
                    actionBar.setTitle(S.support);
                    break;
                case S.action_deposit:
                    fr = new FragmentDeposit();
                    actionBar.setTitle(S.deposit_e_wallet);
                    break;
                case S.action_phone_number_input:
                    fr = new InputPhoneNumber();
                    actionBar.setTitle(S.enter_mobile_number);
                    break;
                case S.action_phone_verification:
                    fr = new VerifyPhoneNumber();
                    actionBar.setTitle(S.verification);
                    break;
                case S.action_profile:
                    fr = new ProfileFragment();
                    actionBar.setTitle(S.profile);
                    break;
                case S.action_change_password:
                    fr = new ChangePasswordFragment();
                    actionBar.setTitle(S.change_password);
                    break;
                case S.action_voucher_game:
                    fr = new BeliVoucherGameFragment();
                    actionBar.setTitle(S.voucher_game);
                    break;
                case S.action_bills_reminder:
                    fr = new TagihanReminderFragment();
                    actionBar.setTitle(S.bills_reminder);
                    break;
                case S.action_bills_reminder_list:
                    fr = new TagihanReminderList();
                    actionBar.setTitle(S.bills_reminder_list);
                    break;
                case S.action_media:
                    fr = new FragmentMedia();
                    actionBar.setTitle(intent.getStringExtra(K.title));
                    break;
                case S.action_price_check:
                    fr = new HargaFragment();
                    actionBar.setTitle(S.price_list);
                    break;
                case S.action_transfer_saldo:
                    fr = new FragmentTransferSaldo();
                    actionBar.setTitle(S.transfer_saldo);
                    break;
                case S.action_redeem_voucher:
                    fr = new FragmentRedeemVoucher();
                    actionBar.setTitle(S.redeem_voucher);
                    break;
                case S.action_bill_input:
                    fr = new TagihanInputFragment();
                    actionBar.setTitle(S.enter_invoice_amount);
                    break;
                case S.action_printer_list:
                    fr = new FragmentPrinterListNew();
                    actionBar.setTitle(S.select_printer);
                    break;
                case S.action_print_trx:
                    fr = new FragmentPrint();
                    actionBar.setTitle(S.print_receipt);
                    break;
                case S.action_add_affiliate:
                    fr = new FragmentTambahDownline();
                    actionBar.setTitle(S.add_affiliate);
                    break;
                case S.action_edit_no_hp:
                    fr = new FragmentEditHandphone();
                    actionBar.setTitle(S.edit_mobile_number);
                    break;
                case S.action_edit_profile:
                    fr = new FragmentEditProfile();
                    actionBar.setTitle(S.edit_profile);
                    break;
                case S.action_member_affiliation:
                    fr = new FragmentAfiliasiMember();
                    actionBar.setTitle(S.affiliate);
                    break;
                case S.action_reset_password:
                    fr = new FragmentForgotPassword();
                    actionBar.setTitle(S.forgot_password);
                    break;
                case S.action_voucher_tv:
                    fr = new BeliVoucherGameFragment();
                    actionBar.setTitle(S.voucher_tv);
                    break;
                case S.action_voucher_other:
                    fr = new BeliVoucherGameFragment();
                    actionBar.setTitle(S.voucher_other);
                    break;
                case S.action_flight:
                    fr = new FlightFragment();
                    actionBar.setTitle(S.product_flight);
                    break;
                case S.action_train:
                    fr = new TrainFragment();
                    actionBar.setTitle(S.train_ticket);
                    break;
            }
        }
        clearActivityStack = new KillReceiver();
        registerReceiver(clearActivityStack, IntentFilter.create(S.clear_stack_activity, S.text_plain));

        action = intent.getStringExtra(K.action);

        Bundle bundle = intent.getExtras();
        fr.setArguments(bundle);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fr);
        fragmentTransaction.commit();

        FreshchatConfig hConfig = new FreshchatConfig(getString(R.string.hotline_id),
                getString(R.string.hotline_key));
        hConfig.setCameraCaptureEnabled(true);
        hConfig.setGallerySelectionEnabled(true);

        Freshchat.getInstance(getApplicationContext()).init(hConfig);

        FreshchatNotificationConfig notificationConfig = new FreshchatNotificationConfig()
                .setNotificationSoundEnabled(true)
                .launchActivityOnFinish(HomeActivity.class.getName())
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        Freshchat.getInstance(getApplicationContext()).setNotificationConfig(notificationConfig);

        //Get the user object for the current installation
        FreshchatUser hlUser = Freshchat.getInstance(getApplicationContext()).getUser();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(clearActivityStack);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

    private final class KillReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    public void open_hotline() {
        Freshchat.showConversations(getApplicationContext());
    }
}
