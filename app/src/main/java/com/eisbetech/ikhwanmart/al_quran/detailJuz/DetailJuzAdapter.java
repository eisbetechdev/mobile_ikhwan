package com.eisbetech.ikhwanmart.al_quran.detailJuz;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.AyahJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.SurahJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.AyahTranslateJuz;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailJuzAdapter extends RecyclerView.Adapter<DetailJuzAdapter.MyViewHolder> {

    private List<SurahJuz> detailArabics;
    private List<AyahJuz> detailJuzArabic;
    private List<AyahTranslateJuz> ayahTranslateJuzs;
    private Context context;

    public DetailJuzAdapter(Context context, List<SurahJuz> detailArabics, List<AyahJuz> detailJuzArabic, List<AyahTranslateJuz> ayahTranslateJuzs) {
        this.context = context;
        this.detailArabics = detailArabics;
        this.detailJuzArabic = detailJuzArabic;
        this.ayahTranslateJuzs = ayahTranslateJuzs;
    }

    public void addItem(SurahJuz detailArabic) {
        this.detailArabics.add(detailArabic);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_juz, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        System.out.println("ini size detail 1 " + this.detailJuzArabic.size());

        holder.tvTitle.setText(detailArabics.get(position).getEnglishName());
        holder.tvTranslate.setText(detailArabics.get(position).getEnglishNameTranslation());
        holder.tvDetail.setText(detailArabics.get(position).getNumberOfAyahs() + " Ayat | " + detailArabics.get(position).getRevelationType());

        List<AyahJuz> ayahList1 = new ArrayList<>();
        List<AyahTranslateJuz> ayahTranslateJuzs1 = new ArrayList<>();
        List<Boolean> isBismillah = new ArrayList<>();
        ;

        if (detailArabics.get(position).getNumber() == 1 || detailArabics.get(position).getNumber() == 9) {
            holder.rvBismillah.setVisibility(View.GONE);
        } else {
            holder.rvBismillah.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < detailJuzArabic.size(); i++) {
            if (detailJuzArabic.get(i).getAyahSurahJuz().getEnglishName().equals(detailArabics.get(position).getEnglishName())) {
                ayahList1.add(detailJuzArabic.get(i));
                ayahTranslateJuzs1.add(ayahTranslateJuzs.get(i));
            }
        }


        for (int i = 0; i < ayahList1.size(); i++) {
            if (i == 0) {
                if (detailArabics.get(position).getNumber() == 1 || detailArabics.get(position).getNumber() == 9) {
                    isBismillah.add(false);
                } else {
                    isBismillah.add(true);
                }
            } else {
                isBismillah.add(true);
            }
        }

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recycleDetailSurah.setLayoutManager(horizontalLayoutManagaer);
        holder.recycleDetailSurah.setNestedScrollingEnabled(false);
        DetailJuzSuratAdapter adapter = new DetailJuzSuratAdapter(context, ayahList1, ayahTranslateJuzs1, isBismillah);
        holder.recycleDetailSurah.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return detailArabics.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_translate)
        TextView tvTranslate;
        @BindView(R.id.tv_detail)
        TextView tvDetail;
        @BindView(R.id.recycle_detail_surah)
        RecyclerView recycleDetailSurah;
        @BindView(R.id.rv_bismillah)
        RelativeLayout rvBismillah;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
