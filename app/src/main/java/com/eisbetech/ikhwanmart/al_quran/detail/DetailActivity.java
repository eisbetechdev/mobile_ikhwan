package com.eisbetech.ikhwanmart.al_quran.detail;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.detailArabic.Ayah;
import com.eisbetech.ikhwanmart.data.model.detailArabic.DetailArabic;
import com.eisbetech.ikhwanmart.data.model.detailTranslate.DetailTranslate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    @BindView(R.id.recycle_detail_surah)
    RecyclerView recycleDetailSurah;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_translate)
    TextView tvTranslate;
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @BindView(R.id.rv_bismillah)
    RelativeLayout rvBismillah;

    private DetailAdapter adapter;

    private DetailContract.UserActionListener mActionListener;
    protected ProgressDialog progressDialog;

    DetailArabic detailArabic;
    DetailTranslate detailTranslate;

    List<Ayah> ayahArabic;
    List<com.eisbetech.ikhwanmart.data.model.detailTranslate.Ayah> ayahTranslate;
    List<Boolean> isBismillah;

    private String no_surat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        no_surat = getIntent().getStringExtra("no_surat");

        progressDialog = new ProgressDialog(this);
        mActionListener = new DetailPresenter(getApplicationContext(), this);
        progressDialog.setCancelable(false);

        init();

        mActionListener.getDetailArabic(no_surat);
    }

    private void init() {
        ayahArabic = new ArrayList<>();
        ayahTranslate = new ArrayList<>();
        isBismillah = new ArrayList<>();

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleDetailSurah.setLayoutManager(horizontalLayoutManagaer);
        recycleDetailSurah.setNestedScrollingEnabled(false);
        adapter = new DetailAdapter(getApplicationContext(), ayahArabic, ayahTranslate, isBismillah);
        recycleDetailSurah.setAdapter(adapter);
    }

    @Override
    public void showProgressDialog(boolean show) {
        if (show) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void setupSurah(DetailArabic detailArabics) {
        tvDetail.setText(detailArabics.getNumberOfAyahs() + " Ayat | " + detailArabics.getRevelationType());
        tvTitle.setText(detailArabics.getEnglishName());
        tvTranslate.setText(detailArabics.getEnglishNameTranslation());

        getSupportActionBar().setTitle(detailArabics.getEnglishName());

        if (detailArabics.getNumber() == 1 || detailArabics.getNumber() == 9) {
            rvBismillah.setVisibility(View.GONE);
        }else{
            rvBismillah.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < detailArabics.getAyahs().size(); i++) {
            if (i == 0) {
                if (detailArabics.getNumber() == 1 || detailArabics.getNumber() == 9) {
                    isBismillah.add(false);
                } else {
                    isBismillah.add(true);
                }
            } else {
                isBismillah.add(true);
            }
        }


        ayahArabic.addAll(detailArabics.getAyahs());

        mActionListener.getDetailTranslate(no_surat);

        if (detailArabics.getAyahs().size() > 0) {

//            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setupSurahTranslate(DetailTranslate detailTranslate) {
        System.out.println("jumlah detail 4 :" + detailTranslate.getAyahs().size());
        if (detailTranslate.getAyahs().size() > 0) {
            ayahTranslate.addAll(detailTranslate.getAyahs());
            System.out.println("jumlah detail : " + ayahTranslate.size());
            System.out.println("jumlah detail 2 : " + ayahArabic.size());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDialog(String message) {
        new MaterialDialog.Builder(this)
                .content(message)
                .positiveText("OK")
                .show();
    }

}
