package com.eisbetech.ikhwanmart.al_quran;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.al_quran.juz.JuzFragment;
import com.eisbetech.ikhwanmart.al_quran.surat.SuratFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAlquranActivity extends AppCompatActivity {

    @BindView(R.id.tab_main)
    TabLayout tabMain;
    @BindView(R.id.vpager_pesan)
    ViewPager vpagerPesan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Baca Al-Qur'an");
        getSupportActionBar().setElevation(0);

//        disableActionbarShadow();

        setupViewPager(vpagerPesan);
        tabMain.setupWithViewPager(vpagerPesan);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new SuratFragment(), "Surat");
        adapter.addFragment(new JuzFragment(), "Juz");
        viewPager.setAdapter(adapter);
    }

    private void disableActionbarShadow() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            View v = findViewById(android.R.id.content);
            if (v instanceof FrameLayout) {
                ((FrameLayout) v).setForeground(null);
            }
        }else {
            View v = ((ViewGroup)getWindow().getDecorView()).getChildAt(0);
            v.setWillNotDraw(true);
        }
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
