package com.eisbetech.ikhwanmart.al_quran.juz;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.al_quran.detailJuz.DetailJuzActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JuzAdapter extends RecyclerView.Adapter<JuzAdapter.MyViewHolder> {

    private List<String> JadwalJuz;
    private Context context;
    private Activity activity;

    public JuzAdapter(Context context, List<String> JadwalJuz, Activity activity) {
        this.context = context;
        this.JadwalJuz = JadwalJuz;
        this.activity = activity;
    }

    public void addItem(String device) {
        this.JadwalJuz.add(device);
    }

    public List<String> getAll() {
        return this.JadwalJuz;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_juz, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvNumber.setText(JadwalJuz.get(position));
        holder.tvJuz.setText("Juz " + JadwalJuz.get(position));

        holder.rvJuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("no_surat",JadwalJuz.get(position));
                Helper.newIntent(activity, DetailJuzActivity.class, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return JadwalJuz.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.rv_number)
        RelativeLayout rvNumber;
        @BindView(R.id.tv_juz)
        TextView tvJuz;
        @BindView(R.id.rv_juz)
        RelativeLayout rvJuz;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
