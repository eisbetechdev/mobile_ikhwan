package com.eisbetech.ikhwanmart.al_quran.detailJuz;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.AyahJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.AyahTranslateJuz;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailJuzSuratAdapter extends RecyclerView.Adapter<DetailJuzSuratAdapter.MyViewHolder> {

    private List<AyahJuz> detailArabics;
    private List<AyahTranslateJuz> detailTranslates;
    private Context context;
    private List<Boolean> isBismillah;

    public DetailJuzSuratAdapter(Context context, List<AyahJuz> detailArabics, List<AyahTranslateJuz> ayahTranslateJuzList, List<Boolean> isBismillah) {
        this.context = context;
        this.detailArabics = detailArabics;
        this.detailTranslates = ayahTranslateJuzList;
        this.isBismillah = isBismillah;
    }

    public void addItem(AyahJuz detailArabic, AyahTranslateJuz detailTranslate) {
        this.detailTranslates.add(detailTranslate);
        this.detailArabics.add(detailArabic);
    }

    @Override
    public DetailJuzSuratAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail, parent, false);

        return new DetailJuzSuratAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailJuzSuratAdapter.MyViewHolder holder, final int position) {
        AyahTranslateJuz detailTranslate = detailTranslates.get(position);
        AyahJuz detailArabic = detailArabics.get(position);

        holder.tvDetailNumber.setText(Integer.toString(detailArabics.get(position).getNumberInSurah()));
        holder.tvDetailTranslate.setText(detailTranslate.getText());
        holder.tvDetailArab.setText(detailArabic.getText());

        if(detailArabic.getNumberInSurah() == 1 && isBismillah.get(position)) {
            holder.tvDetailArab.setText(detailArabic.getText().substring(39));
        } else{
            holder.tvDetailArab.setText(detailArabic.getText());
        }

        if(position % 2 == 0){
            holder.lnDetailSurat.setBackgroundColor(context.getResources().getColor(R.color.background_color));
        }else{
            holder.lnDetailSurat.setBackgroundColor(context.getResources().getColor(R.color.yellow_100));
        }
    }

    @Override
    public int getItemCount() {
        return detailArabics.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_detail_number)
        TextView tvDetailNumber;
        @BindView(R.id.tv_detail_arab)
        TextView tvDetailArab;
        @BindView(R.id.tv_detail_translate)
        TextView tvDetailTranslate;
        @BindView(R.id.ln_detail_surat)
        LinearLayout lnDetailSurat;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
