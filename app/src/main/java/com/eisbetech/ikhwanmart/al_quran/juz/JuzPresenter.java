package com.eisbetech.ikhwanmart.al_quran.juz;

import android.content.Context;


import com.eisbetech.ikhwanmart.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

public class JuzPresenter extends BasePresenter implements JuzContract.UserActionListener {
    private final Context context;
    private final JuzContract.View mView;

    public JuzPresenter(Context context, JuzContract.View mView) {
        this.context = context;
        this.mView = mView;
    }

    @Override
    public void getJuz() {
        mView.showProgressDialog(true);

        List<String> juz = new ArrayList<>();

        for(int i=1; i<=30; i++){
            juz.add(Integer.toString(i));
        }

        mView.showProgressDialog(false);
        mView.setupJuz(juz);
    }
}
