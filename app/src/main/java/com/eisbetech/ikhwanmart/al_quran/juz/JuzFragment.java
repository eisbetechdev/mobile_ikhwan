package com.eisbetech.ikhwanmart.al_quran.juz;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.eisbetech.ikhwanmart.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class JuzFragment extends Fragment implements JuzContract.View{


    @BindView(R.id.recycle_juz)
    RecyclerView recycleJuz;
    Unbinder unbinder;

    private JuzAdapter adapter;

    private JuzContract.UserActionListener mActionListener;
    protected ProgressDialog progressDialog;

    List<String> juzList;
    
    public JuzFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_juz, container, false);
        unbinder = ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getActivity());
        mActionListener = new JuzPresenter(getContext(), this);
        progressDialog.setCancelable(false);

        init();

        mActionListener.getJuz();

        return view;
    }

    private void init(){
        juzList = new ArrayList<>();
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycleJuz.setLayoutManager(horizontalLayoutManagaer);
        adapter = new JuzAdapter(getContext(), juzList, getActivity());
        recycleJuz.setAdapter(adapter);
    }

    @Override
    public void showProgressDialog(boolean show) {
        if (show) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showDialog(String message) {
        new MaterialDialog.Builder(getContext())
                .content(message)
                .positiveText("OK")
                .show();
    }

    @Override
    public void setupJuz(List<String> data) {
        System.out.println("ini data juz :"+data.get(1));
        if(data.size() > 0){
            juzList.addAll(data);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
