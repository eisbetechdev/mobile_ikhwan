package com.eisbetech.ikhwanmart.al_quran.detail;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.detailArabic.Ayah;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.MyViewHolder> {

    private List<Ayah> detailArabics;
    private List<com.eisbetech.ikhwanmart.data.model.detailTranslate.Ayah> detailTranslates;
    private Context context;
    private List<Boolean> isBismillah;

    public DetailAdapter(Context context, List<Ayah> detailArabics, List<com.eisbetech.ikhwanmart.data.model.detailTranslate.Ayah> detailTranslates, List<Boolean> isBismillah) {
        this.context = context;
        this.detailArabics = detailArabics;
        this.detailTranslates = detailTranslates;
        this.isBismillah = isBismillah;
    }

    public void addItem(Ayah detailArabic, com.eisbetech.ikhwanmart.data.model.detailTranslate.Ayah detailTranslate) {
        this.detailTranslates.add(detailTranslate);
        this.detailArabics.add(detailArabic);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        com.eisbetech.ikhwanmart.data.model.detailTranslate.Ayah detailTranslate = detailTranslates.get(position);
        Ayah detailArabic = detailArabics.get(position);

        holder.tvDetailNumber.setText(Integer.toString(detailTranslate.getNumberInSurah()));
        holder.tvDetailTranslate.setText(detailTranslate.getText());

        if(detailArabic.getNumberInSurah() == 1 && isBismillah.get(position)) {
            holder.tvDetailArab.setText(detailArabic.getText().substring(39));
        } else{
            holder.tvDetailArab.setText(detailArabic.getText());
        }

        if(position % 2 == 0){
            holder.lnDetailSurat.setBackgroundColor(context.getResources().getColor(R.color.background_color));
        }else{
            holder.lnDetailSurat.setBackgroundColor(context.getResources().getColor(R.color.yellow_100));
        }
    }

    @Override
    public int getItemCount() {
        return detailArabics.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_detail_number)
        TextView tvDetailNumber;
        @BindView(R.id.tv_detail_arab)
        TextView tvDetailArab;
        @BindView(R.id.tv_detail_translate)
        TextView tvDetailTranslate;
        @BindView(R.id.ln_detail_surat)
        LinearLayout lnDetailSurat;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
