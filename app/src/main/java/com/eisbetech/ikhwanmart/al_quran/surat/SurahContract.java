package com.eisbetech.ikhwanmart.al_quran.surat;


import com.eisbetech.ikhwanmart.data.model.surah.Surah;

import java.util.List;

public class SurahContract {
    interface View{
        void showProgressDialog(boolean show);

        void setupSurah(List<Surah> data);

        void showDialog(String message);
    }

    interface UserActionListener{
        void getSurah();
    }
}
