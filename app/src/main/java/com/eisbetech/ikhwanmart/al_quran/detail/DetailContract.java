package com.eisbetech.ikhwanmart.al_quran.detail;

import com.eisbetech.ikhwanmart.data.model.detailArabic.DetailArabic;
import com.eisbetech.ikhwanmart.data.model.detailTranslate.DetailTranslate;

import java.util.List;

public class DetailContract {
    interface View{
        void showProgressDialog(boolean show);

        void setupSurah(DetailArabic detailArabics);

        void setupSurahTranslate(DetailTranslate detailTranslate);

        void showDialog(String message);
    }

    interface UserActionListener{
        void getDetailTranslate(String id);
        void getDetailArabic(String id);
    }
}
