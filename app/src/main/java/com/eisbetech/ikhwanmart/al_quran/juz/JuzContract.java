package com.eisbetech.ikhwanmart.al_quran.juz;

import java.util.List;

public class JuzContract {
    interface View{
        void showProgressDialog(boolean show);

        void setupJuz(List<String> data);

        void showDialog(String message);
    }

    interface UserActionListener{
        void getJuz();
    }
}
