package com.eisbetech.ikhwanmart.al_quran.surat;

import android.content.Context;
import android.util.Log;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.base.BasePresenter;
import com.eisbetech.ikhwanmart.data.model.surah.SurahResponse;
import com.eisbetech.ikhwanmart.data.service.ApiClient;
import com.eisbetech.ikhwanmart.data.service.modul.SurahService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurahPresenter extends BasePresenter implements SurahContract.UserActionListener {
    private final Context context;
    private final SurahContract.View mView;
    private final SurahService restClient;

    public SurahPresenter(Context context, SurahContract.View mView) {
        this.context = context;
        this.mView = mView;
        this.restClient = ApiClient.getClient().create(SurahService.class);
    }

    @Override
    public void getSurah() {
        mView.showProgressDialog(true);

        Call<SurahResponse> call = restClient.getSurah();
        call.enqueue(new Callback<SurahResponse>() {
            @Override
            public void onResponse(Call<SurahResponse> call, Response<SurahResponse> response) {
                mView.showProgressDialog(false);

                if(response.isSuccessful()) {
                    SurahResponse resource = response.body();
                    if(response.code()==200) {
                        if(resource.getCode() ==200){
                            if (resource.getData().size() > 0){
                                mView.setupSurah(resource.getData());
                            }else{
                                Helper.showToast(context, "No Surah Found");
                            }
                        }else{
                            showDialog(context, resource.getStatus());
                        }
                    }else if(response.code()==500){
                        Helper.showToast(context, context.getString(R.string.error_network_body));
                    }else{
                        showDialog(context, response.code()+", "+response.errorBody());
                    }
                }else{
                    Log.e("Error Code", String.valueOf(response.code()));
                    Log.e("Error Body", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<SurahResponse> call, Throwable t) {
                System.out.println("gagal");
                mView.showProgressDialog(false);
                call.cancel();
            }
        });
    }
}
