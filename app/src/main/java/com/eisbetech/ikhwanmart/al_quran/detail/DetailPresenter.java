package com.eisbetech.ikhwanmart.al_quran.detail;

import android.content.Context;
import android.util.Log;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.base.BasePresenter;
import com.eisbetech.ikhwanmart.data.model.detailArabic.DetailArabicResponse;
import com.eisbetech.ikhwanmart.data.model.detailTranslate.DetailTranslateResponse;
import com.eisbetech.ikhwanmart.data.service.ApiClient;
import com.eisbetech.ikhwanmart.data.service.modul.SurahService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPresenter extends BasePresenter implements DetailContract.UserActionListener {
    private final Context context;
    private final DetailContract.View mView;
    private final SurahService restClient;

    public DetailPresenter(Context context, DetailContract.View mView) {
        this.context = context;
        this.mView = mView;
        this.restClient = ApiClient.getClient().create(SurahService.class);
    }

    @Override
    public void getDetailTranslate(final String id) {
        mView.showProgressDialog(true);

        Call<DetailTranslateResponse> call = restClient.getDetailTranslate(id);
        call.enqueue(new Callback<DetailTranslateResponse>() {
            @Override
            public void onResponse(Call<DetailTranslateResponse> call, Response<DetailTranslateResponse> response) {
                mView.showProgressDialog(false);

                if(response.isSuccessful()) {
                    DetailTranslateResponse resource = response.body();
                    if(response.code()==200) {
                        System.out.println("ini masuk jumlah1");
                        if(resource.getCode() ==200){
                            if (resource.getData().getAyahs().size() > 0){
                                System.out.println("ini masuk jumlah");
                                mView.setupSurahTranslate(resource.getData());
                            }else{
                                Helper.showToast(context, "No Detail Found");
                            }
                        }else{
                            showDialog(context, resource.getStatus());
                        }
                    }else if(response.code()==500){
                        Helper.showToast(context, context.getString(R.string.error_network_body));
                    }else{
                        showDialog(context, response.code()+", "+response.errorBody());
                    }
                }else{
                    Log.e("Error Code", String.valueOf(response.code()));
                    Log.e("Error Body", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DetailTranslateResponse> call, Throwable t) {
                System.out.println("gagal");
                mView.showProgressDialog(false);
                call.cancel();
            }
        });
    }

    @Override
    public void getDetailArabic(final String id) {
        mView.showProgressDialog(true);

        Call<DetailArabicResponse> call = restClient.getDetailArabic(id);
        call.enqueue(new Callback<DetailArabicResponse>() {
            @Override
            public void onResponse(Call<DetailArabicResponse> call, Response<DetailArabicResponse> response) {
                mView.showProgressDialog(false);

                if(response.isSuccessful()) {
                    DetailArabicResponse resource = response.body();
                    if(response.code()==200) {
                        if(resource.getCode() ==200){
                            if (resource.getDetailArabic().getAyahs().size() > 0){
                                mView.setupSurah(resource.getDetailArabic());
                            }else{
                                Helper.showToast(context, "No Detail Found");
                            }
                        }else{
                            showDialog(context, resource.getStatus());
                        }
                    }else if(response.code()==500){
                        Helper.showToast(context, context.getString(R.string.error_network_body));
                    }else{
                        showDialog(context, response.code()+", "+response.errorBody());
                    }
                }else{
                    Log.e("Error Code", String.valueOf(response.code()));
                    Log.e("Error Body", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DetailArabicResponse> call, Throwable t) {
                System.out.println("gagal");
                mView.showProgressDialog(false);
                call.cancel();
            }
        });
    }
}
