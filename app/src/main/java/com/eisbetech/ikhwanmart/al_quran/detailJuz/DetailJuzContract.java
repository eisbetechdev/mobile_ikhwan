package com.eisbetech.ikhwanmart.al_quran.detailJuz;

import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.DetailJuzArabic;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.DetailJuzTranslate;

public class DetailJuzContract {
    interface View{
        void showProgressDialog(boolean show);

        void setupSurah(DetailJuzArabic detailJuzArabics, DetailJuzTranslate detailJuzTranslate);

        void showDialog(String message);
    }

    interface UserActionListener{
        void getDetailTranslate(String id, DetailJuzArabic detailJuzArabic);
        void getDetailArabic(String id);
    }
}
