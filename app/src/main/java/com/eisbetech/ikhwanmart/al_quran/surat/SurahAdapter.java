package com.eisbetech.ikhwanmart.al_quran.surat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.al_quran.detail.DetailActivity;
import com.eisbetech.ikhwanmart.data.model.surah.Surah;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SurahAdapter extends RecyclerView.Adapter<SurahAdapter.MyViewHolder> {

    private List<Surah> JadwalSurah;
    private Context context;
    private Activity activity;

    public SurahAdapter(Context context, List<Surah> JadwalSurah, Activity activity) {
        this.context = context;
        this.JadwalSurah = JadwalSurah;
        this.activity = activity;
    }

    public void addItem(Surah device) {
        this.JadwalSurah.add(device);
    }

    public List<Surah> getAll() {
        return this.JadwalSurah;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_surat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Surah surah = JadwalSurah.get(position);

        holder.tvNumber.setText(surah.getNumber().toString());
        holder.tvNamaSurah.setText(surah.getEnglishName());
        holder.tvDetail.setText(surah.getNumberOfAyahs()+" ayat | "+surah.getEnglishNameTranslation());
        holder.tvArab.setText(surah.getName());

        holder.rvSurah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("no_surat", Integer.toString(surah.getNumber()));
                Helper.newIntent(activity, DetailActivity.class, bundle);
            }
        });


    }

    @Override
    public int getItemCount() {
        return JadwalSurah.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.rv_number)
        RelativeLayout rvNumber;
        @BindView(R.id.tv_nama_surah)
        TextView tvNamaSurah;
        @BindView(R.id.tv_detail)
        TextView tvDetail;
        @BindView(R.id.tv_arab)
        TextView tvArab;
        @BindView(R.id.rv_surah)
        RelativeLayout rvSurah;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
