package com.eisbetech.ikhwanmart.al_quran.detailJuz;

import android.content.Context;
import android.util.Log;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.base.BasePresenter;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.DetailJuzArabic;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.DetailJuzArabicResponse;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.DetailJuzTranslateResponse;
import com.eisbetech.ikhwanmart.data.service.ApiClient;
import com.eisbetech.ikhwanmart.data.service.modul.JuzService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailJuzPresenter extends BasePresenter implements DetailJuzContract.UserActionListener {
    private final Context context;
    private final DetailJuzContract.View mView;
    private final JuzService restClient;

    public DetailJuzPresenter(Context context, DetailJuzContract.View mView) {
        this.context = context;
        this.mView = mView;
        this.restClient = ApiClient.getClient().create(JuzService.class);
    }

    @Override
    public void getDetailTranslate(String id, final DetailJuzArabic detailJuzArabic) {
        mView.showProgressDialog(true);

        Call<DetailJuzTranslateResponse> call = restClient.getDetailJuzTranslate(id);
        call.enqueue(new Callback<DetailJuzTranslateResponse>() {
            @Override
            public void onResponse(Call<DetailJuzTranslateResponse> call, Response<DetailJuzTranslateResponse> response) {
                mView.showProgressDialog(false);
                if(response.isSuccessful()) {
                    DetailJuzTranslateResponse resource = response.body();
                    if(response.code()==200) {
                        if(resource.getCode() ==200){
                            if (resource.getData().getAyahs().size() > 0){
                                mView.setupSurah(detailJuzArabic, resource.getData());
                            }else{
                                Helper.showToast(context, "No Detail Found");
                            }
                        }else{
                            showDialog(context, resource.getStatus());
                        }
                    }else if(response.code()==500){
                        Helper.showToast(context, context.getString(R.string.error_network_body));
                    }else{
                        showDialog(context, response.code()+", "+response.errorBody());
                    }
                }else{
                    Log.e("Error Code", String.valueOf(response.code()));
                    Log.e("Error Body", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DetailJuzTranslateResponse> call, Throwable t) {
                System.out.println("gagal");
                mView.showProgressDialog(false);
                call.cancel();
            }
        });
    }

    @Override
    public void getDetailArabic(final String id) {
        mView.showProgressDialog(true);

        Call<DetailJuzArabicResponse> call = restClient.getDetailJuzArabic(id);
        call.enqueue(new Callback<DetailJuzArabicResponse>() {
            @Override
            public void onResponse(Call<DetailJuzArabicResponse> call, Response<DetailJuzArabicResponse> response) {

                if(response.isSuccessful()) {
                    DetailJuzArabicResponse resource = response.body();
                    if(response.code()==200) {
                        if(resource.getCode() ==200){
                            if (resource.getDetailJuzArabic().getAyahJuzs().size() > 0){
                                getDetailTranslate(id, resource.getDetailJuzArabic());
                            }else{
                                Helper.showToast(context, "No Detail Found");
                            }
                        }else{
                            showDialog(context, resource.getStatus());
                        }
                    }else if(response.code()==500){
                        Helper.showToast(context, context.getString(R.string.error_network_body));
                    }else{
                        showDialog(context, response.code()+", "+response.errorBody());
                    }
                }else{
                    Log.e("Error Code", String.valueOf(response.code()));
                    Log.e("Error Body", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<DetailJuzArabicResponse> call, Throwable t) {
                System.out.println("gagal");
                mView.showProgressDialog(false);
                call.cancel();
            }
        });
    }
}
