package com.eisbetech.ikhwanmart.al_quran.surat;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.surah.Surah;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuratFragment extends Fragment implements SurahContract.View {


    @BindView(R.id.recycle_surah)
    RecyclerView recycleSurah;
    Unbinder unbinder;

    private SurahAdapter adapter;

    private SurahContract.UserActionListener mActionListener;
    protected ProgressDialog progressDialog;
    
    List<Surah> surahList;
    public SuratFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_surat, container, false);
        unbinder = ButterKnife.bind(this, view);

        progressDialog = new ProgressDialog(getActivity());
        mActionListener = new SurahPresenter(getContext(), this);
        progressDialog.setCancelable(false);

        init();

        mActionListener.getSurah();

        return view;
    }
    
    private void init(){
        surahList = new ArrayList<>();
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycleSurah.setLayoutManager(horizontalLayoutManagaer);
        adapter = new SurahAdapter(getContext(), surahList, getActivity());
        recycleSurah.setAdapter(adapter);
    }
    
    @Override
    public void showProgressDialog(boolean show) {
        if (show) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showDialog(String message) {
        new MaterialDialog.Builder(getContext())
                .content(message)
                .positiveText("OK")
                .show();
    }

    @Override
    public void setupSurah(List<Surah> data) {
        if(data.size() > 0){
            surahList.addAll(data);
            adapter.notifyDataSetChanged();
        }
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
