package com.eisbetech.ikhwanmart.al_quran.detailJuz;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.AyahJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.DetailJuzArabic;
import com.eisbetech.ikhwanmart.data.model.detailJuzArabic.SurahJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.AyahTranslateJuz;
import com.eisbetech.ikhwanmart.data.model.detailJuzTranslate.DetailJuzTranslate;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailJuzActivity extends AppCompatActivity implements DetailJuzContract.View{

    @BindView(R.id.recycle_detail_juz)
    RecyclerView recycleDetailJuz;

    private DetailJuzAdapter adapter;

    private DetailJuzContract.UserActionListener mActionListener;
    protected ProgressDialog progressDialog;

    private String no_surat;

    List<SurahJuz> surahJuzList;
    List<AyahJuz> ayahJuzList;
    List<AyahTranslateJuz> ayahTranslateJuzs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_juz);
        ButterKnife.bind(this);
        no_surat = getIntent().getStringExtra("no_surat");

        progressDialog = new ProgressDialog(this);
        mActionListener = new DetailJuzPresenter(getApplicationContext(), this);
        progressDialog.setCancelable(false);

        init();

        getSupportActionBar().setTitle("Juz "+no_surat);

        mActionListener.getDetailArabic(no_surat);
    }

    private void init(){
        surahJuzList = new ArrayList<>();
        ayahJuzList = new ArrayList<>();
        ayahTranslateJuzs = new ArrayList<>();
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleDetailJuz.setLayoutManager(horizontalLayoutManagaer);
        adapter = new DetailJuzAdapter(getApplicationContext(), surahJuzList, ayahJuzList, ayahTranslateJuzs);
        recycleDetailJuz.setAdapter(adapter);
    }

    @Override
    public void showProgressDialog(boolean show) {
        if (show) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void setupSurah(DetailJuzArabic detailJuzArabics, DetailJuzTranslate detailJuzTranslate) {

        int firstKey = Integer.parseInt(detailJuzArabics.getSurahs().keySet().toArray()[0].toString());
        int sizeHashMap = detailJuzArabics.getSurahs().size();

        for(int i=firstKey; i<(firstKey+sizeHashMap); i++){
            surahJuzList.add(detailJuzArabics.getSurahs().get(Integer.toString(i)));
        }

        ayahJuzList.addAll(detailJuzArabics.getAyahJuzs());
        ayahTranslateJuzs.addAll(detailJuzTranslate.getAyahs());

        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDialog(String message) {
        new MaterialDialog.Builder(this)
                .content(message)
                .positiveText("OK")
                .show();
    }
}
