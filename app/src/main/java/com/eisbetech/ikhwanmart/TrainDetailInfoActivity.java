package com.eisbetech.ikhwanmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.LayoutKeretaItem1;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.entity.DetailList;
import com.eisbetech.ikhwanmart.entity.Train;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrainDetailInfoActivity extends AppCompatActivity {
    Train selectedJadwalBerangkat =  null;
    Train selectedJadwalPulang =  null;

    @BindView(R.id.stasiunAsalName)
    protected TextView stasiunAsalName;

    @BindView(R.id.stasiunTujuanName)
    protected TextView stasiunTujuanName;

    @BindView(R.id.stasiunAsalCode)
    protected TextView stasiunAsalCode;

    @BindView(R.id.txt_waktu_berangkat)
    protected TextView txt_waktu_berangkat;

    @BindView(R.id.txt_train_name)
    protected TextView txt_train_name;

    @BindView(R.id.train_type)
    protected TextView train_type;

    @BindView(R.id.tanggal_berangkat)
    protected TextView tanggal_berangkat;

    @BindView(R.id.txt_keterangan_waktu)
    protected TextView txt_keterangan_waktu;

    @BindView(R.id.stasiunTujuanCode)
    protected TextView stasiunTujuanCode;

    @BindView(R.id.txt_waktu_sampai)
    protected TextView txt_waktu_sampai;

    @BindView(R.id.stasiunAsalName_plg)
    protected TextView stasiunAsalName_plg;

    @BindView(R.id.stasiunTujuanName_plg)
    protected TextView stasiunTujuanName_plg;

    @BindView(R.id.stasiunAsalCode_plg)
    protected TextView stasiunAsalCode_plg;

    @BindView(R.id.txt_waktu_berangkat_plg)
    protected TextView txt_waktu_berangkat_plg;

    @BindView(R.id.txt_train_name_plg)
    protected TextView txt_train_name_plg;

    @BindView(R.id.train_type_plg)
    protected TextView train_type_plg;

    @BindView(R.id.tanggal_pulang)
    protected TextView tanggal_pulang;

    @BindView(R.id.txt_keterangan_waktu_plg)
    protected TextView txt_keterangan_waktu_plg;

    @BindView(R.id.stasiunTujuanCode_plg)
    protected TextView stasiunTujuanCode_plg;

    @BindView(R.id.txt_waktu_sampai_plg)
    protected TextView txt_waktu_sampai_plg;

    @BindView(R.id.listdetailBiaya)
    protected RecyclerView listdetailBiaya;

    @BindView(R.id.info_pulang)
    protected LinearLayout info_pulang;

    List<LayoutKeretaItem1> listBiaya;
    FastItemAdapter<LayoutKeretaItem1> fastItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.train_selected_detail_info);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         if( getSupportActionBar() != null){

            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(S.order_detail);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        fastItemAdapter = new FastItemAdapter<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        listdetailBiaya.setLayoutManager(linearLayoutManager);
        listdetailBiaya.setAdapter(fastItemAdapter);
        listdetailBiaya.setNestedScrollingEnabled(false);

        Intent intent = getIntent();

        selectedJadwalBerangkat = intent.getParcelableExtra(K.selected_departure_schedule);
        selectedJadwalPulang = intent.getParcelableExtra(K.selected_return_schedule);

        loadData();
        getDetailBiaya();
    }

    private void loadData(){
        stasiunAsalName.setText(selectedJadwalBerangkat.getStasiunAsalName());
        stasiunAsalCode.setText(selectedJadwalBerangkat.getStasiunAsalCode());
        stasiunTujuanCode.setText(selectedJadwalBerangkat.getStasiunTujuanCode());
        stasiunTujuanName.setText(selectedJadwalBerangkat.getStasiunTujuanName());
        txt_train_name.setText(selectedJadwalBerangkat.getTrain_name());
        train_type.setText(selectedJadwalBerangkat.getTrain_type());
        tanggal_berangkat.setText(selectedJadwalBerangkat.getTglBerangkat());

        String[] waktuBerangkat = selectedJadwalBerangkat.getWaktu().split("-");

        txt_waktu_berangkat.setText(waktuBerangkat[0]);
        txt_waktu_sampai.setText(waktuBerangkat[1]);
        txt_keterangan_waktu.setText(selectedJadwalBerangkat.getKeterangan_waktu());

        if (selectedJadwalPulang != null) {
            stasiunAsalName_plg.setText(selectedJadwalPulang.getStasiunAsalName());
            stasiunAsalCode_plg.setText(selectedJadwalPulang.getStasiunAsalCode());
            stasiunTujuanCode_plg.setText(selectedJadwalPulang.getStasiunTujuanCode());
            stasiunTujuanName_plg.setText(selectedJadwalPulang.getStasiunTujuanName());
            txt_train_name_plg.setText(selectedJadwalPulang.getTrain_name());
            train_type_plg.setText(selectedJadwalPulang.getTrain_type());
            tanggal_pulang.setText(selectedJadwalPulang.getTglPulang());

            String[] waktuPulang = selectedJadwalPulang.getWaktu().split("-");

            txt_waktu_berangkat_plg.setText(waktuPulang[0]);
            txt_waktu_sampai_plg.setText(waktuPulang[1]);
            txt_keterangan_waktu_plg.setText(selectedJadwalPulang.getKeterangan_waktu());
        } else {
            info_pulang.setVisibility(View.GONE);
        }
    }

    private void getDetailBiaya() {
        listBiaya = new ArrayList<>();
        int value;
        int jumlah = 0;
        String departTrainName = selectedJadwalBerangkat.getTrain_name();
        String departAdultAmount = selectedJadwalBerangkat.getJumlahDewasa();

        DetailList detailList = new DetailList();
        detailList.setKey(departTrainName + " (Dewasa) x " + departAdultAmount);
        value = Integer.parseInt(selectedJadwalBerangkat.getAdultFare()) * Integer.parseInt(
            departAdultAmount);
        detailList.setValue(Helper.format_money(Integer.toString(value)));
        LayoutKeretaItem1 layoutKeretaItem1 = new LayoutKeretaItem1(detailList);
        listBiaya.add(layoutKeretaItem1);

        jumlah += value;

        String departInfantAmount = selectedJadwalBerangkat.getJumlahBayi();
        if (Helper.isExist(departInfantAmount)) {
            detailList = new DetailList();
            detailList.setKey(departTrainName + " (Bayi) x "+ departInfantAmount);
            value = Integer.parseInt(selectedJadwalBerangkat.getChildFare()) * Integer.parseInt(
                    departInfantAmount);
            detailList.setValue(Helper.format_money(Integer.toString(value)));
            layoutKeretaItem1 = new LayoutKeretaItem1(detailList);
            listBiaya.add(layoutKeretaItem1);
            jumlah += value;
        }

        if (selectedJadwalPulang != null) {
            String returnTrainName = selectedJadwalPulang.getTrain_name();
            String returnAdultAmount = selectedJadwalPulang.getJumlahDewasa();

            detailList = new DetailList();
            detailList.setKey(returnTrainName + " (Dewasa) x "+ returnAdultAmount);
            value = Integer.parseInt(selectedJadwalPulang.getAdultFare()) * Integer.parseInt(
                    returnAdultAmount);
            detailList.setValue(Helper.format_money(Integer.toString(value)));
            layoutKeretaItem1 = new LayoutKeretaItem1(detailList);
            listBiaya.add(layoutKeretaItem1);
            jumlah += value;

            String returnInfantAmount = selectedJadwalPulang.getJumlahBayi();
            if (Helper.isExist(returnInfantAmount)) {
                detailList = new DetailList();
                detailList.setKey(returnTrainName + " (Bayi) x"+ returnInfantAmount);
                value = Integer.parseInt(selectedJadwalPulang.getChildFare()) * Integer.parseInt(
                        returnInfantAmount);
                detailList.setValue(Helper.format_money(Integer.toString(value)));
                layoutKeretaItem1 = new LayoutKeretaItem1(detailList);
                listBiaya.add(layoutKeretaItem1);
                jumlah += value;
            }
        }
        detailList = new DetailList();
        detailList.setKey("total");
        detailList.setValue(Helper.format_money(Integer.toString(jumlah)));

        layoutKeretaItem1 = new LayoutKeretaItem1(detailList);
        listBiaya.add(layoutKeretaItem1);
        fastItemAdapter.add(listBiaya);
        fastItemAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
