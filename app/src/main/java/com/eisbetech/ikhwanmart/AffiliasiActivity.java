package com.eisbetech.ikhwanmart;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.database.User;
import com.google.android.gms.plus.PlusShare;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AffiliasiActivity extends BaseActivity {

    @BindView(R.id.txt_kode)
    protected TextView txt_kode;

    @BindView(R.id.btn_facebook)
    protected FancyButton btn_facebook;


    @BindView(R.id.btn_gplus)
    protected FancyButton btn_Google;

    @BindView(R.id.btn_whatsapp)
    protected FancyButton btn_whatsapp;

    @BindView(R.id.btn_other)
    protected FancyButton btn_other;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiliasi);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Agen");
            getSupportActionBar().setElevation(0);
        }
        user = User.getUser();
        txt_kode.setText(user.kode_affiliasi);
    }

    @OnClick(R.id.btn_tambah)
    public void tambah_afiliasi() {
        Intent intent =new Intent(AffiliasiActivity.this,MainLayout.class);
        intent.setAction("tambah_afiliasi");
        startActivity(intent);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.btn_detil)
    public void detill() {
        startActivity(new Intent(AffiliasiActivity.this, AfiliasiSummaryActivity.class));
    }

    @OnClick(R.id.layout_code)
    public void copy() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("kode_affiliasi", user.kode_affiliasi);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Kode telah berhasil dicopykan ke clipboard", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_facebook)
    public void share_fb() {
        String urlToShare = "https://ikhwan.eisbetech.com/aff/"+user.kode_affiliasi;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

// See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

// As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }

        startActivity(intent);
    }

    @OnClick(R.id.btn_gplus)
    public void share_google() {
        String urlToShare = "https://ikhwan.eisbetech.com/aff/"+user.kode_affiliasi;
        // Launch the Google+ share dialog with attribution to your app.
        Intent shareIntent = new PlusShare.Builder(this)
                .setType("text/plain")
                .setText("Welcome to the Google+ platform.")
                .setContentUrl(Uri.parse("Beli pulsa, paket data, voucher Pln, Bayar tagihan sekarang lebih mudah dan nyaman dengan Ikhwan Mart, mau?\nMasukkan Kode Sponsor ini jika mendaftar lewat aplikasi mobile: "+user.kode_affiliasi+" \nDownload aplikasinya sekarang juga : "+urlToShare))
                .getIntent();

        startActivityForResult(shareIntent, 0);
    }

    @OnClick(R.id.btn_whatsapp)
    public void share_whatsapp() {
        String urlToShare = "https://ikhwan.eisbetech.com/aff/"+user.kode_affiliasi;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Beli pulsa, paket data, voucher Pln, Bayar tagihan sekarang lebih mudah dan nyaman dengan Ikhwan Mart, mau?\nMasukkan Kode Sponsor ini jika mendaftar lewat aplikasi mobile : " + user.kode_affiliasi + " \n Download aplikasinya sekarang juga : "+urlToShare);
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }

    @OnClick(R.id.btn_other)
    public void other_send () {
        String urlToShare = "https://ikhwan.eisbetech.com/aff/"+user.kode_affiliasi;
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Beli pulsa, paket data, voucher Pln, Bayar tagihan sekarang lebih mudah dan nyaman dengan Ikhwan Mart, mau?\nMasukkan Kode Sponsor ini jika mendaftar lewat aplikasi mobile : " + user.kode_affiliasi + " \n Download aplikasinya sekarang juga : "+urlToShare);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {



        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);

        Drawable icon;
        icon = new IconicsDrawable(this, GoogleMaterial.Icon.gmd_info_outline)
                .color(Color.parseColor("#ffffff")).sizeDp(20);
        menu.add(0, 2, Menu.NONE, "Info").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

//        icon = new IconicsDrawable(this, GoogleMaterial.Icon.gmd_people_outline)
//                .color(Color.parseColor("#ffffff")).sizeDp(20);
//        menu.add(0, 3, Menu.NONE, "Riwayat").setIcon(icon).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                Intent intent = new Intent(AffiliasiActivity.this, ViewWeb.class);
                intent.putExtra("title", "Tentang Afiliasi");
                intent.putExtra("url", "http://ikhwan.eisbetech.com/mobile/info-affiliate");
                startActivity(intent);
                break;
            case 3 :
                startActivity(new Intent(AffiliasiActivity.this, AfiliasiSummaryActivity.class));
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }
}
