package com.eisbetech.ikhwanmart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.service.ReferrerReceiver;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by khadafi on 8/23/2016.
 */
public class CreateAccountFragment extends Fragment {

    @BindView(R.id.txt_name)
    EditText txt_name;
    @BindView(R.id.txt_email)
    EditText txt_email;
    @BindView(R.id.txt_password)
    EditText txt_password;
    @BindView(R.id.txt_password_confirmation)
    EditText txt_password_confirmation;

    @BindView(R.id.txt_sponsor)
    EditText txt_sponsor;

    @BindView(R.id.txt_syarat)
    TextView txt_syarat;

    String syarat = "By signing up to this application, you are agree to  <a href='https://ikhwan.eisbetech.com/home/term'>our term</a> and <a href='https://ikhwan.eisbetech.com/home/privacy'>privacy</a>";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_create_account, container, false);
        ButterKnife.bind(this, root);

        Drawable img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_user_md)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_name.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_envelope)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_email.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password.setCompoundDrawables(img, null, null, null);

        img = new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_lock)
                .color(Color.parseColor("#171e3b")).sizeDp(24);
        img.setBounds(0, 0, 30, 30);  // set the image size
        txt_password_confirmation.setCompoundDrawables(img, null, null, null);

        txt_syarat.setMovementMethod(LinkMovementMethod.getInstance());
        txt_syarat.setText(Helper.fromHtml(syarat));

        SharedPreferences preferences = getContext()
                .getSharedPreferences(ReferrerReceiver.REFERRER, Context.MODE_PRIVATE);


        String utm_content = preferences.getString(ReferrerReceiver.UTM_CONTENT,"");


        if(!utm_content.equals("")){
            txt_sponsor.setVisibility(View.GONE);
            txt_sponsor.setText(utm_content);
        }

        Bundle bundle = getArguments();
        try {
            if (bundle.getString("email") != null) {
                txt_email.setText(bundle.getString("email"));
            }

            if (bundle.getString("name") != null) {
                txt_name.setText(bundle.getString("name"));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }

        return root;
    }

    @OnClick(R.id.btn_create)
    void createAccount() {
        String name = txt_name.getText().toString();
        String email = txt_email.getText().toString().trim();
        String password = txt_password.getText().toString();
        String confirm_password = txt_password_confirmation.getText().toString();

        if (name.equals("")) {
            Toast.makeText(getActivity(), "Nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (email.equals("")) {
            Toast.makeText(getActivity(), "Email tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (password.equals("")) {
            Toast.makeText(getActivity(), "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return;
        } else if (!password.equals(confirm_password)) {
            Toast.makeText(getActivity(), "Password tidak sama", Toast.LENGTH_SHORT).show();
            return;
        } else if (!Helper.isValidEmail(email)) {
            Toast.makeText(getActivity(), "Email tidak valid", Toast.LENGTH_SHORT).show();
        } else {
            register();
        }
        //  getActivity().finish();
//        Intent i = new Intent(getActivity(), MainLayout.class);
//        i.setAction("phone_number_input");
//        startActivity(i);

    }

    private void register() {
        User.ClearUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        try {
            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put("name", txt_name.getText());
            params.put("email", txt_email.getText().toString().trim());
            params.put("password", txt_password.getText());
            params.put("sponsor", txt_sponsor.getText());

            SharedPreferences preferences = getContext()
                    .getSharedPreferences(ReferrerReceiver.REFERRER, Context.MODE_PRIVATE);

            String utm_campaign = preferences.getString(ReferrerReceiver.UTM_CAMPAIGN,"");
            String utm_source = preferences.getString(ReferrerReceiver.UTM_SOURCE,"");
            String utm_medium = preferences.getString(ReferrerReceiver.UTM_MEDIUM,"");
            String utm_term = preferences.getString(ReferrerReceiver.UTM_TERM,"");
            String utm_content = preferences.getString(ReferrerReceiver.UTM_CONTENT,"");

            params.put(ReferrerReceiver.UTM_CAMPAIGN, utm_campaign);
            params.put(ReferrerReceiver.UTM_SOURCE, utm_source);
            params.put(ReferrerReceiver.UTM_MEDIUM, utm_medium);
            params.put(ReferrerReceiver.UTM_TERM, utm_term);
            params.put(ReferrerReceiver.UTM_CONTENT, utm_content);

            BisatopupApi.post("/user/register-acc", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(responseString);

                        boolean error = object.getBoolean("error");

                        if (!error) {
//                            Toast.makeText(getContext(), "Selamat datang, " + object.getJSONObject("data").getString("name"), Toast.LENGTH_LONG).show();

                            User.createUser(object.getJSONObject("data").getString("name"), object.getJSONObject("data").getString("email"),object.getString("key"));
                            Intent i = new Intent(getActivity(), MainLayout.class);
                            i.setAction("phone_number_input");
                            startActivity(i);
                        } else {
                            String message = object.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();


                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
