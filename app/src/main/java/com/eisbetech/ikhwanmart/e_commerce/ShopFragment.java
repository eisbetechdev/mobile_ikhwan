package com.eisbetech.ikhwanmart.e_commerce;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.adapter.ProdukAdapter;
import com.eisbetech.ikhwanmart.item.Kategori;
import com.eisbetech.ikhwanmart.item.Produk;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ShopFragment extends Fragment {
    @BindView(R.id.list_kategori)
    RecyclerView mListKategori;
    @BindView(R.id.list_produk)
    RecyclerView mListProduk;
    private View view;
    private Unbinder unbinder;
    private List<Produk> dataProduk;
    private ProdukAdapter mAdapterProduk;
    private List<Kategori> dataKategori;
    //private KategoriProdukAdapter mAdapterKategori;
    private FirebaseDatabase database;
    private DatabaseReference ProdukRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        unbinder = ButterKnife.bind(this, view);

        database = FirebaseDatabase.getInstance();
        ProdukRef = database.getReference("produk");

        dataKategori = new ArrayList<>();
        dataKategori.add(new Kategori(0,"Beauty"));
        dataKategori.add(new Kategori(1,"Health"));
        dataKategori.add(new Kategori(2,"Fasion"));
        dataKategori.add(new Kategori(3,"Smatrphone"));
        dataKategori.add(new Kategori(4,"Computer"));
        dataKategori.add(new Kategori(5,"Electronics"));
        dataKategori.add(new Kategori(6,"Camera"));

        dataProduk = new ArrayList<>();

        /*mAdapterKategori = new KategoriProdukAdapter(getActivity(),dataKategori);
        mListKategori.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        mListKategori.addItemDecoration(new Constant.LeftItemDecotaion(20));
        mListKategori.setAdapter(mAdapterKategori);
        mListKategori.setNestedScrollingEnabled(false);*/

        mAdapterProduk = new ProdukAdapter(getActivity(),dataProduk);
        mListProduk.setLayoutManager(new GridLayoutManager(getActivity(),2));
        mListProduk.addItemDecoration(new Constant.GridSpacingItemDecoration(2,10,true));
        mListProduk.setAdapter(mAdapterProduk);
        mListProduk.setNestedScrollingEnabled(false);

        getProduct();

        return view;
    }

    public void getProduct() {
        ProdukRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Produk produk = postSnapshot.getValue(Produk.class);
                    dataProduk.add(produk);
                }

                mAdapterProduk = new ProdukAdapter(getActivity(), dataProduk);
                mListProduk.setAdapter(mAdapterProduk);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
