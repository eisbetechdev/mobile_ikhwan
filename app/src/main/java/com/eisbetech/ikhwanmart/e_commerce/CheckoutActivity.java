package com.eisbetech.ikhwanmart.e_commerce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.item.CartItem;
import com.eisbetech.ikhwanmart.item.City.City;
import com.eisbetech.ikhwanmart.item.City.CityItem;
import com.eisbetech.ikhwanmart.item.Cost.Cost;
import com.eisbetech.ikhwanmart.item.Cost.CostsItem;
import com.eisbetech.ikhwanmart.item.Province.Province;
import com.eisbetech.ikhwanmart.item.Province.ProvinceItem;
import com.eisbetech.ikhwanmart.item.Transaksi;
import com.eisbetech.ikhwanmart.service.ApiClientCommerce;
import com.eisbetech.ikhwanmart.service.ApiInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.nama)
    EditText mNama;
    @BindView(R.id.number)
    EditText mNumber;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.province_spinner)
    SearchableSpinner mProvinceSpinner;
    @BindView(R.id.city_spinner)
    SearchableSpinner mCitySpinner;
    @BindView(R.id.kurir_spinner)
    SearchableSpinner mKurirSpinner;
    @BindView(R.id.sub_total)
    TextView mSubTotal;
    @BindView(R.id.range_cost)
    TextView mRangeCost;
    @BindView(R.id.total_all)
    TextView mTotalAll;
    @BindView(R.id.continue_button)
    Button mContinueButton;
    @BindView(R.id.layanan_kurir)
    SearchableSpinner mLayananKurir;
    @BindView(R.id.title_service)
    TextView mTitleService;
    @BindView(R.id.lama_pengiriman)
    TextView mLamaPengiriman;
    private ApiInterface client;
    private ProgressDialog progressDialog;
    private List<ProvinceItem> dataProvince;
    private List<CityItem> dataCity;
    private List<CostsItem> dataServices;
    ProvinceItem selectedProvince;
    CityItem selectedCity;
    CostsItem selectedService;
    private String selectedKurir;
    private Bundle extras;
    private ArrayList<CartItem> dataCart;
    private FirebaseDatabase database;
    private DatabaseReference TransaksiRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        database = FirebaseDatabase.getInstance();
        TransaksiRef = database.getReference("transaction");

        client = ApiClientCommerce.get(this);
        dataCart = new ArrayList<>();

        extras = getIntent().getExtras();
        if (extras!=null){
            ArrayList<CartItem> datas = extras.getParcelableArrayList(Constant.CART_KEY);
            dataCart.addAll(datas);
        }

        selectedKurir = getResources().getStringArray(R.array.ekspedisi)[0];

        dataProvince = new ArrayList<>();
        dataCity = new ArrayList<>();
        dataServices = new ArrayList<CostsItem>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Memuat...");

        getProvice();

        mProvinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedProvince = dataProvince.get(position);
                getCity(selectedProvince.getProvinceId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mCitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = dataCity.get(0);
                getCost(selectedCity.getCityId(), "500", selectedKurir);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mKurirSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedKurir = getResources().getStringArray(R.array.ekspedisi)[position];
                if (selectedCity != null) getCost(selectedCity.getCityId(), "500", selectedKurir);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLayananKurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedService = dataServices.get(position);
                mRangeCost.setText(Constant.toRupiah(selectedService.getCost().get(0).getValue()));
                mLamaPengiriman.setText("Delivery within "+selectedService.getCost().get(0).getEtd().replaceAll("HARI","")+" days");


                long total = 0;

                for (CartItem cartItem : dataCart) {
                    total += cartItem.getCart_item_produk().getHarga() * cartItem.getCart_item_qnt();
                }

                mSubTotal.setText(Constant.toRupiah(total));
                mTotalAll.setText(Constant.toRupiah(total+selectedService.getCost().get(0).getValue()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getProvice() {
        progressDialog.show();
        client.getProvice(Constant.API_KEY)
                .enqueue(new Callback<Province>() {
                    @Override
                    public void onResponse(Call<Province> call, Response<Province> response) {
                        progressDialog.dismiss();
                        dataProvince.clear();
                        List<String> provinsi = new ArrayList<>();
                        dataProvince.addAll(response.body().getRajaongkir().getResults());

                        for (ProvinceItem provinceItem : dataProvince) {
                            provinsi.add(provinceItem.getProvince());
                        }

                        ArrayAdapter<String> provinceAdapter = new ArrayAdapter<String>(CheckoutActivity.this,
                                android.R.layout.simple_spinner_item, provinsi);
                        mProvinceSpinner.setAdapter(provinceAdapter);

                        getCity(dataProvince.get(0).getProvinceId());
                    }

                    @Override
                    public void onFailure(Call<Province> call, Throwable t) {

                    }
                });
    }

    public void getCity(String id) {
        progressDialog.show();
        client.getCity(Constant.API_KEY, id)
                .enqueue(new Callback<City>() {
                    @Override
                    public void onResponse(Call<City> call, Response<City> response) {
                        progressDialog.dismiss();
                        dataCity.clear();
                        List<String> city = new ArrayList<>();
                        dataCity.addAll(response.body().getRajaongkir().getResults());

                        for (CityItem cityItem : dataCity) {
                            city.add(cityItem.getCityName());
                        }

                        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(CheckoutActivity.this,
                                android.R.layout.simple_spinner_item, city);
                        mCitySpinner.setAdapter(cityAdapter);

                        getCost(dataCity.get(0).getCityId(), "500", "jne");
                    }

                    @Override
                    public void onFailure(Call<City> call, Throwable t) {

                    }
                });
    }

    public void getCost(String id, String weight, String kurir) {
        progressDialog.show();
        client.getCost(Constant.API_KEY, "444", id, weight, kurir).enqueue(new Callback<Cost>() {
            @Override
            public void onResponse(Call<Cost> call, Response<Cost> response) {
                progressDialog.dismiss();
                dataServices.clear();
                List<String> services = new ArrayList<>();
                dataServices.addAll(response.body().getRajaongkir().getResults().get(0).getCosts());

                for (CostsItem costsItem : dataServices) {
                    services.add(costsItem.getService());
                }

                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(CheckoutActivity.this,
                        android.R.layout.simple_spinner_item, services);
                mLayananKurir.setAdapter(serviceAdapter);

                if (dataServices.size() > 0) {
                    mTitleService.setVisibility(View.VISIBLE);
                    mLayananKurir.setVisibility(View.VISIBLE);
                } else {
                    mTitleService.setVisibility(View.GONE);
                    mLayananKurir.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Cost> call, Throwable t) {

            }
        });
    }

    @OnClick({R.id.back_button, R.id.continue_button})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.continue_button:
                progressDialog.show();
                long total = 0;

                for (CartItem cartItem : dataCart) {
                    total += cartItem.getCart_item_produk().getHarga() * cartItem.getCart_item_qnt();
                }

                Transaksi transaksi = new Transaksi();
                transaksi.setTransaction_cart(dataCart);
                transaksi.setTransaction_date(System.currentTimeMillis());
                transaksi.setTransaction_id(System.currentTimeMillis());
                transaksi.setTransaction_range_cost(selectedService.getCost().get(0).getValue());
                transaksi.setTransaction_status("Progress");
                transaksi.setTransaction_total(total);

                TransaksiRef.child(transaksi.getTransaction_id()+"").setValue(transaksi).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()){
                            dataCart.clear();
                            Prefs.putString(Constant.CART_KEY,new Gson().toJson(dataCart));
                            Toast.makeText(CheckoutActivity.this,"Succesfully buy",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(CheckoutActivity.this,HomeActivity.class);;
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Toast.makeText(CheckoutActivity.this,task.getException().getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }
    }
}
