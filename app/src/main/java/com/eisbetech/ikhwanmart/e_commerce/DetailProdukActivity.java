package com.eisbetech.ikhwanmart.e_commerce;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.CarouselEffectTransformer;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.MyPagerAdapter;
import com.eisbetech.ikhwanmart.item.CartItem;
import com.eisbetech.ikhwanmart.item.Produk;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailProdukActivity extends AppCompatActivity {
    @BindView(R.id.back_button)
    ImageView mBackButton;
    @BindView(R.id.title_toolbar)
    TextView mTitleToolbar;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.image_product)
    ViewPager mImageProduct;
    @BindView(R.id.harga_product)
    TextView mHargaProduct;
    @BindView(R.id.deskripsi)
    TextView mDeskripsi;
    @BindView(R.id.button_add)
    Button mButtonAdd;
    @BindView(R.id.scroll)
    NestedScrollView mScroll;
    @BindView(R.id.stok)
    TextView mStok;
    @BindView(R.id.current_cart)
    TextView mCurrentCart;
    @BindView(R.id.shadow)
    View mShadow;
    @BindView(R.id.btn_chart)
    ImageView btnChart;
    private List<String> dataImage;
    private MyPagerAdapter mAdapterPager;
    private Bundle extras;
    private Produk dataProduk;
    private List<CartItem> dataCart;
    public static OnCartAdded listener;

    public static void setOnCartAdded(OnCartAdded onCartAdded) {
        listener = onCartAdded;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);
        ButterKnife.bind(this);

        dataImage = new ArrayList<>();

        extras = getIntent().getExtras();
        if (extras != null) {
            dataProduk = extras.getParcelable("produk");
            dataImage.addAll(dataProduk.getGambar());

            mTitleToolbar.setText(dataProduk.getNama());
            mHargaProduct.setText(Constant.toRupiah(dataProduk.getHarga()));
            mDeskripsi.setText(dataProduk.getDeskripsi());
            mStok.setText(dataProduk.getStok() + " Pcs");
        }

        mAdapterPager = new MyPagerAdapter(this, dataImage);
        mImageProduct.setClipChildren(false);
        mImageProduct.setPageMargin(20);
        mImageProduct.setOffscreenPageLimit(dataImage.size());
        mImageProduct.setPageTransformer(false, new CarouselEffectTransformer(this));
        mImageProduct.setAdapter(mAdapterPager);

        mButtonAdd.setTag(false);

        mScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    if (!(boolean) mButtonAdd.getTag()) {
                        mButtonAdd.setTag(true);
                        mButtonAdd.animate().scaleY(0).setDuration(500);
                        mButtonAdd.animate().scaleX(0).setDuration(500);
                        mButtonAdd.animate().withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mButtonAdd.setTag(false);
                            }
                        }).yBy(200).setDuration(500);
                    }
                }
                if (scrollY < oldScrollY) {
                    if (!(boolean) mButtonAdd.getTag()) {
                        mButtonAdd.setTag(true);
                        mButtonAdd.animate().scaleY(1).setDuration(500);
                        mButtonAdd.animate().scaleX(1).setDuration(500);
                        mButtonAdd.animate().withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mButtonAdd.setTag(false);
                            }
                        }).yBy(-200).setDuration(500);
                    }
                }

                if (scrollY == 0) {
                    mShadow.setVisibility(View.GONE);
                } else {
                    mShadow.setVisibility(View.VISIBLE);
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    //Log.i(TAG, "BOTTOM SCROLL");
                }
            }
        });

        dataCart = new ArrayList<>();

        String jsonCart = Prefs.getString(Constant.CART_KEY, null);
        if (jsonCart != null) {
            CartItem[] cartItems = new Gson().fromJson(jsonCart, CartItem[].class);
            dataCart.addAll(Arrays.asList(cartItems));
        }

        int total = 0;
        for (CartItem cartItem : dataCart) {
            total += cartItem.getCart_item_qnt();
//            if (cartItem.getCart_item_produk().getId() == dataProduk.getId()) {
//                mCurrentCart.setText(cartItem.getCart_item_qnt() + "");
//                break;
//            }
        }
        mCurrentCart.setText(total + " ");

    }

    public interface OnCartAdded {
        public void onCartChange(List<CartItem> cartItems);
    }

    @OnClick({R.id.back_button, R.id.button_add, R.id.btn_chart})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.button_add:
                boolean isMatch = false;

                for (CartItem cartItem : dataCart) {
                    if (cartItem.getCart_item_id() == dataProduk.getId()) {
                        cartItem.setCart_item_qnt(cartItem.getCart_item_qnt() + 1);
                        isMatch = true;
                        break;
                    }
                }

                if (!isMatch) {
                    CartItem cartItem = new CartItem();
                    cartItem.setCart_item_id(dataProduk.getId());
                    cartItem.setCart_item_produk(dataProduk);
                    cartItem.setCart_item_qnt(1);
                    dataCart.add(cartItem);
                }

                int total = 0;
                for (CartItem cartItem : dataCart) {
                    total += cartItem.getCart_item_qnt();
//                    if (cartItem.getCart_item_produk().getId() == dataProduk.getId()) {
//                        mCurrentCart.setText(cartItem.getCart_item_qnt() + "");
//                        break;
//                    }
                }
                mCurrentCart.setText(total + " ");


                Prefs.putString(Constant.CART_KEY, new Gson().toJson(dataCart));
                if (listener != null) listener.onCartChange(dataCart);

                break;
            case R.id.btn_chart:
//                replaceFragmentWithAnimationBackStag(new CartFragment(), "Keranjang");
                Helper.showToast(getApplicationContext(),"Coming Soon");
        }
    }


    public void replaceFragmentWithAnimationBackStag(android.support.v4.app.Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

}
