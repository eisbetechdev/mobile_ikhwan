package com.eisbetech.ikhwanmart.e_commerce;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.adapter.CartAdapter;
import com.eisbetech.ikhwanmart.item.CartItem;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CartFragment extends Fragment {
    @BindView(R.id.button_continue)
    Button mButtonContinue;
    @BindView(R.id.total_cart)
    TextView mTotalCart;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.list_cart)
    RecyclerView mListCart;
    @BindView(R.id.shadow)
    View mShadow;
    private ArrayList<CartItem> dataCart;
    private CartAdapter mAdapterCart;
    private View view;
    private Unbinder unbinder;

    public static OnCartAdded listener;

    public static void setOnCartAdded(OnCartAdded onCartAdded) {
        listener = onCartAdded;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_cart, container, false);
        dataCart = new ArrayList<>();
        unbinder = ButterKnife.bind(this, view);

        String jsonCart = Prefs.getString(Constant.CART_KEY, null);
        if (jsonCart != null) {
            CartItem[] cartItems = new Gson().fromJson(jsonCart, CartItem[].class);
            dataCart.addAll(Arrays.asList(cartItems));
        }

        mAdapterCart = new CartAdapter(getActivity(), dataCart, this);
        mListCart.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListCart.setAdapter(mAdapterCart);

        long total = 0;

        for (CartItem cartItem : dataCart) {
            total += cartItem.getCart_item_produk().getHarga() * cartItem.getCart_item_qnt();
        }

        mButtonContinue.setText("Pay " + Constant.toRupiah(total));
        mTotalCart.setText(dataCart.size() + "");

        mListCart.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    mShadow.setVisibility(View.VISIBLE);
                    //Log.i(TAG, "TOP SCROLL");
                } else {
                    mShadow.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    public interface OnCartAdded {
        public void onCartChange(List<CartItem> cartItems);
    }

    public void setDataChange(List<CartItem> cartItems){
        Prefs.putString(Constant.CART_KEY, new Gson().toJson(dataCart));
        if (listener != null) listener.onCartChange(dataCart);
    }

    @OnClick({R.id.button_continue})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.button_continue:
                if (dataCart.size()>0){
//                    Intent intent = new Intent(getActivity(),CheckoutActivity.class);
//                    intent.putParcelableArrayListExtra(Constant.CART_KEY,dataCart);
//                    startActivity(intent);
                    Helper.showToast(getContext(),"Coming Soon");
                } else {
                    Toast.makeText(getActivity(),"Keranjang Kosong",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
