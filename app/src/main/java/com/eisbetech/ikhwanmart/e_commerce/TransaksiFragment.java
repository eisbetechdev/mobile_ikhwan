package com.eisbetech.ikhwanmart.e_commerce;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.adapter.TransaksiAdapter;
import com.eisbetech.ikhwanmart.item.Transaksi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TransaksiFragment extends Fragment {
    @BindView(R.id.list_history)
    RecyclerView mListHistory;
    private View view;
    private Unbinder unbinder;
    private FirebaseDatabase database;
    private DatabaseReference transaksiRef;
    private List<Transaksi> dataHistory;
    private TransaksiAdapter mAdapterHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder = ButterKnife.bind(this, view);

        dataHistory = new ArrayList<>();

        database = FirebaseDatabase.getInstance();
        transaksiRef = database.getReference("transaction");

        mListHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListHistory.addItemDecoration(new Constant.TopItemDecoration(30));

        getHistory();

        return view;
    }

    public void getHistory(){
        transaksiRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Transaksi transaksi = postSnapshot.getValue(Transaksi.class);
                    dataHistory.add(transaksi);
                }

                mAdapterHistory = new TransaksiAdapter(getActivity(),dataHistory);
                mListHistory.setAdapter(mAdapterHistory);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
