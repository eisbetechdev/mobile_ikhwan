package com.eisbetech.ikhwanmart.service;

import com.eisbetech.ikhwanmart.item.City.City;
import com.eisbetech.ikhwanmart.item.Cost.Cost;
import com.eisbetech.ikhwanmart.item.Province.Province;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("province")
    Call<Province> getProvice(@Query("key") String key);

    @GET("city")
    Call<City> getCity(@Query("key") String key, @Query("province") String province);

    @POST("cost")
    @FormUrlEncoded
    Call<Cost> getCost(
            @Field("key") String key,
            @Field("origin") String origin,
            @Field("destination") String destination,
            @Field("weight") String weight,
            @Field("courier") String courier);
}
