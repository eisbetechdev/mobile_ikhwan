package com.eisbetech.ikhwanmart.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.database.Message;
import com.eisbetech.ikhwanmart.database.RealmController;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.fragment.DetailFragment;
import com.eisbetech.ikhwanmart.fragment.TagihanFragment;
import com.freshchat.consumer.sdk.Freshchat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import io.realm.Realm;

/**
 * Created by khadafi on 9/4/2016.
 */
public class MessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        Realm realm = RealmController.getInstance().getRealm();
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (Freshchat.isFreshchatNotification(remoteMessage)) {
            Freshchat.getInstance(this).handleFcmMessage(remoteMessage);
        } else {
            //Handle notifications with data payload for your app

            User user = User.getUser();
            if (user == null) {
                return;
            }
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {

                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                String operation = remoteMessage.getData().get("operation");
                String title = remoteMessage.getData().get("title");
                if (operation != null && operation.equals("tagihan")) {
                    String no_rek = remoteMessage.getData().get("no_rek");
                    ;
                    String product = remoteMessage.getData().get("product");
                    String message = remoteMessage.getData().get("message");
                    int tagihan_id = Integer.parseInt(remoteMessage.getData().get("tagihan_id"));
                    sendNotificationTagihan(message, no_rek, product, tagihan_id);
                } else if (operation != null && operation.equals("transaksi_detail")) {
                    String trans_id = remoteMessage.getData().get("trans_id");
                    boolean is_notif = Boolean.parseBoolean(remoteMessage.getData().get("show_notification"));
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.setAction(operation);
                    intent.putExtra("transaction_id", trans_id);

                    Intent i = new Intent(DetailFragment.MESSAGE_NOTIFIER).putExtra("some_msg", "I will be sent!");
                    this.sendBroadcast(i);

                    if (is_notif) {
                        sendNotification(remoteMessage.getData().get("message"), title, operation, intent);

                        try {
                            Message message = new Message();
                            message.id = message.getNextPrimaryKey(realm);
                            message.title = title;
                            message.content = remoteMessage.getData().get("message");
                            message.date = remoteMessage.getData().get("date");
                            message.is_read = false;
                            realm.beginTransaction();
                            realm.copyToRealm(message);
                            realm.commitTransaction();
                        } catch (Exception ec) {
                            ec.printStackTrace();
                        }
                    }

                } else if (operation != null && operation.equals("tagihan_cek")) {
                    Intent i = new Intent(TagihanFragment.MESSAGE_NOTIFIER).putExtra("some_msg", "I will be sent!");
                    i.putExtra("tagihan_id",remoteMessage.getData().get("tagihan_id"));
                    i.putExtra("no_rek",remoteMessage.getData().get("no_rek"));
                    i.putExtra("no_hp",remoteMessage.getData().get("no_hp"));
                    i.putExtra("product",remoteMessage.getData().get("product_name"));


                    String message = remoteMessage.getData().get("message");
                    String no_hp = remoteMessage.getData().get("no_hp");
                    title = remoteMessage.getData().get("title");
                    String product = remoteMessage.getData().get("product_name");
                    String name = remoteMessage.getData().get("name");
                    int tag_id = Integer.parseInt(remoteMessage.getData().get("tagihan_id"));
                    this.sendBroadcast(i);

                    this.sendTagihan(title,message,remoteMessage.getData().get("no_rek"),product,no_hp,tag_id,name);
                } else {
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.setAction(operation);
                    String full_message = remoteMessage.getData().get("full_message");
                    String image_url = remoteMessage.getData().get("image_url");

                    sendNotification(remoteMessage.getData().get("message"), title, operation, intent);

                    try {
                        Message message = new Message();
                        message.id = message.getNextPrimaryKey(realm);
                        message.title = title;
                        message.content = remoteMessage.getData().get("message");
                        message.date = remoteMessage.getData().get("date");
                        message.is_read = false;
                        if (full_message != null) {
                            message.full_content = full_message;
                        } if (image_url != null) {
                            message.image_url = (image_url);
                        }
                        realm.beginTransaction();
                        realm.copyToRealm(message);
                        realm.commitTransaction();
                    } catch (Exception ec) {
                        ec.printStackTrace();
                    }
                }
            }
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String title, String operation, Intent intent) {

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Spanned ttl;
        ttl = Html.fromHtml(title);


        int notificationId = 0;
        String channelId = "bisatopup-01";
        String channelName = "bisatopup notif";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId)
                .setSmallIcon(R.drawable.android_launcher)
                .setContentTitle(ttl)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                .setPriority(NotificationCompat.PRIORITY_HIGH) //must give priority to High, Max which will considered as heads-up notification
                .setSound(defaultSoundUri)
                .setTicker(ttl)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(
                        channelId, channelName, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());

        Intent intent_notif = new Intent(HomeActivity.MESSAGE_NOTIFIER);

        //put whatever data you want to send, if any
        intent_notif.putExtra("message", messageBody);
        intent_notif.putExtra("title", title);
        intent_notif.setAction(operation);
        //send broadcast
        getApplicationContext().sendBroadcast(intent_notif);
    }

    private void sendNotificationTagihan(String messageBody, String no_rek, String product, int tagihan_id) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setAction("reminder_tagihan");
        intent.putExtra("no_rek", no_rek);
        intent.putExtra("product", product);

        String channelId = "bisatopup-03";
        String channelName = "bisatopup tagihan";

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId)
                .setSmallIcon(R.drawable.android_launcher)
                .setContentTitle("Reminder Tagihan")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                .setPriority(NotificationCompat.PRIORITY_HIGH) //must give priority to High, Max which will considered as heads-up notification
                .setSound(defaultSoundUri)
                .setTicker("Reminder Tagihan")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(
                        channelId, channelName, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        notificationManager.notify(tagihan_id /* ID of notification */, notificationBuilder.build());

        Intent intent_notif = new Intent(HomeActivity.MESSAGE_NOTIFIER);

        //put whatever data you want to send, if any
        intent_notif.putExtra("message", messageBody);
        intent_notif.putExtra("title", "Reminder Tagihan");
        intent_notif.setAction("reminder_tagihan");
        intent_notif.putExtra("no_rek", no_rek);
        intent_notif.putExtra("product", product);

        //send broadcast
        getApplicationContext().sendBroadcast(intent_notif);
    }


    private void sendTagihan(String title,String messageBody, String no_rek, String product,String no_hp, int tagihan_id,String name) {
        Intent intent = new Intent(this, MainLayout.class);
        intent.setAction("tagihan");
        intent.putExtra("id", 20);
        intent.putExtra("no_rek", no_rek);
        intent.putExtra("no_hp", no_hp);
        intent.putExtra("product", product);
        intent.putExtra("product_name", name);


        String channelId = "bisatopup-02";
        String channelName = "bisatopup tagihan";

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId)
                .setSmallIcon(R.drawable.android_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                .setPriority(NotificationCompat.PRIORITY_HIGH) //must give priority to High, Max which will considered as heads-up notification
                .setSound(defaultSoundUri)
                .setTicker("Detil Tagihan")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(
                        channelId, channelName, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        notificationManager.notify(tagihan_id /* ID of notification */, notificationBuilder.build());

        Intent intent_notif = new Intent(HomeActivity.MESSAGE_NOTIFIER);

        //put whatever data you want to send, if any
        intent_notif.putExtra("message", messageBody);
        intent_notif.putExtra("title", "Reminder Tagihan");
        intent_notif.setAction("reminder_tagihan");
        intent_notif.putExtra("no_rek", no_rek);
        intent_notif.putExtra("product", product);
        intent.putExtra("product_name", name);
        //send broadcast
        getApplicationContext().sendBroadcast(intent_notif);
    }

    private void sendNotificationV2(String messageBody) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.android_launcher)
                        .setContentTitle("Bisatopup Notification")
                        .setContentText(messageBody);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, HomeActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(HomeActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }
}