package com.eisbetech.ikhwanmart.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import butterknife.Unbinder;

/**
 * Created by Rahmadewi on 4/18/2018.
 */

public abstract class BaseActivity  extends AppCompatActivity {
    public final static String DATA = "data";
    protected Context mContext;
    protected Unbinder unbinder;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        mContext = this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    protected void toast(String message, int length) {
        Toast.makeText(mContext, message, length).show();
    }

    protected void toast(String message) {
        toast(message, Toast.LENGTH_SHORT);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}
