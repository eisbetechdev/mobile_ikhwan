package com.eisbetech.ikhwanmart.base;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by Rahmadewi on 4/18/2018.
 */

public class BasePresenter {
//    public void showDialogLogout(final Context context){
//        new MaterialDialog.Builder(context)
//                .title("Peringatan")
//                .content("Akun Anda telah digunakan login di perangkat lain")
//                .positiveText("Logout").cancelable(false)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        Utils.showToast(context, "Logout~");
//                        doLogout();
//                    }
//                })
//                .show();
//    }
//
//    public void showDialogMissMatchToken(Context context, String message){
//        new MaterialDialog.Builder(context)
//                .content(message)
//                .positiveText("Logout").cancelable(false)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        doLogout();
//                    }
//                })
//                .show();
//    }

    public void doLogout(){
//        MainActivity.getInstance().doLogout();
    }

    public void showDialog(Context context, String message){
        new MaterialDialog.Builder(context)
                .title("Al-Qur'an")
                .content(message)
                .positiveText("OK")
                .show();
    }

    public void setNewToken(String newToken){
//        DataConfig.storeString(DataConfig.API_TOKEN, newToken);
    }

    public void setSmartpayToken(String token){
//        DataConfig.storeString(DataConfig.SMARTPAY_PRIMARY_TOKEN, token);
    }

}
