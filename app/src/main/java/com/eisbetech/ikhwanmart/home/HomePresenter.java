package com.eisbetech.ikhwanmart.home;

import android.content.Context;
import android.support.annotation.NonNull;

import com.eisbetech.ikhwanmart.base.BasePresenter;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.data.model.Category;
import com.eisbetech.ikhwanmart.item.Produk;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomePresenter extends BasePresenter implements HomeContract.UserActionListener {
    private final Context context;
    private final HomeContract.View mView;
    private FirebaseDatabase database;
    private DatabaseReference ProdukRef;

    public HomePresenter(Context context, HomeContract.View mView) {
        this.context = context;
        this.mView = mView;
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void getProduct() {
        mView.showProgressDialog(true);

        final List<Produk> products = new ArrayList<>();

        ProdukRef = database.getReference("produk");

        ProdukRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Produk produk = postSnapshot.getValue(Produk.class);
                    products.add(produk);

                }
                mView.showProgressDialog(false);
                mView.setupProduct(products);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    @Override
    public void getCategory() {
        mView.showProgressDialog(true);

        List<Category> categoryList = new ArrayList<>();

        categoryList.add(new Category(S.product_balance_mutation, "multifinance"));
        categoryList.add(new Category(S.product_pulse, "pulsa"));
        categoryList.add(new Category(S.product_data_package, "paket_data"));
        categoryList.add(new Category(S.product_pln_token, "token_pln"));
        categoryList.add(new Category(S.product_electricity_bill, "tagihan_listrik"));
        categoryList.add(new Category(S.product_game_voucher,"voucher_game"));
        categoryList.add(new Category(S.product_insurance_bpjs, "asuransi_bpjs"));
        categoryList.add(new Category(S.product_tv, "televisi"));

        mView.showProgressDialog(false);
        mView.setupCategory(categoryList);
    }

    @Override
    public void getCategoryNext() {
        mView.showProgressDialog(true);

        List<Category> categoryList = new ArrayList<>();

        categoryList.add(new Category(S.product_pdam, "pdam"));
        categoryList.add(new Category(S.product_postpaid_phone, "telp_pascabayar"));
        categoryList.add(new Category(S.product_telkom_bill, "tagihan_telkom"));
        categoryList.add(new Category(S.product_internet_bill, "tagihan_internet"));
        categoryList.add(new Category(S.multifinance, "multifinance_2"));
        categoryList.add(new Category(S.product_balance_gopay,"saldo_gopay"));
        categoryList.add(new Category(S.product_balance_grabpay, "saldo_grabpay"));
        categoryList.add(new Category(S.product_state_gas, "gas_negara"));
        categoryList.add(new Category(S.product_tv_voucher, "voucher_tv"));
        categoryList.add(new Category(S.product_other_voucher, "voucher"));
        categoryList.add(new Category("BNI Tapcash", "bni_tapcash"));
        categoryList.add(new Category("Mandiri E-Money", "mandiri_emoney"));
        categoryList.add(new Category(S.product_flight,"pesawat"));
        categoryList.add(new Category(S.product_train, "kereta_api"));

        mView.showProgressDialog(false);
        mView.setupCategory(categoryList);
    }

}
