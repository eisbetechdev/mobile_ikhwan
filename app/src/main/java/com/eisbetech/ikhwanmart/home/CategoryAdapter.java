package com.eisbetech.ikhwanmart.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eisbetech.ikhwanmart.BisaContentActivity;
import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.WalletActivity;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.data.model.Category;
import com.freshchat.consumer.sdk.FaqOptions;
import com.freshchat.consumer.sdk.Freshchat;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.holder> {
    Context context;
    List<Category> data;
    Activity activity;

    public CategoryAdapter(Activity activity, Context context, List<Category> data) {
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, final int position) {
        Category category = data.get(position);

        holder.txtMenu.setText(category.getTitle());

        if (!category.getImage().equals("no_image")) {
            int resId = context.getResources().
                    getIdentifier(category.getImage(), "drawable", context.getApplicationInfo().packageName);

            holder.imgCategory.setImageDrawable(context.getResources().getDrawable(resId));
        } else {
            holder.imgCategory.setVisibility(View.GONE);
        }
        
        holder.rvIconCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 100) {
                    Intent intent = new Intent(activity, BisaContentActivity.class);
                    intent.putExtra(K.id, 0);
                    intent.putExtra(K.title, S.konten_bisa);
                    intent.setAction(S.action_media);
                    context.startActivity(intent);
                } else if (position == 0) {
                    Intent intent = new Intent(activity, WalletActivity.class);
                    context.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 1);
                    intent.putExtra(K.title, "Beli Pulsa");
                    context.startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 84);
                    intent.putExtra(K.title, "Beli Paket Data");
                    context.startActivity(intent);
                } else if (position == 3) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 2);
                    intent.putExtra(K.title, "Beli Token PLN");
                    context.startActivity(intent);
                } else if (position == 4) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.pln);
                    intent.putExtra(K.title, "Tagihan PLN");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 5) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 12);
                    intent.setAction(S.action_voucher_game);
                    intent.putExtra(K.title, "Beli Voucher Game");
                    context.startActivity(intent);
                } else if (position == 6) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 92);
                    intent.putExtra(K.product, S.insurance);
                    intent.putExtra(K.title, "Asuransi");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 7) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.tv);
                    intent.putExtra(K.title, "Tagihan TV");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 8) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.pdam);
                    intent.putExtra(K.title, "Tagihan PDAM");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 9) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.post_paid_phone);
                    intent.putExtra(K.title, "Tagihan Telepon");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 10) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.telkom);
                    intent.putExtra(K.title, "Tagihan Telkom");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 11) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.internet);
                    intent.putExtra(K.title, "Tagihan Internet");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 12) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.multifinance);
                    intent.putExtra(K.title, "Tagihan Multifinance");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 13) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 167);
                    intent.putExtra("title", "Topup Gopay");
                    context.startActivity(intent);
                } else if (position == 14) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 205);
                    intent.putExtra("title", "Topup Grabpay");
                    context.startActivity(intent);
                } else if (position == 15) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 20);
                    intent.putExtra(K.parent_id, 30);
                    intent.putExtra(K.product, S.gas);
                    intent.putExtra(K.title, "Tagihan Gas Negara");
                    intent.setAction(S.action_bill);
                    context.startActivity(intent);
                } else if (position == 16) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 237);
                    intent.setAction(S.action_voucher_tv);
                    intent.putExtra(K.title, "Beli Voucher TV");
                    context.startActivity(intent);
                } else if (position == 17) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra(K.id, 238);
                    intent.setAction(S.action_voucher_other);
                    intent.putExtra(K.title, "Produk lain");
                    context.startActivity(intent);
                } else if (position == 18) {
                    Intent intent = new Intent(activity, MainLayout.class);
//                    intent.putExtra(K.id, 20);
//                    intent.putExtra(K.parent_id, 156);
//                    intent.putExtra(K.product, S.credit_card);
//                    intent.setAction(S.action_bill);
//                    context.startActivity(intent);

                    intent.putExtra("id", 256);
                    intent.putExtra("title", "Topup BNI Tapcash");
                    context.startActivity(intent);
                } else if (position == 19) {
                    Intent intent = new Intent(activity, MainLayout.class);
                    intent.putExtra("id", 244);
                    intent.putExtra("title", "Topup Mandiri E-Money");
                    context.startActivity(intent);
                } else if (position == 20) {
//                    Intent intent = new Intent(activity(), MainLayout.class);
//                    intent.putExtra(K.product, S.product_flight);
//                    intent.setAction(S.action_flight);
//                    context.startActivity(intent);
                    Helper.show_info("Info", "Produk ini masih dalam pengembangan, kami akan menginfokan jika sudah siap digunakan.", activity);
                } else if (position == 21) {
//                    Intent intent = new Intent(activity(), MainLayout.class);
//                    intent.putExtra(K.product, S.product_train);
//                    intent.setAction(S.action_train);
                    Helper.show_info("Info", "Produk ini masih dalam pengembangan, kami akan menginfokan jika sudah siap digunakan.", activity);
                }
//                else if (position == 15) {
//                    Intent intent = new Intent(activity(), MenuLainActivity.class);
//                    context.startActivity(intent);
//                }
                else if (position == 22) {
                    HomeActivity activity1 = (HomeActivity) activity;
                    activity1.open_hotline();

//                    Toast.makeText(activity(), "Produk ini sedang dalam pengembangan", Toast.LENGTH_SHORT).show();
                } else if (position == 23) {
                    FaqOptions faqOptions = new FaqOptions()
                            .showFaqCategoriesAsGrid(false)
                            .showContactUsOnAppBar(true)
                            .showContactUsOnFaqScreens(true)
                            .showContactUsOnFaqNotHelpful(true);
                    Freshchat.showFAQs(activity, faqOptions);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_category)
        ImageView imgCategory;
        @BindView(R.id.rv_icon_category)
        RelativeLayout rvIconCategory;
        @BindView(R.id.txtMenu)
        TextView txtMenu;

        public holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
