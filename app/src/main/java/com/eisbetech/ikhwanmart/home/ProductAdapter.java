package com.eisbetech.ikhwanmart.home;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.Constant;
import com.eisbetech.ikhwanmart.e_commerce.DetailProdukActivity;
import com.eisbetech.ikhwanmart.item.Produk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.holder> {
    Context context;
    List<Produk> data;

    public ProductAdapter(Context context, List<Produk> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product_home, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        Produk produk = data.get(position);

        holder.hargaProduct.setText(Constant.toRupiah(produk.getHarga()));
        holder.namaProduct.setText(produk.getNama());

        Glide.with(context.getApplicationContext()).load(produk.getGambar()!=null?produk.getGambar().get(0):context.getResources().getDrawable(R.drawable.logo)).into(holder.thumbnail);

//        Glide.with(context).load(R.drawable.img12)
//                .apply(RequestOptions.bitmapTransform(new RoundedCorners(20)))
//                .into(holder.ivProductHome);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class holder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.nama_product)
        TextView namaProduct;
        @BindView(R.id.harga_product)
        TextView hargaProduct;
        @BindView(R.id.ln_background)
        LinearLayout lnBackground;

        public holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailProdukActivity.class);
                    intent.putExtra("produk",data.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
