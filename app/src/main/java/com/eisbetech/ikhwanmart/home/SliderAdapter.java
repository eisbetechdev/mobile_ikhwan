package com.eisbetech.ikhwanmart.home;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.eisbetech.ikhwanmart.R;

import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private List<String> mSlider;
    private LayoutInflater inflater;
    private Context context;

    public SliderAdapter(Context context, List<String> artikel) {
        this.context = context;
        this.mSlider = artikel;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mSlider.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.item_slider, view, false);

        ImageView imageView = (ImageView) myImageLayout.findViewById(R.id.img_artikel);

        Glide.with(context).load(mSlider.get(position)).into(imageView);
//        RelativeLayout relative_artikel = (RelativeLayout) myImageLayout.findViewById(R.id.relative_artikel);
//        ImageView img_artikel = (SelectableRoundedImageView) myImageLayout.findViewById(R.id.img_artikel);
//        TextView desk_artikel = (TextView) myImageLayout.findViewById(R.id.desk_artikel);
//
//        //get image from html
//
//        try {
//            Document doc = Jsoup.parse(mString.get(position).getFullText());
//            Element imageElement = doc.select("img").first();
//
//            String absoluteUrl = imageElement.absUrl("src");  //absolute URL on src
//            Log.d("String", absoluteUrl);
//            Glide.with(context).load(absoluteUrl)
//                    .skipMemoryCache(true)
//                    .thumbnail(Utils.getPlaceHolder(context)).into(img_artikel);
//        }catch (Exception e) {
//            Log.d("String", e.getMessage());
//        }
//
//
//        img_artikel.setAlpha(0.7f);
//        desk_artikel.setText(mString.get(position).getTitle());
//
//        relative_artikel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Utils.addFragment(fragment, DetailStringFragment.newInstance(mString.get(position).getId()), null );
//            }
//        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
