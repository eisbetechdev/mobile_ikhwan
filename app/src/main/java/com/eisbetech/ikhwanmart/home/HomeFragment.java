package com.eisbetech.ikhwanmart.home;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.eisbetech.ikhwanmart.BuildConfig;
import com.eisbetech.ikhwanmart.HomeActivity;
import com.eisbetech.ikhwanmart.LockPinActivity;
import com.eisbetech.ikhwanmart.MainLayout;
import com.eisbetech.ikhwanmart.R;
import com.eisbetech.ikhwanmart.Utils.GridSpacingItemDecoration;
import com.eisbetech.ikhwanmart.Utils.Helper;
import com.eisbetech.ikhwanmart.Utils.SessionManager;
import com.eisbetech.ikhwanmart.adapter.NewsAdapter;
import com.eisbetech.ikhwanmart.al_quran.MainAlquranActivity;
import com.eisbetech.ikhwanmart.api.BisatopupApi;
import com.eisbetech.ikhwanmart.components.MyTextView;
import com.eisbetech.ikhwanmart.contants.K;
import com.eisbetech.ikhwanmart.contants.S;
import com.eisbetech.ikhwanmart.data.model.Category;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.database.User;
import com.eisbetech.ikhwanmart.e_commerce.ShopFragment;
import com.eisbetech.ikhwanmart.fragment.BaseFragment;
import com.eisbetech.ikhwanmart.fragment.Berita.FragmentNews;
import com.eisbetech.ikhwanmart.item.News.DataItem;
import com.eisbetech.ikhwanmart.item.News.News;
import com.eisbetech.ikhwanmart.item.Produk;
import com.eisbetech.ikhwanmart.scanner.ScannerActivity;
import com.eisbetech.ikhwanmart.withdraw.ActivityWithdraw;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import me.relex.circleindicator.CircleIndicator;
import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HomeFragment extends BaseFragment implements HomeContract.View {
    //    @Nullable

    @BindView(R.id.txt_wallet)
    protected TextView txt_wallet;

    SessionManager session;
    HashMap<String, String> user;
    boolean slider_load = false;
    protected List<Product> parent_produt;
    @BindView(R.id.textView88)
    MyTextView textView88;
    @BindView(R.id.btn_topup)
    FancyButton btnTopup;
    @BindView(R.id.linearLayout3)
    LinearLayout linearLayout3;
    @BindView(R.id.pager_articel)
    ViewPager pagerArticel;
    @BindView(R.id.indicator_articel)
    CircleIndicator indicatorArticel;
    @BindView(R.id.curve_top)
    RelativeLayout curveTop;
    @BindView(R.id.iv_baca)
    RelativeLayout ivBaca;
    @BindView(R.id.tv_al_quran_title)
    TextView tvAlQuranTitle;
    @BindView(R.id.btn_read_al_quran)
    RelativeLayout btnReadAlQuran;
    @BindView(R.id.labelFAQ)
    RelativeLayout labelFAQ;
    @BindView(R.id.recycle_category)
    RecyclerView recycleCategory;
    @BindView(R.id.btn_more_article)
    RelativeLayout btnMoreArticle;
    @BindView(R.id.recycle_article)
    RecyclerView recycleArticle;
    Unbinder ubinder;
    @BindView(R.id.btn_more_categories)
    RelativeLayout btnMoreCategories;
    @BindView(R.id.btn_hide_categories)
    RelativeLayout btnHideCategories;
    @BindView(R.id.ln_bottom_menu)
    LinearLayout lnBottomMenu;
    @BindView(R.id.ns_background)
    NestedScrollView nsBackground;
    @BindView(R.id.btn_more_product)
    RelativeLayout btnMoreProduct;
    @BindView(R.id.recycle_product)
    RecyclerView recycleProduct;/*
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout mAppbar;
    @BindView(R.id.showWallet)
    LinearLayout mShowWallet;
    @BindView(R.id.txt_wallet2)
    MyTextView mTxtWallet2;
    @BindView(R.id.btn_topup2)
    FancyButton mBtnTopup2;*/


    private SliderAdapter sliderAdapter;
    private CategoryAdapter categoryAdapter;
    private NewsAdapter newsAdapter;
    private ProductAdapter productAdapter;

    private HomeContract.UserActionListener mActionListener;
    protected ProgressDialog progressDialog;

    private Handler handler = new Handler();
    private Runnable Update = null;
    private static int currentPage = 0;

    List<String> sliderList;
    List<Category> categoryList;
    List<DataItem> articleList;
    List<Produk> productList;

    private LinearLayoutManager horizontalLayoutManagaer;
    private String pages;
    private boolean isLoading = true;
    private String mode;
    private int MY_CAMERA_REQUEST_CODE = 0;

    private boolean isShow = true;
    private View view;

    @OnClick({R.id.btn_topup})
    public void topup() {
//        Intent intent = new Intent(getActivity(), WalletActivity.class);
        Intent intent = new Intent(getActivity(), MainLayout.class);
        intent.setAction(S.action_deposit);
        startActivity(intent);
    }

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

        session = new SessionManager(getContext(), getActivity());
    }

    @Override
    public void onStop() {
//        slider_load = false;
//        sliderShow.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onResume() {
//        sliderShow.startAutoCycle();
        super.onResume();
        if (Helper.isNowConnected(getContext())) {
            getProfile();
        } else {
            reconnect();
        }
    }

    private void reconnect() {
        Drawable img = new IconicsDrawable(getContext(), FontAwesome.Icon.faw_wifi).sizeDp(32).color(Color.WHITE);

        new LovelyStandardDialog(getActivity())
                .setTopColorRes(R.color.md_deep_orange_500)
                .setButtonsColorRes(R.color.accent)
                .setIcon(img)
                .setTitle("Koneksi gagal")
                .setMessage("Koneksi internet anda bermasalah, silahkan periksa koneksi internet anda.")
                .setPositiveButton("Coba Lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Helper.isNowConnected(getContext())) {
                            getProfile();
                        } else {
                            reconnect();
                        }
                    }
                })
                .show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Drawable icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_add_alert)
                .color(Color.WHITE).sizeDp(20);
        menu.add(0, 1, Menu.NONE, S.menu_notification).setIcon(icon)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        icon = new IconicsDrawable(getActivity(), GoogleMaterial.Icon.gmd_history)
                .color(Color.WHITE).sizeDp(20);
        menu.add(0, 2, Menu.NONE, S.menu_history).setIcon(icon)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ubinder = ButterKnife.bind(this, root);

        progressDialog = new ProgressDialog(getContext());
        mActionListener = new HomePresenter(getContext(), this);
        progressDialog.setCancelable(false);

        //initCollapsingToolbar();
        init();

        mActionListener.getProduct();
        mActionListener.getCategory();

        slider_load = false;

        sliderList = new ArrayList<>();
        getProfile();
        return root;
    }

    private void init() {
        categoryList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 4);
        if (recycleCategory!=null) recycleCategory.setLayoutManager(mLayoutManager);
        if (recycleCategory!=null) recycleCategory.addItemDecoration(new GridSpacingItemDecoration(4, dpToPx(10), true));
        if (recycleCategory!=null) recycleCategory.setItemAnimator(new DefaultItemAnimator());
        if (recycleCategory!=null) recycleCategory.setNestedScrollingEnabled(false);
        categoryAdapter = new CategoryAdapter(getActivity(), getContext(), categoryList);
        if (recycleCategory!=null) recycleCategory.setAdapter(categoryAdapter);

        articleList = new ArrayList<>();
        horizontalLayoutManagaer = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        if (recycleArticle!=null) recycleArticle.setLayoutManager(horizontalLayoutManagaer);

        newsAdapter = new NewsAdapter(getContext(), articleList);
        if (recycleArticle!=null) recycleArticle.setAdapter(newsAdapter);
        if (recycleArticle!=null) recycleArticle.setNestedScrollingEnabled(false);
        getNewsList();

        productList = new ArrayList<>();
        LinearLayoutManager horizontalLayoutManagaer1
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        if (recycleProduct!=null) recycleProduct.setLayoutManager(horizontalLayoutManagaer1);
//        recycleProduct.setAdapter(productAdapter);
        if (recycleProduct!=null) recycleProduct.setNestedScrollingEnabled(false);


        nsBackground.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    if (isShow) {
                        slideDown(lnBottomMenu);
                        isShow = false;
                    }
                } else {
                    if (!isShow) {
                        slideUp(lnBottomMenu);
                        isShow = true;
                    }
                }
            }
        });
    }

    /*private void initCollapsingToolbar() {
        mAppbar.setExpanded(true);

        mAppbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    mShowWallet.setVisibility(View.GONE);
                    mTxtWallet2.setText(txt_wallet.getText());
                }
                if (scrollRange + verticalOffset == 0) {
                    mShowWallet.setVisibility(View.VISIBLE);
                    mTxtWallet2.setText(txt_wallet.getText());
                    isShow = true;
                } else if (isShow) {
                    mShowWallet.setVisibility(View.GONE);
                    mTxtWallet2.setText(txt_wallet.getText());
                    isShow = false;
                }
            }
        });
    }*/

    private void getNewsList() {
        final OkHttpClient client = new OkHttpClient();
        String url = "http://api.mic.eisbetech.com/news?page=1";
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String textResponse = response.body().string();
                News news = new Gson().fromJson(textResponse, News.class);
                pages = news.getNextPage();
                articleList.addAll(news.getData());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        newsAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }
                });
            }
        });

        if (recycleArticle!=null) recycleArticle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = horizontalLayoutManagaer.getItemCount();
                int lastVisibleItem = horizontalLayoutManagaer.findLastVisibleItemPosition();
                int visibleThreshold = 5;
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && pages != null) {
                    final Request req = new Request.Builder()
                            .url(pages)
                            .get()
                            .build();

                    client.newCall(req).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            final String textResponse = response.body().string();
                            News news = new Gson().fromJson(textResponse, News.class);
                            Log.d("unik", news + "");
                            pages = news.getNextPage();
                            articleList.addAll(news.getData());
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    newsAdapter.notifyDataSetChanged();
                                    isLoading = false;
                                }
                            });
                        }
                    });
                    isLoading = true;
                }
            }
        });
    }

    private void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(300);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    private void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(300);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    @Override
    public void showProgressDialog(boolean show) {
        if (show) {
            progressDialog.setMessage("Loading");
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showDialog(String message) {
        new MaterialDialog.Builder(getContext())
                .content(message)
                .positiveText("OK")
                .show();
    }

    @Override
    public void setupSlider(List<String> data) {

//        if (data.size() > 5) {
//            for (int i = 0; i < 5; i++) {
//                sliderList.add(data.get(i));
//            }
//        } else {
//            sliderList.addAll(data);
//        }


        sliderAdapter = new SliderAdapter(getContext(), sliderList);

        pagerArticel.setAdapter(sliderAdapter);
        indicatorArticel.setViewPager(pagerArticel);


        Update = new Runnable() {
            public void run() {
                if (currentPage == sliderList.size()) {
                    currentPage = 0;
                }

                if (currentPage >= sliderList.size()) { // In my case the number of pages are 5
                    handler.removeCallbacksAndMessages(null);
                } else {
                    try {
                        pagerArticel.setCurrentItem(currentPage++, true);
                    } catch (Exception e) {
                        Log.d("errorMsg", e.getMessage());
                    }

                }

            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 5000);
    }

    @Override
    public void setupCategory(List<Category> data) {
        if (data.size() != 0) {
            categoryList.addAll(data);
        }
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void setupCategoryNext(List<Category> data) {
        if (data.size() != 0) {
            categoryList.addAll(data);
        }
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void setupProduct(List<Produk> data) {
        if (data.size() != 0) {
            if (data.size() > 6) {
                for (int i = 0; i < 6; i++) {
                    productList.add(data.get(i));
                }
            } else {
                productList.addAll(data);
            }
            System.out.println("ini masuk category");
        }
        productAdapter = new ProductAdapter(getActivity(), productList);
        if (recycleProduct!=null) recycleProduct.setAdapter(productAdapter);
        System.out.println("ini category ukuran : " + productAdapter.getItemCount());
//        productAdapter.notifyDataSetChanged();

        System.out.println("ini masuk category1");

    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @OnClick({R.id.btn_more_article, R.id.btn_read_al_quran, R.id.btn_more_categories, R.id.btn_more_product, R.id.btn_hide_categories, R.id.btn_transfer, R.id.btn_qr_code, R.id.btn_withdraw, R.id.iv_transfer, R.id.iv_qr_code, R.id.iv_with_draw})
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.btn_more_article:
                replaceFragmentWithAnimationBackStag(new FragmentNews(), "News");
                break;
            case R.id.btn_read_al_quran:
                Helper.newIntent(getActivity(), MainAlquranActivity.class, null);
                break;
            case R.id.btn_more_categories:
                mActionListener.getCategoryNext();
                btnHideCategories.setVisibility(View.VISIBLE);
                btnMoreCategories.setVisibility(View.GONE);
                break;
            case R.id.btn_hide_categories:
                categoryList.clear();
                mActionListener.getCategory();
                categoryAdapter.notifyDataSetChanged();
                btnHideCategories.setVisibility(View.GONE);
                btnMoreCategories.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_transfer:
                Intent intent = new Intent(getActivity(), MainLayout.class);
                intent.setAction("transfer_saldo");
                startActivity(intent);
                break;
            case R.id.btn_qr_code:
                User user = User.getUser();
                if (user != null && user.lock_pin_enable != null && user.lock_pin_enable) {
                    mode = "scan";
                    Intent intentPin = new Intent(getActivity(), LockPinActivity.class);
                    intentPin.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                    intentPin.setAction("Masukkan PIN");
                    startActivityForResult(intentPin, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                } else {
                   camera_permission();
                }
                break;
            case R.id.btn_withdraw:
                user = User.getUser();
                if (user != null && user.lock_pin_enable != null && user.lock_pin_enable) {
                    mode = "withdraw";
                    Intent intentPin = new Intent(getActivity(), LockPinActivity.class);
                    intentPin.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                    intentPin.setAction("Masukkan PIN");
                    startActivityForResult(intentPin, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                } else {
                    startActivity(new Intent(getActivity(), ActivityWithdraw.class));
                }
                break;
            case R.id.btn_more_product:
                replaceFragmentWithAnimationBackStag(new ShopFragment(), "Berita");
                break;
            case R.id.iv_transfer:
                Intent intent1 = new Intent(getActivity(), MainLayout.class);
                intent1.setAction("transfer_saldo");
                startActivity(intent1);
                break;
            case R.id.iv_qr_code:
                User user1 = User.getUser();
                if (user1 != null && user1.lock_pin_enable != null && user1.lock_pin_enable) {
                    mode = "scan";
                    Intent intentPin = new Intent(getActivity(), LockPinActivity.class);
                    intentPin.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                    intentPin.setAction("Masukkan PIN");
                    startActivityForResult(intentPin, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                } else {
                   camera_permission();
                }
                break;
            case R.id.iv_with_draw:
                user1 = User.getUser();
                if (user1 != null && user1.lock_pin_enable != null && user1.lock_pin_enable) {
                    mode = "withdraw";
                    Intent intentPin = new Intent(getActivity(), LockPinActivity.class);
                    intentPin.putExtra("request_code", LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                    intentPin.setAction("Masukkan PIN");
                    startActivityForResult(intentPin, LockPinActivity.RESULT_PIN_INPUT_VALIDATE);
                } else {
                    startActivity(new Intent(getActivity(), ActivityWithdraw.class));
                }
                break;
        }
    }

    public void replaceFragmentWithAnimationBackStag(Fragment fragment, String tag) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    private void getProduct() {
        final User user = User.getUser();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(S.loading);
        progressDialog.show();
        try {
            RequestParams params = new RequestParams();
            params.put(K.email, user.email);

            BisatopupApi.get("/product/all", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString,
                                      Throwable throwable) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    progressDialog.dismiss();

                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        final JSONArray response = new JSONArray(responseString);
                        realm.executeTransactionAsync(new Realm.Transaction() {

                            @Override
                            public void execute(Realm bgrealm) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(i);

                                        Product product = new Product();
                                        product.id = obj.getInt(K.product_id);
                                        product.parent_id = obj.getInt(K.parent_id);
                                        product.product_name = obj.getString(K.product_name);
                                        product.product = obj.getString(K.product);
                                        product.code = obj.getString(K.code);
                                        product.prefix = obj.getString(K.prefix);
                                        product.description = obj.getString(K.description);
                                        product.img_url = obj.getString(K.img_url);

                                        bgrealm.copyToRealmOrUpdate(product);
                                        Log.i("Product load", "Update data, product ID : " + product.id);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                // Transaction success.
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProfile() {
        user = session.getUserDetails();
        final String name = user.get(SessionManager.KEY_NAME);
        final String email = user.get(SessionManager.KEY_EMAIL);
        txt_wallet.setText(S.loading);
        try {
            String token = Helper.getFirebaseInstanceId(getContext());

            final JSONObject requestJson = new JSONObject();
            RequestParams params = new RequestParams();
            params.put(K.email, email);
            params.put(K.token, token);

            BisatopupApi.get("/profile/index", params, getContext(), new TextHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final String responseString) {
                    try {
                        JSONObject object = new JSONObject(responseString);
                        if (object.getBoolean(K.error)) {
                            Helper.showMessage(getActivity(), object.getString(K.message));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONObject response = new JSONObject(responseString);

                        JSONObject data = response.getJSONObject(K.data);
                        User user = realm.where(User.class).equalTo(K.email, email).findFirst();

                        if (!realm.isInTransaction()) {
                            realm.beginTransaction();
                        }
                        if (user == null) {
                            user = realm.createObject(User.class, 1);
                        }
                        user.email = email;
                        user.user_id = data.getInt(K.user_id);
                        user.fullname = data.getString(K.name);
                        user.phone_number = data.getString(K.phone_number);
                        user.role = data.getString(K.role);
                        user.poin = data.getInt(K.poin);
                        user.wallet = data.getString(K.wallet);
                        //  user.sponsored_by = data.getString("sponsored_by");
                        user.kode_affiliasi = data.getString(K.affiliation_code);
                        user.total_affiliasi = data.getString(K.total_affiliate);
                        user.total_pendapatan = data.getString(K.total_income);
                        user.profile_url = data.getString(K.profile_url);
                        user.is_agent = data.getInt(K.is_agent);
                        user.nama_agen = data.getString(K.agent_name);
                        user.jenis_kelamin = data.getString(K.gender);
                        user.tgl_lahir = data.getString(K.birth_date);
                        realm.commitTransaction();

                        txt_wallet.setText(user.wallet);

                        int need_verify = data.getInt(K.need_verify);
                        if (need_verify == 1) {
                            session.clearSession();
//                            try {
//                                User.ClearUser();
//                            } catch (Exception ec) {
//                                ec.printStackTrace();
//                            }
                            getActivity().finish();
                            Helper.showMessage(getActivity(), S.verification_needed);
                            Intent i = new Intent(getActivity(), MainLayout.class);
                            i.setAction(S.action_phone_verification);
                            startActivity(i);
                        }
                        int force_logout = data.getInt(K.force_logout);

                        if (force_logout == 1) {
                            HomeActivity homeActivity = (HomeActivity) getActivity();
                            homeActivity.logout_user();
                        }

                        if (!slider_load) {
                            JSONArray slider_info = data.getJSONArray(K.mobile_info);
//                            sliderShow.removeAllSliders();
                            for (int i = 0; i < slider_info.length(); i++) {
                                JSONObject slider_obj = slider_info.getJSONObject(i);
                                DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
                                textSliderView.image(slider_obj.getString(K.image_url));
                                sliderList.add(slider_obj.getString(K.image_url));
//                                sliderShow.addSlider(textSliderView);
                                slider_load = true;
                            }
                            setupSlider(sliderList);
                        }
                        HomeActivity activity = (HomeActivity) getActivity();
                        activity.reload_profile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkFirstRun() {

        final String PREFS_NAME = this.getClass().getName();
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;

        // Get current version code
        int currentVersionCode = BuildConfig.VERSION_CODE;

        // Get saved version code
        SharedPreferences prefs = getActivity().getSharedPreferences(PREFS_NAME, getActivity().MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            // This is just a normal run
            return;

        } else if (savedVersionCode == DOESNT_EXIST) {


        } else if (currentVersionCode > savedVersionCode) {

            // TODO This is an upgrade
        }

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == LockPinActivity.RESULT_PIN_INPUT_VALIDATE) {
            if (mode.equals("withdraw")) startActivity(new Intent(getActivity(),ActivityWithdraw.class));
            else if (mode.equals("scan")) camera_permission();
        }
    }

    public void camera_permission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            } else {
                startActivity(new Intent(getActivity(), ScannerActivity.class));
            }
        } else {
            startActivity(new Intent(getActivity(), ScannerActivity.class));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ubinder.unbind();
    }
}
