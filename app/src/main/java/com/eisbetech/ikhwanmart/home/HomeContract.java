package com.eisbetech.ikhwanmart.home;

import com.eisbetech.ikhwanmart.data.model.Category;
import com.eisbetech.ikhwanmart.database.Product;
import com.eisbetech.ikhwanmart.item.Produk;

import java.util.List;

public class HomeContract {
    interface View{
        void showProgressDialog(boolean show);

        void setupSlider(List<String> data);

        void setupProduct(List<Produk> data);

        void setupCategory(List<Category> data);

        void setupCategoryNext(List<Category> data);


        void showDialog(String message);
    }

    interface UserActionListener{
        void getProduct();

        void getCategory();

        void getCategoryNext();

    }
}
